//
//  Custopop.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 13/06/22.
//

import UIKit

class Custopop: UIView {
    
    var vc: UIViewController!
    var view: UIView!
    
    @IBOutlet weak var cusView: Custopop!
    @IBOutlet weak var okTap: UIButton!
    @IBOutlet weak var titlelbl: UILabel!
    
    
//    @IBOutlet weak var titlelbl: UILabel!
//    @IBOutlet weak var okTap: UIButton!
    
    var callback: (()->())?
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    
     init(frame:CGRect,inview: UIViewController) {
        super.init(frame: frame)
        setUpXib(frame: CGRect(x: 0.0, y: 0.0, width: frame.width, height: frame.height))
         vc = inview
//         verify_btn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    
    
    func setUpXib(frame:CGRect)
    {
        self.view = xibload()
        view.frame = frame
        addSubview(view)
        
        
        
    }
    
    
    
    func xibload() -> UIView
    {
        let bundle  = Bundle(for: type(of: self))
        let nib = UINib(nibName: "Custopop", bundle: bundle)
        let view =  nib.instantiate(withOwner: self,options: nil).first as! UIView
        return view
        
    }
    
    @IBAction func okaction(_ sender: UIButton) {
        self.callback?()
        
    }
    
}
