//
////import Foundation
////import Alamofire
////import UIKit
////struct ResponseData:Decodable {
////}
////
////struct Status<T:Decodable>:Decodable {
////    let status:Int
////    let Code:Int
////    let msg:String?
////    let data:T?
////
////    enum CodingKeys: String, CodingKey {
////        case Code, message,data, status
////    }
////
////    init(from decoder: Decoder) throws{
////        let container = try decoder.container(keyedBy: CodingKeys.self)
////
////
////                do{
////                    status = try container.decode(Int.self, forKey: .status)
////
////                }catch{
////                    status = -3
////                }
////        do{
////            Code = try container.decode(Int.self, forKey: .Code)
////        }catch{
////            Code = 4321
////        }
////
////
////        do{
////            msg = try container.decode(String.self, forKey: .message)
////        }catch{
////            msg = nil
////        }
////        do {
////            data = try container.decode(T.self, forKey: .data)
////        } catch {
////            data = nil
////        }
////    }
////}
////
////struct Messages {
////    static let error = "Something went wrong"
////    static let network = "Network not available"
////}
////
////enum Result {
////    case success,failure
////}
////
////enum NetworkResult<T:Decodable> {
////    case success(T?)
////    case failure(String?)
////
////}
////
////struct NetworkManager {
////
////    static let instance = NetworkManager()
////    private init(){}
////
////    func request<T:Decodable>(endPoint: Api,
////                              method:HTTPMethod = .post,
////                              parameters: [String: Any]? = nil,
////                              showIndicator: Bool = true,
////                              showSuccessMessage:Bool = false,
////                              loadingText:String = "loading",
////                              isBody:Bool = true,
////                              encoding: URLEncoding = URLEncoding.queryString,
////                              completion: ((NetworkResult<T>) -> Void)? = nil) {
////
////        if !NetworkReachabilityManager()!.isReachable {
////            completion?(.failure(Messages.network))
////           // showSnackBar(message: Messages.network)
////            return
////        }
////
////
////        var headers = [String:String]()
////        headers["Content-Type"] =  "application/json"
////        if let token:String = MyUserDefaults.instance.get(key: .access_token) ,token.count > 0{
////            headers[ "Authorization"] = "Bearer " +  token
////        }
////
////
////
////        let urlString = Api.baseUrl.value + endPoint.value
////        debugPrint("********************************* API Request **************************************")
////        debugPrint("Request URL: ",urlString)
////        debugPrint("Request Parameters: ",parameters ?? "no parameters")
////        debugPrint("Request Headers: ",headers)
////        debugPrint("************************************************************************************")
////
////        guard let url = URL(string: urlString) else{
////            completion?(.failure(Messages.error))
////            // showSnackBar(message: Messages.error)
////            return
////        }
////        if showIndicator {
////            DispatchQueue.main.async {
////                //Indicator.instance.show(loadingText: loadingText)
////            }
////        }
////        switch isBody {
////        case true:
////
////            do{
////                let data = try JSONSerialization.data(withJSONObject: parameters ?? [])
////                //                let string = String(data:data,encoding: .utf8)
////                //                let httpBody = string?.data(using: .utf8)
////                var request = URLRequest(url: url)
////                request.httpMethod = method.rawValue.uppercased()
////                request.httpBody = data
////                request.allHTTPHeaderFields = headers
////
////
////                let dataRequest = Alamofire.request(request)
//
//                load(from: dataRequest, showIndicator: showIndicator, showSuccessMessage: showSuccessMessage, loadingText: loadingText, encoding: encoding, completion: completion)
//            }catch{
//                debugPrint("ERROR:",error)
//                completion?(.failure(error.localizedDescription))
//            }
//
//
//        default:
//
//
//            let dataRequest = AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
//            load(from: dataRequest, showIndicator: showIndicator, showSuccessMessage: showSuccessMessage, loadingText: loadingText, encoding: encoding, completion: completion)
//
//
//        }
//    }
//
//
//    private func load<T:Decodable>(from request:DataRequest,
//                                   showIndicator: Bool = true,
//                                   showSuccessMessage:Bool = false,
//                                   loadingText:String = "loading",
//                                   encoding: URLEncoding,
//                                   completion: ((NetworkResult<T>) -> Void)? = nil){
//
//
//        request.responseData { (dataResponse) in
//
//            if showIndicator {
//                DispatchQueue.main.async {
//                    //Indicator.instance.hide()
//                }
//            }
//            if let error = dataResponse.error{
//                completion?(.failure(error.localizedDescription))
//               // showSnackBar(message:error.localizedDescription)
//                return
//            }
//
//            guard let data = dataResponse.data else{
//                completion?(.failure(Messages.error))
//               // showSnackBar(message: Messages.error)
//                return
//            }
//
//            debugPrint("********************************* RESPONSE START **************************************")
//
//            let string = String(data: data, encoding: .utf8)
//            debugPrint("JSON",string)
//
//            do{
//                let response = try JSONDecoder().decode(Status<T>.self, from: data)
//                switch response.status{
//                case 200:
//                    // showSnackBar(message: response.msg)
//
//                    completion?(.success(response as? T))
//
//                case -3:
//                    break
//
//
//
//                    //let delegate = UIApplication.shared.delegate as? AppDelegate
//                    // delegate?.logout()
//
//
//
//                default:
//                   // showSnackBar(message: response.msg)
//                    completion?(.failure(response.msg))
//
//                }
//            }catch{
//                completion?(.failure(Messages.error))
//                debugPrint("ERROR:",error)
//            }
//            debugPrint("********************************* RESPONSE END **************************************")
//        }
//    }
//
//
//    func multipart<T:Decodable>(endpoint: Api,image: [String: Data]? ,
//                                parameters: [String: AnyObject]? = nil,fileUrl:URL? = nil,images:[UIImage]? = nil,
//                                showIndicator: Bool = true,completion: ((NetworkResult<T>) -> Void)? = nil) {
//
//        if !NetworkReachabilityManager()!.isReachable {
//            completion?(.failure(Messages.network))
//            return
//        }
//
//        if showIndicator {
//            //Indicator.instance.show(loadingText: "updating profile")
//        }
//
//        let apiString = Api.baseUrl.value + endpoint.value
//
//        var headers = [String:String]()
//        headers["Content-Type"] =  "application/json"
//        if let token:String = MyUserDefaults.instance.get(key: .access_token),token.count > 0{
//            headers[ "Authorization"] = "Bearer " +  token
//        }
//
//        print(apiString)
//        debugPrint("********************************* API Request **************************************")
//        debugPrint("Request URL:\(apiString)")
//        debugPrint("Request Parameters: \(parameters ?? [: ])")
//        debugPrint("Request Headers: \(headers )")
//        debugPrint("************************************************************************************")
//        guard let url = URL(string: apiString) else { return }
//        var request = URLRequest(url: url)
//        request.allHTTPHeaderFields = headers
//
//
//        AF.upload(multipartFormData: { multipartFormData in
//            if let params = parameters {
//                for (key, value) in params {
//                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
//                }
//            }
//            if let image = image{
//                for (key,value) in image {
//                    let interval = NSDate().timeIntervalSince1970 * 1000
//                    let imgMimeType : String = "image/jpg"
//                    let imgFileName = "img\(interval).jpg"
//                    multipartFormData.append(value, withName: key, fileName: imgFileName, mimeType: imgMimeType)
//                }
//            }
////            if fileUrl?.toString?.contains(".pdf") == true{
////
////                if let fileUrl = fileUrl{
////                    multipartFormData.append(fileUrl, withName: "feed_media", fileName: "temp" + "\(Int(arc4random_uniform(100000)))" + ".pdf", mimeType: "application/pdf")
////                }
////            }else if fileUrl?.toString?.contains(".mp4") == true {
////                if let fileUrl = fileUrl{
////                    multipartFormData.append(fileUrl, withName: "feed_media", fileName: "temp" + "\(Int(arc4random_uniform(100000)))" + ".mp4", mimeType: "video/mp4")
////                }
////            }else if fileUrl?.toString?.contains(".MOV") == true{
////                if let fileUrl = fileUrl{
////                    multipartFormData.append(fileUrl, withName: "feed_media", fileName: "temp" + "\(Int(arc4random_uniform(100000)))" + ".MOV", mimeType: "video/mp4")
////                }
////            }
//
//
//
//            if let count = images?.count,count > 0{
//                for data in images!
//                {
//                    let imageData2 = data.jpegData(compressionQuality: 0.5)
//                    let interval = NSDate().timeIntervalSince1970 * 1000
//                    let imgMimeType : String = "image/jpg"
//                    let imgFileName = "img\(interval).jpg"
//
//                    multipartFormData.append(imageData2!, withName: "feed_media", fileName: imgFileName, mimeType: imgMimeType)
//
//                }
//            }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//        }, to: apiString,headers: headers,
//                         encodingCompletion: { encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//
//                upload.responseData(completionHandler: { (dataResponse) in
//                    if showIndicator {
//                        DispatchQueue.main.async {
////                            Indicator.instance.hide()
//                        }
//                    }
//
//                    if let error = dataResponse.error{
//                        completion?(.failure(error.localizedDescription))
//                        return
//                    }
//
//                    guard let data = dataResponse.data else{
//                        completion?(.failure(Messages.error))
//                        return
//                    }
//
//                    debugPrint("********************************* RESPONSE START **************************************")
//                    do{
//                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
//                        debugPrint("JSON",json)
//                    }catch{
//
//                    }
//
//
//                    do{
//                        let response = try JSONDecoder().decode(Status<T>.self, from: data)
//
//                        switch response.Code{
//                        case 200:
//                            completion?(.success(response.data))
//                            break
//                        default: break
////                            showSnackBar(message: response.msg)
//                        }
//                    }catch{
//                        completion?(.failure(Messages.error))
//                        debugPrint("ERROR:",error)
//                    }
//
//                    debugPrint("********************************* RESPONSE END **************************************")
//                })
//
//                upload.uploadProgress { (progress) in
//                    print("Uploaded",progress.fractionCompleted)
//                }
//
//
//            case .failure(let error):
//                completion?(.failure(error.localizedDescription))
//            }
//
//        })
//
//    }
//
//
//
//}
//
//
//class ProfileUpdate: Decodable {
//    var user_id:String?
//
//}
//
//
