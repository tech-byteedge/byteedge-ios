
//  Apis.swift
//  ADChat
//  Created by Aquib on 06/08/19.
//  Copyright © 2019 Aquib. All rights reserved.

import Foundation
import SwiftUI
//let kAlertNoNetworkMessage = "A network connection is required. Please verify your network settings & try again."

var kNetworkProblemKey = "Network Problem"
var kWentWrongKey = "Something Went Wrong, Please try again  later"
var kOK = "OK"
//let baseUrl = "http://3.12.253.202:2900/api/"
 let baseUrl  =  "https://www.byteedgelearning.com:2900/api/" // production
enum Api: String {
    case login
    case updateProfile
    case skillList
    case usercategoriesSearch
    case searchlist
    case createGroup
    case feedlist
    case topSearch
    case contactUs
    case CommumnityGroupList
    case myGroupDetails
    case GroupdetailswithJoinIcon
    case joinGroup
    case ViewAllApi
    case editgroupDetail
    case createpost
    case AcceptBlock
    case faq
    case logout
    case term
    case subsCriptionCourse
    case learningViewAll
    case notification
    case favouriteCourse
    case payment
    case subscriptionPlane
    case  transactionpaymentSave
    case orderDetailsafterPurchase
    case paymentpurchasehistory
    case subcriptionVideo
    case trackingLearning
    case graphAdd
    case graphList
    case graphcourselistperc
    case ratingAdd
    case removeandBlock
    case groupAdmin
    case QuizQues
    case matchQue
    case resultQuiz
    case feedsAll
    case particularCourse
    case ApiVersion
    case updateCourse
    case userinvite
    case ComppleteCourse
    case paymentcurrentpurchasehistory
    case shareReals
    
    //---------------------
    case homeData
    case commentList
    case likeCommentReply
    case learningList
    case recommended
    case PopularCourse
    case trending
    case coursePreviewDetail
    case categoryList
    case postDetail
    case recentSearch
    case DeleteFeed
    case addBookmark
    case favReels
    case getReportCerti
    case downloadCertificate
    case userExit
    
}
extension Api {
    
    var url : String {
        switch self {
        case .login:
            return "\(baseUrl)user/login"
        case .updateProfile:
        return "\(baseUrl)user/update"
        case .skillList:
        return "\(baseUrl)user/list/skills"
        case .usercategoriesSearch:
        return "\(baseUrl)user/categories"
        case .searchlist:
        return "\(baseUrl)user/search"
        case .createGroup:
        return "\(baseUrl)group/create"
        case .feedlist:
        return "\(baseUrl)feeds/list"
        case .topSearch:
        return "\(baseUrl)user/top/search"
        case .CommumnityGroupList:
        return "\(baseUrl)group/list"
        case .myGroupDetails:
        return "\(baseUrl)group/my/detail"
        case .GroupdetailswithJoinIcon:
        return "\(baseUrl)group/detail"
        case .joinGroup:
        return "\(baseUrl)group/join"
        case .ViewAllApi:
        return "\(baseUrl)group/view/all/list"
        case .editgroupDetail:
        return "\(baseUrl)group/edit"
        case .createpost:
        return "\(baseUrl)feeds/create/post"
        case .AcceptBlock:
        return "\(baseUrl)group/accept/reject/request"
        case .removeandBlock:
        return "\(baseUrl)group/member/remove/block"
        case .groupAdmin:
        return "\(baseUrl)group/member/make/admin"
        case .recommended:
        return "\(baseUrl)learning/course/recommend"
        case .trending:
        return "\(baseUrl)learning/course/trending"
        case .QuizQues:
        return "\(baseUrl)question/list"
        case .matchQue:
        return "\(baseUrl)question/match"
        case .resultQuiz:
        return "\(baseUrl)learning/course/complete"
        case .feedsAll:
        return "\(baseUrl)feeds/all"
        case .particularCourse:
        return "\(baseUrl)learning/course/details"
        case .PopularCourse:
        return "\(baseUrl)learning/course/popular"
        case .updateCourse:
        return "\(baseUrl)learning/course/update/id"
        case .userinvite:
        return "\(baseUrl)user/invite"
        case .ComppleteCourse:
        return "\(baseUrl)learning/course/complete"
        case .recentSearch:
        return "\(baseUrl)user/add/recent"
        case .addBookmark:
        return "\(baseUrl)user/add/reel"
        case .favReels:
        return "\(baseUrl)user/bookmark/list"
        case .getReportCerti:
        return "\(baseUrl)user/get/report"
        case .downloadCertificate:
        return "\(baseUrl)user/download"
        case .paymentcurrentpurchasehistory:
        return "\(baseUrl)payment/current/purchase/history"
        case .shareReals:
        return "\(baseUrl)user/reel/id"
        case .userExit:
        return "\(baseUrl)feeds/group/user/exist"
            
            

        case .faq:
        return "\(baseUrl)faq"
        case .logout:
        return "\(baseUrl)user/logout"
        case .term:
        return "\(baseUrl)cms/get"
        case .learningViewAll:
        return "\(baseUrl)learning/view/all"
        case .notification:
        return "\(baseUrl)notification/list"
        case .favouriteCourse:
        return "\(baseUrl)learning/favourite"
        case .payment:
        return "\(baseUrl)payment/id"
        case .subscriptionPlane:
        return "\(baseUrl)notification/subscription/plan"
        case .transactionpaymentSave:
        return "\(baseUrl)payment/save"
        case .orderDetailsafterPurchase:
        return "\(baseUrl)payment/order/detail"
        case .contactUs:
        return "\(baseUrl)contact/add"
        case .paymentpurchasehistory:
        return "\(baseUrl)payment/purchase/history"
        case .subcriptionVideo:
        return "\(baseUrl)plearning/course/video/list"
        case .subsCriptionCourse: // YourCourse
        return "\(baseUrl)learning/course/video/list"
        case .trackingLearning: // percentage
        return "\(baseUrl)learning/course/track/percent"
        case .graphAdd: // percentage
        return "\(baseUrl)graph/add"
        case .graphList: //
        return "\(baseUrl)graph/list"
        case .ratingAdd: //
        return "\(baseUrl)rating/add"
        case .ApiVersion: //
        return "\(baseUrl)user/version/list"

            
       //--------------------------------
        case .homeData:
            return "\(baseUrl)user/home/list"
            
        case .commentList:
            return "\(baseUrl)feeds/comment/reply/list"
            
        case .likeCommentReply:
            return "\(baseUrl)feeds/like"
            
        case .learningList:
            return "\(baseUrl)learning"
            
        case .coursePreviewDetail:
            return "\(baseUrl)learning/course/preview"
            
        case .categoryList:
            return "\(baseUrl)user/categories/list"
        case .postDetail:
            return "\(baseUrl)feeds/detail"
        case .graphcourselistperc:
        return "\(baseUrl)graph/course/list"
        
        case .DeleteFeed:
            return "\(baseUrl)feeds/delete"
        }
    }
}



