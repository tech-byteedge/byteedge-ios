//
//  MIKeyboardManager.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 15/06/22.
//

import Foundation
import UIKit

protocol MIKeyboardManagerDelegate : class {
    func keyboardWillShow(notification:Notification , keyboardHeight:CGFloat)
    func keyboardDidHide(notification:Notification)
}

class MIKeyboardManager  {
    private init() {}
    private static let miKeyboardManager:MIKeyboardManager = {
        let miKeyboardManager = MIKeyboardManager()
        return miKeyboardManager
    }()
    
    static var shared:MIKeyboardManager {
    return miKeyboardManager
    }
    weak var delegate:MIKeyboardManagerDelegate?
    @objc  private func keyboardWillShow(notification:Notification) {
        
        if let info = notification.userInfo {
            
            if let keyboardRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                
                delegate?.keyboardWillShow(notification: notification, keyboardHeight: keyboardRect.height)
            }
        }
    }
    
    @objc private func keyboardDidHide(notification:Notification) {
        delegate?.keyboardDidHide(notification: notification)
    }
    
}
