
import Foundation
import UIKit
import SDWebImage
import Photos
import PDFKit

enum AppStoryboard : String {
    case Main
    case Account
    case Quiz
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function,line : Int = #line, file : String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }
}

extension UIViewController {
    func logout()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let rootVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "SplashScreen") as? SplashScreen else {
            print("ViewController not found")
           return
        }
        let rootNC = UINavigationController(rootViewController: rootVC)
        UIApplication.shared.windows.first?.rootViewController = rootNC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        rootNC.navigationBar.isHidden = true
        UserDefaults.standard.removeObject(forKey: "signup_info")
        UserDefaults.standard.removeObject(forKey: "TOKEN")
        kUserDefaults.set(false, forKey: "isLoggedIn")
        kUserDefaults.set(false, forKey: "wasAlreadyShown")
        kUserDefaults.removeFromDefault(.firstName)
        kUserDefaults.removeFromDefault(.lastName)
        kUserDefaults.removeFromDefault(.email)
        kUserDefaults.removeFromDefault(.mobileNumber)
        kUserDefaults.removeFromDefault(.dob)
        kUserDefaults.removeFromDefault(.week)
        kUserDefaults.removeFromDefault(. nationality)
//      kUserDefaults.removeFromDefault(. image)
        
        kUserDefaults.removeObject(forKey: "FirstName")
        kUserDefaults.removeObject(forKey: "lastName")
        kUserDefaults.removeObject(forKey: "emailAdress")
        kUserDefaults.removeObject(forKey: "Dob")
        kUserDefaults.removeObject(forKey: "SelectedLDropdown")
        kUserDefaults.removeObject(forKey: "SelectedSkill")
        kUserDefaults.removeObject(forKey: "SESSIONID")
//        kUserDefaults.removeObject(forKey: "Image")
        
        
    }
    
    // Mark :- Date Conversion
    func dateConvertion(strDate: String) -> String {
        let dateAsString = strDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date = df.date(from: dateAsString) else{return ""}
        df.dateFormat = "d MMMM yyyy"
        let time12 = df.string(from: date)
        return time12
    }
    
    func dateConverted(strDate: String) -> String {
        let dateAsString = strDate
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        guard let date = df.date(from: dateAsString) else{return ""}
        df.dateFormat = "d MMMM yyyy"
        let time12 = df.string(from: date)
        return time12
    }
    
    func hoursConvertion(strDate: String) -> String {
        let dateAsString = strDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        guard let date = df.date(from: dateAsString) else{return ""}
        df.dateFormat = "HH:mm"
        let time12 = df.string(from: date)
        return time12
    }
    
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
                    var arrayOfImages = [UIImage]()
                    for asset in assets {
                        let manager = PHImageManager.default()
                        let option = PHImageRequestOptions()
                        option.deliveryMode = .highQualityFormat
                        option.resizeMode = .exact
                        var image = UIImage()
                        option.isSynchronous = true
    //                    PHImageManager.default().requestImage(for:asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: option) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
    //
                       manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                            image = result ?? UIImage()
                            arrayOfImages.append(image)
                        })
                    }

                    return arrayOfImages
                }
  //  func getAssetThumbnail(assets: [PHAsset]) -> [UIImage]
//    {
//        var arrayOfImages = [UIImage]()
//        for asset in assets {
//            let manager = PHImageManager.default()
//            let option = PHImageRequestOptions()
//            option.version = .original
//            option.isSynchronous = true
//            manager.requestImageData(for: asset, options: option) { data, _, _, _ in
//                if let data = data {
//                    arrayOfImages.append(UIImage(data: data)!)
//                }
//            }
//        }
//
//        return arrayOfImages
//    }
    
    
    
    
    @objc func popup(){
        navigationController?.popViewController(animated: true)
    }
    
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
    
    func generatePdfThumbnail(of thumbnailSize: CGSize , for documentUrl: URL, atPage pageIndex: Int) -> UIImage? {
        let pdfDocument = PDFDocument(url: documentUrl)
        let pdfDocumentPage = pdfDocument?.page(at: pageIndex)
        return pdfDocumentPage?.thumbnail(of: thumbnailSize, for: PDFDisplayBox.trimBox)
    }
    
    func imageTapped(image:UIImage){
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage(_:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func imgTapped(image:UIImage){
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissEntirescreenImage(_:)))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    @objc func dismissEntirescreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
        self.tabBarController?.tabBar.isHidden = false
    }

}

extension String {
    var isNumber: Bool {
        let characters = CharacterSet.decimalDigits.inverted
        return !self.isEmpty && rangeOfCharacter(from: characters) == nil
    }
    
}

extension UILabel {
    func setLineHeight(lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment
        
        let attrString = NSMutableAttributedString()
        if (self.attributedText != nil) {
            attrString.append( self.attributedText!)
        } else {
            attrString.append( NSMutableAttributedString(string: self.text!))
            attrString.addAttribute(NSAttributedString.Key.font, value: self.font, range: NSMakeRange(0, attrString.length))
        }
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
    
    
    func roundlabel() {
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
}

// Mark :- SDWebImage Extension

extension UIImageView {
    func setImageWithURL(_ url:URL, placeholderImage: UIImage){
        self.sd_setImage(with: url, placeholderImage: placeholderImage, options: .continueInBackground, context: nil)
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UIButton{
    
    func isselected( borderColor: UIColor = UIColor.white)
    {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.init(cgColor: #colorLiteral(red: 0.1764705882, green: 0.3215686275, blue: 0.6392156863, alpha: 1)).cgColor
        self.layer.cornerRadius = 5
        self.setTitleColor(UIColor.init(cgColor: #colorLiteral(red: 0.1764705882, green: 0.3215686275, blue: 0.6392156863, alpha: 1)), for: .normal)
    }
    func setNormalTitle(normalTitle:String?) {
        self.setTitle(normalTitle, for: .normal)
    }
}

// Mark :- Color Extension
extension UIColor {
    //UIColor().setSubThemeColor().cgColor
    func setThemeColor() -> UIColor{
        return UIColor(red: 0.9137, green: 0.3137, blue: 0.4706, alpha: 1.0) /* #e95078 */
    }
    
    func setSubThemeColor() -> UIColor{
        return UIColor(red: 0.5098, green: 0.1608, blue: 0.5647, alpha: 1.0) /* #822990 */
    }
}

extension UITextView {
    func scrollTextViewToBottom() {
        if self.text.count > 0 {
            let location = self.text.count - 1
            let bottom = NSMakeRange(location, 1)
            self.scrollRangeToVisible(bottom)
        }
    }
    
    func textViewData(htmlString:String, imageArray:[String], onSuccess success: @escaping (_ attributedString: NSAttributedString) -> Void)
    {
        DispatchQueue.global(qos: .background).async {
            var html = htmlString
            for (index, element) in imageArray.enumerated() {
                let temp =  "Image[" + "\(index)" + "]"
                let string =  "\"" + String(element)  + "\" width= \"200\" height= \"200\""
                html = (html as NSString).replacingOccurrences(of: temp, with: string)
            }
            print("textViewData( html................................\(html)")
            let htmlData = NSString(string: html).data(using: String.Encoding.unicode.rawValue)
            let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            do {
                let attributedString = try NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
                
                DispatchQueue.main.async {
                    () -> Void in
                    success(attributedString)
                    self.attributedText = attributedString
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
}


extension UICollectionView{
    
    func setEmptyMessage()
    {
        let nodatavw = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let nodataimage = UIImageView(frame: CGRect(x: 10, y: (self.bounds.size.height / 2) - 50  , width: self.bounds.size.width, height: 330))
        nodataimage.image = #imageLiteral(resourceName: "Union 7")
        nodataimage.contentMode = .center
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: nodataimage.frame.size.height + nodataimage.frame.origin.y + 30 , width: self.bounds.size.width, height: 40))
        messageLabel.text = ""
        messageLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        
        nodatavw.addSubview(nodataimage)
        nodatavw.addSubview(messageLabel)
        
        self.backgroundView = nodatavw;
    }
    
    func restore() {
        self.backgroundView = nil
    }
    
    
    
    func setEmptyMessageAmend()
    {
        
        let nodatavw = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let nodataimage = UIImageView(frame: CGRect(x: 10, y: (self.bounds.size.height / 2) - 50  , width: self.bounds.size.width, height: 50))
        nodataimage.image = #imageLiteral(resourceName: "nodataBackgroudImg")
        nodataimage.contentMode = .center
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: nodataimage.frame.size.height + nodataimage.frame.origin.y + 30 , width: self.bounds.size.width, height: 40))
        messageLabel.text = "No Slots Availabe"
        messageLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        
        nodatavw.addSubview(nodataimage)
        nodatavw.addSubview(messageLabel)
        
        self.backgroundView = nodatavw;
    }
    
    func restoreset() {
        self.backgroundView = nil
    }
    
    
}

extension UITableView
{
    func setEmptyMessageshow()
    {
        let nodatavw = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        let nodataimage = UIImageView(frame: CGRect(x:0, y: (self.bounds.size.height / 2.5), width: self.bounds.size.width, height: 50))
        nodataimage.image = #imageLiteral(resourceName: "nodataBackgroudImg")
        nodataimage.contentMode = .center
        
        nodataimage.contentMode = .center
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: nodataimage.frame.size.height + nodataimage.frame.origin.y + 30 , width: self.bounds.size.width, height: 40))
        messageLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        messageLabel.text = "No data found"
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        
        nodatavw.addSubview(nodataimage)
        nodatavw.addSubview(messageLabel)
        
        self.backgroundView = nodatavw;
    }
    
    // for feed
    func setemptyforfeed()
    {
        let nodatavw = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let nodataimage = UIImageView(frame: CGRect(x: -10, y: (self.bounds.size.height / 2), width: self.bounds.size.width, height: 50))
        nodataimage.image = #imageLiteral(resourceName: "nodataBackgroudImg")
        nodataimage.contentMode = .center
        
        nodataimage.contentMode = .center
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: nodataimage.frame.size.height + nodataimage.frame.origin.y + 30 , width: self.bounds.size.width, height: 40))
        messageLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        messageLabel.text = "no post available"
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        
        nodatavw.addSubview(nodataimage)
        nodatavw.addSubview(messageLabel)
        
        self.backgroundView = nodatavw;
    }
    
    func restore() {
        self.backgroundView = nil
    }
    
    
    
    
    
//    func restore() {
//        self.backgroundView = nil
//    }
//
    
    func setEmptyMessageshownotification()
    {
        
        let nodatavw = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let nodataimage = UIImageView(frame: CGRect(x: -10, y: (self.bounds.size.height / 2) - 50  , width: self.bounds.size.width, height: 50))
        nodataimage.image = #imageLiteral(resourceName: "nodataBackgroudImg")
        nodataimage.contentMode = .center
        
        
        
        nodataimage.contentMode = .center
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: nodataimage.frame.size.height + nodataimage.frame.origin.y + 30 , width: self.bounds.size.width, height: 40))
        messageLabel.text = "No data found"
        messageLabel.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        
        nodatavw.addSubview(nodataimage)
        nodatavw.addSubview(messageLabel)
        
        self.backgroundView = nodatavw;
    }
    
    func restoredata() {
        self.backgroundView = nil
    }
    
    
    
}

extension UIViewController {
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 200, y: self.view.frame.size.height-200, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }


extension UIView{
    
    func roundspecificCorners(corners: UIRectCorner, radius: Int = 8) {
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
    
    
    
    
    func selectedborder( borderColor: UIColor = UIColor.white)
    {
        self.layer.borderWidth = 0.3
        self.layer.borderColor = UIColor.init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)).cgColor
    }
    func makeRoundCornerwithborder(_ radius:CGFloat, bordercolor :UIColor, borderwidth : CGFloat)
    {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderwidth
        self.layer.borderColor = bordercolor.cgColor
        self.clipsToBounds = true
    }
    
    func selectedborderset( borderColor: UIColor = UIColor.white)
    {
        self.layer.borderWidth = 1.5
        self.layer.borderColor = UIColor.init(cgColor: #colorLiteral(red: 0.7882352941, green: 0.1411764706, blue: 0.537254902, alpha: 1)).cgColor
        
    }
    
    
    func roundUpperBothCorner(cornerRadius : Double){
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func roundbottomBothCorner(cornerRadius : Double){
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func roundParticularCorners(cornerRadius: Double)
    {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.clipsToBounds = true
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func dropShadow(shadowColor: UIColor = UIColor(red: 0.0824, green: 0.2157, blue: 0.2706, alpha: 0.5), borderColor: UIColor = UIColor.white , opacity: Float = 0.4, offSet: CGSize = CGSize(width: 0, height: 0), radius: CGFloat = 5, scale: Bool = true) {
        
        
        self.layer.masksToBounds = false
        self.layer.borderColor = borderColor.cgColor
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        //    self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func dropShadowblue() {
        //self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 5
    }
    
    func bottomView()
    {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.3
    }
    
    class bottomView:UIView{
        required init(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)!
            self.layer.shadowColor = UIColor.darkGray.cgColor
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowOpacity = 0.3
            // self.layer.masksToBounds = true
        }
    }
    func makeRounded() {
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
       
    }
    
    func makeRoundCorner(_ radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func randomPoint(forView : UIView) -> CGPoint {
        let newX = CGFloat(arc4random_uniform(UInt32(bounds.size.width-forView.bounds.size.width+1)))
        let newY = CGFloat(arc4random_uniform(UInt32(bounds.size.height-forView.bounds.size.height+1)))
        let newPoint = CGPoint(x: newX, y: newY)
        return newPoint
    }
    
    
    func delay(interval: TimeInterval, closure: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + interval) {
            closure()
        }
        func cornerRadius ()
        {
            self.layer.cornerRadius = CGFloat(12)
            self.clipsToBounds = true
        }
        
    }
    
    
    func addGradientBackground(firstColor: UIColor, secondColor: UIColor){
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        print(gradientLayer.frame)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func applyGradient(colorOne: UIColor, ColorTwo: UIColor) {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [colorOne.cgColor, ColorTwo.cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        // gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradientForInsta(colorOne: UIColor, ColorTwo: UIColor, ColorThree: UIColor)
    {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [colorOne.cgColor, ColorTwo.cgColor, ColorThree.cgColor]
        gradient.locations = [0.0,0.5,1.1]
        //gradient.startPoint = CGPoint(x: 0.3, y: 0.2)
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        // gradient.endPoint = CGPoint(x: 0.4, y: 0.9)
        //        gradient.type = .axial
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        // gradient.cornerRadius = 10
        layer.insertSublayer(gradient, at: 0)
        self.clipsToBounds = true
        //self.layer.masksToBounds = false
    }
    
    func dropShadows() {
        layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        layer.shadowOffset = CGSize(width: 2, height: 5)
        layer.masksToBounds = false
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 10
        layer.rasterizationScale = UIScreen.main.scale
        layer.shouldRasterize = true
    }
    
    func setCircle() {
        layer.cornerRadius = frame.size.width/2
        clipsToBounds = true
    }
    
}

@IBDesignable
class RadialGradientView: UIView {
    
    @IBInspectable var InsideColor: UIColor = UIColor.clear
    @IBInspectable var OutsideColor: UIColor = UIColor.clear
    
    override func draw(_ rect: CGRect) {
        let colors = [InsideColor.cgColor, OutsideColor.cgColor] as CFArray
        let endRadius = min(frame.width, frame.height)
        let center = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
        let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        UIGraphicsGetCurrentContext()!.drawRadialGradient(gradient!, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: endRadius, options: CGGradientDrawingOptions.drawsBeforeStartLocation)
    }
}

extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        // iOS 14 and above
        if #available(iOS 14, *) {// Added condition for iOS 14
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
        self.resignFirstResponder()
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}

extension UIViewController {
    
    func createAlert (title:String, message:String)
    {
        let okcallback: (()->())?
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    func createAlertCallback (title:String, message:String,cancelTitle:String?,completionBlock:@escaping (_ okPressed:Bool)->())
     {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            completionBlock(true)
        })
        if let cancelTitle = cancelTitle{
            let cancelOption = UIAlertAction(title: cancelTitle, style: .cancel, handler: { (axn) in
                completionBlock(false)
            })
            alertView.addAction(cancelOption)
            // self.dismiss(animated: true, completion: nil)
        }
        
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func createAlertCallbackpop (title:String, message:String,completionBlock:@escaping (_ okPressed:Bool)->())
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            completionBlock(true)
        })
//        if let cancelTitle = cancelTitle{
//            let cancelOption = UIAlertAction(title: cancelTitle, style: .cancel, handler: { (axn) in
//                completionBlock(false)
//            })
//            alertView.addAction(cancelOption)
//            // self.dismiss(animated: true, completion: nil)
//        }
        
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    //    func presentAlertViewWithOneButtonset(alertTitle:String? , alertMessage:String? , btnOneTitle:String , btnOneTapped:alertActionHandler) {
    //
    //            let alertController = UIAlertController(title: alertTitle ?? "", message: alertMessage ?? "", preferredStyle: .alert)
    //
    //            alertController.addAction(UIAlertAction(title: btnOneTitle, style: .default, handler: btnOneTapped))
    //
    //            self.present(alertController, animated: true, completion: nil)
    //        }
    //    func createAlertShow (title:String, message:String)
    //    {
    //        let screenWidth = UIScreen.main.bounds.width
    //        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    //        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
    //        let alertView = UIAlertController(title:AppName, message: message, preferredStyle: .alert)
    //        let barButton = UIBarButtonItem(title: "OK", style: .plain, target: target, action: #selector(ok))
    //        _ = UIBarButtonItem(title: "Cancel", style: .plain, target: target, action: #selector(cancel))
    //        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
    //
    //        })
    //        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
    //
    //
    //        })
    //        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
    //        self.inputAccessoryView = toolBar //9
    //            self.resignFirstResponder()
    //        alertView.addAction(action)
    //        alertView.addAction(actionCancel)
    //        self.present(alertView, animated: true, completion: nil)
    //    }
    @objc func ok() {
        self.resignFirstResponder()
    }
    
    @objc func cancel() {
        self.resignFirstResponder()
    }
    
    
    
    
//    func isValidEmail(testStr:String) -> Bool
//    {
//        let emailRegEx = "[A-Z0-9a-z]+([._%+-]{1}[A-Z0-9a-z]+)*@[A-Z0-9a-z]+([.-]{1}[A-Z0-9a-z]+)*(\\.[A-Za-z]"
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//        return emailTest.evaluate(with: testStr)
//    }
    
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    func getCountryPhonceCode (country : String) -> String
    {
        let countryDictionary  = ["AF":"+93",
                                  "AL":"+355",
                                  "DZ":"+213",
                                  "AS":"+1",
                                  "AD":"+376",
                                  "AO":"+244",
                                  "AI":"+1",
                                  "AG":"+1",
                                  "AR":"+54",
                                  "AM":"+374",
                                  "AW":"+297",
                                  "AU":"+61",
                                  "AT":"+43",
                                  "AZ":"+994",
                                  "BS":"+1",
                                  "BH":"+973",
                                  "BD":"+880",
                                  "BB":"+1",
                                  "BY":"+375",
                                  "BE":"+32",
                                  "BZ":"+501",
                                  "BJ":"+229",
                                  "BM":"+1",
                                  "BT":"+975",
                                  "BA":"+387",
                                  "BW":"+267",
                                  "BR":"+55",
                                  "IO":"+246",
                                  "BG":"+359",
                                  "BF":"+226",
                                  "BI":"+257",
                                  "KH":"+855",
                                  "CM":"+237",
                                  "CA":"+1",
                                  "CV":"+238",
                                  "KY":"+345",
                                  "CF":"+236",
                                  "TD":"+235",
                                  "CL":"+56",
                                  "CN":"+86",
                                  "CX":"+61",
                                  "CO":"+57",
                                  "KM":"+269",
                                  "CG":"+242",
                                  "CK":"+682",
                                  "CR":"+506",
                                  "HR":"+385",
                                  "CU":"+53",
                                  "CY":"+537",
                                  "CZ":"+420",
                                  "DK":"+45",
                                  "DJ":"+253",
                                  "DM":"+1",
                                  "DO":"+1",
                                  "EC":"+593",
                                  "EG":"+20",
                                  "SV":"+503",
                                  "GQ":"+240",
                                  "ER":"+291",
                                  "EE":"+372",
                                  "ET":"+251",
                                  "FO":"+298",
                                  "FJ":"+679",
                                  "FI":"+358",
                                  "FR":"+33",
                                  "GF":"+594",
                                  "PF":"+689",
                                  "GA":"+241",
                                  "GM":"+220",
                                  "GE":"+995",
                                  "DE":"+49",
                                  "GH":"+233",
                                  "GI":"+350",
                                  "GR":"+30",
                                  "GL":"+299",
                                  "GD":"+1",
                                  "GP":"+590",
                                  "GU":"+1",
                                  "GT":"+502",
                                  "GN":"+224",
                                  "GW":"+245",
                                  "GY":"+595",
                                  "HT":"+509",
                                  "HN":"+504",
                                  "HU":"+36",
                                  "IS":"+354",
                                  "IN":"+91",
                                  "ID":"+62",
                                  "IQ":"+964",
                                  "IE":"+353",
                                  "IL":"+972",
                                  "IT":"+39",
                                  "JM":"+1",
                                  "JP":"+81",
                                  "JO":"+962",
                                  "KZ":"+77",
                                  "KE":"+254",
                                  "KI":"+686",
                                  "KW":"+965",
                                  "KG":"+996",
                                  "LV":"+371",
                                  "LB":"+961",
                                  "LS":"+266",
                                  "LR":"+231",
                                  "LI":"+423",
                                  "LT":"+370",
                                  "LU":"+352",
                                  "MG":"+261",
                                  "MW":"+265",
                                  "MY":"+60",
                                  "MV":"+960",
                                  "ML":"+223",
                                  "MT":"+356",
                                  "MH":"+692",
                                  "MQ":"+596",
                                  "MR":"+222",
                                  "MU":"+230",
                                  "YT":"+262",
                                  "MX":"+52",
                                  "MC":"+377",
                                  "MN":"+976",
                                  "ME":"+382",
                                  "MS":"+1",
                                  "MA":"+212",
                                  "MM":"+95",
                                  "NA":"+264",
                                  "NR":"+674",
                                  "NP":"+977",
                                  "NL":"+31",
                                  "AN":"+599",
                                  "NC":"+687",
                                  "NZ":"+64",
                                  "NI":"+505",
                                  "NE":"+227",
                                  "NG":"+234",
                                  "NU":"+683",
                                  "NF":"+672",
                                  "MP":"+1",
                                  "NO":"+47",
                                  "OM":"+968",
                                  "PK":"+92",
                                  "PW":"+680",
                                  "PA":"+507",
                                  "PG":"+675",
                                  "PY":"+595",
                                  "PE":"+51",
                                  "PH":"+63",
                                  "PL":"+48",
                                  "PT":"+351",
                                  "PR":"+1",
                                  "QA":"+974",
                                  "RO":"+40",
                                  "RW":"+250",
                                  "WS":"+685",
                                  "SM":"+378",
                                  "SA":"+966",
                                  "SN":"+221",
                                  "RS":"+381",
                                  "SC":"+248",
                                  "SL":"+232",
                                  "SG":"+65",
                                  "SK":"+421",
                                  "SI":"+386",
                                  "SB":"+677",
                                  "ZA":"+27",
                                  "GS":"+500",
                                  "ES":"+34",
                                  "LK":"+94",
                                  "SD":"+249",
                                  "SR":"+597",
                                  "SZ":"+268",
                                  "SE":"+46",
                                  "CH":"+41",
                                  "TJ":"+992",
                                  "TH":"+66",
                                  "TG":"+228",
                                  "TK":"+690",
                                  "TO":"+676",
                                  "TT":"+1",
                                  "TN":"+216",
                                  "TR":"+90",
                                  "TM":"+993",
                                  "TC":"+1",
                                  "TV":"+688",
                                  "UG":"+256",
                                  "UA":"+380",
                                  "AE":"+971",
                                  "GB":"+44",
                                  "US":"+1",
                                  "UY":"+598",
                                  "UZ":"+998",
                                  "VU":"+678",
                                  "WF":"+681",
                                  "YE":"+967",
                                  "ZM":"+260",
                                  "ZW":"+263",
                                  "BO":"+591",
                                  "BN":"+673",
                                  "CC":"+61",
                                  "CD":"+243",
                                  "CI":"+225",
                                  "FK":"+500",
                                  "GG":"+44",
                                  "VA":"+379",
                                  "HK":"+852",
                                  "IR":"+98",
                                  "IM":"+44",
                                  "JE":"+44",
                                  "KP":"+850",
                                  "KR":"+82",
                                  "LA":"+856",
                                  "LY":"+218",
                                  "MO":"+853",
                                  "MK":"+389",
                                  "FM":"+691",
                                  "MD":"+373",
                                  "MZ":"+258",
                                  "PS":"+970",
                                  "PN":"+872",
                                  "RE":"+262",
                                  "RU":"+7",
                                  "BL":"+590",
                                  "SH":"+290",
                                  "KN":"+1",
                                  "LC":"+1",
                                  "MF":"+590",
                                  "PM":"+508",
                                  "VC":"+1",
                                  "ST":"+239",
                                  "SO":"+252",
                                  "SJ":"+47",
                                  "SY":"+963",
                                  "TW":"+886",
                                  "TZ":"+255",
                                  "TL":"+670",
                                  "VE":"+58",
                                  "VN":"+84",
                                  "VG":"+284",
                                  "VI":"+340"]
        if countryDictionary[country] != nil {
            return countryDictionary[country]!
        }
        
        else {
            return ""
        }
    }
}

extension Date
{
    
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
        //RESOLVED CRASH HERE
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}


class CommonUtilities
{
    static let shared = CommonUtilities()
    var currentTab = 0
    //differnce time betweeen created post time and current time
    func differnceTime(sendTime:String) -> String
    {
        let dateFormatter = DateFormatter() //"2021-08-24T05:47:59.899Z"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSS'Z'"
        
        let fmt = ISO8601DateFormatter()
        fmt.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        
        let date1 = fmt.date(from: sendTime) ?? Date()
        let date2 = fmt.date(from: getCurrentTime()) ?? Date()
        
        let diffs = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date1, to: date2)
        
        
        if (diffs.year! > 0)
        {
            print("\(diffs.year!) years")
            return "\(diffs.year!) years"
        }
        else if(diffs.month! > 0)
        {
            print("\(diffs.month!) months")
            return "\(diffs.month!) months"
        }
        else if(diffs.day! > 0)
        {
            print("\(diffs.day!) day")
            return "\(diffs.day!) day"
        }
        else if(diffs.hour! > 0)
        {
            print("\(diffs.hour!) hours")
            return "\(diffs.hour!) hours"
        }
        else if(diffs.minute! > 0)
        {
            print("\(diffs.minute!) minutes")
            return "\(diffs.minute!) minutes"
        }
        else
        {
            print("\(diffs.second!) seconds")
            return "\(diffs.second!) seconds"
        }
        
    }
    func getCurrentTime() -> String
    {
        let date = Date()
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:SSS'Z'"
        let dateString = df.string(from: date)
        return dateString
    }
    
    
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage]
    {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.version = .original
            option.isSynchronous = true
            manager.requestImageData(for: asset, options: option) { data, _, _, _ in
                if let data = data {
                    arrayOfImages.append(UIImage(data: data)!)
                }
            }
        }
        
        return arrayOfImages
    }
}

extension Date{
    
    func dateToString(format : String) -> String {
        let df = DateFormatter()
        df.dateFormat = format
        df.timeZone = TimeZone.current
//        df.timeZone = TimeZone(abbreviation: "GMT")
        return df.string(from: self)
    }
    
    static func getCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: Date())
    }
}

extension String {
    func stringToDate(format : String) -> Date? {
        let df = DateFormatter()
        df.dateFormat = format
        df.locale = Locale(identifier: "en_US_POSIX")
        df.timeZone = TimeZone(abbreviation: "GMT")
        return df.date(from: self)
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd/MM/yyyy"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
}

extension Int {
    var ordinal: String {
        get {
            var suffix = "th"
            switch self % 10 {
            case 1:
                suffix = "st"
            case 2:
                suffix = "nd"
            case 3:
                suffix = "rd"
            default: ()
            }
            if 10 < (self % 100) && (self % 100) < 20 {
                suffix = "th"
            }
            return String(self) + suffix
        }
    }
}

extension String {
    
    var trim:String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    var isBlank:Bool {
        return self.trim.isEmpty
    }
    
    var isAlphanumeric:Bool {
        return !isBlank && rangeOfCharacter(from: .alphanumerics) != nil
    }
    
    var isAlpha:Bool {
        let allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let characterSet = CharacterSet(charactersIn: allowed)
        return !isBlank && rangeOfCharacter(from: characterSet) != nil
    }
    
    var isValidEmail:Bool {
        //        let emailRegEx = "[A-Z0-9a-z_%+-]+.@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailRegEx = "[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with:self)
    }
    
    var isValidPhoneNo:Bool {
        let phoneCharacters = CharacterSet(charactersIn: "+0123456789").inverted
        let arrCharacters = self.components(separatedBy: phoneCharacters)
        return self == arrCharacters.joined(separator: "")
    }
}



extension UIViewController {
    
    func addChildViewControllerWithView(_ childViewController: UIViewController, toView view: UIView? = nil) {
        let view: UIView = view ?? self.view
        childViewController.removeFromParent()
        childViewController.willMove(toParent: self)
        addChild(childViewController)
        childViewController.didMove(toParent: self)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(childViewController.view)
        view.addConstraints([
            NSLayoutConstraint(item: childViewController.view!, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: -10),
            NSLayoutConstraint(item: childViewController.view!, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: childViewController.view!, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: childViewController.view!, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
        ])
        view.layoutIfNeeded()
    }
    
    func removeChildViewController(_ childViewController: UIViewController) {
        childViewController.removeFromParent()
        childViewController.willMove(toParent: nil)
        childViewController.removeFromParent()
        childViewController.didMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        view.layoutIfNeeded()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}



extension UIColor {
    
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension PHAsset {
    var image : UIImage {
        var thumbnail = UIImage()
        let imageManager = PHCachingImageManager()
        imageManager.requestImage(for: self, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: nil, resultHandler: { image, _ in
            thumbnail = image!
        })
        return thumbnail
    }
}


// MARK: - Extension of UIView For draw a shadowView of it.
extension UIView {
    
    /// -  Parameters:
    ///   - color: Pass the UIColor that you want to see as shadowColor.
    ///   - shadowOffset: Pass the CGSize value for how much far you want shadowView from parentView.
    ///   - shadowRadius: Pass the CGFloat value for how much length(Blur Spreadness) you want in shadowView.
    func shadow(color:UIColor , shadowOffset:CGSize , shadowRadius:CGFloat) {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        //earlier it was 0.5
        self.layer.shadowOpacity = 0.6
        // self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        // self.layer.shouldRasterize = true
        // self.layer.rasterizationScale = CMainScreen.scale
    }
}


extension UISwitch {
    
    func set(width: CGFloat, height: CGFloat) {
        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51
        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth
        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}


extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

extension UIImage {
    func imageResized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}


extension String {
    static func format(strings: [String],
                       boldFont: UIFont = UIFont.systemFont(ofSize: 13),
                       boldColor: UIColor = UIColor.blue,
                       inString string: String,
                       font: UIFont = UIFont.systemFont(ofSize: 13),
                       color: UIColor = UIColor.black) -> NSAttributedString {
        let attributedString =
        NSMutableAttributedString(string: string,
                                  attributes: [
                                    NSAttributedString.Key.font: font,
                                    NSAttributedString.Key.foregroundColor: color
                                  ])
        let boldFontAttribute = [
            NSAttributedString.Key.font: boldFont,
            NSAttributedString.Key.foregroundColor: boldColor,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue
        ] as [NSAttributedString.Key : Any]
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of:bold))
        }
        return attributedString
    }
}



extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        guard let attributedText = label.attributedText else { return false }
        
        let mutableStr = NSMutableAttributedString.init(attributedString: attributedText)
        mutableStr.addAttributes([NSAttributedString.Key.font : label.font!], range: NSRange.init(location: 0, length: attributedText.length))
        
        // If the label have text alignment. Delete this code if label have a default (left) aligment. Possible to add the attribute in previous adding.
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        mutableStr.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedText.length))
        
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: mutableStr)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}



extension UICollectionView
{
    func getSizeForCollectionViewCell() -> CGSize{
        let flowLayout: UICollectionViewFlowLayout = {
            let layout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 0)
            return layout
        }()
        
        let width = self.frame.size.width
        var numberOfItemsPerRow : CGFloat = 2
        if width < 450{
            numberOfItemsPerRow = 2
        }else if width < 800 {
            numberOfItemsPerRow = 3
        }else {
            if UIApplication.shared.statusBarOrientation.isLandscape {
            numberOfItemsPerRow = 5
            }
            else{
            numberOfItemsPerRow = 4
            }
        }
        let spacing: CGFloat = flowLayout.minimumInteritemSpacing
        let availableWidth = width - spacing * (numberOfItemsPerRow + 1)
        let itemDimension = floor(availableWidth / numberOfItemsPerRow)
        return CGSize(width: itemDimension, height: itemDimension)
    }
    
    func getSizeForCollectionViewCell(numberOfItemsPerRow : CGFloat, height : CGFloat,miniSpacing : CGFloat = 10) -> CGSize{
        let flowLayout: UICollectionViewFlowLayout = {
            let layout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = miniSpacing
            layout.minimumLineSpacing = miniSpacing
            layout.sectionInset = UIEdgeInsets(top: miniSpacing, left: miniSpacing, bottom: miniSpacing, right: miniSpacing)
            return layout
        }()
        
        let width = self.frame.size.width
        let spacing: CGFloat = flowLayout.minimumInteritemSpacing
        let availableWidth = width - spacing * (numberOfItemsPerRow + 1)
        let itemDimension = floor(availableWidth / numberOfItemsPerRow)
        return CGSize(width: itemDimension, height: height)
    }
}


extension UIImageView {

    public func loadGif(name: String) {
        DispatchQueue.global().async {
            let image = UIImage.gif(name: name)
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }

}

extension UIImage {

    public class func gif(data: Data) -> UIImage? {
        // Create source from data
        guard let source = CGImageSourceCreateWithData(data as CFData, nil) else {
            print("SwiftGif: Source for the image does not exist")
            return nil
        }

        return UIImage.animatedImageWithSource(source)
    }

    public class func gif(url: String) -> UIImage? {
        // Validate URL
        guard let bundleURL = URL(string: url) else {
            print("SwiftGif: This image named \"\(url)\" does not exist")
            return nil
        }

        // Validate data
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(url)\" into NSData")
            return nil
        }

        return gif(data: imageData)
    }

    public class func gif(name: String) -> UIImage? {
        // Check for existance of gif
        guard let bundleURL = Bundle.main
          .url(forResource: name, withExtension: "gif") else {
            print("SwiftGif: This image named \"\(name)\" does not exist")
            return nil
        }

        // Validate data
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }

        return gif(data: imageData)
    }

    internal class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
        var delay = 0.0

        // Get dictionaries
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifPropertiesPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 0)
        if CFDictionaryGetValueIfPresent(cfProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque(), gifPropertiesPointer) == false {
            return delay
        }

        let gifProperties:CFDictionary = unsafeBitCast(gifPropertiesPointer.pointee, to: CFDictionary.self)

        // Get delay time
        var delayObject: AnyObject = unsafeBitCast(
            CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
            to: AnyObject.self)
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }

        delay = delayObject as? Double ?? 0

        if delay < 0.1 {
            delay = 0.03 // Make sure they're not too fast
        }

        return delay
    }

    internal class func gcdForPair( a: Int?,  b: Int?) -> Int {
        var a = a
        var b = b
        // Check if one of them is nil
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }

        // Swap for modulo
        if a! < b! {
            let c = a
            a = b
            b = c
        }

        // Get greatest common divisor
        var rest: Int
        while true {
            rest = a! % b!

            if rest == 0 {
                return b! // Found it
            } else {
                a = b
                b = rest
            }
        }
    }

    internal class func gcdForArray(_ array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }

        var gcd = array[0]

        for val in array {
            gcd = UIImage.gcdForPair(a: val, b: gcd)
        }

        return gcd
    }

    internal class func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()

        // Fill arrays
        for i in 0..<count {
            // Add image
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }

            // At it's delay in cs
            let delaySeconds = UIImage.delayForImageAtIndex(Int(i),
                source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }

        // Calculate full duration
        let duration: Int = {
            var sum = 0

            for val: Int in delays {
                sum += val
            }

            return sum
            }()

        // Get frames
        let gcd = gcdForArray(delays)
        var frames = [UIImage]()

        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)

            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }

        // Heyhey
        let animation = UIImage.animatedImage(with: frames,
            duration: Double(duration) / 1000.0)

        return animation
    }

}


prefix operator /

public prefix func /(value: Int?) -> Int {
    return value ?? 0
}
public prefix func /(value : String?) -> String {
    return value ?? ""
}
public prefix func /(value : Bool?) -> Bool {
    return value ?? false
}
public prefix func /(value : Double?) -> Double {
    return value ?? 0.0
}
public prefix func /(value : Float?) -> Float {
    return value ?? 0.0
}
public prefix func /(value : Array<AnyObject>) -> Array<AnyObject> {
    return value
}
extension UIViewController{
    
  
}
