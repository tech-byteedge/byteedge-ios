
import Foundation
import Alamofire
import UIKit

class ServiceManager: NSObject {
    private override init() {}
    var reachability : NetworkReachabilityManager?
    static let instance = ServiceManager()
    var alertShowing = false
    enum Method: String {
        case GET_REQUEST = "GET"
        case POST_REQUEST = "POST"
        case PUT_REQUEST = "PUT"
        case DELETE_REQUEST = "DELETE"
    }
}

// MARK: -
extension ServiceManager {
    func request<A: Codable>(
        method: HTTPMethod,
        apiURL: URLConvertible,
        headers:  HTTPHeaders?,
        parameters: [String : Any]?,
        decodable: A.Type,
        encoding: ParameterEncoding = JSONEncoding.default,
        completionHandler: @escaping (_ success:Bool?, [String : Any]?, A?, String?) -> ()
    ) {
        guard Reachability.isConnectedToNetwork()  else {
            if self.alertShowing {
                self.alertShowing = true
                let alert = UIAlertController(title: kNetworkProblemKey, message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: kOK, style: UIAlertAction.Style.cancel, handler: { (act) in
                    self.alertShowing = false
                }))
              //  UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
            completionHandler(false ,nil, nil, kWentWrongKey)
            return
        }
        
//        print("URL:===========> \(apiURL)")
//        print("Parameter:===========> \(parameters ?? [:])")
        
        AF.request(apiURL, method: method, parameters: parameters, encoding: encoding, headers: headers).response(completionHandler: { (response) in
//            print(response)
            print(response.request)
             guard response.error == nil, let _data = response.data else {
                completionHandler(false, nil, nil, nil ?? kWentWrongKey)
                return
             }
            
            do {
                if let responseJSON = try JSONSerialization.jsonObject(with: _data, options: []) as? [String : Any] {
//                    print("Response JONS:===========\(apiURL)==========> \(responseJSON)")
                    let responseModel = try JSONDecoder().decode(A.self, from: _data)
//                    print(responseModel)
                    completionHandler(true, responseJSON, responseModel, nil)
                    
                } else {
                    completionHandler(false ,nil, nil, "Unable to convert into dictionary")
                }
                
            } catch let error as NSError {
                print(error)
                completionHandler(false ,nil, nil, kWentWrongKey)
            }
            
        })
        
    }
}

// MARK: - MULTIPART API WITH SINGLE IMAGE
 extension ServiceManager {
    func uploadImageRequestWithDecodable<T: Decodable>(
        method: HTTPMethod,
        apiURL: URLConvertible,
        headers:  HTTPHeaders?,
        parameters: [String : Any],
        decodable: T.Type,
        encoding: ParameterEncoding = JSONEncoding.default,
        isShowLoader: Bool = false,
        imageParameters: [String : UIImage],
        completionHandler: @escaping (_ success:Bool?, T?, String?) -> ()
    ) {
        
        guard Reachability.isConnectedToNetwork()  else {
             if(!self.alertShowing)
            {
                self.alertShowing = true
                let alert = UIAlertController(title: kNetworkProblemKey, message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: { (act) in
                    self.alertShowing = false
                }))
                
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
            //completionHandler(false ,nil, nil, NSError(domain: "somedomain", code:
            completionHandler(false , nil, kWentWrongKey)
            return
        }
//        print("URL:===========> \(apiURL)")
//        print("Parameter:===========> \(parameters)")
        AF.upload(
            multipartFormData: { multipartFormData in
                /// Append dictionary in FormData
                for (key, value) in parameters {
                    if let v = value as? String, let valueAsData = v.data(using: .utf8) {
                     multipartFormData.append(valueAsData, withName: key)
                    }
                }
                for (key, image) in imageParameters {
                    if let imgData = image.jpegData(compressionQuality: 0.3) {
                        //multipartFormData.append(imgData , withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
                        multipartFormData.append(imgData , withName: key, fileName: "image.jpg", mimeType: "image/jpeg")
                        print("Success image append")
                    }
                }
                
                /*
                 /// Append  image in formData
                 if let img = img {
                 let frontImage = img.jpegData(compressionQuality: 0.8)
                 multipartFormData.append(frontImage!, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
                 Log("Success image append")
                 } */
                
            },
            to: apiURL, method: method , headers: headers)
            .response { resp in
                
                
                guard resp.error == nil, let _data = resp.data else {
                    completionHandler(false, nil, resp.error!.errorDescription)
                    return
                }
                
                do {
                    
                    if let responseJSON = try JSONSerialization.jsonObject(with: _data, options: []) as? [String : Any] {
                        
                        
//                        print("Response JONS:===========> \(responseJSON)")
                        
                        let responseModel = try JSONDecoder().decode(T.self, from: _data)
                        completionHandler(true, responseModel, nil)
                    }
                    
                } catch let error as NSError {
                    print(error)
                    completionHandler(false, nil, kWentWrongKey)
                }
            }
    }
 }

// MARK: - MULTIPART API WITH MULITPLE IMAGE  WITH VIDEO URL
extension ServiceManager {
    func uploadImageArraandVedioyRequestWithDecodable<T: Decodable>(
        method: HTTPMethod,
        apiURL: URLConvertible,
        headers:  HTTPHeaders?,
        parameters: [String : Any],
        decodable: T.Type,
        encoding: ParameterEncoding = JSONEncoding.default,
        isShowLoader: Bool = true,
        imgArray: [UIImage],
        videoURL: URL?,
        pdfURL : URL?,
        thumbnails:UIImage? = nil,
        completionHandler: @escaping (_ success: Bool?, T?, String?) -> ()
    ) {
        guard Reachability.isConnectedToNetwork()  else {
            if(!self.alertShowing)
            {
                self.alertShowing = true
                let alert = UIAlertController(title: kNetworkProblemKey, message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: kOK, style: UIAlertAction.Style.cancel, handler: { (act) in
                    self.alertShowing = false
                }))
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
            completionHandler(false ,nil, kWentWrongKey)
            return
        }
        
//        print("URL:===========> \(apiURL)")
//        print("Parameter:===========> \(parameters)")
        var imageDataArray: [Data] = []
        _ = imgArray.map {
            if let data = $0.jpegData(compressionQuality: 1.0) {
            imageDataArray.append(data)
            }
        }
        
          AF.upload(
            multipartFormData: { multipartFormData in
                /// Append dictionary in FormData
                for (key, value) in parameters {
                    if let v = value as? String, let valueAsData = v.data(using: .utf8) {
                    multipartFormData.append(valueAsData, withName: key)
                    }
                }
                /// APPEND Images in multipart
                _ = imageDataArray.map {
                    multipartFormData.append($0, withName: "image", fileName: "img.jpg", mimeType: "image/jpeg")
                        print("Success Front image append")
                }
                
                if let thumbnail = thumbnails ,thumbnail.size.width != 0
                {
                let thumbnailData = thumbnail.pngData()
                multipartFormData.append(thumbnailData! , withName: "thumbnails", fileName: "img.jpg", mimeType: "image/jpeg")
                }
                
                //Append Video
                if let url = videoURL {
                    multipartFormData.append(url, withName: "video")
                        print("Video Add sucessfully")
                }
                
                
//                if let url = thumbnails {
//                    multipartFormData.append(url, withName: "thumbnails")
//                    print("thumbnails Add sucessfully")
//                }
                
                
                //Append pdf
                if let url = pdfURL {
                    multipartFormData.append(url, withName: "attachment")
                            print("pdf Add sucessfully")
                }
            },
            to: apiURL, method: method , headers: headers)
            .response { resp in
                
                guard resp.error == nil, let _data = resp.data else {
                    completionHandler(false, nil, resp.error!.errorDescription)
                    return
                }
                
                do {
                    if let responseJSON = try JSONSerialization.jsonObject(with: _data, options: []) as? [String : Any] {
                        
                        print("Response JONS:===========> \(responseJSON)")
                        let responseModel = try JSONDecoder().decode(T.self, from: _data)
                        completionHandler(true, responseModel, nil)
                    }
                    
                } catch let error as NSError {
                    print(error)
                    completionHandler(false ,nil,kWentWrongKey)
                }
            }
    }
}



