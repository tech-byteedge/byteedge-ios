
import Foundation
struct UDKeys
{
static let fcmId = "fcmId"
}
struct MyUserDefaults {
    private let defaults = UserDefaults.standard
    private init(){}
    static var instance = MyUserDefaults()

    enum Key:String {
        case access_token
    }
    
    func set<T>(value:T,key:Key){
        defaults.set(value, forKey: key.rawValue)
    }
    
    func get<T>(key:Key) -> T?{
        return defaults.value(forKey: key.rawValue) as? T
    }
    
}
