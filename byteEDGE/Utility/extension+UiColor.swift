//
//  extension+UiColor.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.
//

import Foundation
import UIKit

enum AssetsColor: String {
    case white //FFFFFF
    case whiteLight //FFFFFF99
    case whiteExtraLight //F8FCFD
    case black //#000000
    case gray //707070
    case pink //DA3467
    case graylight //AAAAAA
    case blackLight //#1C1D23
    case grayBorder //#70707057
    case orangeLight //#FFC85B
    case orangeDark //#FFAA00
    case grayLight //#828282
    case green //#03BB47
    case grayCompleteVC //#40B9EC0F
    
}

extension UIColor {
    
    static func appColor(_ name: AssetsColor, alpha: CGFloat = 1.0) -> UIColor {
        
        switch name {
        case .white:
            return UIColor(rgb: 0xFFFFFF).withAlphaComponent(alpha)
            
        case .whiteLight:
            return UIColor(rgb: 0xFFFFFF99).withAlphaComponent(0.6)
            
        case .whiteExtraLight:
            return UIColor(rgb: 0xF8FCFD).withAlphaComponent(alpha)
            
        case .black:
            return UIColor(rgb: 0x000000).withAlphaComponent(alpha)
            
        case .gray:
            return UIColor(rgb: 0x707070).withAlphaComponent(alpha)
            
        case .pink:
            return UIColor(rgb: 0xDA3467).withAlphaComponent(alpha)
            
        case .graylight:
            return UIColor(rgb: 0xAAAAAA).withAlphaComponent(alpha)
            
        case .blackLight:
            return UIColor(rgb: 0x1C1D23).withAlphaComponent(alpha)
            
        case .grayBorder:
            return UIColor(rgb: 0x70707057).withAlphaComponent(alpha)
            
        case .orangeLight:
            return UIColor(rgb: 0xFFC85B).withAlphaComponent(alpha)
            
        case .orangeDark:
            return UIColor(rgb: 0xFFAA00).withAlphaComponent(alpha)
            
        case .grayLight:
            return UIColor(rgb: 0x828282).withAlphaComponent(alpha)
            
        case .green:
            return UIColor(rgb: 0x03BB47).withAlphaComponent(alpha)
            
        case .grayCompleteVC:
            return UIColor(rgb: 0x40B9EC0F).withAlphaComponent(alpha)
            
          
            
        }
    }
}

//USE
//UIColor.appColor(.lightTeal)

extension UIColor {
    
    //How To use
    //let color = UIColor(red: 0xFF, green: 0xFF, blue: 0xFF)
    //let color2 = UIColor(rgb: 0xFFFFFF)
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

