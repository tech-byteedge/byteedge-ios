import Foundation
import SwiftUI

extension UserDefaults {
    enum Updateprofiledata : String {
        case firstName
        case lastName
        case email
        case mobileNumber
        case dob
        case token
        case image
        case nationality
        case week
        case city
        case gender
        case _id
        
          
    }
    
    
    enum Updateprofiledata_get : Int {
        case week
        case gender
    }
    
    func saveString(_ value: String?, forKey: Updateprofiledata ) {
        if let value = value {
        kUserDefaults.setValue(value, forKey: forKey.rawValue)
        }
    }
    func saveInt(_ value: Int?, forKey: Updateprofiledata ) {
        if let value = value {
            kUserDefaults.setValue(value, forKey: forKey.rawValue)
        }
    }

    func saveBool(_ value: Bool?, forKey: Updateprofiledata ) {
        kUserDefaults.setValue(value, forKey: forKey.rawValue)
        print(forKey)
    }
    
    func retriveString(_ key: Updateprofiledata) -> String {
    return kUserDefaults.value(forKey: key.rawValue) as? String ?? ""
    }
    
    
    func retriveInt(_ key: Updateprofiledata) -> Int {
        return kUserDefaults.value(forKey: key.rawValue) as? Int ?? 0
    }
    
    func retriveBool(_ key: Updateprofiledata) -> Bool {
        return kUserDefaults.value(forKey: key.rawValue) as? Bool ?? false
    }
    
        func saveToDefault(obj: UserDetail?) {
        kUserDefaults.saveString(obj?.firstName, forKey: .firstName)
        kUserDefaults.saveString(obj?.lastName, forKey: .lastName)
        kUserDefaults.saveString(obj?.email, forKey: .email)
        kUserDefaults.saveString(obj?.mobileNumber , forKey: .mobileNumber)
            if obj?.dob  != nil {
                kUserDefaults.saveString(obj?.dob ?? "", forKey: .dob)
            }
        kUserDefaults.saveString(obj?.nationality, forKey: .nationality)
        kUserDefaults.saveInt(obj?.week, forKey: .week)
        kUserDefaults.saveInt(obj?.gender, forKey: .gender)
        kUserDefaults.saveString(obj?.image, forKey: .image)
    }
    
    //MARK: LOGIN DATA SAVE
    func saveToDefaultlogin(obj: UserDetailDurinLogin?) {
        kUserDefaults.saveString(obj?.firstName, forKey: .firstName)
        kUserDefaults.saveString(obj?.lastName, forKey: .lastName)
        kUserDefaults.saveString(obj?.email, forKey: .email)
        kUserDefaults.saveString(obj?.mobileNumber , forKey: .mobileNumber)
        
        if obj?.dob  != nil {
        kUserDefaults.saveString(self.convertFromUTC(dateString: obj?.dob ?? "")  , forKey: .dob)
        }
        kUserDefaults.saveString(obj?.nationality, forKey: .nationality)
        kUserDefaults.saveString(obj?._id, forKey: ._id)
        kUserDefaults.saveString(obj?.image, forKey: .image)
//        kUserDefaults.saveString(obj?.skills, forKey: .ski)
        
    }
    
    func convertFromUTC(dateString: String) -> String {
        let dateFormattor = DateFormatter()
        dateFormattor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormattor.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormattor.date(from: dateString)
        dateFormattor.dateFormat = "yyyy-MM-dd"
       // dateFormattor.timeZone = .NSTimeZone.local
        let timeStamp = dateFormattor.string(from: date!)
        return timeStamp
    }
    
    func removeFromDefault(_ forKey: Updateprofiledata ) {
    kUserDefaults.removeObject(forKey: forKey.rawValue)
    kUserDefaults.removeObject(forKey: Updateprofiledata.firstName.rawValue)
    }
}
