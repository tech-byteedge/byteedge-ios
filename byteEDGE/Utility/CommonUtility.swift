
import UIKit
//import Alamofire
import LocalAuthentication
import Localize_Swift
import Alamofire

class CommonUtility: NSObject {
    static let instance = CommonUtility()
    var deviceid = ""
    var deviceToken = ""
    var sessionID = ""
    
    var headers: HTTPHeaders  {
        [
            "Authorization": ( UserDefaults.standard.object(forKey:"TOKEN") as? String ?? "" ),
            "Content-Type": "application/json",
            "Accept":"application/json"
        ]
    }
    
        func homevc()
        {
        guard let rootVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "SplashScreen") as? UINavigationController else {
            return
            }
            UIApplication.shared.windows.first?.rootViewController = rootVC
            rootVC.navigationBar.isHidden = true
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
    
    
    
//    func addgraph(startTime:String,endTime: String,type: String,devicetype: String,sessionId:String){
//        var param: [String: Any] = [:]
//        param["startTime"] = startTime
//        param ["endTime"] = endTime
//        param["deviceType"] = devicetype
//        param["sessionId"] = sessionId
//        
//        ServiceManager.instance.request(method: .post,
//                                        apiURL: Api.graphAdd.url,
//                                        headers:CommonUtility.instance.headers,
//                                        parameters: param,
//                                        decodable: GraphTimeModel.self
//        ) { (status, json, model, error) in
//            if model?.status == 200
//            {
//                
//             if let sessionID = model?.data?.graph.sessionId
//             {
//              UserDefaults.standard.set(sessionID, forKey: "SesionID")
//             }
//                self.sessionID = model?.data?.graph.sessionId ?? ""
////            print(model)
//            }
//            else {
//            print(model?.message ?? "")
//            }
//        }
//    }
    
    
    
    func getTime(time: String) -> String{
        let dateAsString = time
        let df = DateFormatter()
        df.dateFormat = "HH:mm:ss"
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = " hh:mma"
        let time12 = df.string(from: date)
        return time12
    }
    
    func getCurrentDatedate() -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        
        
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss a"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS+0530"
        let current_Date = Date()
        let dateText: String = dateFormatter.string(from: current_Date)
        return dateText
    }
    
    
    
    
    func gettimeConversion12(time24: String) -> String {
        let dateAsString = time24
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = " hh:mma"
        let time12 = df.string(from: date)
        return time12
    }
    
    
    func personalinfotime(time24: String) -> String {
        let dateAsString = time24
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = "yyyy- MM-dd"
        let time12 = df.string(from: date)
        return time12
    }

    
    
    //Date
    func getDate(inputDate: String) -> String {
        let dateAsString = inputDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = "d MMMM yyyy"
        //"MM-dd-yyyy"
        
        let newDate = df.string(from: date)
        return newDate
    }
    
    //    DFGHJK
    func getDateTIMEFromString(inputDate: String) -> String {
        let dateAsString = inputDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        2022-05-13T13:35:05.074Z
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = "dd MMM yyyy, hh:mma"
        //"dd MMM yyyy, hh:mma"
        // 1 "MM-dd-yyyy"
        let newDate = df.string(from: date)
        return newDate
    }
    
    func getDateTIMEFromString1(inputDate: String) -> String {
        let dateAsString = inputDate
        let df = DateFormatter()
        df.dateFormat =  "yyyy-MM-dd'T'HH:mm:ssZ"
       
//        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//        2022-05-13T13:35:05.074Z
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = "dd MMM yyyy, hh:mma"
        //"dd MMM yyyy, hh:mma"
        // 1 "MM-dd-yyyy"
        let newDate = df.string(from: date)
        return newDate
    }
    
    
    
    func getDateTIMEFrom(inputDate: String) -> String {
        let dateAsString = inputDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = "d MMM yyyy"
        // "MM-dd-yyyy"
        
        let newDate = df.string(from: date)
        return newDate
    }
    
    func setDate(inputDate: String) -> String {
        let dateAsString = inputDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = "d MMM yyyy"
        //        "MM-dd-yyyy"
        
        let newDate = df.string(from: date)
        return newDate
    }
    
    func setTime(inputDate: String) -> String {
        let dateAsString = inputDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = "hh:mm a"
        df.timeZone = NSTimeZone.local
        //        "MM-dd-yyyy"
        
        let newDate = df.string(from: date)
        return newDate
    }
    
    func gettimeNoti(inputDate: String) -> String {
        let dateAsString = inputDate
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        guard let date = df.date(from: dateAsString) else { return "" }
        df.dateFormat = " hh:mm a"
        df.timeZone = TimeZone(abbreviation: "UTC")
        //        "MM-dd-yyyy"
        
        let newDate = df.string(from: date)
        return newDate
    }
    
    
    
    
    
    func getdeviceID() -> String
    {
    return UIDevice.current.identifierForVendor!.uuidString
        
    }
    
    func setHomeWithoutFaceID()
    {
        guard let rootVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "tabbarvc") as? UINavigationController else {
            return
        }
        UIApplication.shared.windows.first?.rootViewController = rootVC
        rootVC.navigationBar.isHidden = true
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "need_biometric_login".localized()//"Need your Biometric to Login"
        var authorizationError: NSError?
        let reason = "authentication_access".localized()//"Authentication required to access the secure data"
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authorizationError) {
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
        if success {
                    DispatchQueue.main.async() {
                        guard let rootVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "tabbarvc") as? UINavigationController else {
                            return
                        }
                        UIApplication.shared.windows.first?.rootViewController = rootVC
                        rootVC.navigationBar.isHidden = true
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                    }
                    
                } else {
                    // Failed to authenticate
                    guard let error = evaluateError else {
                    return
                    }
                    print(error)
                    DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.rootViewController?.createAlert(title: error.localizedDescription , message: "Message")
                    self.authenticationWithTouchID()
                    }
                }
            }
        } else {
            guard let error = authorizationError else {
                return
            }
            print(error)
            DispatchQueue.main.async {
                UIApplication.shared.keyWindow?.rootViewController?.createAlert(title: error.localizedDescription , message: "Message")
                self.authenticationWithTouchID()
            }
        }
    }
    
    func setLogout() {
        let alertController = UIAlertController(title: AppName, message:"Are you sure you want to Logout?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let rootVC = storyboard.instantiateViewController(withIdentifier: "SplashScreen") as? SplashScreen else {
                print("ViewController not found")
                return
            }
            let rootNC = UINavigationController(rootViewController: rootVC)
            UIApplication.shared.windows.first?.rootViewController = rootNC
            UIApplication.shared.windows.first?.makeKeyAndVisible()
            rootNC.navigationBar.isHidden = true
            UserDefaults.standard.removeObject(forKey: "loginkey")
            kUserDefaults.set(false, forKey: "isLoggedIn")
            UserDefaults.standard.set(false, forKey: "wasAlreadyShown")
        }
        
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        UIApplication.shared.windows.last?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
}

typealias alertActionHandler = ((UIAlertAction) -> ())?
typealias alertTextFieldHandler = ((UITextField) -> ())
typealias apiErrorHandler = (( _ index: Int,  _ btnTitle: String) -> ())
// MARK: - Extension of UIViewController For AlertView with Different Numbers of Buttons
extension UIViewController {
    
    /// This Method is used to show AlertView with one Button.
    ///
    /// - Parameters:
    ///   - alertTitle: A String value that indicates the title of AlertView , it is Optional so you can pass nil if you don't want Alert Title.
    ///   - alertMessage: A String value that indicates the title of AlertView , it is Optional so you can pass nil if you don't want alert message.
    ///   - btnOneTitle: A String value - Title of button.
    ///   - btnOneTapped: Button Tapped Handler (Optional - you can pass nil if you don't want any action).
    func presentAlertViewWithOneButton(alertTitle:String? , alertMessage:String? , btnOneTitle:String , btnOneTapped:alertActionHandler)
    {
        
        let alertController = UIAlertController(title: alertTitle ?? "", message: alertMessage ?? "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: btnOneTitle, style: .default, handler: btnOneTapped))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    /// This Method is used to show AlertView with two Buttons.
    ///
    /// - Parameters:
    ///   - alertTitle: A String value that indicates the title of AlertView , it is Optional so you can pass nil if you don't want Alert Title.
    ///   - alertMessage: A String value that indicates the title of AlertView , it is Optional so you can pass nil if you don't want alert message.
    ///   - btnOneTitle: A String value - Title of button one.
    ///   - btnOneTapped: Button One Tapped Handler (Optional - you can pass nil if you don't want any action).
    ///   - btnTwoTitle: A String value - Title of button two.
    ///   - btnTwoTapped: Button Two Tapped Handler (Optional - you can pass nil if you don't want any action).
    ///   
    func presentAlertViewWithTwoButtons(alertTitle:String? , alertMessage:String? , btnOneTitle:String , btnOneTapped:alertActionHandler , btnTwoTitle:String , btnTwoTapped:alertActionHandler) {
        
        let alertController = UIAlertController(title: alertTitle ?? "", message: alertMessage ?? "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: btnOneTitle, style: .default, handler: btnOneTapped))
        
        alertController.addAction(UIAlertAction(title: btnTwoTitle, style: .default, handler: btnTwoTapped))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentAlertWithAPIErrorTitle(title: String, message: String, handler:@escaping apiErrorHandler) {
        self.presentAlertViewWithTwoButtons(alertTitle: title, alertMessage: message, btnOneTitle: "RETRY", btnOneTapped: { (retryAction) in
            handler(0, "RETRY")
        }, btnTwoTitle: "CANCEL") { (cancelAction) in
            handler(1, "CANCEL")
        }
    }
    
    /// This Method is used to show AlertView with three Buttons.
    ///
    /// - Parameters:
    ///   - alertTitle: A String value that indicates the title of AlertView , it is Optional so you can pass nil if you don't want Alert Title.
    ///   - alertMessage: A String value that indicates the title of AlertView , it is Optional so you can pass nil if you don't want alert message.
    ///   - btnOneTitle: A String value - Title of button one.
    ///   - btnOneTapped: Button One Tapped Handler (Optional - you can pass nil if you don't want any action).
    ///   - btnTwoTitle: A String value - Title of button two.
    ///   - btnTwoTapped: Button Two Tapped Handler (Optional - you can pass nil if you don't want any action).
    ///   - btnThreeTitle: A String value - Title of button three.
    ///   - btnThreeTapped: Button Three Tapped Handler (Optional - you can pass nil if you don't want any action).
    func presentAlertViewWithThreeButtons(alertTitle:String? , alertMessage:String? , btnOneTitle:String , btnOneTapped:alertActionHandler , btnTwoTitle:String , btnTwoTapped:alertActionHandler , btnThreeTitle:String , btnThreeTapped:alertActionHandler) {
        
        let alertController = UIAlertController(title: alertTitle ?? "", message: alertMessage ?? "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: btnOneTitle, style: .default, handler: btnOneTapped))
        
        alertController.addAction(UIAlertAction(title: btnTwoTitle, style: .default, handler: btnTwoTapped))
        
        alertController.addAction(UIAlertAction(title: btnThreeTitle, style: .default, handler: btnThreeTapped))
        self.present(alertController, animated: true, completion: nil)
    }
    //MARK:- Date and Time Conversion
    
}
