
import Foundation
import UIKit
extension Float {
    
    var toInt:Int? {
        return Int(self)
    }
    
    var toDouble:Double? {
        return Double(self)
    }
    
    var toString:String {
        return "\(self)"
    }
}

extension UIFont {
       public enum product: String {
        case regular = "Regular"
        case italic = "Italic"
        case bold = "Bold"
        case boldItalic = "BoldItalic"
        case black = "Black"
        case blackItalic = "BlackItalic"
        case light = "Light"
        case lightItalic = "LightItalic"
        case medium = "Medium"
        case MediumItalic =  "Medium Italic"
        case mediumItalic = "MediumItalic"
        case semibold = "semibold"
        case thin = "thin"
        case ExtraBold = "ExtraBold"
    }
    
    public enum productSansSize: CGFloat {
        case five = 5
        case nine = 9
        case oneZero = 10
        case oneOne = 11
        case oneTwo = 12
        case oneThree = 13
        case oneFour = 14
        case oneFive = 15
        case oneSix = 16
        case oneSeven = 17
        case oneEight = 18
        case oneNine = 19
        case twoTwo = 22
        case twoThree = 23
        case twoFour = 24
        case twoFive = 25
        case twoOne = 21
        case threeTwo = 32
        case fourFive = 45
        case fiveNine = 59
    }
  

    static func axiforma(_ type: product = .regular, size: productSansSize ) -> UIFont {
        return UIFont(
            name: "product-\(type.rawValue)",
            size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue
        )
    }
    
    static func ProductSans(_ type: product = .regular, size: productSansSize) -> UIFont {
        return UIFont(
            name: "ProductSans-\(type.rawValue)",
            size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue
            )
    }
}
extension UILabel {
    
    public func setAppFontColor(_ color:  UIColor, font: UIFont) {
        self.textColor = color//UIColor.appColor(.almostBlack)
        self.font = font//UIFont.ProductSans(.medium, size: 17)
    }
}


extension UITextField {
    
      public func setAppFontColor(_ color:  UIColor, font: UIFont) {
        self.textColor = color
        self.font = font
    }
}

extension UIButton {
    
    public func setAppFontColor(_ color:  UIColor, font: UIFont) {
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font =  font
    }
    
    
}
