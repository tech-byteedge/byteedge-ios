
//  Constants.swift
//  Created by apple on 25/11/20.
//  Copyright © 2020 apple. All rights reserved.

import Foundation
import UIKit
//let BASEURL = "http://3.12.253.202/api/"
let kCameraPermissionMsg = "Please accees camera permission to select image"
let kGalleryPersissionMSg = "Please accees Gallery permission to select image"
let kAppName = "Byte Edge"
let kIconprofileperson = "profile_selected"

let sign = "https://web.blossombookings.com.au/vendor/register"
let AppName = "byteEDGE "
let appDelegate  = UIApplication.shared.delegate as! AppDelegate
let  kUserDefaults = UserDefaults.standard
let kAlertNoNetworkMessage = "A network connection is required. Please verify your network settings & try again."
let kAlertEmail = "Please enter your Email address"
let kAlertCategory_List = "Please enter your Category"
let kAlertSubCategory_List = "Please enter your Select_Service"

let kAlertValidEmail = "Please enter valid Email address"
let kAlertFullName = "Please enter your full name"
let kAlertFirstName = "Please enter your first name"
let kAlertLastName = "Please enter your last name"
let kAlertName = "Please enter your first name"
let kAlertMobile = "Please enter your mobile number"
let kAlertMobileValid = "Pls enter valid mobile number"
let kAlertMobilecharacter = "Please enter Maximum 10 number mobile number"

let kAlertPin = "Please enter your password"
let kAlertConfirmPin = "Please enter your confirm pin"
let kAlertPinNotMatch = "Pin and confirm pin does not match"
let kAlertCorrectOTP = "please enter correct otp"
let kAlertImage = "Please select image"
let kAlertOldPin = "Please enter old pin"
let kAlertNewPin = "Please enter new pin"
let kAlertConfirmNewPin = "New pin and confirm pin does not match"
let kAlertDate = "Please select Date"
let kAlertSpouseDOB = "Please select date of birth"
let kAlertReligion = "Please select religion"
let kAlertSpouseReligion = "Please select spouse religion"
let kAlertSpouseName = "Please enter spouse name"
let kAlertCanton = "Please select canton"
let kAlertCity = "Please select city"
let kAlertPostal = "Please enter postal code"
let kAlertYear = "Please select financial year"
let kAlertFurtherPayment = "Please approve tax prepared for further payment process"
let kAlertIncomeWithYou = "Do you have Income documents with you?"
let kAlertWealthWithYou = "Do you have Wealth documents with you?"
let kAlertDebtWithYou = "Do you have Debt documents with you?"
let kAlertDeductionWithYou = "Do you have Deductions documents with you?"
let kAlertPreviousWithYou = "Do you have Previous year tax filing documents with you?"
let kAlertAllQuestions = "Make sure you have answered all questions and have uploaded related documents."
let kAlertemailnotverified = "You have not verified your email yet. Please verify your email by clicking the link sent to the registered email for further process of approval."
let kAlertpending = "Profile approval is pending from Admin side, please wait for while your profile gets approved. It will take a maximum of 4 hrs to be approved."
let kAlertprofileDiasable = "You have not verified your email yet. Please verify your email by clicking the link sent to the registered email for further process of approval."

let kAlertBusinessName = "Please enter business name"
let kAlertBusinessEmail = "Please enter business email"
let kAlertValidBusinessMobile = "Please enter valid business email"
let kAlertBusinessMobile = "Please enter your mobile number"
let kAlertDateRegistration = "Please select date of registration"
let kAlertBusinessType = "Please enter business type"
let kAlertRegistrationNumber = "Please enter registration number"
let kPurchaseHistory = "Purchase History"
let kOrderId = "ORDER ID"
let kOrderDate = "ORDER DATE"
let kDownloadInvoice = "Download Invoice"

let kAgo = "ago"
let kTime = "- day ago"



//MARK: - CreatePostVc

let kPlzAddSomthng = "Please add something to post"
