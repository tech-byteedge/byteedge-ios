//
//  JWPlayerViewController.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 04/04/22.
//

import UIKit
import  JWPlayerKit

class JWPlayerViewController:UIViewController,
                              RelatedEventDelegate,
                              AVPictureInPictureControllerDelegate,
                              InternalEventHandlerDelegate,
                              JWPlayerViewDelegate,
                              JWPlayerDelegate,
                              JWPlayerStateDelegate,
                              JWAVDelegate,
                              JWAdDelegate,
                              JWAirPlayDelegate,
                              JWAccessLogMetadataDelegate,
                              JWDateRangeMetadataDelegate,
                              JWID3MetadataDelegate,
                              JWExternalMetadataDelegate,
                              JWProgramDateTimeMetadataDelegate,
                              JWMediaMetadataDelegate,
                              JWCastDelegate,
                              JWTimeEventListener,
                              UIPopoverPresentationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
