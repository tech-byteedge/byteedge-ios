//
//import Foundation
//import Alamofire
//
//class ApiManager: NSObject
//{
//    var reachability : NetworkReachabilityManager?
//    static let instance = ApiManager()
//    var alertShowing = false
//    
//    enum Method: String
//    {
//        case GET_REQUEST = "GET"
//        case POST_REQUEST = "POST"
//        case PUT_REQUEST = "PUT"
//        case DELETE_REQUEST = "DELETE"
//    }
//    
//    func request(method: HTTPMethod, URLString: String, parameters: [String : AnyObject]?, encoding: ParameterEncoding, headers:  HTTPHeaders? ,completionHandler: @escaping (_ success:Bool?,[String : AnyObject]?, NSError?) -> ())
//    {
//        var status = Int()
//        if ReachabilityNetwork.isConnectedToNetwork() == true
//        {
//            let kApiURL = BASEURL + URLString
//            print(kApiURL)
//            
//            AF.request(kApiURL, method: method, parameters: parameters, encoding: encoding, headers: headers).response(completionHandler: { (response) in
//                do{
//                    if (response.error == nil)
//                    {
//                        status = response.response!.statusCode
//                        if status == 412
//                        {
//                            print(status)
//                        }
//                        
//                        print(response.data ?? "")
//                        let jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String : AnyObject]
//                        completionHandler(true, jsonResult, nil)
//                    }
//                    else
//                    {
//                        print(response.error as NSError? as Any)
//                        completionHandler(false, nil, response.error as NSError?)
//                    }
//                }
//                catch let error as NSError
//                {
//                    print(error)
//                    completionHandler(false, nil, nil)
//                }
//            })
//        }
//        else
//        {
//            if(!self.alertShowing)
//            {
//                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
//                    self.alertShowing = true
//                    let alert = UIAlertController(title: "Network Problem", message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertAction.Style.default, handler: { (act) in
//                        self.alertShowing = false
//                        completionHandler(false ,nil, NSError(domain:"somedomain", code:9001))
//                    }))
//                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (act) in
//                        self.alertShowing = false
//                        completionHandler(false ,nil, NSError(domain:"somedomain", code:9002))
//                    }))
//                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
//                }
//            }
//        }
//    }
//    
//    func requests(method: HTTPMethod, URLString: String, parameters: [String : AnyObject]?, encoding: ParameterEncoding, headers:  HTTPHeaders? ,completionHandler: @escaping (_ success:Bool?,[String : AnyObject]?, NSError?) -> ())
//    {
//        if ReachabilityNetwork.isConnectedToNetwork() == true
//        {
//            let kApiURL = "http://3.12.253.202:5008/api" + URLString
//            print(kApiURL)
//            AF.request(kApiURL, method: method, parameters: parameters, encoding: encoding, headers: headers).response(completionHandler: { (response) in
//                do{
//                    if (response.error == nil)
//                    {
//                        print(response.data ?? "")
//                        let jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: []) as! [String : AnyObject]
//                        completionHandler(true, jsonResult, nil)
//                    }
//                    else
//                    {
//                        print(response.error as NSError? as Any)
//                        completionHandler(false, nil, response.error as NSError?)
//                    }
//                }
//                catch let error as NSError
//                {
//                    print(error)
//                    completionHandler(false, nil, nil)
//                }
//            })
//        }
//        else
//        {
//            if(!self.alertShowing)
//            {
//                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
//                    self.alertShowing = true
//                    let alert = UIAlertController(title:"Network Problem", message: kAlertNoNetworkMessage, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title:"Retry", style: UIAlertAction.Style.default, handler: { (act) in
//                        self.alertShowing = false
//                        completionHandler(false ,nil, NSError(domain:"somedomain", code:9001))
//                    }))
//                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (act) in
//                        self.alertShowing = false
//                        completionHandler(false ,nil, NSError(domain:"somedomain", code:9002))
//                    }))
//                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
//                }
//            }
//        }
//    }
//}
//
//extension UIApplication
//{
//    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController?
//    {
//        if let nav = base as? UINavigationController
//        {
//            return topViewController(base: nav.visibleViewController)
//        }
//        if let tab = base as? UITabBarController
//        {
//            if let selected = tab.selectedViewController
//            {
//                return topViewController(base: selected)
//            }
//        }
//        if let presented = base?.presentedViewController
//        {
//            return topViewController(base: presented)
//        }
//        return base
//    }
//}
//
