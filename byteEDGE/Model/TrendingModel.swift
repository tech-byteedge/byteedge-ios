//
//  TrendingModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 23/05/22.
//

import Foundation

struct TrendingModel:Codable {
    let Code, status: Int?
    let message: String?
    let data: DataTrending?
}

// MARK: - DataClass
struct DataTrending:Codable {
    let trending: [Trending]
}

// MARK: - Trending
struct Trending:Codable {
    let _id: String?
    let courseName:String?
    let courseType: String?
    let playlistId: String?
    let previewId: String?
    let status: Bool?
    let createdAt: String?
//    let mediaId: [MediaIdArray]
    let rating: Int?
    let thumbnails: String
    let Description: String?
    let isQuizExist: Bool?
    let courseUuid: String?
    
}

//struct MediaIdArray:Codable {
//let mediaId: String?
//}
