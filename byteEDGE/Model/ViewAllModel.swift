//
//  ViewAllModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 02/02/22.

import Foundation

// MARK: - Welcome
struct ViewAll: Codable {
    let Code, status: Int?
    let message: String
    var data: DataofViewAll?
    
}

// MARK: - DataofViewAll
struct DataofViewAll: Codable {
    let count: Int?
    var  myGroup: [MyGroupofViewAll]?
    var mayLikeGroup:[MaylikeGroup]?
    var popularGroup:[Populgroup]?
    
    
}

// MARK: - MyGroup
struct MyGroupofViewAll: Codable {
    let _id, name, createdAt: String?
    let image: String?
    let isJoin: Bool?
    let memberDetails: [MemeberDetaisOfViewALL]?
    
}

// MARK: - MyGroup
struct MaylikeGroup:Codable {
    var _id, name, createdAt: String?
    var image: String?
    var isJoin:Bool?
    var  joinStatus : String?
    let memberDetails: [MemeberDetaisOfViewALL]?
}

// MARK: - MyGroup
struct Populgroup: Codable {
    var  _id, name, createdAt: String?
    var image: String?
    var isJoin:Bool?
    var  joinStatus : String?
    let memberDetails: [MemeberDetaisOfViewALL]?
    
}

struct MemeberDetaisOfViewALL: Codable {
        let memberId: String?
        let memberImage: String?
       
    }






