//
//  RecommnndedModel.swift
//  byteEDGE
//  Created by Pawan Kumar on 23/05/22.


import Foundation
// MARK: - Welcome
struct RecommnndedModel:Codable{
    let Code, status: Int
    let message: String
    let data: DataRecommended?
}

// MARK: - DataClass
struct DataRecommended:Codable {
 let recommend, newCourseCount: [NewCourseCount]
}

// MARK: - NewCourseCount
struct NewCourseCount:Codable {
    let _id: String?
    let categoryID: String?
    let courseName: String
    let categoryId:String?
    let Description: String?
    let courseType: String?
    let playlistId, previewId: String?
    let status: Bool
    let categoryName: String?
    let createdAt: String?
    let rating: Int
    let thumbnails: String?
    let isQuizExist: Bool?
    let courseUuid: String?
}

