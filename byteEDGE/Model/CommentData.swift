//
//  CommentData.swift
//  byteEDGE
//
//  Created by gaurav on 27/01/22.
//

import Foundation

struct CommentModel: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : CommentModelData?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
       case Code, data
    }
}

struct CommentModelData: Codable {
    let commentList : [CommentListData]?
    enum CodingKeys: String, CodingKey {
        case commentList
    }
}

struct CommentListData: Codable {
    let id, postId, comment, userId, createdAt : String?
    let commentUser : CommentUserData?
    let reply : [ReplyData]?
    let totalReply, totalLikeOnComment : Int?
    let isLiked : Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
       case  postId, comment, userId, createdAt, commentUser, reply, totalReply, isLiked, totalLikeOnComment
    }
}

struct CommentUserData: Codable {
    let id, name, image : String?
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, image
    }
}

struct ReplyData: Codable {
    let id, commentId, reply, userId, createdAt : String?
    let replyUser : CommentUserData?
    let isLiked : Bool?
    let totalLikeOnReply : Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
       case  commentId, reply, userId, createdAt, replyUser, isLiked, totalLikeOnReply
    }
}

