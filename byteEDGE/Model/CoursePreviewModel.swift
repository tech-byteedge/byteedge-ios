//
//  CoursePreviewModel.swift
//  byteEDGE
//  Created by gaurav on 31/01/22.


import Foundation

struct CoursePreviewDetailModel: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : CoursePreviewDetailData?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
       case Code, data
    }
}

struct CoursePreviewDetailData: Codable {
    let coursePreview : CoursePreviewData?
   
    enum CodingKeys: String, CodingKey {
        case coursePreview
    }
}

struct CoursePreviewData : Codable {
    var previewId: String?
    var _id : String?
    let  courseName : String?
    let  Description : String?
    let  courseType : String?
    let  playlistId : String?
    let rating: Int?
    let  thumbnails : String?
}



