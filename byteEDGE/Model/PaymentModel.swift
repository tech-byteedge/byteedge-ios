//
//  PaymentModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 25/02/22.
//

import Foundation
import  UIKit

// MARK: - payment
struct PaymentMaodel: Codable
{
    let Code: Int?
    let status: Int?
    let message: String?
    let data:OrderId?
}


struct OrderId : Codable
{
let orderId:PaymentList?
}

struct PaymentList: Codable
{
    let id: String?
    let entity: String?
    let amount: Int?
    let amount_paid: Int?
    let currency: String
    let receipt: String?
    let status: String?
    let attempts: Int?
    let created_at: Int?
    
}
