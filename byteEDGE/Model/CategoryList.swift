//
//  CategoryList.swift
//  byteEDGE
//
//  Created by gaurav on 31/01/22.
//

import Foundation

struct CategoryListModel: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : CategoryListData?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
       case Code, data
    }
}

struct CategoryListData: Codable {
    let count : Int?
    let courseList : [Courselistcategory]?
    
    enum CodingKeys: String, CodingKey {
        case count, courseList
    }
}

    struct Courselistcategory:Codable {
    let _id: String?
    let categoryId:  String?
    let courseName: String?
    let Description:String?
    let courseType: String?
    let playlistId:  String?
    let previewId: String?
    let thumbnails: String?
   }


