//  GroupModel.swift
//  byteEDGE
//  Created by Pawan Kumar on 26/01/22.

import Foundation

struct GroupList: Codable {
    let Code: Int?
    let status: Int?
    let message: String?
    var data: DataofGroup?
}

// MARK: - DataClass
struct DataofGroup: Codable {
    var myGroup: [MyGroup]?
    var mayLikeGroup:[MayLikeGroupMemberDetail]?
    var popularGroup: [PoplarGroup]?
}

// MARK: - MyGroup
struct MyGroup: Codable {
    let _id:String
    let name: String?
    let createdAt: String?
    let memberDetails: [MemberDetail]?
    let totalMember: Int?
    let image: String?
}

// MARK: - MayLikeGroupMemberDetail
struct MayLikeGroupMemberDetail: Codable {
    //    let memberId: String?
    let _id: String?
    let name: String?
    let createdAt :String?
    let image :String?
    let totalMember: Int?
    let isJoin: Bool?
    let joinStatus: String?
    let memberDetails: [MemberDetail]?
}

// MARK: - PopularGroup
struct PoplarGroup: Codable {
    let _id: String?
    let name: String?
    let createdAt: String?
    let memberDetails: [MemberDetail]?
    let totalMember: Int?
    let isJoin: Bool?
    let joinStatus: String?
    let image: String?
  
}


struct MemberDetail: Codable {
    let memberId: String?
    let memberImage: String?
}


//MARK: groupDetails
struct GroupDetails:Codable
{

    
}











