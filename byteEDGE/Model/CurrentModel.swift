//
//  CurrentModel.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 05/08/22.
//

import Foundation
// MARK: - Welcome
struct CurrentModel: Codable {
    let Code, status: Int
    let message: String
    let data: DataCurrent?
}

// MARK: - DataClass
struct DataCurrent: Codable {
    let purchaseList: PurchaseList?
}

// MARK: - PurchaseList
struct PurchaseList: Codable {
    let _id:String?
    let  userId:String?
    let paymentType: String?
    let amount: Int
    let currency, orderID: String?
    let subscriptionType: Int
    let paymentId, transactionId, subsriptionExpired: String
    let status: Bool
    let createdAt, updatedAt: String
}
