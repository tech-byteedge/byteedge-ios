
//  File.swift
//  byteEDGE
//  Created by Pawan Kumar on 21/01/22.

import Foundation
struct SearchCategoryList: Codable
{
var Code : Int?
    var status: Int?
    var message:String?
    var data: DataOfcategory?
}
struct DataOfcategory: Codable
{
    var browseCategory: [BrowseCategory]?
}
struct BrowseCategory: Codable
{
    var categoryName: String?
    var _id: String?
}





// MARK: SEARCH  BY USER
struct SearchList: Codable
{
    var Code : Int?
    var status: Int?
    var message:String?
    var data: DataOfSearch?
}

struct DataOfSearch: Codable
{
    var searchData: [SearchData]?
}

struct SearchData: Codable
{
    var _id: String?
    var courseName: String?
    var hashTag: [Hashtag]?
    var isQuizExist: Bool?
    var playlistId:  String?
    
    
}

struct Hashtag: Codable
{
    var hashTag: String?
    var _id: String?
}



// MARK: SEARCH  TOP
struct TopSearchList: Codable
{
    var Code : Int?
    var status: Int?
    var message:String?
    var data: DataOftopSearch?
}



struct DataOftopSearch: Codable
{
    var topSearch: [SearchTop]?
}

struct SearchTop: Codable
{
    var _id: String?
    var courseName: String?
    var categoryId:String?
    var Description:  String?
    var thumbnails: String?
    var courseType: String?
    var playlistId:  String?
    var previewId:  String?
    var isQuizExist: Bool?
    var courseUuid: String?
}





