//
//  NotificationModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 23/02/22.
import Foundation

// MARK: - Welcome
struct NotificationModel: Codable {
    let code, status: Int?
    let message: String?
    var data: NotificationListAarray?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case status, message, data
    }
}

// MARK: - DataClass
struct NotificationListAarray: Codable {
    var  notificationList: [NotificationList]?
}

// MARK: - NotificationList
struct NotificationList: Codable {
    let id: String?
    var notification: [NotifyData]?
    let count: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case notification, count
    }
}
// MARK: - Notification
struct NotifyData: Codable {
    var _id,image, title, description, createdAt: String?
    let date: String?
}
