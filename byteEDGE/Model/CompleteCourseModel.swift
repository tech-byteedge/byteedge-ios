//
//  CompleteCourseModel.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 23/06/22.
//

import Foundation

struct CompleteCourseModel: Codable
{
    let Code: Int?
    let status: Int?
    let message: String?
    let data: CompletData?
    
}
struct CompletData: Codable
{
    let complete: [Complete]
}

struct Complete: Codable
{
        let _id: String?
        let count:Int?
        let percentage: Int?
        var openQuiz: Bool?
        let courseDetail: CourseDetailCompleted?
}

  struct CourseDetailCompleted: Codable
 {
    let _id: String?
    let courseName:String?
    let Description: String?
    let thumbnails: String?
    var playlistId: String?
    var mediaId: [MediaIdComplted]
    var previewId: String?
    var createdAt: String?
    var courseType: String?
    var courseUuid:String?
      
  }

struct MediaIdComplted: Codable
{

}
