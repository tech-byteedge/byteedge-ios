//
//  ParticularCoursemodel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 01/06/22.
//

import Foundation

struct ParticularCoursemodel: Codable
{
    var Code: Int?
    var status:Int?
    var message: String?
    var data: Paticular_Data?
}

struct Paticular_Data:Codable {
    var courseDetail:CourseDetail
}

struct CourseDetail:Codable {
    var _id: String?
    var courseName:String?
    var Description: String?
     var courseType: String?
    var playlistId: String?
    var previewId: String?
    var createdAt: String?
    var sQuizExist: Bool?
    var rating:Int?
    var thumbnails: String?
    var openQuiz:Bool?
}


