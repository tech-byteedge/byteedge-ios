//
//  GetUserData.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 05/08/22.
//

import Foundation
struct GetUserData: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : DataGetReport?
}
struct DataGetReport:Codable {
    var result: [ResultGet]?
}

struct ResultGet: Codable {
    let name: String?
    let fname: String?
    let email_ : String?
    let certificate_name : String?
    let issued_status: String?
    let issue_date: String?
    var request_date : String?
    var target_hash: String?
    var certificate_path : String?
    var entity_uuid: String?
    var  public_verifier_link: String?
}








