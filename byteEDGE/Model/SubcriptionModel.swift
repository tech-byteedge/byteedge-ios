//
//  SubcriptionModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 02/03/22.
//

import Foundation

// MARK: - Welcome
struct SubcriptionModel: Codable {
    let code, status: Int
    let message: String
    let data: DataOfSubcription
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case status, message, data
    }
}

// MARK: - DataClass
struct DataOfSubcription: Codable {
    let count: Int
    let subscriptionList: [SubscriptionList]
}

// MARK: - SubscriptionList
struct SubscriptionList: Codable {
    let id: String
    let subscriptionType: Int
    let price: String
    let status: Bool
    let createdAt, updatedAt: String

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case subscriptionType, price, status, createdAt, updatedAt
    }
}

