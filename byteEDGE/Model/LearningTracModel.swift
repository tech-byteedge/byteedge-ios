//
//  LearningTracModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 13/04/22.
//

import Foundation

struct LearningTracModel: Codable
{
    let Code: Int?
    let status: Int?
    let message: String?
    let data: LearningTrack?
}

struct LearningTrack: Codable
{
   // let count : Int?
    let trackedCourse: [TrackCourse?]
}

struct TrackCourse: Codable
{
        let _id: String?
        let count:Int?
        let percentage: Int?
        let courseDetail: CourseDetailTrack?
}

struct CourseDetailTrack: Codable
{
    let _id: String?
    let courseName:String?
    let Description: String?
    let thumbnails: String?
    var playlistId: String?
    var mediaId: [MediaId]
    var previewId: String?
    var createdAt: String?
    var courseType: String?
    let courseUuid: String?
}

struct MediaId: Codable
{
    
}




