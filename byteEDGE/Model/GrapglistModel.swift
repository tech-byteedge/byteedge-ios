//
//  GrapglistModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 14/04/22.
//

import Foundation

struct GrapglistModel: Codable
{
    var Code: Int?
    var status: Int?
    var message: String?
    var  data: Graphlist?
}

struct Graphlist: Codable
{
   var  userData: [UserData?]
}

struct UserData: Codable
{
 var _id: GraphID?
 var duration : Int
}

struct GraphID: Codable
{
 let month: Int?
 let day: Int?
 let year: Int?
}


struct GrapglistpercentageModel: Codable
{
    var Code: Int?
    var status: Int?
    var message: String?
    var  data: Graphlistpercen?
}
struct Graphlistpercen:Codable{
    var totalCount : Int?
    var totalPercentage : Int?
}


