//
//  FeedDataModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 30/05/22.
import Foundation

struct FeedDataModel:Codable {
    var  Code: Int?
    var status: Int?
    var  message: String?
    var  data: DataFeed?
}

// MARK: - DataClass
struct DataFeed: Codable {
    let count: Int?
    var feedList: [FeedNewModel]
//    var feedDetail:FeedNewModel?
}

// MARK: - FeedList
struct FeedNewModel: Codable {
    var  _id:String?
    var groupId: String?
    var feedType: Int?
    var description: String?
    let status: Bool?
    let createdAt: String?
    let userDetail: FeedUser?
    var ownLikes: Bool?
    var likes: Int?
    var comments: Int?
    var image: [String]?
    var thumbnails: String?
    var  video: String?
}

struct FeedUser: Codable {
     var  _id: String?
    let name: String?
    let image: String?
}
