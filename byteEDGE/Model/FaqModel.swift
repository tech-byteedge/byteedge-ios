
//  FaqModel.swift
//  byteEDGE
//  Created by Pawan Kumar on 11/02/22.

import Foundation

struct FaqModelData: Codable {
    let Code, status: Int?
    let message: String?
    let data: FaqData?
    
}

// MARK: - DataClass
struct FaqData: Codable {
    let count: Int?
    let faqList: [FAQList]?
}

// MARK: - FAQList
struct FAQList: Codable {
    let _id, question, answer: String?
    let isDeleted, status: Bool?
    let createdAt, updatedAt: String?
}
