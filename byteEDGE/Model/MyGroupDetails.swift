//
//  MyGroupDetails.swift
//  byteEDGE
//  Created by Pawan Kumar on 31/01/22.

//import Foundation
//struct MyGroupDetail: Codable
//{
//let Code: Int?
//let status: Int?
//let message: String?
//let data: DataMyGroupDetails?
//
//}
//
//struct DataMyGroupDetails: Codable
//{
//let detail: Details?
//}
//
//
//struct Details: Codable // Details
//{
//let  _id: String?
//let groupDetail:GropDetailsData?
//let myGroupMembers: [MyGroupMembers]?
//let  pendingRequest:[pendingRequest]
//let totalMyGroupMembers: Int?
//}
//
//
//struct GropDetailsData: Codable
//{
//   let name: String?
//   let about: String?
//   let joinReason: String?
//   let createdAt: String?
//   let image: String?
//   let  _id: String?
//}
//
//struct MyGroupMembers: Codable
//{
//   let _id: String?
//   let memberDetail:MemberDetailofGroupDetail?
//}
//
//struct MemberDetailofGroupDetail: Codable
//{
//   let _id: String?
//   let image: String?
//   let  name: String?
//}
//
//struct pendingRequest: Codable
//{
//}







import Foundation
struct MyGroupDetail: Codable
{
    var Code: Int?
    var status: Int?
    var message: String?
    var data: DataMyGroupDetails?
}

struct DataMyGroupDetails: Codable
{
var detail: Details?
}


struct Details: Codable // Details
{
var  _id: String?
var groupDetail:GropDetailsData?
var myGroupMembers: [MyGroupMembers]?
var pendingRequest:[PendingRequest]?
let totalMyGroupMembers: Int?
let totalPendingRequest: Int?
    
}

struct GropDetailsData: Codable
{
    var name: String?
    var about: String?
    var joinReason: String?
    var createdAt: String?
    var image: String?
    var  _id: String?
}

struct MyGroupMembers: Codable
{
    var _id: String?
    var memberDetail:MemberDetailofGroupDetail?
}

struct PendingRequest:Codable
{
    var _id: String?
    var memberDetail:MemberDetailofGroupDetail?
}

struct MemberDetailofGroupDetail: Codable
{
    var _id: String?
    var image: String?
    var  name: String?
    
}















