//
//  Feell_ListModel.swift
//  byteEDGE
//  Created by Pawan Kumar on 25/01/22.
import Foundation
import AVFoundation

struct FeelList: Codable
{
var code,status: Int?
var message: String?
var  data: FeedData?
}

struct FeedData: Codable
{
var  cout: Int?
var  feedList: [FeedListinData]?
}

struct FeedListinData: Codable
{
var _id:String?
var feedType: Int?
var description: String?
var status: Bool?
var createdAt: String?
var userDetail :UserDetailOfFeedlist?
var ownLikes: Bool?
var likes: Int?
var comments: Int?
var thumbnails: String?
var image: [String]?
var video: String?
}


// MARK: Detail of User
struct UserDetailOfFeedlist: Codable
{
    var _id: String?
    var name : String?
    var image:String?
}

