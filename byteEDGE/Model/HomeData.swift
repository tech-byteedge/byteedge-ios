//
//  HomeData.swift
//  byteEDGE
//
//  Created  by gaurav on 24/01/22.

import Foundation

struct CommonResponseModel: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
       case Code
    }
}


struct GraphTimeModel: Codable {
    let Code : Int?
    let status: Int?
    let message: String?
    var data: GraphTimeData?
}

struct GraphTimeData: Codable
{
 var  graph: GraphTimeListing
}

struct GraphTimeListing: Codable
{
 var sessionId:String?
}


struct Home: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : HomeData?
}

struct HomeData: Codable {
    let count: Int?
    var dashBoardList : [DashBoardListData]?
}

struct DashBoardListData: Codable {
    let _id: String?
    let video: String?
    let thumbnails : String?
    var likes : Int?
    let teasureName : String?
    let description: String?
    let courseId: String?
    var isLiked : Bool?
    var isBookMark: Bool?
    var readMore : Bool?
    var  courseData: courseData?
}

   struct courseData:Codable {
    var _id:  String?
    var courseName:  String?
    var  Description: String?
    var courseType: String?
    var playlistId: String?
    var previewId:  String?
    var thumbnails: String?
    var isQuizExist: Bool?
   }









