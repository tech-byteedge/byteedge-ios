
import Foundation
import UIKit

struct UpdateProfile:Codable {
    let Code: Int?
    let status : Int?
    let message: String?
    let data:DataofUpdateprofile?
}

struct DataofUpdateprofile:Codable {
let userDetail: UserDetail?
    
}

struct UserDetail: Codable {
let mobileNumber: String?
let countryCode: String?
let firstName: String?
let lastName: String?
let email: String?
let dob: String?
let  _id: String?
let isFullRegister: Bool?
let week: Int?
let name : String?
let image : String?
let nationality :  String?
let gender:Int?
var skills:[String]?
    
}

struct commonResonseModel: Decodable {
    let code,Status : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case Status = "status"
        case message = "message"
        case code
    }
}

struct Mathques: Codable {
    let code,Status : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case Status = "status"
        case message = "message"
        case code
    }

}


//struct SkillsChoose:Codable {
//    let Code: Int?
//    let status : Int?
//    let message: String?
//    var data: DataofSKills?
//}
//
//       struct DataofSKills: Codable {
//        var skillList: [skillList]
//       }
//
//        struct skillList: Codable {
//            let  _id: Int?
//            var skillType: [SkillType]?
//        }
//
//        struct SkillType: Codable
//           {
//            var skills: String?
//            var skillType: Int?
//            var _id: String?
//
//
//        }

struct SkillsChoose: Codable {
    var code, status: Int?
    var message: String?
    var data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    var skillList: [SkillList]?
}

// MARK: - SkillList
struct SkillList: Codable {
    var _id: String?
    var skillType: [SkillType]?
}
// MARK: - SkillType
struct SkillType: Codable {
    var _id: String?
    var skillType: Int?
    var skills: String?
}
