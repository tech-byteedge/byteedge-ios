//
//  LearningList.swift
//  byteEDGE
//
//  Created by gaurav on 28/01/22.

import Foundation
import Foundation
import SwiftUI

struct LearningListModel: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : LearningListData?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case Code, data
    }
}
 //...................
struct FavouriteCourse : Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data: FavouriteCourselike?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
       case Code, data
    }
}

struct FavouriteCourselike: Codable
{
}

struct LearningListData: Codable {
    var course : [Course]?
    var yourCourse: [YourCourse]?
    var favouriteCourse : [FavouriteCourseData]?
    enum CodingKeys: String, CodingKey {
        case course,favouriteCourse,yourCourse
    }
}

//MARK: Course -
struct Course : Codable {
    let _id: String?
    let courseName: String?
    let Description: String?
    let rating: Int?
    let thumbnails: String?
    var  isFavourite: Bool?
    var courseType: String?
    var playlistId: String?
    var createdAt: String?
    var previewId: String?
    var isQuizExist: Bool?
    let courseUuid: String?
    }

//MARK: YourCourse -
 struct YourCourse : Codable {
    let _id: String?
    let courseName: String?
    let Description: String?
    let rating: Int?
    let thumbnails: String?
    var  isFavourite: Bool?
    var courseType: String?
    var playlistId: String?
    var createdAt: String?
    var isQuizExist: Bool?
     var courseUuid: String?
     
}

struct HashTag : Codable {
    let hashTag, id : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case hashTag
    }
}

//MARK: FavouriteCourseData -

struct FavouriteCourseData : Codable {
    var _id: String?
    var userId: String?
    var courseId: String?
    var createdAt: String?
    var courseName: String?
    var hashTag:[HashTag]?
    var Description: String?
    var courseType: String?
    var playlistId:String?
    var previewId:String?
    var status: Bool?
    var isQuizExist:Bool?
    var rating:Int?
    var thumbnails: String?
    var courseUuid: String?
}



struct Popular:Codable {
    var Code: Int?
    var status: Int?
    var message: String?
    var data: DataPopular?
}
struct DataPopular:Codable {
    var Popular:[PopularData]?
}

struct PopularData:Codable
{
     var _id: String?
    var courseName: String?
    var Description:String?
    var courseType: String?
    var playlistId: String?
    var previewId: String?
    var status:Bool?
    var createdAt: String?
//    var mediaId:[String]
    var rating: Int?
    var isQuizExist: Bool?
    var thumbnails: String?
    let courseUuid: String?
}
