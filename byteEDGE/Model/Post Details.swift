//
//  Post Details.swift
//  byteEDGE
//
//  Created by gaurav on 04/02/22.
//

import Foundation
import UIKit
import  AVKit

struct PostDetailModel: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : PostDetailModelData?
    
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
       case Code, data
    }
}

struct PostDetailModelData: Codable {
    let feedDetail : FeedDetailData?
    enum CodingKeys: String, CodingKey {
        case feedDetail
    }
}

struct FeedDetailData: Codable {
    let id, description,video : String?
    let feedType: Int?
    let userDetail : UserDetails?
    let image : [String]?
    var thumbnails: String?
    var createdAt: String?
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case  description, createdAt,video, feedType, userDetail, image,thumbnails
    }
    
}
struct UserDetails: Codable{
    let _id : String?
    let name: String?
    let image: String?
    
}

