//
//  PurchaseCourseModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 12/04/22.
//

import Foundation
struct PurchaseCourseModel: Codable
{
    var playlist: [PlaylistData]
}

struct PlaylistData: Codable {
    var description: String?
    var duration  : Int?
    var feedid : String?
    var image: String?
    var link : String?
    var mediaid : String?
    var pubdate: Int?
    var title : String?

}



