//
//  ShareReals.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 09/08/22.
//

import Foundation

struct ShareReals:Codable {
    var Code: Int?
    var status: Int?
    var message: String?
    var data: DataReals?
}

struct DataReals:Codable {
    var reelList:  ReelList?
}
 struct ReelList:Codable {
    var _id: String?
    var teasureName: String?
    var description: String?
    var courseId: String?
    var video: String?
    var isBookMark: Bool?
    var likes: Int?
    var isLiked: Bool?
    var courseData: CourseData?
   
}

struct CourseData:Codable {
    var _id: String?
    var courseName: String?
    var Description: String?
    var courseType: String?
    var playlistId: String?
    var previewId: String?
    var courseUuid: String?
    var thumbnails: String?

}



