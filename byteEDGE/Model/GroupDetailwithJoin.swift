

import Foundation
struct GroupDetailwithJoin: Codable {
    let Code, status: Int
    let message: String
    let data: DataOfGroupdetails?
}

// MARK: - DataClass
struct DataOfGroupdetails :Codable{
let groupDetails: GroupDetailsofJoin?
}

// MARK: - GroupDetails
   struct GroupDetailsofJoin:Codable {
    let _id, name, about, joinReason: String?
    let isAdmin: Bool?
    let createdAt: String?
    let groupAdminDetail: GroupAdminDetail?
    let memberDetails: [MemberDetailofJoin]?
    let totalMembers: Int?
    let image: String?
    let joinStatus: String?
}

// MARK: - GroupAdminDetail
struct GroupAdminDetail:Codable {
    let _id, mobileNumber, name: String
}

// MARK: - MemberDetail
struct MemberDetailofJoin:Codable {
    let memberId: String?
    let memberImage: String?
}

