//
//  RecentSearch.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 28/06/22.
//

import Foundation

struct RecentSearchModel: Codable
{
    let Code: Int?
    let status: Int?
    let message: String?
    let data: DataRecent
}

struct DataRecent:Codable
{
}

