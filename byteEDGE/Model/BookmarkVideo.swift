//
//  BookmarkVideo.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 01/08/22.
//

import Foundation

struct BookmarkVideo: Codable {
    let status: Int?
    let Code : Int?
    let message: String?
    var data : BookmarkData?
}

struct BookmarkData: Codable {
    let count: Int?
    var bookmark : [BookmarkListData]?
}

struct BookmarkListData: Codable {
    let _id: String?
    let videoId: String?
    let teasureName : String?
    let video: String?
    var likes : Int?
    let description: String?
    var isLiked : Bool?
    var isBookMark: Bool?
    var  courseData: courseDataBookMark?
    
}
   struct courseDataBookMark:Codable {
    var _id:  String?
    var courseName:  String?
    var categoryId: String?
    var  Description: String?
    var courseType: String?
    var playlistId: String?
    var previewId:  String?
    var thumbnails: String?
       
   }










