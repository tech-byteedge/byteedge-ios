//
//  LearningListViewAll.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 28/02/22.
//

import Foundation
// MARK: - Welcome
struct LearningListViewAll: Codable {
    let Code, status: Int?
    let message: String?
    var data: DataofViewAlllLearning?
}

// MARK: - DataClass
struct DataofViewAlllLearning: Codable {
    var count: Int?
    var course: [CourseViewall]?
    var yourCourse: [CourseViewall]?
    var favouriteCourse: [FavouriteCourselistViewAll]?
}

// MARK: - PaidCourse
struct CourseViewall: Codable {
    let  Description: String
    let _id: String?
    let courseName: String
    var isFavourite : Bool
    let thumbnails: String
    var  rating: Int?
    var  createdAt:String?
    var  playlistId: String?
    var previewId: String?
    
}

// MARK: - freeCourselist
struct FavouriteCourselistViewAll: Codable {
    
    var _id: String?
    var createdAt: String?
    var courseName: String?
    var Description: String?
    var courseType: String?
    var playlistId: String?
    var previewId: String?
    var isQuizExist: Bool?
    var rating: Int?
    var thumbnails: String?
  
}
