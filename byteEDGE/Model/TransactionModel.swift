
//  TransactionModel.swift
//  byteEDGE
//  Created by Pawan Kumar on 04/03/22.
import Foundation
// MARK: - Welcome
struct TransactionModel: Codable {
    let code, status: Int
    let message: String
    let data: DataofTransactionDetails

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case status, message, data
    }
}

// MARK: - DataClass
struct DataofTransactionDetails: Codable {
    let orderDetail: OrderDetail
}

// MARK: - OrderDetail
struct OrderDetail: Codable {
    let userID: String?
    let paymentType: String
    let amount: Int
    let currency, orderID: String
    let subscriptionType: Int
    let paymentID, transactionID: String
    let status: Bool
    let id, createdAt, updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case paymentType, amount, currency
        case orderID = "orderId"
        case subscriptionType
        case paymentID = "paymentId"
        case transactionID = "transactionId"
        case status
        case id = "_id"
        case createdAt, updatedAt
    }
}
