
import Foundation

struct LoginModel:Codable {
    let Code: Int?
    let status : Int?
    let message: String?
    let data:DataofLogin?
}

struct DataofLogin:Codable {
let userDetail: UserDetailDurinLogin?
let  token : String?
}

struct UserDetailDurinLogin: Codable {
let mobileNumber: String?
let countryCode: String?
let firstName: String?
let lastName: String?
let email: String?
let dob: String?
let  _id: String?
let gender: Int?
let isFullRegister: Bool?
let week: Int?
let name : String?
let image : String?
let nationality :  String?
let isSubscribed: Bool?
let skills: [String]
}

