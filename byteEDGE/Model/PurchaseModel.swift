//
//  PurchaseModel.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 10/03/22.
//

import Foundation

struct PurchaseModel: Codable
{
    
    let Code: Int?
    let status: Int?
    let message: String?
    let data: Datapurchase?
}

struct Datapurchase: Codable
{
    let count : Int?
    let purchaseList: [Purchaselist]
}

struct Purchaselist: Codable
{
    let _id: String?
    let userId:String?
    let paymentType: String?
    let amount: Int?
    let currency: String?
    let orderId: String?
    let subscriptionType:Int?
    let paymentId: String?
    let transactionId: String?
    let status: Bool
    let updatedAt: String?
    let createdAt: String?
}

