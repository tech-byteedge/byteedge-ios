
import UIKit
import  SDWebImage
import  Alamofire

class GrpTopCellTableViewCell: UITableViewCell {
    @IBOutlet weak var topimg: UIImageView!
    @IBOutlet weak var lblTxt: UILabel!
    @IBOutlet weak var collectionImg: UICollectionView!
    @IBOutlet weak var memberTxt: UILabel!
    var callBackimgShow:(()->())?
    var obj:GroupDetailwithJoin?
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionImg.delegate = self
        collectionImg.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func imgTap(_sender:UIButton)
    {
     self.callBackimgShow?()
    }
    
   
    
    
    
}

extension GrpTopCellTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if obj?.data?.groupDetails?.memberDetails?.count ?? 0 <= 3
        {
            return obj?.data?.groupDetails?.memberDetails?.count ?? 0
        }
        else
        {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionImg.dequeueReusableCell(withReuseIdentifier: "ImgCollection", for: indexPath) as! ImgCollection
        //        cell.imgMultiple.image = UIImage(named: "communityprofile")
        cell.imgMultiple.layer.masksToBounds = true
        cell.imgMultiple.layer.cornerRadius = cell.imgMultiple.frame.size.height/2;
        cell.imgMultiple.layer.masksToBounds = true
        cell.imgMultiple.layer.borderWidth=1;
        var urlImage = obj?.data?.groupDetails?.memberDetails?[indexPath.row].memberImage ?? ""
        cell.imgMultiple.sd_setImage(with: URL(string: urlImage), placeholderImage:UIImage(named:"grpIcon"), options: .highPriority, context: nil)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return -15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
}

class ImgCollection: UICollectionViewCell
{
    @IBOutlet weak var imgMultiple: UIImageView!
    @IBOutlet weak var lblcount: UILabel!
    
}
