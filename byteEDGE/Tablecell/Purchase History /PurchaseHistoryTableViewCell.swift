//
//  PurchaseHistoryTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.
//

import UIKit

class PurchaseHistoryTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var arrowBtn: UIButton!
    @IBOutlet weak var projectImg: UIImageView!
    @IBOutlet weak var byUserLbl: UILabel!
    @IBOutlet weak var lessonLbl: UILabel!
    
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnArrowBtn(_ sender: Any) {
        
    }
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        projectImg.backgroundColor = .lightGray
        titleLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        byUserLbl.setAppFontColor(.appColor(.white, alpha: 0.74), font: .ProductSans(.regular, size: .oneThree))
        dateLbl.setAppFontColor(.appColor(.white, alpha: 0.74), font: .ProductSans(.regular, size: .oneThree))
//        arrowBtn.setTitle(.none, for: .normal)
//        arrowBtn.isHidden = false
//        arrowBtn.setImage(UIImage(named: "rightIcon"), for: .normal)
    }
    
    func setCellForOrderDetail() {
        dateLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        arrowBtn.isHidden = true
    }
}
