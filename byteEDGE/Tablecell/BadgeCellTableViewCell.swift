//
//  BadgeCellTableViewCell.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 07/01/22.
//

import UIKit

class BadgeCellTableViewCell: UITableViewCell {

    @IBOutlet weak var badgeview: UIView!
    @IBOutlet weak var subtitlelbl: UILabel!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var badgeImg: UIImageView!
    @IBOutlet weak var badgetxtbtn: UIButton!
    @IBOutlet weak var cellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
