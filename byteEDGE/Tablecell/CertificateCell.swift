
import UIKit
class CertificateCell: UITableViewCell {

    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var downloadCertificateBtn: UIButton!
    @IBOutlet weak var line: UILabel!

    var callBack: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    @IBAction func download(_ sender: UIButton) {
        self.callBack?()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
