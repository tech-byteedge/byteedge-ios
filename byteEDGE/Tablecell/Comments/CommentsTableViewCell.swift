
//  CommentsTableViewCell.swift
//  byteEDGE
//  Created by gaurav on 21/01/22.
import UIKit

class CommentsTableViewCell: UITableViewCell {

    //MARK: -VARIABLES-
        var likeBtnAciton: ((Bool)-> ())?
        var commentBtnAction: (()-> ())?
        //MARK: - OUTLETS -
        @IBOutlet weak var outerView: UIView!
        @IBOutlet weak var commentsView: UIView!
        @IBOutlet weak var userImg: UIImageView!
        @IBOutlet weak var stackView: UIView!
        @IBOutlet weak var likeOuterView: UIView!
        @IBOutlet weak var likeBtn: UIButton!
        @IBOutlet weak var likeButtonLbl: UILabel!
        @IBOutlet weak var commentsBtn: UIButton!
        @IBOutlet weak var userNameLbl: UILabel!
        @IBOutlet weak var timeLbl: UILabel!
        @IBOutlet weak var commentLbl: UILabel!
        //MARK: - VIEW LIFE CYCLE -
        override func awakeFromNib() {
            super.awakeFromNib()
            setView()
        }
    
        //MARK: - ACTIONS -
        @IBAction func likeBtn(_ sender: UIButton) {
            if likeBtn.imageView?.image == UIImage(named: "heart") {
               
                likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
                UIApplication.topViewController()?.view.isUserInteractionEnabled = true
                if let action = likeBtnAciton {
                                action(false)
                
                            }
               
            }else {
                likeBtn.setImage(UIImage(named: "heart"), for: .normal)
                UIApplication.topViewController()?.view.isUserInteractionEnabled = true
    //            likeBtn.isUserInteractionEnabled = false
                if let action = likeBtnAciton {
                               action(true)
                           }
               
            }
//            UIApplication.topViewController()?.createAlert(title: kAppName, message: "Under devlopment")
           
        }
        @IBAction func commentsBtn(_ sender: Any) {
            if let action = commentBtnAction {
                action()
            }
        }
      
        //MARK: -FUNCTIONS-
        func setView() {
            self.tintColor = .clear
            self.backgroundColor = .black
            selectionStyle = .none
            outerView.backgroundColor = .clear
            ///set user image
            userImg.makeRounded()
            userImg.image = UIImage(named: "profileperson")
            
            ///message view
            commentsView.backgroundColor = .clear
            //set user name lbl
            userNameLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.ExtraBold, size: .oneThree))
            userNameLbl.text = "User Name"
            timeLbl.setAppFontColor(.appColor(.graylight), font: .ProductSans(.regular, size: .nine))
            timeLbl.text = "2 days ago"
            commentLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneOne))
            commentLbl.text = "This Session is awesome. My hair stand still till today coz the AH effects of the morning."
            commentsView.backgroundColor = .appColor(.blackLight)
            commentsView.layer.borderColor = .init(_colorLiteralRed: 112, green: 112, blue: 112, alpha: 0.34)
            commentsView.layer.borderWidth = 1.0
            
            
            likeOuterView.backgroundColor = .clear
            likeBtn.backgroundColor = .clear
            likeBtn.imageView?.isHighlighted = true
            likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
            likeBtn.setTitle(.none, for: .normal)
            likeBtn.titleLabel?.textAlignment = .left
            likeButtonLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneOne))
            likeButtonLbl.text = "like"
    //        let gesture = UITapGestureRecognizer(target: self, action: #selector(likeListing))
    //        gesture.numberOfTouchesRequired = 1
    //        likeButtonLbl.addGestureRecognizer(gesture)
    //        likeButtonLbl.isUserInteractionEnabled = true
            
            commentsBtn.backgroundColor = .clear
            commentsBtn.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneOne))
            commentsBtn.titleLabel?.textAlignment = .center
            commentsBtn.setTitle("Reply", for: .normal)
            commentsBtn.isHidden = true
        }
    }
