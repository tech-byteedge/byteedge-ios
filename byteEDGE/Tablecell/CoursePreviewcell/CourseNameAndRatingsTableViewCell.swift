//
//  CourseNameAndRatingsTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 25/01/22.
//

import UIKit
import Cosmos

class CourseNameAndRatingsTableViewCell: UITableViewCell {
    
    //MARK: -VARIABLES-
   
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var ratingOuterView: UIView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var starImg: UIImageView!
    @IBOutlet weak var courseNameLbl: UILabel!
    //MARK: -OUTLETS-
   
    var callback: (()->())?
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    
    @IBAction func tappedOnShareBtn(_ sender: Any) {
        self.callback?()
//        UIApplication.topViewController()?.createAlert(title: kAppName, message: "Under Development")
    }
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        
        ///set rating button
        ratingOuterView.addGradientBackground(firstColor: .appColor(.orangeLight), secondColor: .appColor(.orangeDark))
        ratingOuterView.makeRoundCornerwithborder(12, bordercolor: .appColor(.white), borderwidth: 0.5)
        starImg.image = UIImage(named: "ratingStarBlack")
        ratingLbl.setAppFontColor(.appColor(.black), font: .ProductSans(.regular, size: .oneThree))
//        ratingLbl.text = "4.5"
        
        /// set name label
        courseNameLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.ExtraBold, size: .oneSeven))
        
        shareBtn.makeRoundCornerwithborder(15, bordercolor: .appColor(.white), borderwidth: 0.5)
        shareBtn.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size:.oneFour))
    }
    
}
