//
//  AboutCourseTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 25/01/22.
//

import UIKit

class AboutCourseTableViewCell: UITableViewCell {

    //MARK: -VARIABLES-
   
    
    //MARK: -OUTLETS-
   
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var aboutCourseLbl: UILabel!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }

    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        
        aboutCourseLbl.setAppFontColor(.appColor(.white, alpha: 0.96), font: .ProductSans(.regular, size: .oneFive))
        aboutCourseLbl.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos \nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor"
        

    }
    
}
