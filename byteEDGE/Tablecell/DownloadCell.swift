//
//  DownloadCell.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 07/01/22.
//

import UIKit

class DownloadCell: UITableViewCell {
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var celltap: UIButton!
    @IBOutlet weak var leftimg: UIImageView!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
