//
//  SubcriptionTableCell.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 10/03/22.
//

import UIKit

class SubcriptionTableCell: UITableViewCell {
    @IBOutlet weak var subcriptionType: UILabel!
    @IBOutlet weak var datalbl: UILabel!
    @IBOutlet weak var monthView: UIView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        self.monthView.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
