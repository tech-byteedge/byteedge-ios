

import UIKit
class AccountListCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var content_lbl: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lineView: UILabel!
    @IBOutlet weak var cellTap: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
