
//  ViewAllCell.swift
//  byteEDGE
//  Created by Pawan Kumar on 29/01/22.

import UIKit

class ViewAllCell: UITableViewCell {
    
    @IBOutlet weak var collectionAll: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionAll.delegate = self
        self.collectionAll.dataSource = self
        self.collectionAll.alwaysBounceHorizontal = true
        self.collectionAll.alwaysBounceVertical = false
        self.collectionAll.backgroundView?.tintColor = .black
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension ViewAllCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionAll.dequeueReusableCell(withReuseIdentifier: "ViewCollectionCell", for:indexPath) as! ViewCollectionCell
        cell.contentView.backgroundColor = .black
        cell.lblTitle.text = "bYteEdge"
        cell.lblTitle.textColor = .white
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return -5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 5
    }

      
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            let padding: CGFloat = 5
            let collectionCellSize = collectionAll.frame.size.width - padding
            return CGSize(width: collectionCellSize / 2, height: 150)

   }
}


















//MARK: CLASS CELL
class ViewCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var mainviewCell: UIView!
    @IBOutlet weak var imgCell: UIImageView!
}
