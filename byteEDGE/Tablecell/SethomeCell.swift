
//  SethomeCell.swift
//  byteEDGE
//  Created by Pawan Kumar on 09/12/21.

import UIKit
//protocol dateset {
//    func date()
//}

class SethomeCell: UITableViewCell ,UITextFieldDelegate{
    
    @IBOutlet weak var calender_img: UIImageView!
    @IBOutlet weak var sub_tilte: UITextField!
    @IBOutlet weak var user_lbl: UILabel!
    @IBOutlet weak var user_inputDetails: UIView!
    var callBack:((_ data:String)->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        sub_tilte.delegate = self
        UITextField.appearance().tintColor = .white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.callBack?(sub_tilte.text ?? "")
    }
 
}









