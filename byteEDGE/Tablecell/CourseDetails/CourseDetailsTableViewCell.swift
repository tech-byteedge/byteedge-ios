//
//  CourseDetailsTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 31/01/22.
//

import UIKit

class CourseDetailsTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
    
    var courseBtnAction: (()->())?
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var courseNameLbl: UILabel!
    @IBOutlet weak var courseBtn: UIButton!
    @IBOutlet weak var courseImg: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnCourseBtn(_ sender: Any) {
        if let action = courseBtnAction {
            action()
        }
    }
    
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        
        courseNameLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.bold, size: .oneFive))
        courseNameLbl.text = "Course Name"
        
        ///seperator view
        seperatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
       
       

    }
    
}
