//
//  PostCaptionTableViewCell.swift
//  Betcha
//
//  Created by Gaurav_kumar on 09/07/21.
//

import UIKit
import ExpandableLabel

class PostCaptionTableViewCell: UITableViewCell {
    //MARK: - VARIABLES -
    var expandCollapseLabelAction : (()->())?
    //MARK: - OUTLETS -
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var outerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var postContentLbl: ExpandableLabel!
    //MARK: - CELL LIFE CYCEL -
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: - PRIVATE FUNCTION -
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        sepratorView.isHidden = true
        ///post lable
        postContentLbl.backgroundColor = .clear
        postContentLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneThree))
        postContentLbl.delegate = self
        postContentLbl.numberOfLines = 5
        postContentLbl.collapsedAttributedLink = NSAttributedString(string: "Read More")
        postContentLbl.expandedAttributedLink = NSAttributedString(string: "Read Less")
        postContentLbl.layer.masksToBounds = false
        postContentLbl.textAlignment = .left
       
        postContentLbl.setLessLinkWith(lessLink: "Read More", attributes: [NSAttributedString.Key.foregroundColor:UIColor.appColor(.pink)], position: nil)
        postContentLbl.setLessLinkWith(lessLink: "Read Less", attributes: [NSAttributedString.Key.foregroundColor:UIColor.appColor(.pink)], position: nil)
    }
}
//MARK: -EXPANDABLE LABEL DELEGATE-
extension PostCaptionTableViewCell : ExpandableLabelDelegate{
    func willExpandLabel(_ label: ExpandableLabel) {
        print("will Expand label")
        if let action =  expandCollapseLabelAction {
            action()
        }
    }
    func didExpandLabel(_ label: ExpandableLabel) {
        print("did Expand label")
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        print("will collapse label")
        postContentLbl.numberOfLines = 5
    }
    func didCollapseLabel(_ label: ExpandableLabel) {
        print("did collapse label")
        if let action =  expandCollapseLabelAction {
            action()
        }
    }
}
