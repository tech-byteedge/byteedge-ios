//
//  PostDetailImageTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 04/02/22.
//

import UIKit

class PostDetailImageTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
    

    var playBtnAction : (()->())?
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var userNameLB: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    
    
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
        if UIDevice.current.userInterfaceIdiom == .pad {
            self.height.constant = 350
        }
            else
            {
            self.height.constant = 250
            }
    }
    
    
    //MARK: -ACTIONS-
   
    @IBAction func tappedOnPlayBtn(_ sender: Any) {
        if let action = playBtnAction {
            action()
        }
    }
    
    //MARK: -FUNCTIONS-
    private func setCell() {
       // self.backgroundColor = .clear
        selectionStyle = .none
      //  outerView.backgroundColor = .clear
       
    }
    
}
