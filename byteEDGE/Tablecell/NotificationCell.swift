//
//  NotificationCell.swift
//  byteEDGE
//  Created by Pawan Kumar on 22/02/22.

import UIKit
class NotificationCell: UITableViewCell {
    @IBOutlet weak var cell_view: UIView!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var leading_con: NSLayoutConstraint!
    @IBOutlet weak var bottam_cons: NSLayoutConstraint!
    @IBOutlet weak var trailing_cons: NSLayoutConstraint!
    @IBOutlet weak var height_constant: NSLayoutConstraint!
    @IBOutlet weak var timeNoti: UILabel!
    
    @IBOutlet weak var timeNotification: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
extension UIView {
    func roundSpecificCorners(corners: UIRectCorner, radius: Int = 8) {
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
