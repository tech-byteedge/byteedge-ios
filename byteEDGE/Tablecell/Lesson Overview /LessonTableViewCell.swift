//
//  LessonTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 26/01/22.
//

import UIKit

class LessonTableViewCell: UITableViewCell {
    
    //MARK: -VARIABLES-
    var lessonBtnAction: (()->())?
    var downloadBtnAction: (()->())?
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lessonPlayimg: UIImageView!
    @IBOutlet weak var videoPreviewImg: UIImageView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var videoTimeLbl: UILabel!
    @IBOutlet weak var lessonBtn: UIButton!
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var downloadLbl: UILabel!
    @IBOutlet weak var videoTitleLbl: UILabel!
    @IBOutlet weak var videoDescLbl: UILabel!
    
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    
    @IBAction func tappedOnLessonBtn(_ sender: UIButton) {
    lessonBtnAction?()
    }
    
//    @IBAction func tappedOnDownloadBtn(_ sender: UIButton) {
//        downloadBtnAction?()
    
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        
//        blurView.backgroundColor = .clear
//        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.backgroundColor = .clear
//        blurEffectView.frame = blurView.bounds
//        blurEffectView.alpha = 60.0
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
//        blurView.addSubview(blurEffectView)
          blurView.makeRoundCorner(12.5)
        
//        videoTimeLbl.backgroundColor = .clear
//        videoTimeLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneOne))
//        videoTimeLbl.text = "6:50"
//        videoTimeLbl.makeRoundCorner(12.5)
        
//        lessonBtn.backgroundColor = .appColor(.pink)
//        lessonBtn.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneOne))
//        lessonBtn.setTitle("LESSON 1", for: .normal)
        
//        downloadLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .nine))
//        downloadBtn.backgroundColor = .clear
//        downloadBtn.isHidden = true
     
        videoTitleLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.bold, size: .oneSeven))
        videoTitleLbl.text = "Course Video Title"
        videoDescLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneThree))
        videoDescLbl.text = "Learn how to get in to the Project management mindset and achieve success in helping organization evolve."
    }
    
}
