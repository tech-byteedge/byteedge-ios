//
//  OverviewTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 26/01/22.
//

import UIKit

class OverviewTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var lessonDescLbl: UILabel!
    @IBOutlet weak var percentageLbl: UILabel!
    @IBOutlet weak var lessonLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var projectImg: UIImageView!
    @IBOutlet weak var playBtnImg: UIImageView!
  
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
 
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        projectImg.backgroundColor = .lightGray
        titleLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        
        blurView.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.backgroundColor = .clear
        blurEffectView.frame = blurView.bounds
        blurEffectView.alpha = 60.0
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        blurView.addSubview(blurEffectView)
        blurView.makeRoundCorner(12.5)
        
        timeLbl.backgroundColor = .clear
        timeLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .nine))
        timeLbl.text = "6:50"
//        
//        lessonLbl.backgroundColor = .appColor(.grayLight, alpha: 0.26)
//        lessonLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneOne))
//        lessonLbl.text = "LESSON 6"
        
        lessonDescLbl.setAppFontColor(.appColor(.white, alpha: 0.74), font: .ProductSans(.regular, size: .oneOne))
        
        percentageLbl.backgroundColor = .appColor(.green)
        percentageLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneThree))
        percentageLbl.makeRoundCornerwithborder(0, bordercolor: .appColor(.white, alpha: 0.24), borderwidth: 0.5)
     
        
    }
    
   
}
