//
//  DownloadInvoiceTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.
//

import UIKit

class DownloadInvoiceTableViewCell: UITableViewCell {

    //MARK: -VARIABLES-
   
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var downloadLbl: UILabel!
    @IBOutlet weak var invoiceImg: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnRightBtn(_ sender: Any) {
        
    }
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        downloadLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneFive))
        downloadLbl.text = kDownloadInvoice
        seperatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
   
    }
    
}
