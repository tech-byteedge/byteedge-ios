//
//  RateUsThisProductTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.

import UIKit

class RateUsThisProductTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
   
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
 
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var rateUsLbl: UILabel!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    //MARK: -FUNCTIONS-
       private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        rateUsLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneFive))
        ///register collection cell
        collectionView.registerCell(type: RatingStarCollectionViewCell.self)
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false
        ///connectors collection view
        collectionView.delegate = self
        collectionView.dataSource = self
        
        rateUsLbl.text = "Rate this Product"
        
    }
    
}

//MARK: -COLLECTION VIEW DELEGATE-
extension RateUsThisProductTableViewCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.isUserInteractionEnabled = false
    }
}

//MARK: -COLLECTION VIEW DATA SOURCE-

extension RateUsThisProductTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(withType: RatingStarCollectionViewCell.self, for: indexPath) as? RatingStarCollectionViewCell else {
            fatalError("RatingStarCollectionViewCell is not initialize")
        }
        
        indexPath.row == 4 ?  (cell.starImg.image = UIImage(named: "starWhite")) :  (cell.starImg.image = UIImage(named: "starColor"))
        return cell
    }
}
// MARK: - COLLECTIONVIEW DELEGATE FLOW LAYOUT -
extension RateUsThisProductTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        return CGSize(width: 35, height: 30)  //height 74
    }
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 0.10
    }
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 3.0 //-74.0/2
    }
}

