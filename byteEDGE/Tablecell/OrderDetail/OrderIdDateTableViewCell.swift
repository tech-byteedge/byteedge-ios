//
//  OrderIdDateTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.
//

import UIKit

class OrderIdDateTableViewCell: UITableViewCell {

    //MARK: -VARIABLES-
   
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var upperSeperatorView: UIView!
    @IBOutlet weak var lowerSepratorView: UIView!
    @IBOutlet weak var middleSepratorView: UIView!
    @IBOutlet weak var orderIdHeadingLbl: UILabel!
    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var orderDateHeadingLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnArrowBtn(_ sender: Any) {
        
    }
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        upperSeperatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        lowerSepratorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        middleSepratorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        orderIdHeadingLbl.setAppFontColor(.appColor(.white, alpha: 0.64), font: .ProductSans(.regular, size: .oneFive))
        orderIdHeadingLbl.text = kOrderId
        orderDateHeadingLbl.setAppFontColor(.appColor(.white, alpha: 0.64), font: .ProductSans(.regular, size: .oneFive))
        orderDateHeadingLbl.text = kOrderDate
        orderIdLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        orderIdLbl.text = "--"
        orderDateLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        orderDateLbl.text = "--"

    }
    
}
