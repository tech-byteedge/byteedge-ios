
//  PaymentMethodTableViewCell.swift
//  byteEDGE
//  Created by gaurav on 20/01/22.


import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
   
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
 
    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var upperSepatorView: UIView!
    @IBOutlet weak var lowerSepratorView: UIView!
    @IBOutlet weak var cardTypeLbl: UILabel!
    @IBOutlet weak var paymentMethodLbl: UILabel!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
 
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        
        upperSepatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        lowerSepratorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        cardTypeLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        cardTypeLbl.textAlignment = .right
        paymentMethodLbl.setAppFontColor(.appColor(.white, alpha: 0.94), font: .ProductSans(.regular, size: .oneFive))
    }
    
}
