//
//  PaymentSummaryTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.
//

import UIKit

class PaymentSummaryTableViewCell: UITableViewCell {

    //MARK: -VARIABLES-
   
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
 
    @IBOutlet weak var upperSepatorView: UIView!
    @IBOutlet weak var lowerSepratorView: UIView!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var headingLbl: UILabel!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
 
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        
        upperSepatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        lowerSepratorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        valueLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        valueLbl.textAlignment = .right
        headingLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneFive))
    }
    
}
