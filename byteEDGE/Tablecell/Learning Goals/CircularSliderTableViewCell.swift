//
//  CircularSliderTableViewCell.swift
//  byteEDGE
//  Created by gaurav on 21/01/22.

import UIKit
import KDCircularProgress

class CircularSliderTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var circularSlider: KDCircularProgress!
    @IBOutlet weak var percentageLbl: UILabel!
    
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        circularSlider.clockwise = true
        circularSlider.roundedCorners = false
        circularSlider.lerpColorMode = false
        circularSlider.trackColor = .appColor(.white)
        percentageLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.bold, size: .oneNine))
    }
    
}
