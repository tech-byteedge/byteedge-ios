
import UIKit
import Charts

class GraphLearningGoalsTableViewCell: UITableViewCell
{
    var objGraphlist : GrapglistModel?
    var userdata:[UserData?] = []
    var selectedtype = ""
    
    //MARK: -OUTLETS-
    @IBOutlet weak var lineChart: LineChart!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    

    func generateRandomEntries() -> [PointEntry] {
        var result: [PointEntry] = []
        
         result.append(PointEntry(value:0, label: "Sun"))
         result.append(PointEntry(value:0, label: "Mon"))
         result.append(PointEntry(value:0, label: "Tue"))
         result.append(PointEntry(value:0, label: "Wed"))
         result.append(PointEntry(value:0, label: "Thu"))
         result.append(PointEntry(value:0, label: "Fri"))
         result.append(PointEntry(value:0, label: "Sat"))
         
         if /objGraphlist?.data?.userData.count > 0{
             for i in 0...(objGraphlist?.data?.userData.count ?? 0) - 1{
                 switch objGraphlist?.data?.userData[i]?._id?.day {
                 case 1:
                     result[0] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "Sun")
                     break
                 case 2:
                result[1] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "Mon")
                     break
                 case 3:
                result[2] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "Tue")
                     break
                 case 4:
                result[3] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "Wed")
                     break
                 case 5:
                result[4] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "Thu")
                     break
                 case 6:
                result[5] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "Fri")
                     break
                 case 7:
                result[6] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "Sat")
                
                break
                 default:
                     break
                 }
             }
         }
        
        return result
    }
    //for Month
    func monthGraphData() -> [PointEntry] {
       var result: [PointEntry] = []
        for i in 0...30{
            result.append(PointEntry(value:0, label: "\(i + 1)"))
        }
        
        if /objGraphlist?.data?.userData.count > 0{
           
                for i in 0...(objGraphlist?.data?.userData.count ?? 0) - 1{
            
                    if  objGraphlist?.data?.userData[i]?._id?.day ?? 0 > 0{
                        
                        result[/objGraphlist?.data?.userData[i]?._id?.day - 1] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "\(/objGraphlist?.data?.userData[i]?._id?.day - 1)")
                    }
                   
                }
        }
       return result
   }
    
    func yearGraphData() -> [PointEntry] {
       var result: [PointEntry] = []
        let arrayMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Num","Dec"]
        for i in 0...arrayMonth.count - 1{
            result.append(PointEntry(value:0, label: "\(arrayMonth[i])"))
        }
        
        if /objGraphlist?.data?.userData.count > 0{
            for i in 0...(objGraphlist?.data?.userData.count ?? 0) - 1{
                if objGraphlist?.data?.userData[i]?._id?.month ?? 0 > 0{
                    result[/objGraphlist?.data?.userData[i]?._id?.month - 1] = PointEntry(value: /objGraphlist?.data?.userData[i]?.duration, label: "\(/arrayMonth[/objGraphlist?.data?.userData[i]?._id?.month - 1])")
                }
                
            }
        }
        
       return result
   }
}
