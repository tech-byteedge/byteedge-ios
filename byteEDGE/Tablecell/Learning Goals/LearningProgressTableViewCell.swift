//
//  LearningProgressTableViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 21/01/22.
//

import UIKit

class LearningProgressTableViewCell: UITableViewCell {

    //MARK: -VARIABLES-
   
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var goadHeadingLbl: UILabel!
    @IBOutlet weak var goalsImg: UIImageView!
    @IBOutlet weak var byUserNameLbl: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progessCompeleteLbl: UILabel!
    //MARK: -CELL LIFE CYCLE-
    //let progress = Progress(totalUnitCount: 4)
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        outerView.backgroundColor = .clear
        
        goadHeadingLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.semibold, size: .oneFive))
        goadHeadingLbl.text = "Graphic Designer - Learn Creative"
        byUserNameLbl.setAppFontColor(.appColor(.white, alpha: 0.74), font: .ProductSans(.regular, size: .oneThree))
        byUserNameLbl.text = "By Shreya Mohanty"
        progessCompeleteLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneThree))
//        progessCompeleteLbl.text = "60% Completed"
//        progressView.trackTintColor = .appColor(.pink)
//        progressView.trackTintColor = .appColor(.white)
//        progressView.progress = 0.6
        

    }
    
}
