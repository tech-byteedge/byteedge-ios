
//  FriendsCollectionTableViewCell.swift
//  Created by Gaurav_kumar on 24/06/21.

import UIKit
import SDWebImage
import  Foundation
import  Cosmos
import Alamofire

protocol SubsCriptionCourse
{
    func getIndex(index:Int)
}

class FriendsCollectionTableViewCell: UITableViewCell {
    //MARK: - OUTLETS -
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var outerViewtop: NSLayoutConstraint!
    @IBOutlet weak var outerViewbottom: NSLayoutConstraint!
    @IBOutlet weak var friendsCollectionView: UICollectionView!
    @IBOutlet weak var outerViewHeight: NSLayoutConstraint!
    
    //MARK: - VARIABLES -
    var cellState : CourseState = .none //for set data of collectionview according to the state
    var selectedAction : ((_ index:Int)->())?
    var courseData : LearningListData?
    var recom_courseData: RecommnndedModel?
    var trend_obj: TrendingModel?
    var popular_ob: Popular?
    var callBackdata : ((IndexPath)->())?
    var likeAction: (()->())?
    var delegate :SubsCriptionCourse!
    
    //MARK: - CELL LIFE CYCLE -
    override func awakeFromNib() {
        super.awakeFromNib()
        setcell()
    }
    //MARK: - FUNCTION -
    private func setcell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        //outerView.backgroundColor = .clear
        friendsCollectionView.registerCell(type: CourseCollectionViewCell.self)
        friendsCollectionView.delegate = self
        friendsCollectionView.dataSource = self
        friendsCollectionView.backgroundColor = .clear
        friendsCollectionView.isScrollEnabled = true
        friendsCollectionView.showsVerticalScrollIndicator = false
        friendsCollectionView.showsHorizontalScrollIndicator = false
    }
}

//MARK: COLLECTION VIEW DELEGATE
extension FriendsCollectionTableViewCell: UICollectionViewDelegate  {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        selectedAction?(indexPath.row)
    }
}

//MARK: COLLECTION VIEW DELEGATE
extension FriendsCollectionTableViewCell: UICollectionViewDataSource  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch cellState  {
        case .none :
            return 0
            
        case.couese:
            return 0
            
        case .yourCourse :
            return self.courseData?.yourCourse?.count ?? 0 // Not generate Yourcoursekey
        case .popular :
            return self.popular_ob?.data?.Popular?.count ?? 0
        case .favouriteCourse :
            return self.courseData?.favouriteCourse?.count ?? 0
            
        case .recommendedCourse:
            return self.recom_courseData?.data?.newCourseCount.count ??  0
            
        case .recommendedCoursesetdata:
            return self.recom_courseData?.data?.recommend.count ??  0
            
        case .trendingCourse:
            return self.trend_obj?.data?.trending.count ??  0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueCell(withType: CourseCollectionViewCell.self, for: indexPath) as? CourseCollectionViewCell else {
            fatalError("CourseCollectionViewCell is not initialize...")
        }
        cell.starView.isUserInteractionEnabled = false
        cell.cellState = self.cellState
        
        switch cellState  {
        case .none :
            print(indexPath.row)
        case .couese :
            print(indexPath.row)
            
        case .yourCourse :
            if let image = self.courseData?.yourCourse?[indexPath.row].thumbnails, let url = URL(string: image) {
                cell.courseImg.sd_setImage(with: url , placeholderImage: UIImage(named: "logo"), options: SDWebImageOptions.continueInBackground )
                
            }else {
                cell.courseImg.image = UIImage(named: "SplashThird")
            }
            
            cell.likeBtn.isHidden = false
            if let courseName = self.courseData?.yourCourse?[indexPath.row].courseName {
                cell.courseNameLbl.text = courseName
            }else{
                cell.courseNameLbl.text = "Course Name"
            }///set description
            if let desc = self.courseData?.yourCourse?[indexPath.row].Description {
                cell.courseDiscLbl.text = desc
            }else{
                cell.courseDiscLbl.text = "Course Description"
            }
            
            let rating = Double(/self.courseData?.yourCourse?[indexPath.row].rating)
            cell.ratingsLbl.text = String(rating)
            cell.starView.rating =  Double(rating)
            //            }
            //            else{
            //                cell.ratingsLbl.text = "Course Description"
            //            }
            
            
            
            if courseData?.yourCourse?[indexPath.row].isFavourite ==  true {
                cell.likeBtn.isSelected = true
                
            }else {
                cell.likeBtn.isSelected = false
            }
            
            cell.callBackOflike = {
                if let id = self.courseData?.yourCourse?[indexPath.row]._id {
                    self.favooriteApi(id: id, state: .couese, index: indexPath)
                }
            }
            
        case .popular :
            
            DispatchQueue.main.async {
                if let image = self.popular_ob?.data?.Popular?[indexPath.row].thumbnails, let url = URL(string: image) {
                    cell.courseImg.sd_setImage(with: url , placeholderImage: UIImage(named: "logo"), options: SDWebImageOptions.continueInBackground )
                }else {
                    cell.courseImg.image = UIImage(named: "SplashThird")
                }
            }
            
            cell.likeBtn.isHidden = true
            if let courseName = self.popular_ob?.data?.Popular?[indexPath.row].courseName {
                cell.courseNameLbl.text = courseName
            }else{
                cell.courseNameLbl.text = "Course Name"
            }
            let rating =  Double(/self.popular_ob?.data?.Popular?[indexPath.row].rating)
            cell.ratingsLbl.text = String(rating)
            cell.starView.rating =  Double(rating)
            
            //            }else{
            //                cell.ratingsLbl.text = ""
            //            }
            //
            // show starview
            let val =  Double(self.popular_ob?.data?.Popular?[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
            cell.starView.isUserInteractionEnabled = false
            //Double(self.courseData?.course?[indexPath.row].rating ?? 0)
            
            
            if let desc = self.popular_ob?.data?.Popular?[indexPath.row].Description {
                cell.courseDiscLbl.text = desc
            }else{
                cell.courseDiscLbl.text = "Course Description"
            }
            
            
            //            if courseData?.course?[indexPath.row].isFavourite ==  true {
            //                cell.likeBtn.isSelected = true
            //
            //            }else {
            //                cell.likeBtn.isSelected = false
            //            }
            
            //            cell.callBackOflike = {
            //                if let id = self.courseData?.course?[indexPath.row]._id {
            //                self.favooriteApi(id: id, state: .couese, index: indexPath)
            //                }
            //            }
            
        case .favouriteCourse :
            if let courseName = self.courseData?.favouriteCourse?[indexPath.row].courseName {
                cell.courseNameLbl.text = courseName
            }else{
                cell.courseNameLbl.text = "Course Name"
            }
            
            let rating = Double(/self.courseData?.favouriteCourse?[indexPath.row].rating)
            cell.ratingsLbl.text = String(rating)
            cell.starView.rating =  Double(rating)
            
            //            }else{
            //                cell.ratingsLbl.text = "0"
            //            }
            //
            let val = Double(self.courseData?.favouriteCourse?[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
            
            if let desc = self.courseData?.favouriteCourse?[indexPath.row].Description  {
                cell.courseDiscLbl.text = desc
            }else{
                cell.courseDiscLbl.text = "Course Description"
            }
            if let image = self.courseData?.favouriteCourse?[indexPath.row].thumbnails, let url = URL(string: image) {
                cell.courseImg.sd_setImage(with: url , placeholderImage: UIImage(named: "logo"), options: SDWebImageOptions.continueInBackground )
            }else {
                cell.courseImg.image = UIImage(named: "SplashThird")
            }
            
            cell.likeBtn.isHidden = true
            cell.callBackOflike = {
                if let id = self.courseData?.favouriteCourse?[indexPath.row]._id {
                }
            }
            
        case .recommendedCourse:
            cell.likeBtn.isHidden = true
            if let courseName = self.recom_courseData?.data?.newCourseCount[indexPath.row].courseName {
                cell.courseNameLbl.text = courseName
            }else{
                cell.courseNameLbl.text = "Course Name"
            }
            let rating = Double(/self.recom_courseData?.data?.newCourseCount[indexPath.row].rating)
            cell.ratingsLbl.text = String(rating)
            cell.starView.rating =  Double(rating)
            let val =  Double(self.recom_courseData?.data?.newCourseCount[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
            cell.starView.isUserInteractionEnabled = false
            
            if let desc = self.recom_courseData?.data?.newCourseCount[indexPath.row].Description {
                cell.courseDiscLbl.text = desc
            }else{
                cell.courseDiscLbl.text = "Course Description"
            }
            if let image = self.recom_courseData?.data?.newCourseCount[indexPath.row].thumbnails, let url = URL(string: image) {
                cell.courseImg.sd_setImage(with: url , placeholderImage: UIImage(named: "logo"), options: SDWebImageOptions.continueInBackground )
            }else {
                
                cell.courseImg.image = UIImage(named: "logo")
            }
            
        case .recommendedCoursesetdata:
            cell.likeBtn.isHidden = true
            if let courseName = self.recom_courseData?.data?.recommend[indexPath.row].courseName {
                cell.courseNameLbl.text = courseName
            }else{
                cell.courseNameLbl.text = "Course Name"
            }
            let rating = Double(/self.recom_courseData?.data?.recommend[indexPath.row].rating)
            cell.ratingsLbl.text = String(rating)
            cell.starView.rating =  Double(rating)
            
            //            }else{
            //                cell.ratingsLbl.text = ""
            //            }
            //show starview
            let val =  Double(self.recom_courseData?.data?.recommend[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
            cell.starView.isUserInteractionEnabled = false
            
            if let desc = self.recom_courseData?.data?.recommend[indexPath.row].Description {
                cell.courseDiscLbl.text = desc
            }else{
                cell.courseDiscLbl.text = "Course Description"
            }
            if let image = self.recom_courseData?.data?.recommend[indexPath.row].thumbnails, let url = URL(string: image) {
                cell.courseImg.sd_setImage(with: url , placeholderImage: UIImage(named: "logo"), options: SDWebImageOptions.continueInBackground )
            }else {
                
                cell.courseImg.image = UIImage(named: "logo")
            }
            
        case .trendingCourse:
            cell.likeBtn.isHidden = true
            if let courseName = self.trend_obj?.data?.trending[indexPath.row].courseName {
                cell.courseNameLbl.text = courseName
            }else{
                cell.courseNameLbl.text = "Course Name"
            }
            let rating = Double(/self.trend_obj?.data?.trending[indexPath.row].rating)
            cell.ratingsLbl.text = String(rating)
            cell.starView.rating =  Double(rating)
            
            //            }else{
            //             cell.ratingsLbl.text = ""
            //            }
            // show starview
            let val =  Double(self.trend_obj?.data?.trending[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
            cell.starView.isUserInteractionEnabled = false
            
            if let desc = self.trend_obj?.data?.trending[indexPath.row].Description {
                cell.courseDiscLbl.text = desc
            }else
            {
                cell.courseDiscLbl.text = "Course Description"
            }
            if let image = self.trend_obj?.data?.trending[indexPath.row].thumbnails, let url = URL(string: image) {
                cell.courseImg.sd_setImage(with: url , placeholderImage: UIImage(named: "logo"), options: SDWebImageOptions.continueInBackground )
            }else {
                cell.courseImg.image = UIImage(named: "logo")
            }
        }
        return cell
    }
}

//MARK: COLLECTION VIEW DELEGATE
extension FriendsCollectionTableViewCell: UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let width =  friendsCollectionView.frame.width
            let size = width - 100
            return CGSize(width: friendsCollectionView.frame.width/2.5, height: 350)
        }
        else
        {
            return CGSize(width: friendsCollectionView.frame.width / 1.6, height: friendsCollectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

//MARK: Api

extension FriendsCollectionTableViewCell {
    func favooriteApi(id: String, state : CourseState, index : IndexPath) {
        var param : [String: Any] = [:]
        param["courseId"] = id
        ServiceRequest.instance.favoriteCourseApi(param){ (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
                return
            }
            if response?.Code == 1 { // for like
                switch state {
                case .none:
                    print("none")
                case .yourCourse:
                    self.courseData?.yourCourse?[index.row].isFavourite = true
                case .couese:
                    self.courseData?.course?[index.row].isFavourite = true
                case .favouriteCourse:
                    print("favorite like work")
                case .recommendedCourse:
                    print("favorite like work")
                    
                case .trendingCourse:
                    print("favorite like work")
                case .recommendedCoursesetdata:
                    print("favorite like work")
                case .popular:
                    print("favorite like work")
                }
                
            }else { // for unlike
                switch state {
                case .none:
                    print("none")
                case .yourCourse:
                    self.courseData?.yourCourse?[index.row].isFavourite = false
                case .couese:
                    self.courseData?.course?[index.row].isFavourite = false
                case .favouriteCourse:
                    print("favorite like work")
                case .recommendedCourse:
                    print("favorite like work")
                    
                case .trendingCourse:
                    print("favorite like work")
                case .recommendedCoursesetdata:
                    print("favorite like work")
                case .popular:
                    print("favorite like work")
                }
                
            }
            
            if let action = self.likeAction {
                action()
            }
            DispatchQueue.main.async {
                self.friendsCollectionView.reloadData()
            }
        }
    }
}
