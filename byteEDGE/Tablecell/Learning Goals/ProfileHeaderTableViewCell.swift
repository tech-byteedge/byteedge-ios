//
//  ProfileHeaderTableViewCell.swift
//  Betcha
//
//  Created by Gaurav_kumar on 24/06/21.
//

import UIKit
class ProfileHeaderTableViewCell: UITableViewCell {
    
    //MARK: - VARIABLES -
    var viewAllBtnTapped: (()->())?
    //MARK: - OUTLETS -
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var outerViewtop: NSLayoutConstraint!
    @IBOutlet weak var outerViewbottom: NSLayoutConstraint!
    @IBOutlet weak var headingLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var viewAll: UIButton!
    //MARK: - CELL LIFE CYCLE -
    override func awakeFromNib() {
        super.awakeFromNib()
        setcell()
    }
    //MARK: - ACTIONS -
    @IBAction func editBtn(_ sender: Any) {
        viewAllBtnTapped?()
    }
    //MARK: - FUNCTION -
    private func setcell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        ////outer view
//        outerView.makeRoundCornerwithborder(12.0, bordercolor: .appColor(.white), borderwidth: 1.0)
        outerView.backgroundColor = .clear
        ///heading Label
        headingLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.bold, size: .oneSeven))
        headingLbl.textAlignment = .left
        ///Edit button
        editBtn.setAppFontColor(.appColor(.white), font: .ProductSans(.medium, size: .oneSeven))
        editBtn.setNormalTitle(normalTitle: "View All")
        
        editBtn.titleLabel?.textAlignment = .center
       
    }
}
