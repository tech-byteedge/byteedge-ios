
//  StackWeekMonthYearTableViewCell.swift
//  byteEDGE
//  Created by gaurav on 21/01/22.

import UIKit
class StackWeekMonthYearTableViewCell: UITableViewCell {
    //MARK: -VARIABLES-
    var weekBtnAction : (()->())?
    var monthBtnAction : (()->())?
    var yearBtnAction : (()->())?
    
    //MARK: -OUTLETS-
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var WeekLbl: UILabel!
    @IBOutlet weak var weekDotImg: UIImageView!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var WeekBtn: UIButton!
    @IBOutlet weak var monthDotImg: UIImageView!
    @IBOutlet weak var monthlbl: UILabel!
    @IBOutlet weak var monthBtn: UIButton!
    @IBOutlet weak var yearDotImg: UIImageView!
    @IBOutlet weak var yearLbl: UILabel!
    
    @IBOutlet weak var yearBtn: UIButton!
    //MARK: -CELL LIFE CYCLE-
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnWeekBtn(_ sender: Any) {
        setLabelFont(selectedBtn: WeekLbl, unselectedBtn: monthlbl, unselectedBtn2: yearLbl)
        weekDotImg.isHidden = false
        monthDotImg.isHidden = true
        yearDotImg.isHidden = true
        setViewBackground(selectedView: weekView, unselectedview: monthView, unselectedView2: yearView)
        if let action = weekBtnAction {
            action()
        }
    }
    
      @IBAction func tappedOnMonthBtn(_ sender: Any) {
        setLabelFont(selectedBtn: monthlbl, unselectedBtn: WeekLbl, unselectedBtn2: yearLbl)
        weekDotImg.isHidden = true
        monthDotImg.isHidden = false
        yearDotImg.isHidden = true
        setViewBackground(selectedView: monthView, unselectedview: weekView, unselectedView2: yearView)
        if let action = monthBtnAction {
            action()
        }
    }
    
    @IBAction func tappedOnYearBtn(_ sender: Any) {
        setLabelFont(selectedBtn: yearLbl, unselectedBtn: WeekLbl, unselectedBtn2: monthlbl)
        weekDotImg.isHidden = true
        monthDotImg.isHidden = true
        yearDotImg.isHidden = false
         setViewBackground(selectedView: yearView, unselectedview: weekView, unselectedView2: monthView)
        if let action = yearBtnAction {
            action()
        }
    }
    
    
    //MARK: -FUNCTIONS-
    private func setCell() {
        self.backgroundColor = .clear
        selectionStyle = .none
        
        setLabelFont(selectedBtn: WeekLbl, unselectedBtn: monthlbl, unselectedBtn2: yearLbl)
        setViewBackground(selectedView: weekView, unselectedview: monthView, unselectedView2: yearView)
        WeekLbl.text = "Week"
        monthlbl.text = "Month"
        yearLbl.text = "Year"
        
        weekDotImg.isHidden = false
        monthDotImg.isHidden = true
        yearDotImg.isHidden = true
        weekView.layer.borderWidth = 1
        weekView.layer.borderColor = UIColor.appColor(.blackLight).cgColor
        monthView.layer.borderWidth = 1
        monthView.layer.borderColor = UIColor.appColor(.blackLight).cgColor
        yearView.layer.borderWidth = 1
        yearView.layer.borderColor = UIColor.appColor(.blackLight).cgColor
    }
    
    ///FUNCTION TO MANAGE SEGMENT BTN
    private func setLabelFont(selectedBtn: UILabel, unselectedBtn: UILabel, unselectedBtn2: UILabel) {
        selectedBtn.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        unselectedBtn.setAppFontColor(.appColor(.white, alpha: 0.64), font: .ProductSans(.regular, size: .oneSeven))
        unselectedBtn2.setAppFontColor(.appColor(.white,alpha: 0.64), font: .ProductSans(.regular, size: .oneSeven))
 
    }
    private func setViewBackground(selectedView: UIView, unselectedview: UIView, unselectedView2: UIView) {
        selectedView.backgroundColor = .appColor(.blackLight)
        unselectedview.backgroundColor = .black
        unselectedView2.backgroundColor = .black
 
    }
    
}
