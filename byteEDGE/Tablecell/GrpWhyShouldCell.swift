
import UIKit

class GrpWhyShouldCell: UITableViewCell {

    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var txtLbl: UILabel!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.iconView.makeRounded()
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
      
        super.setSelected(selected, animated: animated)

    }

}
