//
//  SubCriptionCell.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 02/03/22.
//

import UIKit

class SubCriptionCell: UITableViewCell{
    @IBOutlet weak var subCriptionView: UIView!
    @IBOutlet weak var subAmount: UILabel!
    @IBOutlet weak var monthplane: UILabel!
    @IBOutlet weak var selectedIcon: UIImage!
    
    @IBOutlet weak var IconImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
