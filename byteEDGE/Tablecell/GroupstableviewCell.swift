
import UIKit
import  Alamofire
import KRProgressHUD
import SwiftUI

protocol VcTransfer {
  func sendVc(index:Int,type:String?)
}

enum Cell{
 case description,DescWithImage,MultipleImage
}

class GroupstableviewCell: UITableViewCell,JoinDelegate {
    @IBOutlet weak var memberLbl: UILabel!
    private let itemsPerRow: CGFloat = 3
    var delegatevc : VcTransfer?
    var objgrouplist_get: DataofGroup?
    var type = ""
    var typereco = String()
    var selectedCell = Int()
    var selectedCallBack: (()->())?
    var isjoin = Bool()
    var delegate :JoinDelegate!
    
    @IBOutlet weak var collectionGroups: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionGroups.delegate = self
        self.collectionGroups.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        print(objgrouplist_get?.mayLikeGroup?.count)
        let collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   
    func join(index: Int, type: String) {
        print(collectionGroups.tag)
        delegate.join(index: index, type:type)
        print(index)
    }
}

extension GroupstableviewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if type ==  "My Groups"{
            return objgrouplist_get?.myGroup?.count ?? 0
            print(objgrouplist_get?.myGroup)
        }
        
        else if type == "Groups you may like"{
        return objgrouplist_get?.mayLikeGroup?.count ?? 0
        }
        else // popular
        {
        return objgrouplist_get?.popularGroup?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionGroups.dequeueReusableCell(withReuseIdentifier: "GroupCollectionCell", for: indexPath) as! GroupCollectionCell
        cell.objgrouplist_get = self.objgrouplist_get
        cell.imgCountCollection.tag = indexPath.row
        cell.typeofgroup = self.type
        cell.delegate = self
        
        if type == "My Groups"
        {
            cell.lblTxt.text =  objgrouplist_get?.myGroup?[indexPath.row].name
            cell.joinBtn.isHidden = true
            cell.joinBtn.isUserInteractionEnabled = false
            cell.memberLbl.text = "Member"
            cell.memberLbl.isHidden = false
            let urlImage = objgrouplist_get?.myGroup?[indexPath.row].image ?? ""
            cell.topImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named:"logo"), options: .highPriority, context: nil)
            
        
        }
        else if type == "Groups you may like"
        {
            print(objgrouplist_get?.mayLikeGroup?[indexPath.row].isJoin)
            cell.joinBtn.tag = indexPath.row
            
            if objgrouplist_get?.mayLikeGroup?[indexPath.row].isJoin == false  &&
            objgrouplist_get?.mayLikeGroup?[indexPath.row].joinStatus == "pending"
            {
                cell.joinBtn.isHidden = false
                cell.joinBtn.isUserInteractionEnabled = false
                cell.joinBtn.setTitle("Pending", for: .normal)
            }
            else if  objgrouplist_get?.mayLikeGroup?[indexPath.row].isJoin ==  false  &&
            objgrouplist_get?.mayLikeGroup?[indexPath.row].joinStatus == ""
            {
            cell.joinBtn.isHidden = false
            cell.joinBtn.isUserInteractionEnabled = true
            cell.joinBtn.setTitle("Join", for: .normal)
                }
            
            
            else if objgrouplist_get?.mayLikeGroup?[indexPath.row].isJoin ==  true  && objgrouplist_get?.mayLikeGroup?[indexPath.row].joinStatus == ""
            {
                cell.joinBtn.isHidden = true
                cell.joinBtn.isUserInteractionEnabled = true
            }
            else
            {
            }
                
                   
            cell.memberLbl.isHidden = true
            cell.memberLbl.text = ""
            cell.joinBtn.layer.cornerRadius = 15
            cell.lblTxt.text = objgrouplist_get?.mayLikeGroup?[indexPath.row].name
            let urlImage = objgrouplist_get?.mayLikeGroup?[indexPath.row].image ?? ""
            cell.topImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named:"logo"), options: .highPriority, context: nil)
        }
        
        else if type == "Most popular"
        {
            
            if objgrouplist_get?.popularGroup?[indexPath.row].isJoin == false  &&
                objgrouplist_get?.popularGroup?[indexPath.row].joinStatus == "pending"
                {
                    cell.joinBtn.isHidden = false
                    cell.joinBtn.isUserInteractionEnabled = false
                    cell.joinBtn.setTitle("Pending", for: .normal)
                
                }
                else if  objgrouplist_get?.popularGroup?[indexPath.row].isJoin ==  false  &&
                objgrouplist_get?.popularGroup?[indexPath.row].joinStatus == ""
                {
                    cell.joinBtn.isHidden = false
                    cell.joinBtn.isUserInteractionEnabled = true
                    cell.joinBtn.setTitle("Join", for: .normal)
                    }
            
                else if objgrouplist_get?.popularGroup?[indexPath.row].isJoin ==  true  && objgrouplist_get?.popularGroup?[indexPath.row].joinStatus == ""
                {
                    cell.joinBtn.isHidden = true
                    cell.joinBtn.isUserInteractionEnabled = true
                }
            
                else
                {
                    
                }
            
            cell.joinBtn.tag = indexPath.row
            cell.memberLbl.isHidden = true
            cell.memberLbl.text = ""
            cell.joinBtn.layer.cornerRadius = 15
            cell.lblTxt.text = objgrouplist_get?.popularGroup?[indexPath.row].name
        let urlImage = objgrouplist_get?.popularGroup?[indexPath.row].image ?? ""
            cell.topImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: "logo"), options: .highPriority, context: nil)
//            cell.topImg.layer.cornerRadius = 12
        }
        else
        {
            
        }
        cell.imgCountCollection.reloadData()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {

            let width =  collectionGroups.frame.width
            let size = width - 100
           return CGSize(width: collectionGroups.frame.width/2.5, height: 350)
            
//        let  wirth = ((collectionGroups.bounds.width - 10)/2.3)
//            return CGSize(width: collectionGroups.frame.width/1.6, height:collectionGroups.frame.height)
        }
        else
        {
        let  wirth = ((800)/2.5)
        return CGSize(width: collectionGroups.frame.width / 1.6, height:collectionGroups.frame.height)
        }
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }

    func collectionView(collectionView: UICollectionView, layout
                        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    delegatevc?.sendVc(index: indexPath.row, type: type)
    }
}

