//
//  EmptyTableViewCell.swift
//  Betcha
//
//  Created by gaurav on 13/10/21.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {
    
    //MARK: -OUTLETS-
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var desclbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /// Initialization code
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        outerView.backgroundColor = .clear
        desclbl.isHidden = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
