
import UIKit

protocol AcceptandsBlock{
    func acceptclick(at index:IndexPath,buttonType:String)
}

class RequestCell: UITableViewCell {
    @IBOutlet weak var nameReq: UILabel!
    @IBOutlet weak var imgReq: UIImageView!
    @IBOutlet weak var acceptReq: RoundedCornerbutton!
    @IBOutlet weak var moreOption: UIButton!
    @IBOutlet weak var rejectReq: RoundedCornerbutton!
    var moreoption: ((_ cellObj:UITableViewCell)->())?
    var accept:((_ index: Int)->())?
    var reject:((_ index: Int)->())?
    var indexPath:IndexPath!
    var delegate:AcceptandsBlock!
    
    override func awakeFromNib() {
        imgReq.image = UIImage(named: "Ind")
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func moreoptionClick(_ sender: UIButton) {
        self.moreoption?(sender.superview?.superview?.superview as! UITableViewCell)
    }
    
    @IBAction func acceptBtn(_sender: UIButton)
    {
    self.delegate?.acceptclick(at: indexPath, buttonType:"Accept")
    }
    
    @IBAction func rejectBtn(_sender: UIButton)
    {
    self.delegate?.acceptclick(at: indexPath, buttonType:"Block")
    }

}
