
//  ListCell.swift
//  byteEDGE
//  Created by Pawan Kumar on 19/01/22.

import UIKit
class ListCell: UITableViewCell ,UITextViewDelegate{
    @IBOutlet weak var listtxtview: GrowingTextView!
    var isAbout = false
    var callBAck:((_ data: String?,_ isAbout:Bool?)->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        listtxtview.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        self.callBAck?(textView.text ?? "", isAbout)
    }
}




