
import UIKit

class CreateGroupsCellTableViewCell: UITableViewCell {
    
    //@IBOutlet weak var emptyImg: UIImageView!
//    /@IBOutlet weak var createBTN: UIButton!
    @IBOutlet weak var heightCreate: NSLayoutConstraint!
    @IBOutlet weak var createGroupbtn: UIButton!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var createGroupBtn: UIButton!
    
    var creatAlert: (()->())?
       override func awakeFromNib() {
        super.awakeFromNib()
           viewTop.layer.borderWidth = 1
           viewTop.layer.borderColor = UIColor.init(hexString: "#707070", alpha: 0.36).cgColor
        self.createGroupbtn.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
           self.createGroupBtn.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
           self.createGroupBtn.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    }
    
    @IBAction func createAction(_ sender: Any) {
        self.creatAlert?()
    }
    @IBAction func createGroupAction(_ sender: Any) {
        self.creatAlert? ()
    }
}
