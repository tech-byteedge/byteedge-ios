
import UIKit
class CommonButtonView: UIView {
    
    enum VCState{
    case LogoutpopUpVc, community,GroupDetails,EditgroupDetails,groupFeeds
    }
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    // Label
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var secondName: UILabel!
    @IBOutlet weak var thirdName: UILabel!
    //View
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var firstView: UIView!
    
    //image
    @IBOutlet weak var img1st: UIImageView!
    @IBOutlet weak var img2nd: UIImageView!
    @IBOutlet weak var img3rd: UIImageView!
    var firstCallBack: (()->())?
    var secondCallBack: (()->())?
    var thirdCallBack: (()->())?
    var selected = Int()
    
    func initSetup(_ state: VCState) {
        switch state {
            
    
         case .community:
            firstName.text = "Feeds"
            secondName.text = "Groups"
            thirdView.isHidden = true
            secondView.isHidden = false
            firstView.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            secondView.backgroundColor = #colorLiteral(red: 0.1461485922, green: 0.1526356339, blue: 0.1830024123, alpha: 1)
            secondName.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            secondName.textColor = .white
            img1st.isHidden = true
            img2nd.isHidden = false
            
            break
            
            case .groupFeeds:
                secondName.text = "Feeds"
                thirdView.isHidden = true
                secondView.isHidden = false
                firstView.backgroundColor = #colorLiteral(red: 0.1461485922, green: 0.1526356339, blue: 0.1830024123, alpha: 1)
                secondView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                firstName.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                firstName.textColor = .white
                img1st.isHidden = false
                img2nd.isHidden = true
              break
            
            case .LogoutpopUpVc:
            firstName.text = "No"
            secondName.text = "Yes"
            firstName.textColor = .white
            secondName.textColor = .white
            thirdView.isHidden = true
            firstView.backgroundColor = #colorLiteral(red: 0.1461495757, green: 0.1526317596, blue: 0.183000803, alpha: 1)
            secondView.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            img1st.isHidden = true
            img2nd.isHidden = true
            break
            
        case .GroupDetails:
            firstName.text = "Cancel"
            secondName.text = "Join"
            firstName.textColor = .white
            secondName.textColor = .white
            thirdView.isHidden = true
            firstView.backgroundColor = #colorLiteral(red: 0.1461495757, green: 0.1526317596, blue: 0.183000803, alpha: 1)
            secondView.backgroundColor = #colorLiteral(red: 0.01176470588, green: 0.7333333333, blue: 0.2784313725, alpha: 1)
            img1st.isHidden = true
            img2nd.isHidden = true
            
            case .EditgroupDetails:
            firstName.text = "About"
            secondName.text = "Request"
            thirdName.text = "Members"
            firstName.textColor = .white
            secondName.textColor = .gray
            thirdName.textColor = .gray
            firstView.backgroundColor = #colorLiteral(red: 0.1461485922, green: 0.1526356339, blue: 0.1830024123, alpha: 1)
            secondView.backgroundColor = .black
            thirdView.backgroundColor = .black
            img1st.isHidden = false
            img2nd.isHidden = true
            img3rd.isHidden = true
            
        }
    }
    
    
    @IBAction func firstClick(_ sender: UIButton) {
        self.firstCallBack?()
        self.selected = 0
    }
    
    @IBAction func secondClick(_ sender: UIButton) {
        self.secondCallBack?()
        self.selected = 1
    }
    
    
    @IBAction func thirdClik(_ sender: UIButton) {
        self.thirdCallBack?()
        self.selected = 2
    }
    
}

