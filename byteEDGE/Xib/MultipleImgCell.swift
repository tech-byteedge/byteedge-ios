//
//  MultipleImgCell.swift
//  byteEDGE
//  Created by Pawan Kumar on 27/12/21.

import UIKit

class MultipleImgCell: UITableViewCell {
    
    var commentsAction : (()->())?
    var likeBtnAciton: ((Bool)-> ())?
    var share : (()->())?
    
    
    @IBOutlet weak var likecommentheight: NSLayoutConstraint!
    @IBOutlet weak var linelbl: UILabel!
    @IBOutlet weak var commetnBtn: UIButton!
    @IBOutlet weak var stackViewImg: UIStackView!
    @IBOutlet weak var singleImg: UIImageView!
    @IBOutlet weak var secodImg: UIImageView!
    @IBOutlet weak var twoImgStackView:
        UIStackView!
    @IBOutlet weak var multiDES: UILabel!
    @IBOutlet weak var multiProfileImage: UIImageView!
    @IBOutlet weak var multiName: UILabel!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var thirdImg: UIImageView!
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var fourthImage: UIImageView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var btnFeedOptions: UIButton!
    
    @IBOutlet weak var viewBiottom: UIView!
    @IBOutlet weak var vedioPLAY: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var countLbl: UILabel!
    var selectedCell = Int()
    @IBOutlet weak var likeBtn: UIButton!

    var callBackvedio:(()->())?
    var deleteOption:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        multiProfileImage.clipsToBounds = true
        multiProfileImage.makeRounded()
        likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
    }
    @IBAction func likeBtn(_ sender: Any) {
        UIApplication.topViewController()?.view.isUserInteractionEnabled = false
        if likeBtn.imageView?.image == UIImage(named: "heart") {
           
            likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
            UIApplication.topViewController()?.view.isUserInteractionEnabled = true
            if let action = likeBtnAciton {
                            action(false)
                
                        }
           
        }else {
            likeBtn.setImage(UIImage(named: "heart"), for: .normal)
            UIApplication.topViewController()?.view.isUserInteractionEnabled = true
//            likeBtn.isUserInteractionEnabled = false
            if let action = likeBtnAciton {
                           action(true)
                       }
        }

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func vedioClick(_ sender: UIButton) {
        self.callBackvedio?()
    }
    
    @IBAction func commentBtn(_ sender: Any) {
        if let action = commentsAction {
            action()
        }
    }
    
    @IBAction func actionOptionsFeed(_ sender: UIButton) {
        self.deleteOption?()
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
    self.share?()
    }
    
}
