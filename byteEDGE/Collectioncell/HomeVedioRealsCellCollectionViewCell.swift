//
//  HomeVedioRealsCellCollectionViewCell.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 01/04/22.

import UIKit
protocol BookMarkStatus {
    func status(int:Int)
}

class HomeVedioRealsCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var sharebtn: UIButton!
    @IBOutlet weak var vedioImgView: UIImageView!
    @IBOutlet weak var centreBtn: UIButton!
    @IBOutlet weak var counlikeLbl: UILabel!
    @IBOutlet weak var courseHeadinglbl: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnReadMore: UIButton!
    @IBOutlet weak var bookmarkTap: UIButton!
    
    @IBOutlet weak var captionTap: UIButton!
    @IBOutlet weak var btnclick: UIButton!
    @IBOutlet weak var heartbtn: UIButton!
    var callBackofCentreBtn : (()->())?
    var callBackofTreasure : (()->())?
    var callBackofLike : (()->())?
    var callBackbookmark: (()->())?
    var back: (()->())?
    var bookMarkDelegte: BookMarkStatus!
    var callBackofsharebtn : (()->())?
    var selectedAction : (()->())?
    var callBackReadMore : ((UIButton) -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.sharebtn.addBlurEffect()
        self.sharebtn.makeRounded()
        self.bookmarkTap.makeRounded()
        self.bookmarkTap.addBlurEffect()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
        self.sharebtn.makeRounded()
    }
    }
   
    @IBAction func btnReadMOre(_ sender: UIButton) {
    self.callBackReadMore?(sender)
    }
    
    @IBAction func bookarkClick(_ sender: UIButton) {
        self.callBackbookmark?()
    }
    
    @IBAction func tapOncentrebtn(_ sender: Any) {
        if let action = callBackofCentreBtn {
            action()
        }
    }
    
    @IBAction func clickonTreasure(_ sender: UIButton) {
        self.callBackofTreasure?()
        self.selectedAction?()
    }
    
    @IBAction func likeTap(_ sender: UIButton) {
        
        if let action = callBackofLike {
            action()
        }
    }
    
    
    
    @IBAction func shareBtn(_ sender: UIButton) {
        if let action = callBackofsharebtn {
            action()
        }
    }
}

extension UIButton {
    func addBlurEffect(style: UIBlurEffect.Style = .regular, cornerRadius: CGFloat = 0, padding: CGFloat = 0) {
        backgroundColor = .clear
        let blurView = UIVisualEffectView(effect: UIBlurEffect(style: style))
        blurView.isUserInteractionEnabled = false
        blurView.backgroundColor = .clear
        if cornerRadius > 0 {
            blurView.layer.cornerRadius = cornerRadius
            blurView.layer.masksToBounds = true
        }
        
        self.insertSubview(blurView, at: 0)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: blurView.leadingAnchor, constant: padding).isActive = true
        self.trailingAnchor.constraint(equalTo: blurView.trailingAnchor, constant: -padding).isActive = true
        self.topAnchor.constraint(equalTo: blurView.topAnchor, constant: padding).isActive = true
        self.bottomAnchor.constraint(equalTo: blurView.bottomAnchor, constant: -padding).isActive = true

        if let imageView = self.imageView {
            imageView.backgroundColor = .clear
            self.bringSubviewToFront(imageView)
        }
    }
}


