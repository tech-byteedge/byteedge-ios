//
//  ShareCollectionViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 03/02/22.
//

import UIKit

class ShareCollectionViewCell: UICollectionViewCell {

    //MARK: - OUTLETS -
 
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var outerView: UIView!
    //MARK: - CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    //MARK: - PRIVATE FUNCTION -
    private func setCell() {
        self.backgroundColor = .clear
        outerView.backgroundColor = .clear
        iconImg.backgroundColor = .clear

       
}
}
