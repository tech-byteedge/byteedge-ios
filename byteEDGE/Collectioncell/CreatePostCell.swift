//
//  CreatePostCell.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 04/02/22.
//
import UIKit
class CreatePostCell: UICollectionViewCell {
    
    //MARK: - Property's
    var delegate: MyProtocol?
    
    //MARK: - Outlet's
    @IBOutlet weak var AddImage: UIImageView!
    @IBOutlet weak var removeBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initSetup()
    }
    
    @IBAction func btnRemove(_ sender: UIButton) {
        delegate?.delete(type: sender.tag)
    }
}

extension CreatePostCell{
    private func initSetup(){
        AddImage.layer.cornerRadius = 8
    }
}

protocol MyProtocol {
    func delete(type: Int)
}

