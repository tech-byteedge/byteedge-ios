
import UIKit
import KRProgressHUD
import Alamofire
protocol JoinDelegate
{
 func join(index:Int,type: String)
}

class GroupCollectionCell: UICollectionViewCell {
    @IBOutlet weak var topImg: UIImageView!
    @IBOutlet weak var lblTxt: UILabel!
    @IBOutlet weak var imgCountCollection: UICollectionView!
    @IBOutlet weak var joinBtn: UIButton!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    var  delegate:JoinDelegate!
       var section = Int()
       let imageCount = 4
       var typeofgroup = ""
       var typeofgroupS  = Int()
       var objgrouplist_get: DataofGroup?{
        didSet{
        imgCountCollection.reloadData()
        }
       }
       override func awakeFromNib() {
       super.awakeFromNib()
        imgCountCollection.delegate = self
        imgCountCollection.dataSource = self
           if UIDevice.current.userInterfaceIdiom == .pad {
//               self.viewHeight.constant = 550
           }
               else
               {
//                self.viewHeight.constant = 175
               }
     
    }
    
    @IBAction func joinTap(_sender:UIButton)
    {
    delegate.join(index:_sender.tag, type: self.typeofgroup)
    }
}

// Cell of MultipleImg Show
extension GroupCollectionCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
     return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if typeofgroup == "My Groups"
    {
        if objgrouplist_get?.myGroup?[imgCountCollection.tag].memberDetails?.count ?? 0 <= 3
        {
         return   objgrouplist_get?.myGroup?[imgCountCollection.tag].memberDetails?.count ?? 0
        }
        else
        {
         return 3
        }
    
    return   objgrouplist_get?.myGroup?[imgCountCollection.tag].memberDetails?.count ?? 0
    }
        
    else if typeofgroup == "Groups you may like"
    {
        if objgrouplist_get?.mayLikeGroup?[imgCountCollection.tag].memberDetails?.count ?? 0 <= 3
        {
         return   objgrouplist_get?.mayLikeGroup?[imgCountCollection.tag].memberDetails?.count ?? 0
        }
        else
        {
         return 3
        }
    }
        
    else if typeofgroup == "Most popular"
    {
        if objgrouplist_get?.popularGroup?[imgCountCollection.tag].memberDetails?.count ?? 0 <= 3
        {
        return   objgrouplist_get?.popularGroup?[imgCountCollection.tag].memberDetails?.count ?? 0
        }
        else
        {
         return 5
        }
    }
        
    else
    {
       return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.imgCountCollection.dequeueReusableCell(withReuseIdentifier: "CountImgShowCell", for: indexPath) as! CountImgShowCell
        
        cell.firstIMG.maskCircle()
        if typeofgroup == "My Groups"
        {
        var urlImage = objgrouplist_get?.myGroup?[imgCountCollection.tag].memberDetails?[indexPath.row].memberImage ?? ""
        cell.firstIMG.sd_setImage(with: URL(string: urlImage), placeholderImage:UIImage(named:"grpIcon"), options: .highPriority, context: nil)
      
        }
        else if typeofgroup ==  "Groups you may like"
        {
        var urlImage = objgrouplist_get?.mayLikeGroup?[imgCountCollection.tag].memberDetails?[indexPath.row].memberImage ?? ""
        cell.firstIMG.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
        }
        else if typeofgroup == "Most popular"
        {
        var urlImage = objgrouplist_get?.popularGroup?[imgCountCollection.tag].memberDetails?[indexPath.row].memberImage ?? ""
        cell.firstIMG.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named:"grpIcon") , options: .highPriority, context: nil)
        cell.firstIMG.maskCircle()
        }
        
        let totalRow = imgCountCollection.numberOfItems(inSection: indexPath.section)
        if(indexPath.row == totalRow - 1)
        {
            
        }
        else
        {
        }
         return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return -6.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0.0
    }
}

// Class of MultipleImg Show
class CountImgShowCell: UICollectionViewCell
{
    @IBOutlet weak var firstIMG: UIImageView!
    @IBOutlet weak var countap: UIButton!
    var typeimg = ""
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

