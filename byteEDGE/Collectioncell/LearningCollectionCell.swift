//
//  LearningCollectionCell.swift
//  byteEDGE
//  Created by Pawan Kumar on 28/02/22.

import UIKit
class LearningCollectionCell: UICollectionViewCell {
    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak  var discriptionLbl: UILabel!
    @IBOutlet weak var ratingCollectionViewlearningAll: UICollectionView!
    @IBOutlet weak var  ratingLblView: UIView!
    @IBOutlet weak var  ratingsLbl: UILabel!
    @IBOutlet weak var  ratingsOuterView: UIView!
    @IBOutlet weak var  outerView: UIView!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
//        self.titleImage.layer.cornerRadius = 10
    }
    
    private func setCell() {
        self.backgroundColor = .clear
        outerView.backgroundColor = .clear
        ///register collection cell
        ratingCollectionViewlearningAll.registerCell(type: RatingStarCollectionViewCell.self)
        ratingCollectionViewlearningAll.backgroundColor = .clear
        ratingCollectionViewlearningAll.showsVerticalScrollIndicator = false
        ratingCollectionViewlearningAll.showsHorizontalScrollIndicator = false
        ratingCollectionViewlearningAll.isScrollEnabled = true
        ///connectors collection view
        ratingCollectionViewlearningAll.delegate = self
        ratingCollectionViewlearningAll.dataSource = self
        
//        ratingsOuterView.backgroundColor = .clear
        ratingLblView.addGradientBackground(firstColor: .appColor(.orangeLight), secondColor: .appColor(.orangeDark))
        ratingLblView.makeRoundCornerwithborder(9, bordercolor: .appColor(.white), borderwidth: 0.5)
        ratingsLbl.setAppFontColor(.appColor(.black), font: .ProductSans(.regular, size: .oneOne))
        ratingsLbl.backgroundColor = .clear
        ratingsLbl.text = "4"
        
//        likeBtn.isHidden = true
        
    }
}

extension LearningCollectionCell: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(withType: RatingStarCollectionViewCell.self, for: indexPath) as? RatingStarCollectionViewCell else {
            fatalError("RatingStarCollectionViewCell is not initialize")
        }
        indexPath.row == 4 ?  (cell.starImg.image = UIImage(named: "starWhite")) :  (cell.starImg.image = UIImage(named: "starColor"))

        return cell
    }
}
// MARK: - COLLECTIONVIEW DELEGATE FLOW LAYOUT -
extension LearningCollectionCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        return CGSize(width: 16, height: 16)  //height 74
    }
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 0.10
    }
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return 1.0 //-74.0/2
    }
}


