//
//  RatingStarCollectionViewCell.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.
//

import UIKit
import Cosmos

class RatingStarCollectionViewCell: UICollectionViewCell {
    //MARK: - OUTLETS -
 
    @IBOutlet weak var starImg: UIImageView!
    @IBOutlet weak var ratingCosmosView: CosmosView!
    //MARK: - CELL LIFE CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    //MARK: - PRIVATE FUNCTION -
    private func setCell() {
//        ratingCosmosView.totalStars = 5
        self.backgroundColor = .clear
//        self.contentView.makeRoundCornerwithborder(<#T##radius: CGFloat##CGFloat#>, bordercolor: <#T##UIColor#>, borderwidth: <#T##CGFloat#>)
    }
}
