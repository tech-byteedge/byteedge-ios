
//  CourseCollectionViewCell.swift
//  byteEDGE
//  Created by gaurav on 28/01/22.


import UIKit
import Cosmos
class CourseCollectionViewCell: UICollectionViewCell {
    
    //MARK: -VARIABLES-
    //MARK: - OUTLETS-
    
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var ratingsLbl: UILabel!
    @IBOutlet weak var ratingsOuterView: UIView!
    @IBOutlet weak var courseDiscLbl: UILabel!
    @IBOutlet weak var courseNameLbl: UILabel!
    @IBOutlet weak var courseImg: UIImageView!
    @IBOutlet weak var ratingLblView: UIView!
    @IBOutlet weak var ratingCollectionView: UICollectionView!
    //MARK: - CELL LIFE CYCLE
    
    var callBackOflike: (()->())?
    var viewlearnig :LearningListViewAll?
    var typeCourse = Int()
    var cellState : CourseState = .none
    override func awakeFromNib() {
        super.awakeFromNib()
        setCell()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnLikeBtn(_ sender: Any) {
     self.callBackOflike?()
//        let nc = NotificationCenter.default
//        nc.post(name: Notification.Name("UserLoggedIn"), object: nil)
    }
    
    //MARK: - PRIVATE FUNCTION -
    private func setCell() {
        self.backgroundColor = .clear
       // outerView.backgroundColor = .clear
        ratingsLbl.makeRoundCornerwithborder(9, bordercolor: .orange, borderwidth: 1)
        ///set course Name
        courseNameLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.semibold, size: .oneSix))
        courseDiscLbl.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneThree))
   
    }
}
