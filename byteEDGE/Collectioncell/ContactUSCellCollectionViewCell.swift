
//  ContactUSCellCollectionViewCell.swift
//  byteEDG
//  Created by Pawan Kumar on 09/03/22.

import UIKit
protocol Myprotocol
{
 func delete(type: Int)
}

class ContactUSCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var removeImg: UIButton!
    var delegate : Myprotocol?
    override  func awakeFromNib()
    {
        super.awakeFromNib()
        self.initSetup()
    }
    
    @IBAction func removeImg(_ sender:UIButton)
    {
     delegate?.delete(type: sender.tag)
    }
}

extension ContactUSCellCollectionViewCell{
    private func initSetup(){
        imgContact.layer.cornerRadius = 12
    }
}


