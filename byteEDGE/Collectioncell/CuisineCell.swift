//
//  CuisineCell.swift
//  Ride Chef
//
//  Created by Aaditya on 21/01/22.
//

import UIKit

protocol JoinDelegateofViewAll
{
 func joinIndex(index:Int,type:Int)
}

class CuisineCell: UICollectionViewCell {
    //MARK: - IBOutlets
    
    @IBOutlet weak var thirdImg: UIImageView!
    @IBOutlet weak var secondImg: UIImageView!
    @IBOutlet weak var firstImg: UIImageView!
    @IBOutlet weak var imgStackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!    
    @IBOutlet weak var joinTap: UIButton!
    var delegateofViewAll :JoinDelegateofViewAll?
    var typeOfviewAll = Int()
    override func awakeFromNib() {
        super.awakeFromNib()

        titleLabel.setAppFontColor(.appColor(.white), font: .ProductSans(.bold, size: .oneFive))
//        self.titleImage.layer.cornerRadius = 12
    }
    
    @IBAction func joinViewAll(_sender: UIButton) {
        delegateofViewAll?.joinIndex(index:_sender.tag,type:typeOfviewAll)
    }
}
