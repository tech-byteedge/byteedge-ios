//
//  ServiceRequest.swift
//  byteEDGE
//  Created by gaurav on 27/01/22.

import Foundation
import Alamofire
import UIKit


class ServiceRequest {
    static let instance = ServiceRequest()
    
    // MARK: -LIKE Post API -
       func likeCommentReply(
        _ param :[String: Any],
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: CommonResponseModel?, _ errorMsg: String? )-> Void
    )
    {
        ServiceManager.instance.request(
            method: .post,
            apiURL: Api.likeCommentReply.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: CommonResponseModel.self
        ) { (status, json, model, error) in
            completionHandler(status, model, error)
        }
    }
    
    
    func addBookMark(
     _ param :[String: Any],
     completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: CommonResponseModel?, _ errorMsg: String? )-> Void
  )
   {
     ServiceManager.instance.request(
         method: .post,
         apiURL: Api.addBookmark.url,
         headers: CommonUtility.instance.headers,
         parameters: param,
         decodable: CommonResponseModel.self
     ) { (status, json, model, error) in
         completionHandler(status, model, error)
     }
  }
    
    
    func learningList(
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: LearningListModel?, _ errorMsg: String? )-> Void
    ){
//        Loader.isLoader(show: false)
         ServiceManager.instance.request(
            method: .get,
            apiURL: Api.learningList.url,
            headers: CommonUtility.instance.headers,
            parameters: nil,
            decodable: LearningListModel.self
        ) { (status, json, model, error) in
//            Loader.isLoader(show: false)
            completionHandler(status, model, error)
        }
    }

    func recomCourse(
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: RecommnndedModel?, _ errorMsg: String? )-> Void
    ){
//        Loader.isLoader(show: false)
         ServiceManager.instance.request(
            method: .get,
            apiURL: Api.recommended.url,
            headers: CommonUtility.instance.headers,
            parameters: nil,
            decodable: RecommnndedModel.self
        ) { (status, json, model, error) in
//            Loader.isLoader(show: false)
            completionHandler(status, model, error)
        }
    }
    
    //popular.......................
    func popularCourse(
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: Popular?, _ errorMsg: String? )-> Void
    ){
//        Loader.isLoader(show: false)
         ServiceManager.instance.request(
            method: .get,
            apiURL: Api.PopularCourse.url,
            headers: CommonUtility.instance.headers,
            parameters: nil,
            decodable: Popular.self
        ) { (status, json, model, error) in
//            Loader.isLoader(show: false)
            completionHandler(status, model, error)
        }
    }
    
    
    
    
    
    
    
    func trendingcourse(
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: TrendingModel?, _ errorMsg: String? )-> Void
    ){
         ServiceManager.instance.request(
            method: .get,
            apiURL: Api.trending.url,
            headers: CommonUtility.instance.headers,
            parameters: nil,
            decodable: TrendingModel.self
        ) { (status, json, model, error) in
            completionHandler(status, model, error)
        }
    }
    
    
    func coursePreviewDetail(
        _ param :[String: Any],
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: CoursePreviewDetailModel?, _ errorMsg: String? )-> Void
    ){
        Loader.isLoader(show: true)
        ServiceManager.instance.request(
            method: .get,
            apiURL: Api.coursePreviewDetail.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: CoursePreviewDetailModel.self,
            encoding: URLEncoding.queryString
        ) { (status, json, model, error) in
            Loader.isLoader(show: false)
            completionHandler(status, model, error)
        }
    }
    
    func categoryList(
        _ param :[String: Any],
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: CategoryListModel?, _ errorMsg: String? )-> Void
    ){
        Loader.isLoader(show: true)
        ServiceManager.instance.request(
            method: .get,
            apiURL: Api.categoryList.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: CategoryListModel.self,
            encoding: URLEncoding.queryString
        ) { (status, json, model, error) in
            Loader.isLoader(show: false)
            
            completionHandler(status, model, error)
        }
    }
    //MARK: -POST DETAIL-
    func postDetail(
        _ param :[String: Any],
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: PostDetailModel?, _ errorMsg: String? )-> Void
    ){
        Loader.isLoader(show: true)
        ServiceManager.instance.request(
            method: .get,
            apiURL: Api.postDetail.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: PostDetailModel.self,
            encoding: URLEncoding.queryString
        ) { (status, json, model, error) in
            Loader.isLoader(show: false)
            
            completionHandler(status, model, error)
        }
    }
    
    //MARK: UpdateProfile -
    func updateProfile(
        _ param:[String:Any],
        completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: UpdateProfile?, _ errorMsg: String? )-> Void
    ){
        Loader.isLoader(show: true)
        ServiceManager.instance.request(
            method: .post,
            apiURL: Api.updateProfile.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: UpdateProfile.self,
            encoding: JSONEncoding.default
        ) { (status, json, model, error) in
            Loader.isLoader(show: false)
            completionHandler(status, model, error)
        }
    }
    
    //MARK: -ADD POST API-
        func addPost(
            _ param :[String: Any],
            imagArry: [UIImage],
            videoURL: URL?,
            thumbnails:UIImage,
            completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: commonResonseModel?, _ errorMsg: String? )-> Void
        ){
            
            Loader.isLoader(show: true)
            ServiceManager.instance.uploadImageArraandVedioyRequestWithDecodable(
                method: .post,
                apiURL: Api.createpost.url,
                headers: CommonUtility.instance.headers,
                parameters: param,
                decodable: commonResonseModel.self,
                imgArray: imagArry,
                videoURL: videoURL,
                pdfURL: nil,
                thumbnails:thumbnails
            ) { (isSucess, response, error) in
                
                completionHandler(isSucess, response, error)
                Loader.isLoader(show: false)
            }
        }
    
    
    
    //MARK: -ADD POST API-
        func contactUs(
            _ param :[String: Any],
            imagArry: [UIImage],
            videoURL: URL?,
            thumbnails:UIImage,
            completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: commonResonseModel?, _ errorMsg: String? )-> Void
        ){
            
            Loader.isLoader(show: true)
            ServiceManager.instance.uploadImageArraandVedioyRequestWithDecodable(
                method: .post,
                apiURL: Api.contactUs.url,
                headers: CommonUtility.instance.headers,
                parameters: param,
                decodable: commonResonseModel.self,
                imgArray: imagArry,
                videoURL: nil,
                pdfURL: nil,
                thumbnails:nil
            ) { (isSucess, response, error) in
                
                completionHandler(isSucess, response, error)
                Loader.isLoader(show: false)
            }
        }
    
    
    
    
    
    
    
    

//MARK: Favaroute Api
func favoriteCourseApi(
    _ param :[String: Any],
    completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel:FavouriteCourse?, _ errorMsg: String? )-> Void
){
//    Loader.isLoader(show: false)
    ServiceManager.instance.request(
        method: .post,
        apiURL: Api.favouriteCourse.url,
        headers: CommonUtility.instance.headers,
        parameters: param,
        decodable: FavouriteCourse.self
    ) { (status, json, model, error) in
//      Loader.isLoader(show: false)
        completionHandler(status, model, error)
    }
}
    
    func DeleteFeedApi(
     _ param :[String: Any],
     completionHandler: @escaping( _ isSucess: Bool?,  _ responseModel: CommonResponseModel?, _ errorMsg: String? )-> Void
 )
 {
     ServiceManager.instance.request(
         method: .post,
         apiURL: Api.DeleteFeed.url,
         headers: CommonUtility.instance.headers,
         parameters: param,
         decodable: CommonResponseModel.self
     ) { (status, json, model, error) in
         completionHandler(status, model, error)
     }
 }
    
}
