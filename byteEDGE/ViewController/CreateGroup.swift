
import UIKit
import Alamofire
import KRProgressHUD
import  Photos
import IQKeyboardManagerSwift

class CreateGroup: UIViewController,UITextViewDelegate {
    //MARK: -VARIABLES-
    
    var groupImage: UIImage?
    var refershCallBack : (()->())?
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var uploadImgTap: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var grpTxt: UITextField!
    @IBOutlet weak var abtTxt: UITextField!
    @IBOutlet weak var txtwhyshold: LimitedLengthField!
    @IBOutlet weak var createClick: UIButton!
    @IBOutlet weak var crtTbl: UITableView!
    @IBOutlet weak var heightCreateview: NSLayoutConstraint!
//    @IBOutlet weak var  submitBtn: UIButton!
    
    var maxLength: Int = 10
    var placeholderLabel : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
    }
    
    private func setui()
    {
       self.grpTxt.attributedPlaceholder = NSAttributedString(string: "Enter Group name",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.abtTxt.attributedPlaceholder = NSAttributedString(string: "Write here...",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.txtwhyshold.attributedPlaceholder = NSAttributedString(string: "Write here...",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.crtTbl.delegate = self
        self.crtTbl.dataSource = self
        self.uploadImgTap.layer.borderWidth = 1.2
        self.uploadImgTap.layer.cornerRadius = 20
        self.uploadImgTap.layer.borderColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [255.0, 255.0, 255.0, 1.0])

    }
    

//     
//    func addplaceholder()
//    {
//        placeholderLabel = UILabel()
//        placeholderLabel.text = "Write something here......"
//        placeholderLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
//        placeholderLabel.sizeToFit()
//        txtwhyshold.placeholder = "Write something here......"
////        txtView_WhyShould.addSubview(placeholderLabel)
//        placeholderLabel.frame.origin = CGPoint(x: 5, y: (txtwhyshold.font?.pointSize)! / 2)
//        placeholderLabel.textColor = UIColor.lightGray
//        placeholderLabel.isHidden = !(txtwhyshold.text?.isEmpty ?? "")
//    }
    
    override func viewWillLayoutSubviews() {
    self.createClick.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    @IBAction func backTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func clickImg(_ sender: Any) {
            let photos = PHPhotoLibrary.authorizationStatus()
                if photos == .notDetermined {
                    PHPhotoLibrary.requestAuthorization({status in
                        if status == .authorized{
                        print("OKAY")
                        } else {
                        print("NOTOKAY")
                        }
                    })
                }
                checkLibrary()
                checkPermission()
    }
    
    @IBAction func createClick(_ sender: UIButton) {
        if validation()
        {
            DispatchQueue.main.async {
            self.createGroup()
            }
        }
        else
        {
        createAlert(title: AppName, message: kWentWrongKey)
        }
    }
}

extension CreateGroup: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  900
    }
}

extension CreateGroup:UITextFieldDelegate
{
    func validation() -> Bool{
        
        if (imgView?.image == nil)
        {
            createAlert(title: AppName, message: "please selecte Image")
            return false
        }
        
        guard let first = grpTxt.text, !first.isBlank else{
            createAlert(title: AppName, message: "Please Enter Group Name")
            return false
        }
        guard let last = abtTxt.text, !last.isBlank else{
            createAlert(title: AppName, message: "Enter About Group")
            return false
        }
        guard let last = txtwhyshold.text, !last.isBlank else{
            createAlert(title: AppName, message: "please Enter Reason")
            return false
        }
        if(groupImage == nil){
            createAlert(title: AppName, message: kAlertImage)
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == grpTxt) {
            abtTxt.becomeFirstResponder()
            return false
        }
        if(textField == abtTxt) {
            txtwhyshold.becomeFirstResponder()
            return false
        }
        return true
    }
}

//MARK: api

extension CreateGroup
{
    func createGroup() {
        var param: [String: Any] = [:]
        param["name"] = self.grpTxt.text
        param["about"] = self.abtTxt.text
        param["joinReason"] = self.txtwhyshold.text
        
        var imgParam: [String : UIImage] = [:]
        if let img = self.groupImage {
        imgParam["image"] = img
        }
        KRProgressHUD.show()
        ServiceManager.instance.uploadImageRequestWithDecodable(
            method: .post,
            apiURL: Api.createGroup.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: commonResonseModel.self,
            imageParameters: imgParam) { success, response, Error in
                KRProgressHUD.dismiss()
                guard response?.Status == 200 else {
                    self.createAlert(title: kAppName, message: response?.message ?? "")
                    return}
                self.createAlertCallbackpop(title: AppName, message:response?.message ?? "") { (value)in
                     if value == true
                     {
//                    NotificationCenter.default.addObserver(self, selector: #selector(self.updateGrouplist(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
//
                         
                         NotificationCenter.default.post(name:NSNotification.Name("GrouupUpdate"),
                                                         object: nil)
                                     
                         
                    self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                     print("Nothing")
                    }
                }
                self.navigationController?.popViewController(animated: false)
            }
    }
}

extension CreateGroup: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UNUserNotificationCenterDelegate
{
    func displayUploadImageDialog() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        let alertController = UIAlertController(title: "", message: "Upload profile photo?".localized(), preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel action"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            alertController.dismiss(animated: true) {() -> Void in }
        })
        alertController.addAction(cancelAction)
        let cameraRollAction = UIAlertAction(title: NSLocalizedString("Open library".localized(), comment: "Open library action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UI_USER_INTERFACE_IDIOM() == .pad {
                OperationQueue.main.addOperation({() -> Void in
                    picker.sourceType = .photoLibrary
                    self.present(picker, animated: true) {() -> Void in }
                })
            }
            else {
                picker.sourceType = .photoLibrary
                self.present(picker, animated: true) {() -> Void in }
            }
        })
        
        let cameraRollAction1 = UIAlertAction(title: NSLocalizedString("Open Camera".localized(), comment: "Open library action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UI_USER_INTERFACE_IDIOM() == .pad {
                OperationQueue.main.addOperation({() -> Void in
                    picker.sourceType = .camera
                    self.present(picker, animated: true) {() -> Void in }
                })
            }
            if UI_USER_INTERFACE_IDIOM() == .phone {
                OperationQueue.main.addOperation({() -> Void in
                    picker.sourceType = .camera
                    self.present(picker, animated: true) {() -> Void in }
                })
            }
            
        })
        
        alertController.addAction(cameraRollAction)
        alertController.addAction(cameraRollAction1)
        alertController.view.tintColor = .black

        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }

        present(alertController, animated: true) {() -> Void in }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.imgView.image = image
        imgView.contentMode = UIView.ContentMode.scaleAspectFit
        self.groupImage = image
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
        }
    
    func checkPermission() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized:
        self.displayUploadImageDialog()
        case .denied:
        print("Error")
        default:
            break
        }
    }

    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }

    func checkLibrary() {
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .authorized {
            switch photos {
            case .authorized: // Access  All photo
            self.displayUploadImageDialog()
            case .denied:
                print("Error")
            default:
                break
            }
        }
    }
}





class LimitedLengthField: UITextField {
    var maxLength: Int = 100
    override func willMove(toSuperview newSuperview: UIView?) {
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        editingChanged()
    }
    @objc func editingChanged() {
        text = String(text!.prefix(maxLength))
    }
}
