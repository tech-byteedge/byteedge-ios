
//  SubcriptionVc.swift
//  byteEDGE
//  Created by Pawan Kumar on 10/03/22.

import UIKit
import Alamofire
import  KRProgressHUD

class SubcriptionVc: UIViewController {
    @IBOutlet weak var subTblView: UITableView!
    
    var objPurchase: PurchaseModel?
    var purchaseId = ""
//    var callBackofSeepreview:(()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.subApiCall()
        self.setui()
    }
    
    func setui()
    {
    self.subTblView.dataSource = self
    self.subTblView.delegate = self
    }
    
    @IBAction func backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: TABLEDELEGAE && DATASOURCE
extension SubcriptionVc: UITableViewDelegate,UITableViewDataSource
{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objPurchase?.data?.purchaseList.count == 0
        {
        self.subTblView.setEmptyMessageshow()
        }
        else
        {
        self.subTblView.restore()
        }
        return objPurchase?.data?.purchaseList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.subTblView.dequeueReusableCell(withIdentifier: "SubcriptionTableCell", for: indexPath) as! SubcriptionTableCell
        let subtype = Int(objPurchase?.data?.purchaseList[indexPath.row].subscriptionType ?? Int(0.0))
        cell.subcriptionType.text =   String(subtype)  + " " + "Month"
        cell.datalbl.text  = CommonUtility.instance.getDateTIMEFromString(inputDate:(objPurchase?.data?.purchaseList[indexPath.row].createdAt)!)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
        let purchaseId = (objPurchase?.data?.purchaseList[indexPath.row]._id)
        vc.orderID = purchaseId ?? ""
        
    }
}

//MARK: - api

extension SubcriptionVc
{
     func subApiCall()
        {
            KRProgressHUD.show()
            ServiceManager.instance.request(method: .get,
                                            apiURL: Api.paymentpurchasehistory.url,
                                            headers: CommonUtility.instance.headers,
                                            parameters: nil,
                                            decodable: PurchaseModel.self,
                                            encoding: URLEncoding.default
            )
            
            { (status, json, model, error) in
                KRProgressHUD.dismiss()
                if model?.status == 200
                {
                    self.objPurchase = model
                    print(model)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.subTblView.reloadData()
                    }
                }
                else
                {
//                self.createAlert(title: AppName, message:model?.message ?? "")
                }
            }
        }
    }
 
