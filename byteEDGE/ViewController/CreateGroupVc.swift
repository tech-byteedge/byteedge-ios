import Foundation
import UIKit
import IQKeyboardManagerSwift

class CreateGroupVc: UIViewController,UITextViewDelegate {
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var postbtn: UIButton!
    
    var placeholderLabel : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addplaceholder()
        registerKeyboardNotifications()
        self.bottomCons.constant = 10
        postbtn.clipsToBounds = true
        postbtn.layer.cornerRadius = 20
//      self.txtViewHeight.constant = self.txtView.contentSize.height
    }
    override func viewWillAppear(_ animated: Bool) {
        //    adjustUITextViewHeight(arg: self.txtView)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y -= 150
    }

    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y += 150
    }
    
    
    func addplaceholder()
    {
        self.txtView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Write something here......"
        placeholderLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        placeholderLabel.sizeToFit()
        txtView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (txtView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !txtView.text.isEmpty
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
   

  

    

   

 
    func registerKeyboardNotifications() {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillShow(notification:)),
                                                   name: UIResponder.keyboardWillShowNotification,
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillHide(notification:)),
                                                   name: UIResponder.keyboardWillHideNotification,
                                                   object: nil)
        }
    @objc func keyboardWillShow(notification: NSNotification) {
            let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue
            let keyboardSize = keyboardInfo.cgRectValue.size
            let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 30, right: 0)
//            UI?.scrollView.contentInset = contentInsets
//        table
//            UI?.scrollView.scrollIndicatorInsets = contentInsets
        }
        
        @objc func keyboardWillHide(notification: NSNotification) {
//            UI?.scrollView.contentInset = .zero
//            UI?.scrollView.scrollIndicatorInsets = .zero
        }
    @IBAction func camera(_sender:UIButton)
    {
         ImagePickerManager().pickImage(self){ image in
            self.ImageView.image = image
        }
        
//    self.chooseImage()
    }
    @IBAction func gallery(_sender:UIButton)
    {
        ImagePickerManager().pickImage(self){ image in
            self.ImageView.image = image
        }
    }
    @IBAction func postTap(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func back_tap(_ sender: Any) {
    self.navigationController?.popViewController(animated: false)
    }
}


//extension CreateGroupVc: UIImagePickerControllerDelegate,UINavigationControllerDelegate
//{
//    func chooseImage()
//    {
//        let imagePickerController = UIImagePickerController()
//        imagePickerController.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
//        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction)in
//            if UIImagePickerController.isSourceTypeAvailable(.camera)
//            {
//                imagePickerController.sourceType = .camera
//                self.present(imagePickerController, animated: true, completion: nil)
//            }
//            else
//            {
//                let alertController = UIAlertController(title: AppName, message:
//                    "Camera Not Available !!" , preferredStyle: .alert)
//                alertController.addAction(UIAlertAction(title: "Ok", style: .default))
//                self.present(alertController, animated: true, completion: nil)
//            }
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action:UIAlertAction)in
//            imagePickerController.sourceType = .photoLibrary
//            self.present(imagePickerController, animated: true, completion: nil)
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        self.present(actionSheet,animated: true,completion: nil)
//    }
//
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
//    {
//        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
//        ImageView.image = image
//        picker.dismiss(animated: true, completion: nil)
//    }
//
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        picker.dismiss(animated: true, completion: nil)
//
//    }
//}
//
//
