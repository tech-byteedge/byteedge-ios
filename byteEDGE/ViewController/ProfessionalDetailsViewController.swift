
import UIKit
import  IQKeyboardManagerSwift
import  iOSDropDown

class ProfessionalDetailsViewController: UIViewController {
    
    @IBOutlet weak var termLbl: UILabel!
    @IBOutlet weak var chekUnchek: UIButton!
    @IBOutlet weak var selectWeek: DropDown!
    @IBOutlet weak var Cap_view: UIView!
    @IBOutlet weak var next_btn: UIButton!
    var dropselection = ""
    let termText = "I accept all the Terms and Conditions and Privacy Policy"
    let term = "Terms and Conditions"
    let privacy = "Privacy Policy"
    var selected = false
    var dropdownisselected = Bool()
    var weekselected =  Int()
    var weekSelect = ["20Mins - Per Week",
                      "30Mins - Per Week",
                      "60Mins - Per Week",
                      "2HR - Per Week",
                      "3HR - Per Week",
                      "4HR - Per Week",
                      "5HR Per Week"]
    
    var textfield = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()

        IQKeyboardManager.shared.enable = true
        attributedTextLabel()
        setui()
    }
    @IBAction func back_tap(_sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func next_step (_sender:UIButton)
    {
        if dropdownisselected{
            if chekUnchek.isSelected {
            }
            else{
                chekUnchek.isUserInteractionEnabled = true
                createAlert(title: AppName, message: "Please Accept the Terms and Conditions by Clicking on the Check Box")
            }
            
            let vc =  AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChooseSkillVc") as! ChooseSkillVc
           
            vc.weekselected = self.weekselected
            vc.viaLogin = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            self.createAlert(title: AppName, message: "Please select week")
        }
    }
    
    @IBAction func chek_tap(_ sender: UIButton) {
        
        chekUnchek.isSelected = !chekUnchek.isSelected
        if chekUnchek.isSelected {
            self.next_btn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        }
        else{
            self.next_btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.76)
        }
    }
    func setui()
    {
        
        self.selectWeek.attributedPlaceholder = NSAttributedString(string: "Select",
                                                                   attributes: [NSAttributedString.Key.foregroundColor:UIColor(red: 0.8549, green: 0.2039, blue: 0.4039, alpha: 1.0)])
        self.termLbl.setLineSpacing(lineSpacing: 10.0)
        //            self.Cap_view.clipsToBounds = true
        next_btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.76)
        //           Cap_view.makeRounded()
        self.setDropdown()
    }
    
    func attributedTextLabel(){
        termLbl.text = termText
        var formattedText = String.format(strings: [term,privacy],
                                          boldFont: UIFont.boldSystemFont(ofSize: 13),
                                          boldColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1),
                                          inString: termText,
                                          font: UIFont.systemFont(ofSize: 13),
                                          color: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1))
        
        
        termLbl.attributedText = formattedText
        self.termLbl.isUserInteractionEnabled = true
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tappedOnLabel(_ :)))
        tapgesture.numberOfTapsRequired = 1
        self.termLbl.addGestureRecognizer(tapgesture)
    }
    
    // tappedOnLabel
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = self.termLbl.text else { return }
        let privacyPolicyRange = (text as NSString).range(of: privacy)
        let termsAndConditionRange = (text as NSString).range(of:term)
        if gesture.didTapAttributedTextInLabel(label: self.termLbl, inRange: privacyPolicyRange) {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "Term") as! Term
            vc.titleMain = "Privacy Policy"
            vc.type = 3
            self.navigationController?.pushViewController(vc, animated: true)
        } else if gesture.didTapAttributedTextInLabel(label: self.termLbl, inRange: termsAndConditionRange){
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "Term") as! Term
            vc.titleMain = "Terms & Conditions"
            vc.type = 2
            self.navigationController?.pushViewController(vc, animated: true)
            print("user tapped on terms and conditions text")
        }
    }
    
    //Ios DropDown
    func setDropdown()
    {
        self.selectWeek.optionArray = self.weekSelect
        self.selectWeek.didSelect{(selectedText , index ,id) in
            self.selectWeek.text = "\(selectedText)"
            self.weekselected = index
            
            
            kUserDefaults.saveString(selectedText, forKey: .week)
            self.dropdownisselected = true
        }
        
        selectWeek.listWillAppear
        {
            self.selectWeek.resignFirstResponder()
            if(self.textfield == self.selectWeek) {
                self.selectWeek.resignFirstResponder()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setDropdown()
    }
}
