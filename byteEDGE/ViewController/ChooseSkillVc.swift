
import UIKit
import CoreAudio
import CoreMIDI
import CloudKit



enum SetChooseSkill {
case login
case personalnfo
}

class ChooseSkillVc: UIViewController {
    @IBOutlet weak var coll_skill : UICollectionView!
    @IBOutlet weak var finsh_btn : UIButton!
    var firstName = ""
    var lastName = ""
    var emailAdree = ""
    var isSelect = Bool()
    var weekselected : Int = 0
    var skillselected = [String]()
    var mob = ""
    var arr = [String]()
    var dateVal = ""
    var idofSkills = [String]()
    var obj: SkillsChoose?
    var callBack:((_ arr:[String]?)->())?
    var viaLogin = false
    var arrSelectedIndex = [IndexPath]() // This is selected cell Index array
    var arrSelectedData = [String]()
    var selected = -1
    let columnLayout = CustomViewFlowLayout()
    var contnetSet = ["Digital","Leadership","Business","productivity","IT & Technology","Career Services","Job"]
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        coll_skill.allowsMultipleSelection = true
        self.coll_skill.collectionViewLayout = columnLayout
        coll_skill.register(UINib(nibName: "HeaderReasuableViewCollectionReusableView", bundle: nil), forCellWithReuseIdentifier: "HeaderReasuableViewCollectionReusableView")
          self.setui()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    self.hitServiceForSkillschoose(param: [:])
    }
    
    func setui()
    {
    self.coll_skill.delegate = self
    self.coll_skill.dataSource = self
    }
    
    @IBAction func finish_tap(_sender:UIButton)
    {
        if !viaLogin{
            self.callBack?(idofSkills)
            if idofSkills.count >= 5
            {
            self.hitServiceForUpdateProfile()
            self.navigationController?.popViewController(animated: true)
            }
            else
            {
            createAlert(title: AppName, message: "Select maximum 5 skills")
            }
           
        }
        else{
        if arrSelectedData.count >= 5
        {
        self.hitServiceForUpdateProfile()
        }
        else
        {
        createAlert(title: AppName, message: "Select maximum 5 skills")
        }
        }
        
    }
    
    @IBAction func back_tap(_sender:UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
}

extension ChooseSkillVc : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return  obj?.data?.skillList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = obj?.data?.skillList?[section].skillType?.count
        return count!+1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(indexPath.row == 0){
            let cell =  self.coll_skill.dequeueReusableCell(withReuseIdentifier: "HeaderReasuableViewCollectionReusableView", for: indexPath) as! HeaderReasuableViewCollectionReusableView
//              cell.titleLbl.text = contnetSet[indexPath.section]
              cell.titleLbl.text = obj?.data?.skillList?[indexPath.section]._id
              cell.titleLbl.textColor = .white
              cell.vuiew.backgroundColor = .clear
            return cell
        }
         else{
            let cell =  self.coll_skill.dequeueReusableCell(withReuseIdentifier: "Skillcell", for: indexPath) as! Skillcell
            print(indexPath.section)
            print(indexPath.row)
            
            cell.lbl_Cell.text = obj?.data?.skillList?[indexPath.section].skillType?[indexPath.row - 1].skills
            cell.contentView.clipsToBounds = true
            cell.contentView.backgroundColor = #colorLiteral(red: 0.1529411765, green: 0.1529411765, blue: 0.1529411765, alpha: 1)
            if !viaLogin{
                if !isSelect{
            for i in 0..<self.idofSkills.count{
                if obj?.data?.skillList?[indexPath.section].skillType?[indexPath.row-1]._id == idofSkills[i]{
                    arrSelectedIndex.append(indexPath)
                    print("arrSelectedIndex.count")
                    print(arrSelectedIndex.count)
                }
                }
                
            }
                if arrSelectedIndex.count >= 5
                {
                finsh_btn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
                }
                else
                {
                finsh_btn.backgroundColor = #colorLiteral(red: 0.7370415926, green: 0.7366356254, blue: 0.7233269811, alpha: 1)
                }
                
            }
            if arrSelectedIndex.contains(indexPath) { // You need to check wether selected index array contain current index if yes then change the color
                cell.contentView.layer.borderWidth = 1
                cell.contentView.layer.borderColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
                cell.lbl_Cell.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
                cell.contentView.layer.cornerRadius = 15
            }
             else {
                cell.contentView.layer.borderWidth = 1
                cell.contentView.layer.borderColor = #colorLiteral(red: 0.2235294118, green: 0.2235294118, blue: 0.2235294118, alpha: 0.76)
                cell.lbl_Cell.textColor = #colorLiteral(red: 0.7215686275, green: 0.7215686275, blue: 0.7215686275, alpha: 1)
                cell.contentView.layer.cornerRadius = 15
            }
            cell.layoutSubviews()
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isSelect = true
        print("You selected cell #\(indexPath.item)!")
        if(indexPath.row == 0){
        }
          else{
            let strData =  obj?.data?.skillList?[indexPath.section].skillType?[indexPath.row-1].skills
            print(obj?.data?.skillList?[indexPath.section].skillType?[indexPath.row-1]._id)
            if arrSelectedIndex.contains(indexPath) {
                arrSelectedIndex = arrSelectedIndex.filter { $0 != indexPath}
                arrSelectedData = arrSelectedData.filter { $0 != strData}
                idofSkills = idofSkills.filter{ $0 != obj?.data?.skillList?[indexPath.section].skillType?[indexPath.row-1]._id}
            }
            else {
                arrSelectedIndex.append(indexPath)
                
                idofSkills.append((obj?.data?.skillList?[indexPath.section].skillType?[indexPath.row-1]._id) ?? "")
                arrSelectedData.append(strData ?? "")
                kUserDefaults.set(idofSkills, forKey: "SelectedSkill")

            }
            var new = arrSelectedIndex.count
            if  new >= 5 {
            finsh_btn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            }
            else {
            finsh_btn.backgroundColor = #colorLiteral(red: 0.7370415926, green: 0.7366356254, blue: 0.7233269811, alpha: 1)
            }
            coll_skill.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0{
            let width = coll_skill.frame.width
            let height = coll_skill.frame.height
            //        columnLayout.estimatedItemSize = CGSize(width:coll_skill.frame.width, height: 50)
            return CGSize(width: width, height: 40)
        }
        else
        {
            let width = coll_skill.frame.width
            //        columnLayout.estimatedItemSize = CGSize(width:coll_skill.frame.width, height: 50)
            return CGSize(width:width / 2.5, height: 40)
        }
    }
}

extension ChooseSkillVc{
    
    func hitServiceForSkillschoose(param: [String:Any]){
        
        
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.skillList.url,
                                        headers: nil,
                                        parameters: nil,
                                        decodable: SkillsChoose.self
        ) { (status, json, model, error) in
            if model?.status == 200
            {
                
                self.obj = model
                
                DispatchQueue.main.async {
                    self.coll_skill.reloadData()
                }
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}


extension UILabel {
    func getSize(constrainedWidth: CGFloat) -> CGSize {
        return systemLayoutSizeFitting(CGSize(width: constrainedWidth, height: UIView.layoutFittingCompressedSize.height), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
    }
}

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}

    class CustomViewFlowLayout: UICollectionViewFlowLayout {
    let cellSpacing: CGFloat = 10.0
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        
        return attributes
    }
    
}

extension CGSize{
    init(_ width:CGFloat,_ height:CGFloat) {
        self.init(width:width,height:height)
    }
}

extension ChooseSkillVc
{
    func hitServiceForUpdateProfile(){
        let mobNum = UserDefaults.standard.object(forKey: "MOBILENUM")
        self.mob = mobNum as! String
        
        let firsName = kUserDefaults.retriveString(.firstName)
        self.firstName = firsName as! String

        
        let lastName = kUserDefaults.retriveString(.lastName)
        self.lastName = lastName as! String

        
        let email = kUserDefaults.retriveString(.email)
        self.emailAdree = email as! String
        
        let dob = kUserDefaults.retriveString(.dob)
        self.dateVal = dob as! String
        let selectedSkill = kUserDefaults.object(forKey: "SelectedSkill")
        self.idofSkills = selectedSkill as! [String]
        
        
        let param = ["mobileNumber":self.mob,
                     "firstName": self.firstName,
                     "lastName": self.lastName,
                     "email": self.emailAdree,
                     "dob":self.dateVal,
                     "skills": self.idofSkills,
                     "week": self.weekselected ] as [String : Any]
        print(CommonUtility.instance.headers)
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.updateProfile.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: UpdateProfile.self
        )
         { (status, json, model, error) in
            if model?.status == 200
            {
            kUserDefaults.set(1, forKey: "profileCompleted")
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            self.navigationController?.pushViewController(vc, animated: false)
            }
             
            else
            {
             self.createAlert(title: AppName, message:model?.message ?? "")
            }
            
        }
    }
}
