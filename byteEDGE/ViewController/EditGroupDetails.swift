import UIKit
import Foundation
import KRProgressHUD
import Alamofire
import SwiftUI
import  SKPhotoBrowser

enum CallBackType
{
    case firstCallBack,secondCallBack,thirdCallBack
}

// MARK: GroupDetails Class
class EditGroupDetails: UIViewController,AcceptandsBlock {
    
    @IBOutlet weak var imgtop: UIImageView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var abtDisc: UILabel!
    @IBOutlet weak var whyshouldDis: UILabel!
    @IBOutlet weak var tblEdit: UITableView!
    @IBOutlet weak var headerEdit: UIView!
    @IBOutlet weak var nameLbk: UILabel!
    @IBOutlet weak var  saveBtn: UIButton!
    
    var callBAckType : CallBackType = .firstCallBack
    var removeCallBAck:(()->())?
    var _id = String()
    var memberID = ""
    var grpId = ""
    var typeAll = Int()
    var mayGroupDetailsdata: MyGroupDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refeshData),
                                               name: NSNotification.Name("refreshValue"),
                                               object: nil)
        
        self.setui()
        self.groupDetailsApi(type: typeAll)
        let view = CommonButtonView.fromNib()
        view.firstCallBack = {
            self.groupDetailsApi(type: self.typeAll)
            self.callBAckType = .firstCallBack
            view.firstView.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.1137254902, blue: 0.137254902, alpha: 1)
            view.secondView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.thirdView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.firstName.textColor = .white
            view.secondName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            view.thirdName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            view.img1st.isHidden = false
            view.img2nd.isHidden = true
            view.img3rd.isHidden = true
            self.tblEdit.reloadData()
        }
        
        view.secondCallBack = {
            self.groupDetailsApi(type: self.typeAll)
            self.callBAckType = .secondCallBack
            view.secondView.backgroundColor = #colorLiteral(red: 0.1461485922, green: 0.1526356339, blue: 0.1830024123, alpha: 1)
            view.firstView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.thirdView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.tblEdit.reloadData()
            
            view.firstName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            view.secondName.textColor = .white
            view.thirdName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            
            view.img1st.isHidden = true
            view.img2nd.isHidden = false
            view.img3rd.isHidden = true
            self.tblEdit.reloadData()
        }
        view.thirdCallBack = {
            self.callBAckType = .thirdCallBack
            view.secondView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.firstView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.thirdView.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.1137254902, blue: 0.137254902, alpha: 1)
            
            view.firstName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            view.secondName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            view.thirdName.textColor = .white
            
            view.img1st.isHidden = true
            view.img2nd.isHidden = true
            view.img3rd.isHidden = false
            self.tblEdit.reloadData()
        }
        
        view.initSetup(.EditgroupDetails)
        view.frame = btnView.bounds
        self.btnView.addSubview(view)
        btnView.backgroundColor = .black
    }
    
    @objc func refeshData(note: NSNotification){
        self.groupDetailsApi(type: self.typeAll)
        self.tblEdit.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        self.saveBtn.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    func setui()
    {
        self.tblEdit.register(UINib(nibName: "TitleCell", bundle: nil), forCellReuseIdentifier: "TitleCell")
        self.tblEdit.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
        self.tblEdit.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
        tblEdit.delegate = self
        tblEdit.dataSource = self
    }
    
    //MARK: CALL BACK PROTOCOL
    func acceptclick(at index: IndexPath, buttonType: String) {
        if buttonType == "Accept"
        {
            let memberId = mayGroupDetailsdata?.data?.detail?.pendingRequest?[index.row - 1].memberDetail?._id
            self.acceptandBlock(joinStatus:"accept",isjoin: true, memberid: memberId!)
        }
        else if buttonType == "Block" // Block
        {
            let memberId = mayGroupDetailsdata?.data?.detail?.pendingRequest?[index.row - 1].memberDetail?._id
            self.acceptandBlock(joinStatus:"reject",isjoin: false, memberid: memberId!)
        }
        else
        {
        }
    }
    
    @IBAction func feedsClick(_ sender: Any) {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"GroupFeeds") as! GroupFeeds
        vc.idforFeed = self._id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func imageClick(_sender:UIButton)
    {
        self.imagefullSize()
    }
    
    
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func moremenu(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "PopMember") as! PopMember
            vc.grpId = self._id
            vc.mayGroupDetailsdata = self.mayGroupDetailsdata
            vc.callBAck = {
                grpId,mayGroupDetailsdata in
                let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "UserCanEditGroupDetaisVc") as! UserCanEditGroupDetaisVc
                vc.grpId = grpId ?? ""
                vc.mayGroupDetailsdata = mayGroupDetailsdata
                print(vc.mayGroupDetailsdata)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            vc.modalPresentationStyle = .fullScreen
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
            vc.removeCallBAck = {
                self.removeCallBAck?()
                self.navigationController?.popViewController(animated: false)
            }
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func imagefullSize()
    {
        var images = [SKPhoto]()
        var urlImage = self.mayGroupDetailsdata?.data?.detail?.groupDetail?.image ?? ""
        let photo = SKPhoto.photoWithImageURL(urlImage)
        photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
        images.append(photo)
        
        // 2. create PhotoBrowser Instance, and present.
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
}

extension EditGroupDetails: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if callBAckType == .firstCallBack
        {
            return 4
        }
        
        else if callBAckType == .secondCallBack // Request
        {
            return (mayGroupDetailsdata?.data?.detail?.pendingRequest?.count ?? 0) + 1
        }
        else // Member
        {
            return (mayGroupDetailsdata?.data?.detail?.myGroupMembers?.count ?? 0) + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if callBAckType ==  .firstCallBack
        {
            if indexPath.row == 0
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
                cell.titlLbl.text = "ABOUT GROUP"
                return cell
            }
            else if indexPath.row == 1
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
                cell.listtxtview.isUserInteractionEnabled = false
                cell.listtxtview.text = mayGroupDetailsdata?.data?.detail?.groupDetail?.about
                return cell
            }
            else if  indexPath.row == 2
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
                cell.titlLbl.text = "WHY SHOULD YOU JOIN GROUP?"
                return cell
            }
            else
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
                cell.listtxtview.isUserInteractionEnabled = false
                cell.listtxtview.text = mayGroupDetailsdata?.data?.detail?.groupDetail?.joinReason
                return cell
            }
        }
        else if callBAckType == .secondCallBack // Request
        {
            if indexPath.row == 0
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
                cell.titlLbl.text = "Total Requests" + " " + String(mayGroupDetailsdata?.data?.detail?.totalPendingRequest ?? 0)
                return cell
            }
            
            else
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
                cell.moreOption.isHidden = true
                cell.acceptReq.isHidden = false
                cell.rejectReq.isHidden = false
                cell.delegate = self
                
                cell.indexPath = indexPath
                cell.nameReq.text =  mayGroupDetailsdata?.data?.detail?.pendingRequest?[indexPath.row - 1 ].memberDetail?.name ?? ""
                var urlImage = mayGroupDetailsdata?.data?.detail?.pendingRequest?[indexPath.row - 1].memberDetail?.image ?? ""
                cell.imgReq.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.imgReq.layer.masksToBounds = true
                cell.imgReq.layer.cornerRadius = cell.imgReq.frame.size.height/2;
                cell.imgReq.layer.masksToBounds = true
                cell.imgReq.layer.borderWidth=1;
                return cell
            }
        }
        
        else // Member
        {
            if indexPath.row == 0
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
                cell.titlLbl.text =    "Total Members" + " " + String(mayGroupDetailsdata?.data?.detail?.totalMyGroupMembers ?? 0)
                return cell
            }
            
            else
            {
                let cell = tblEdit.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
                cell.imgReq.layer.masksToBounds = true
                cell.imgReq.layer.cornerRadius = cell.imgReq.frame.size.height/2;
                cell.imgReq.layer.masksToBounds = true
                cell.imgReq.layer.borderWidth=1;
                cell.moreOption.isHidden = false
                cell.acceptReq.isHidden = true
                cell.rejectReq.isHidden = true
                cell.nameReq.text = mayGroupDetailsdata?.data?.detail?.myGroupMembers?[indexPath.row - 1 ].memberDetail?.name ?? ""
                var urlImage = mayGroupDetailsdata?.data?.detail?.myGroupMembers?[indexPath.row - 1].memberDetail?.image ?? ""
                cell.imgReq.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                
                
                if  indexPath.row == 1
                {
                    cell.moreOption.isHidden = true
                }
                
                else
                {
                    cell.moreOption.isHidden = false
                    cell.moreoption = {
                        (index) -> Void in
                        let indexPath =   self.tblEdit.indexPath(for: index)
                        print(indexPath?.row)
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier:
                                                                                            "MemberOptionClickPopVc") as! MemberOptionClickPopVc
                            print(/self.mayGroupDetailsdata?.data?.detail?.myGroupMembers?[/indexPath?.row - 1]._id)
                            vc.groupDetail = /self.mayGroupDetailsdata?.data?.detail?.groupDetail?._id
                            print(vc.groupDetail)
                            vc.grpID = /self.mayGroupDetailsdata?.data?.detail?.myGroupMembers?[/indexPath?.row - 1]._id
                            vc.grouAdmin = /self.mayGroupDetailsdata?.data?.detail?.myGroupMembers?[/indexPath?.row - 1].memberDetail?._id
                            vc.modalPresentationStyle = .fullScreen
                            vc.providesPresentationContextTransitionStyle = true
                            vc.definesPresentationContext = true
                            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                            vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
                            let nav = UINavigationController(rootViewController: vc)
                            nav.navigationBar.isHidden = true
                            nav.modalPresentationStyle = .fullScreen
                            nav.providesPresentationContextTransitionStyle = true
                            nav.definesPresentationContext = true
                            nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                            nav.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
                            UIApplication.topViewController()?.present(nav, animated: true, completion: nil)
                        }
                    }
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerEdit
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 360
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if callBAckType == .firstCallBack
        {
            if  indexPath.row == 0
            {
                if UIDevice.current.userInterfaceIdiom == .pad
                {
                    return 80
                }
                else
                { return 45
                }
            }
            else if indexPath.row == 1
            {
                return UITableView.automaticDimension
            }
            else if indexPath.row == 2
            {
                return 45
            }
            else
            {
                return UITableView.automaticDimension
            }
        }
        else if callBAckType == .secondCallBack
        {
            return 55
        }
        else
        {
            return 55
        }
    }
}

extension EditGroupDetails
{
    func groupDetailsApi(type: Int)
    {
        var param: [String: Any] = [:]
        param["groupId"] =  _id
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.myGroupDetails.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: MyGroupDetail.self,
                                        encoding: URLEncoding.queryString)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            self.mayGroupDetailsdata = model
            
            self.grpId = self.mayGroupDetailsdata?.data?.detail?.groupDetail?._id ?? ""
            var urlImage = self.mayGroupDetailsdata?.data?.detail?.groupDetail?.image ?? ""
            self.imgtop.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: "logo"), options: .highPriority, context: nil)
            self.imgtop.contentMode = .scaleAspectFit
            self.nameLbk.text = self.mayGroupDetailsdata?.data?.detail?.groupDetail?.name
            
            DispatchQueue.main.async {
                self.tblEdit.reloadData()
            }
            
        }
    }
}

//MARK: Accept and Block
extension EditGroupDetails
{
    func acceptandBlock(joinStatus:String,isjoin:Bool,memberid: String)
    {
        var param: [String: Any] = [:]
        param["groupId"] = _id
        param["memberId"] = memberid
        param["joinStatus"] = joinStatus
        param["isJoin"] = isjoin
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.AcceptBlock.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: AccepModel.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
                DispatchQueue.main.async {
                    self.tblEdit.reloadData()
                }
                self.navigationController?.popViewController(animated: false)
            }
            else
            {
                self.createAlert(title:AppName, message: model?.message ?? "")
            }
        }
    }
}

struct AccepModel: Codable
{
    let Code:Int?
    let status: Int?
    let message: String?
}

