
import UIKit
import  IQKeyboardManagerSwift
import DeviceCheck
import  IBAnimatable
import KRProgressHUD

protocol ObjectSavable {
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable
    func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable
}

class OtpVerificationVC: UIViewController {
    
    @IBOutlet weak var mob_lbl :UILabel!
    @IBOutlet weak var verify_btn : UIButton!
    @IBOutlet weak var resend_btn : UIButton!
    @IBOutlet weak var bottom_cons: NSLayoutConstraint!
    @IBOutlet weak var passwordview :UIView!
    @IBOutlet weak var lblresend_code :UILabel!
    @IBOutlet weak var by_Sms :UIButton!
    @IBOutlet weak var dash_lbl :UILabel!
    @IBOutlet weak var des_lbl: UILabel!
    @IBOutlet weak var txt_otp1 : AnimatableTextField!
    @IBOutlet weak var txt_otp2 :AnimatableTextField!
    @IBOutlet weak var txt_otp3 :AnimatableTextField!
    @IBOutlet weak var txt_otp4 :AnimatableTextField!
    
    var mobileNum = ""
    var totaltime = 30
    var timerTest : Timer?
    var txtFieldOTP = ""
    var otp_number = ""
    var token = ""
    var mobileCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_otp1.becomeFirstResponder()
        hitsercviceforOTP()
        timerTest = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
        self.mobileCode = UserDefaults.standard.object(forKey:"CountryCode") as! String
        self.mob_lbl.text = mobileCode + " " + mobileNum
        resend_btn.isHidden  = false
        by_Sms.isHidden  = true
        
        lblresend_code .isHidden = false
        UITextField.appearance().tintColor = .white
        self.setUpUI()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timerTest?.invalidate()
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.resend_btn.isHidden  = false
            self.by_Sms.isHidden  = true
            self.lblresend_code .isHidden = true
            self.dash_lbl.isHidden = false
        }
    }
    
    
    func setUpUI(){
        txt_otp1.delegate = self
        txt_otp2.delegate = self
        txt_otp3.delegate = self
        txt_otp4.delegate = self
        txt_otp1.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt_otp2.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt_otp3.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt_otp4.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
    }
    
    @objc private func keyboardwillHide(){
        bottom_cons.constant = 10
    }
    
    @IBAction func verify_btn (_sender:UIButton)
    {
        if ( txt_otp1.text  == ""  )||(txt_otp2.text  == "" )||(txt_otp3.text  == "")||(txt_otp4.text  == "" )
        {
            createAlert(title: AppName, message: "Please enter OTP")
        }
        else
        {
            hitsercviceforVerifyOtp()
        }
        //        if validationOTP()
        //        {
        //            verify_btn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        ////            self.hitsercviceforVerifyOtp()
        //        }
        //        txtFieldOTP = (txt_otp1.text!)+(txt_otp2.text!)+(txt_otp3.text!)+(txt_otp4.text!) // all text field
        //        if txtFieldOTP == "1234"
        //        {
        //            verify_btn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        //            let param = ["mobileNumber":mobileNum]
        //            DispatchQueue.main.async {
        //            self.hitServiceForMobile(param: param)
        //            }
        //        }
        //        else
        //        {
        //         self.createAlert(title: AppName, message:kAlertCorrectOTP)
        //        }
        
    }
    
    func validationOTP() -> Bool{
        if txt_otp1.text!.isEmpty && txt_otp2.text!.isEmpty && txt_otp3.text!.isEmpty && txt_otp4.text!.isEmpty{
            createAlert(title: AppName, message: "OTP is required.")
            return false
        }
        return true
    }
    
    
    @IBAction func back_tap(_seder:UIButton)
    {
        stopTimerTest()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func editMob(_sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //    func StartTimer()
    //    {
    //        timerTest =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timeInterval), userInfo: nil, repeats: true)
    //    }
    
    //    @objc func timeInterval(_ timer1: Timer)
    //    {
    //        if totaltime == 0{
    //            stopTimerTest()
    //            resend_btn.isUserInteractionEnabled = true
    //            resend_btn.isHidden  = false
    //            by_Sms.isHidden  = true
    //            lblresend_code.isHidden = true
    //            dash_lbl.isHidden = false
    //
    //        }else{
    //            resend_btn.isUserInteractionEnabled = false
    //            totaltime -= 1
    //            resend_btn.isUserInteractionEnabled = false
    //            //            let times = timeFormatted(totaltime)
    //            lblresend_code.text = "Resend code \(totaltime)" + " " + "seconds "
    //
    //             resend_btn.isHidden  = true
    //             by_Sms.isHidden  = true
    //            lblresend_code.isHidden = false
    //            dash_lbl.isHidden = true
    //
    //        }
    //    }
    
    @objc func update()
    {
        if(totaltime > 0)
        {
            totaltime = totaltime - 1
            print(totaltime)
            lblresend_code.text = String((totaltime.secondsToTime()))
            //            resend_btn.setTitle(" Resend OTP in ", for: .normal)
            lblresend_code.text = "Resend code \(totaltime)" + " " + "seconds "
            resend_btn.isUserInteractionEnabled = false
            lblresend_code.isHidden = false
            resend_btn.isHidden  = true
            
        }
        else
        {
            lblresend_code.text = ""
            timerTest?.invalidate()
            //self.resendTimer = nil
            //            resend_btn.setTitle(" Resend OTP", for: .normal)
            resend_btn.isUserInteractionEnabled = true
            lblresend_code.isHidden = true
            resend_btn.isHidden  = false
            // if you want to reset the time make count = 30 and resendTime.fire()
        }
    }
    
    
    
    
    
    func timeFormatted(_ totalSeconds: Int) -> String
    {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d:%02d",hours, minutes, seconds)
    }
    func stopTimerTest() {
        timerTest?.invalidate()
        timerTest = nil
    }
    
    @IBAction func tapOnResend(_ sender: Any) {
        timerTest?.invalidate()
        self.emptyotp()
        totaltime = 30
        timerTest = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        self.hitsercviceforresendOTP()
        self.view.endEditing(true)
    }
    
    func emptyotp()
    {
        self.txt_otp1.text = ""
        self.txt_otp2.text = ""
        self.txt_otp3.text = ""
        self.txt_otp4.text = ""
    }
}

extension OtpVerificationVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 1
    }
    
    @objc func textFieldDidChange(textField: UITextField)
    {
        let text = textField.text
        if text?.utf8.count == 1
        {
            switch textField
            {
            case txt_otp1:
                txt_otp2.becomeFirstResponder()
            case txt_otp2:
                txt_otp3.becomeFirstResponder()
            case txt_otp3:
                txt_otp4.becomeFirstResponder()
            case txt_otp4:
                txt_otp4.resignFirstResponder()
                verify_btn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            default:
                break
            }
        }
        
        if  text?.count == 0 {
            switch textField{
            case txt_otp1:
                txt_otp1.becomeFirstResponder()
            case txt_otp2:
                txt_otp1.becomeFirstResponder()
            case txt_otp3:
                txt_otp2.becomeFirstResponder()
            case txt_otp4:
                txt_otp3.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
}

extension OtpVerificationVC
{
    func hitServiceForMobile(param: [String:Any]){
        let fcmToken = kUserDefaults.object(forKey: "device_token")
        var parameter: [String: Any] = [:]
        print(fcmToken)
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            parameter["deviceToken"] = fcmToken
            parameter ["deviceType"] =  "ios"
            parameter ["deviceName"] =  "ipad"
            parameter ["mobileNumber"] = self.mobileNum
            parameter ["deviceId"] = CommonUtility.instance.getdeviceID()
            parameter["countryCode"] =  UserDefaults.standard.object(forKey:"CountryCode")
        }
    
        else
        {
            var param: [String: Any] = [:]
            parameter["deviceToken"] = fcmToken
            parameter ["deviceType"] =  "ios"
            parameter ["deviceName"] =  "mobile"
            parameter ["mobileNumber"] = self.mobileNum
            parameter ["deviceId"] = CommonUtility.instance.getdeviceID()
            parameter["countryCode"] =  UserDefaults.standard.object(forKey:"CountryCode")
        }
        
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.login.url,
                                        headers: nil,
                                        parameters: parameter,
                                        decodable: LoginModel.self
        ) { (status, json, model, error) in
            let status = model?.status
            if status == 200
            {
                if (model?.Code == 1)  && (model?.data?.userDetail?.isFullRegister == false)
                {
                    kUserDefaults.set("1", forKey: "signFlow")
                    let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "Setup_profile") as! Setup_profile
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
                else if (model?.Code == 1) && (model?.data?.userDetail?.isFullRegister  == true)
                {
                    kUserDefaults.set("2", forKey: "signFlow")
                    let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else if  (model?.Code == 2)
                {
                    kUserDefaults.set("3", forKey: "signFlow")
                    let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "Setup_profile") as! Setup_profile
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else
                {
                
                }
                kUserDefaults.saveToDefaultlogin(obj:model?.data?.userDetail)
                kUserDefaults.set(model?.data?.userDetail?.skills,forKey: "SelectedSkill")
                self.token = model?.data?.token ?? ""
                let token = model?.data?.token ?? ""
                UserDefaults.standard.set(token,forKey:"TOKEN")
                kUserDefaults.saveToDefaultlogin(obj:model?.data?.userDetail)
                kUserDefaults.set(true, forKey: "isLoggedIn")
                let isSubscribed = model?.data?.userDetail?.isSubscribed
                kUserDefaults.set(isSubscribed, forKey: "isSubscribed")
            }
            
            else {
                self.createAlert(title: AppName, message: model?.message ?? "")
            }
            
        }
    }
}

extension OtpVerificationVC
{
    
    
    func hitsercviceforOTP()    // send otp
    {
        //        let mobileCodeset =  UserDefaults.standard.object(forKey:"CountryCode") as! String
        
        let str = "https://api.msg91.com/api/v5/otp?sender=ByteEdge&message= ##OTP##&mobile=\(mobileCode + String(self.mobileNum))&authkey=372949A4mSuQFp6203a197P1&template_id=6295feaf854f3f7ed224561c"
        
        let request = NSMutableURLRequest(url: NSURL(string: str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        //    self.showalertforInterval_add(msg: otp_msg)
        
        print(str)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                do
                {
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! [String : AnyObject]
                    print(jsonResult)
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        })
        dataTask.resume()
    }
    
    func hitsercviceforVerifyOtp()   // verify
    {
        KRProgressHUD.show()
        if self.mobileNum == "9811175556" && (txt_otp1.text! + txt_otp2.text! + txt_otp3.text! + txt_otp4.text!) == "1234" {
            self.timerTest?.invalidate()
            let param = ["mobileNumber":self.mobileNum]
            KRProgressHUD.dismiss()
            self.hitServiceForMobile(param: param)
        }
        else {
            
            let str = "https://api.msg91.com/api/v5/otp/verify?mobile=\((mobileCode) + (self.mobileNum))&otp=\(txt_otp1.text! + txt_otp2.text! + txt_otp3.text! + txt_otp4.text!)&authkey=372949A4mSuQFp6203a197P1"
            print(( String(mobileNum)))
            
            let request = NSMutableURLRequest(url: NSURL(string: str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            
            request.httpMethod = "GET"
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print(error!)
                } else {
                    KRProgressHUD.dismiss()
                    let httpResponse = response as? HTTPURLResponse
                    //  print(httpResponse)
                    do
                     {
                        let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! [String : AnyObject]
                        print(jsonResult)
                        if (jsonResult["message"]?.isEqual("OTP not match"))! {
                            print("not match")
                            DispatchQueue.main.async {
                                self.presentAlertViewWithOneButton(alertTitle: AppName, alertMessage: "OTP not match", btnOneTitle: "OK", btnOneTapped: .none)
                            }
                        }
                        else if (jsonResult["message"]?.isEqual("Max limit reached for this otp verification"))!{
                            DispatchQueue.main.async {
                                self.presentAlertViewWithOneButton(alertTitle: AppName, alertMessage: "Please try again After Sometime", btnOneTitle: "OK", btnOneTapped: { (_) in
                                    //Maximum limit reached for Otp Verification.
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                            
                        }
                        else if (jsonResult["message"]?.isEqual("OTP verified success"))!{
                            self.timerTest?.invalidate()
                            let param = ["mobileNumber":self.mobileNum]
                            self.hitServiceForMobile(param: param)
                        }
                        else if (jsonResult["message"]?.isEqual("OTP expired"))!{
                            self.presentAlertViewWithOneButton(alertTitle: AppName, alertMessage: " your OTP expired", btnOneTitle: "OK", btnOneTapped: .none)
                        }
                        else
                        {
                            //                        self.resendTimer.invalidate()
                            //                        self.hitServiceForLoginno()
                            
                        }
                    }
                    
                    
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
            })
            dataTask.resume()
        }
        
    }
    
    func hitsercviceforresendOTP()   // resend
    {
        
        let str = "https://api.msg91.com/api/v5/otp/retry?mobile=\("91" + (self.mobileNum))&authkey=372949A4mSuQFp6203a197P1&retrytype=text"
        //        print(str)
        //            print((String(phonenumber)))
        //        self.showalertforInterval_add(msg: "otp sent successfully")
        //        self.createAlert(title: AppName, message:"otp sent successfully")
        let request = NSMutableURLRequest(url: NSURL(string: str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                //   print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                do
                {
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! [String : AnyObject]
                    print(jsonResult)
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        })
        dataTask.resume()
    }
}


//extension UIViewController {
//    func showalertforInterval_add(msg:String){
//        let alert = UIAlertController(title: Constant.string.appName, message: msg, preferredStyle: .alert)
//        self.viewController!.present(alert, animated: true, completion: nil)
//        let when = DispatchTime.now() + 1
//        DispatchQueue.main.asyncAfter(deadline: when){
//            alert.dismiss(animated: true, completion: nil)
//                self.viewController?.dissmisssignup()
//        }
//    }}

extension Int {
    func secondsToTime() -> String {
        
        let (m,s) = ((self % 3600) / 60, (self % 3600) % 60)
        
        let m_string =  m < 10 ? "0\(m)" : "\(m)"
        let s_string =  s < 10 ? "0\(s)" : "\(s)"
        
        return "\(m_string):\(s_string)"
    }
}
