
import UIKit
import IQKeyboardManagerSwift
import SKCountryPicker

class EnterMobVc: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var entermob : UILabel!
    @IBOutlet weak var discription : UILabel!
    @IBOutlet weak var mob_txt :UITextField!
    @IBOutlet weak var mobileview: UIView!
    
    @IBOutlet weak var lblCountrycode: UILabel!
    @IBOutlet weak var countryImg: UIImageView!
    
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    @IBOutlet weak var countrycode : UIButton!
    @IBOutlet weak var sendVerificationcodebtn : UIButton!
    @IBOutlet weak var bottomcons: NSLayoutConstraint!
    var mobileNum = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mob_txt.keyboardType = .numberPad
        self.mob_txt.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        mob_txt.resignFirstResponder()
        self.setUi()
        guard let country = CountryManager.shared.currentCountry else {
         return
        }
        print(country)
        lblCountrycode.text = country.dialingCode
        print(lblCountrycode)
        UserDefaults.standard.set(lblCountrycode.text, forKey: "CountryCode")
        countryImg.image = country.flag
    }
    
    @IBAction func countrypicker(_ sender: Any) {
        self.mob_txt.text = ""
        setUi()
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
            guard let self = self else { return }
            self.countryImg.image = country.flag
            self.lblCountrycode.text = country.dialingCode
            UserDefaults.standard.set(self.lblCountrycode.text, forKey: "CountryCode")
            
        }
        countryController.detailColor = UIColor.red
    }
    
    @objc private func keyboardwillHide(){
        bottomcons.constant = 15
        }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
     return false
    }
    
      private func setUi()
    {
        self.mob_txt.keyboardType = .phonePad
        self.mob_txt.attributedPlaceholder = NSAttributedString(string: "XXX XXX XXXX",
                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.mob_txt.setAppFontColor(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), font: .axiforma(.bold,size: .twoFour ))
        self.mob_txt.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        self.mob_txt.delegate = self
        self.lblCountrycode.textColor = UIColor(red: 0.4157, green: 0.4157, blue: 0.4902, alpha: 1.0)
        self.mobileview.backgroundColor = .clear
        
    }

    
    
//    @objc func keyboardWillShow(notification: NSNotification)
//    {
////        scrollToBottom()
//        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
//        {
//            UIView.animate(withDuration: 0.15, animations:
//                {
//                let keyboardRectangle = keyboardFrame.cgRectValue
//                    let keyboardHeight = keyboardRectangle.height
//                    self.bottomcons.constant = keyboardHeight
//                   self.heightCons.constant = 250
//                  self.heightCons.isActive = true
//
//                    self.view.layoutIfNeeded()
//            })
//        }
//    }
    
//    @objc func keyboardWillHide(notification: NSNotification)
//    {
//        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil
//        {
//            UIView.animate(withDuration: 0.15, animations:
//                {
//                    self.bottomcons.constant = 0
//                   self.heightCons.isActive = false
//                    self.view.layoutIfNeeded()
//            })
//        }
//    }
//
    

    override func viewWillLayoutSubviews() {
    
    }
    
//    if mob_txt.text!.count >= 10
//    {
//    self.sendVerificationcodebtn.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
//    }
//    else
//    {
//    self.sendVerificationcodebtn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//    }
//    }
    
//    func keyboardWillHide(sender: NSNotification) {
//    self.view.frame.origin.y = 0
//    }
    
    @IBAction func back_tap(_sender:UIButton)
    {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendVerificationcodebtn (_sender:UIButton)
    {
        if validation()
        {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OtpVerificationVC") as! OtpVerificationVC
            vc.mobileNum =  mob_txt.text!
            UserDefaults.standard.set(mob_txt.text!, forKey: "MOBILENUM")
            vc.mobileCode = lblCountrycode.text!
            print( UserDefaults.standard.set(mob_txt.text!, forKey: "MobileNum"))
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
}

    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }

  extension EnterMobVc: UITextFieldDelegate
    {
      
      func textFieldDidChangeSelection(_ textField: UITextField) {
          if /mob_txt.text?.count >= 7
          {
            self.sendVerificationcodebtn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
              self.sendVerificationcodebtn.isUserInteractionEnabled = true
          }
          else
          {
           self.sendVerificationcodebtn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
              self.sendVerificationcodebtn.isUserInteractionEnabled = false
          }
      }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
       let maxLength = 12
       let currentString: NSString = (textField.text ?? "") as NSString
       let newString: NSString =
           currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
        if textField == mob_txt {
                    let allowedCharacters = "1234567890"
                    let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
                    let typedCharacterSet = CharacterSet(charactersIn: string)
                    let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
                  return alphabet
        }
   }
    
    func validation ()-> Bool
    {
        if(mob_txt.text?.count == 0){
            createAlert(title: AppName, message: kAlertMobile)
            return false
        }
        guard let number = mob_txt.text , Int(number) ?? 0 / 2 != 0 else
        {
            createAlert(title: AppName, message: kAlertMobileValid)
            return false
        }
        if (mob_txt.text!.count < 7 ||  mob_txt.text!.count > 13 ){
            createAlert(title: AppName, message: kAlertMobilecharacter)
            
        }
        
        
    return true
}
  }
  
