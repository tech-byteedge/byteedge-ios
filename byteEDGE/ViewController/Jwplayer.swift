
import UIKit
import JWPlayerKit
import AVFoundation
//import BSImagePicker

class Jwplayer: JWPlayerViewController {
    
    var url = ""
    var mediaID = ""
    var videoUrlString = ""
    
    override func viewDidLoad()
    {
     super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func setUpPlayer(isautostart : Bool, volume: Double)
    {
        if(player.getState() == .playing)
        {
            
            player.pause()
//            player.volume()
        }
        
        videoUrlString = "https://cdn.jwplayer.com/manifests/\(mediaID).m3u8"
//        = https://cdn.jwplayer.com/v2/media/{media_id}/poster.jpg
        if let videoUrl =  URL(string:videoUrlString)
        {
            do {
                let playerItem = try JWPlayerItemBuilder()
                    .file(videoUrl)
                    .autostart(isautostart)
                    .build()
                
                let config = try JWPlayerConfigurationBuilder()
                    .playlist([playerItem])
                    .autostart(isautostart)
                    .build()
                  player.configurePlayer(with: config)
                  player.volume = 0.0
            }
            
            catch {
            print(error.localizedDescription)
            return
            }
        }
    }
    
    override func jwplayer(_ player: JWPlayer, didPauseWithReason reason: JWPauseReason) {
     super.jwplayer(player, didPauseWithReason: reason)
    }
    
    override func jwplayerIsReady(_ player: JWPlayer) {
    super.jwplayerIsReady(player)
    }
    override func jwplayer(_ player: JWPlayer, failedWithError code: UInt, message: String) {
    }
    override func jwplayer(_ player: JWPlayer, failedWithSetupError code: UInt, message: String) {
        print(message)
    }
    
    override func playerView(_ view: JWPlayerView, sizeChangedFrom oldSize: CGSize, to newSize: CGSize) {
    }
    override func jwplayer(_ player: AnyObject, adEvent event: JWAdEvent) {
    }
    
    override func jwplayerPlaylistHasCompleted(_ player: JWPlayer) {
     print("jwplayerPlaylistHasCompleted\(jwplayerPlaylistHasCompleted)")
    NotificationCenter.default.post(name: NSNotification.Name(rawValue:"jwplayerPlaylistHasCompleted"), object: nil)
    }
    
}
