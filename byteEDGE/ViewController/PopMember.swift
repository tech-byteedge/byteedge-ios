
import UIKit
import  KRProgressHUD
import  Alamofire

class PopMember: UIViewController {
    @IBOutlet weak var popMemberTbl: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var saveBtn : UIButton!
    
    @IBOutlet weak var heightpopcons: NSLayoutConstraint!
    var option = ["Edit","Delete"]
    var selected = Int()
    var grpId = ""
     var FeedId = ""
    var obj_ModelFeed:FeedDataModel?
    var viafeedpost = false
    var mayGroupDetailsdata: MyGroupDetail?
    var icon = [UIImage(named: "editImg"),UIImage(named: "delete")]
    var callBAck :((_ grpId:String?,_ mayGroupDetailsdata: MyGroupDetail?)->())?
    var removeCallBAck:(()->())?
    var feedcallback:(()->())?
    override func viewDidLoad() {
    super.viewDidLoad()
        if viafeedpost == true
        {
            self.heightpopcons.constant = 100
        }
    self.setui()
    }
    
    override func viewWillLayoutSubviews() {
    }
    
    func setui()
    {
        popMemberTbl.delegate = self
        popMemberTbl.dataSource = self
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
     dismiss(animated: false, completion: nil)
    }
}

extension PopMember: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if viafeedpost == true
        {
        return 1
       }
      
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = popMemberTbl.dequeueReusableCell(withIdentifier: "popmemberCell", for: indexPath) as!  popmemberCell
        if  viafeedpost == true
        {
        cell.celllbl.text = "Delete"
        cell.cellimg.image = UIImage(named:"delete" )
        }
        else
        {
        cell.celllbl.text = option[indexPath.row]
        cell.cellimg.image = icon[indexPath.row]
        cell.celllbl.textColor = .white
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
     if indexPath.row == 0
        {
         if viafeedpost == true
         {
            self.feedcallback?()
            }
         else
         {
             
         }
         
        self.callBAck?(self.grpId, self.mayGroupDetailsdata)
        self.dismiss(animated: false, completion: nil)
        }
        else
        {
//        removeAndBlockedApihit(role:"deleted", groupID: self.grpId)
        removeApi()
        }
        popMemberTbl.reloadData()
    }
}

extension PopMember
{
    func removeApi(){
        var param: [String: Any] = [:]
        param["isDeleted"] = true
        param["groupId"] = self.grpId
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.editgroupDetail.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: EditDetail.self,
                                        encoding : JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            if model?.Status == 200
            {
                self.dismiss(animated: false, completion: {
                self.removeCallBAck?()
                })
            }
            else
            {
            self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}

//extension PopMember
//{
//    func removeAndBlockedApihit(role: String,groupID:String)
//    {
//        var param: [String: Any] = [:]
//        param["groupMemberId"] = self.grpId
//        param["role"] = role
//        KRProgressHUD.show()
//        ServiceManager.instance.request(method: .put,
//                                        apiURL: Api.removeandBlock.url,
//                                        headers: CommonUtility.instance.headers,
//                                        parameters: param,
//                                        decodable: CommonResponseModel.self,
//                                        encoding: JSONEncoding.default)
//        { (status, json, model, error) in
//            print(model)
//            KRProgressHUD.dismiss()
//            let message = model?.message
//            let status = model?.status
//            if status == 200
//            {
//            self.dismiss(animated: true, completion: nil)
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshValue"), object: nil)
//                DispatchQueue.main.async {
//                self.popMemberTbl.reloadData()
//                }
//                self.navigationController?.popViewController(animated: false)
//                }
//            else
//            {
//            self.createAlert(title:AppName, message: model?.message ?? "")
//            }
//        }
//    }
//}



