
import UIKit
import  Foundation
import  JXPageControl
import Photos
class SplashScreen: UIViewController,UIGestureRecognizerDelegate {
    @IBOutlet  weak var Coll_splash: UICollectionView!
    @IBOutlet  weak var pagecontroller_view: JXPageControlLine!
    @IBOutlet weak var lbblSwipeLeft: UILabel!
    
     var data = [
        ["title":"BE Curious","subtitle":"Learn Real World Job Skills","bottom":"Investment in knowledge pays the best interest"],
        
        ["title":"Build your Edge","subtitle":"Learn 3 Things in 3 Minutes","bottom":"Education is not preparation for life, it is life itself"],
        
        ["title":"Learn in your Community","subtitle":"Ability Group based Social Learning","bottom":"Education is the currency of the future"]
        ]
    
    var moveToIndexAt = IndexPath(item: 0, section: 0)
    var imageSlider = [#imageLiteral(resourceName: "darkSplsh1"),#imageLiteral(resourceName: "SeconScroll"),#imageLiteral(resourceName: "splashdardhand")]
    internal let numberOfPages = 3
    override func viewDidLoad() {
    super.viewDidLoad()
    self.Coll_splash.isPagingEnabled = true
    self.setui()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func setui()
   {
       self.lbblSwipeLeft.isHidden = false
   self.pagecontroller_view.numberOfPages = imageSlider.count
   self.Coll_splash.delegate = self
   self.Coll_splash.dataSource = self
   self.Coll_splash.showsVerticalScrollIndicator = true
   self.pagecontroller_view.tintColor = .white
   self.pagecontroller_view.activeColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
   self.pagecontroller_view.inactiveColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        
   }
}

extension SplashScreen: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate
{
    
    @objc func getStarted(_ sender : UIButton) {
        if sender.tag == 2
        {
            
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "EnterMobVc")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func scrollToNextCell(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            if self.moveToIndexAt.item < 2 {
                self.moveToIndexAt.item += 1
            } else {
                self.moveToIndexAt.item = 0
            }

            if self.moveToIndexAt.item == 0 {
                self.Coll_splash.moveToFrame(contentOffset: 0.0)
            } else {
                self.Coll_splash.scrollToNextItem()
            }
        }
       }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.Coll_splash.dequeueReusableCell(withReuseIdentifier: "SplashScreenCell", for: indexPath) as! SplashScreenCell
        cell.heading.text  = data[indexPath.item]["title"]
        cell.subheading.text  = data[indexPath.item]["subtitle"]
        cell.bottom_lbl.text  = data[indexPath.item]["bottom"]
        cell.img_Splash.image  = imageSlider[indexPath.item]
        cell.getStartedbtn.addTarget(self, action: #selector(getStarted(_:)), for:.touchUpInside)
        cell.getStartedbtn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        cell.getStartedbtn.tag = indexPath.item
        
        if indexPath.item == 0
        {
            let stringValue = "BE Curious"
            let stringValue1 = "Learn Real World Job Skills"
            let stringValue2 = "Investment in knowledge pays the best interest"
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
            let attributedString1: NSMutableAttributedString = NSMutableAttributedString(string: stringValue1)
            let attributedString2: NSMutableAttributedString = NSMutableAttributedString(string: stringValue2)
            
            attributedString.setColorForText(textForAttribute: "Curious", withColor: #colorLiteral(red: 0.892123282, green: 0.3039176464, blue: 0.47864151, alpha: 1))
            attributedString1.setColorForText(textForAttribute: "Job Skills", withColor: #colorLiteral(red: 0.892123282, green: 0.3039176464, blue: 0.47864151, alpha: 1))
            attributedString2.setColorForText(textForAttribute: stringValue2, withColor: UIColor.white)
            cell.heading.attributedText = attributedString
            cell.subheading.attributedText = attributedString1
            cell.bottom_lbl.attributedText = attributedString2
        }
        
          else if indexPath.item == 1
          {
            let stringValue = "Build your Edge"
            let stringValue1 = "Learn 3 Things in 3 Minutes"
            let stringValue2 = "Education is not preparation for life, it is life itself"
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
            let attributedString1: NSMutableAttributedString = NSMutableAttributedString(string: stringValue1)
            let attributedString2: NSMutableAttributedString = NSMutableAttributedString(string: stringValue2)
            
            attributedString.setColorForText(textForAttribute: "Edge", withColor: #colorLiteral(red: 0.892123282, green: 0.3039176464, blue: 0.47864151, alpha: 1))
            attributedString1.setColorForText(textForAttribute: "3 Minutes", withColor: #colorLiteral(red: 0.892123282, green: 0.3039176464, blue: 0.47864151, alpha: 1))
            attributedString2.setColorForText(textForAttribute: stringValue2, withColor: UIColor.white)
            cell.heading.attributedText = attributedString
            cell.subheading.attributedText = attributedString1
            cell.bottom_lbl.attributedText = attributedString2
        }
        if indexPath.item == 2
        {
            let stringValue = "Learn in your Community"
            let stringValue1 = "Ability Group based Social Learning"
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
            let attributedString1: NSMutableAttributedString = NSMutableAttributedString(string: stringValue1)
            
            attributedString.setColorForText(textForAttribute: " Community", withColor: #colorLiteral(red: 0.892123282, green: 0.3039176464, blue: 0.47864151, alpha: 1))
            attributedString1.setColorForText(textForAttribute: "Social Learning", withColor: #colorLiteral(red: 0.892123282, green: 0.3039176464, blue: 0.47864151, alpha: 1))
            cell.heading.attributedText = attributedString
            cell.subheading.attributedText = attributedString1
           
            
            
        }
       
        
        if indexPath.item  == 0
        {
            
        cell.getStartedbtn.isHidden = true
        }
        else if indexPath.item == 1
        {
            
        cell.getStartedbtn.isHidden = true
        }
        else
        {
            
       cell.getStartedbtn.isHidden = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.Coll_splash.bounds.width , height: self.Coll_splash.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
   {
        pagecontroller_view.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
       
    
       if Int(scrollView.contentOffset.x) / Int(scrollView.frame.width) == 2{
       }
    }
    
   func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
      pagecontroller_view.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
        if Int(scrollView.contentOffset.x) / Int(scrollView.frame.width) == 1{
            print("1")
         }
        
     else  if Int(scrollView.contentOffset.x) / Int(scrollView.frame.width) == 2{
         print("2")
        }
   }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pagewidth = scrollView.frame.width
        let currentPage = Int((scrollView.contentOffset.x + pagewidth / 2) / pagewidth)
        if currentPage == 1{
            self.lbblSwipeLeft.isHidden = false
        }else if currentPage == 2{
            self.lbblSwipeLeft.isHidden = true
        }
    }
    
}


























   extension UICollectionView {
    func scrollToNextItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }

    func moveToFrame(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
    }
}


// MARK: CollectionCell
class SplashScreenCell: UICollectionViewCell
{
    @IBOutlet  weak var img_Splash: UIImageView!
    @IBOutlet weak var top_image: UIImageView!
    @IBOutlet  weak var heading: UILabel!
    @IBOutlet  weak var subheading: UILabel!
    @IBOutlet  weak var bottom_lbl: UILabel!
    @IBOutlet  weak var pageControll: UIView!
    @IBOutlet  weak var getStartedbtn: UIButton!
    
    override class func awakeFromNib()
    {
    super.awakeFromNib()
    }
}

extension NSMutableAttributedString {
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)

        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)

        // Swift 4.1 and below
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
}
