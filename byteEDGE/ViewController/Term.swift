
//  Term.swift
//  byteEDGE
//  Created by Pawan Kumar on 15/02/22.


import UIKit
import  WebKit
import  KRProgressHUD
import  Alamofire

class Term: UIViewController {
    @IBOutlet weak var containerView: UIView!
    var showsHorizontalScrollIndicator = Bool()
    var webView: WKWebView!
    var datamodelterm : TermModel?
    var type = Int()
    var discriptionofTerm = ""
    var titleMain = ""
    @IBOutlet weak var topHeading: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpWkWebView()
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 10
        self.topHeading.text = titleMain
        DispatchQueue.main.async {
            self.termApi(type:self.type)
        }
    }
    
    
    private func setUpWkWebView() {
        self.automaticallyAdjustsScrollViewInsets = true
        self.webView = WKWebView(frame: self.containerView.bounds, configuration: WKWebViewConfiguration())
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.webView.navigationDelegate = self
        self.webView.layer.cornerRadius = 10
        self.webView.dropShadows()
        self.containerView.addSubview(self.webView)
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.isOpaque = false
        self.webView.backgroundColor = UIColor.white

//        self.webView.backgroundColor = UIColor.black
//        self.webView.tintColor = UIColor.white
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.termApi(type:type )
    }
    
    override func viewWillLayoutSubviews() {
        webView.layer.masksToBounds = true;
    }
    @IBAction func backTap(_sender: UIButton)
    {
        self.navigationController?.popViewController(animated: false)
    }
}

//MARK: ApiTerm

extension Term
{
    func termApi(type:Int)
    {
        KRProgressHUD.show()
        var param: [String: Any] = [:]
        param["type"] = type
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.term.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: TermModel.self,
                                        encoding: URLEncoding.default)
        
        { (status, json, model, error) in
            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                self.datamodelterm = model
                  self.type = self.datamodelterm?.data?.cms?.type ?? 0
                  self.discriptionofTerm = self.datamodelterm?.data?.cms?.description ?? ""
                  self.webView.loadHTMLString(self.discriptionofTerm, baseURL: nil)
             
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
            
        }
    }
}






//-------------------------------------------------------
//MARK: ModelClass
struct TermModel: Codable {
    let Code, status: Int?
    let message: String?
    let data: DataofTerm?
}
// MARK: - DataClass
struct DataofTerm: Codable {
    let cms: CMS?
}
// MARK: - CMS
struct CMS: Codable {
    let _id: String?
    let type: Int?
    let description: String?
    let status: Bool?
    let createdAt, updatedAt: String?
}
//-------------------------------------------------------



extension Term: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let textSize = 100
        let javascript = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(textSize)%'"
        webView.evaluateJavaScript(javascript) { (response, error) in
            print()
        }
    }
}


//extension Data {
//    var html2AttributedString: NSAttributedString? {
//        do {
//            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch {
//            print("error:", error)
//            return  nil
//        }
//    }
//    var html2String: String { html2AttributedString?.string ?? "" }
//}
//
//extension StringProtocol {
//    var html2AttributedString: NSAttributedString? {
//        Data(utf8).html2AttributedString
//    }
//    var html2String: String {
//        html2AttributedString?.string ?? ""
//    }
//}
