
import UIKit
class RequestGrp: UIViewController {
    @IBOutlet weak var requestTbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
    }
    
    func setui()
    {
        requestTbl.delegate = self
        requestTbl.dataSource = self
    }
}
extension RequestGrp: UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        else
        {
            return 10
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell = requestTbl.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            return cell
        }
        else
        {
            let cell = requestTbl.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
