
import UIKit
import Foundation
import  Alamofire
import KRProgressHUD

class UserCanEditGroupDetaisVc: UIViewController,UITextFieldDelegate{
    @IBOutlet weak var impTopHeader: UIImageView!
    @IBOutlet weak var tblUserEditgroup: UITableView!
    @IBOutlet weak var headerUserEdit: UIView!
    @IBOutlet var footer: UIView!
    @IBOutlet weak var saveBtnclick: UIButton!
    @IBOutlet weak var saveBtn: RoundedCornerbutton!
    @IBOutlet weak var namelbl: UITextField!
    
    
    @IBOutlet weak var uploadImg: UIButton!
    var mayGroupDetailsdata: MyGroupDetail?
    var grpId = ""
    var about = ""
    var reason = ""
    var groupImage: UIImage?
    var name = ""
    var callBack:(()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        impTopHeader.image = UIImage(named: "")
        self.setui()
    }
    
    override func viewWillLayoutSubviews() {
    self.saveBtnclick.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    func setui()
    {
        self.namelbl.delegate = self
        self.tblUserEditgroup.register(UINib(nibName: "TitleCell", bundle: nil), forCellReuseIdentifier: "TitleCell")
        self.tblUserEditgroup.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
        tblUserEditgroup.delegate = self
        tblUserEditgroup.dataSource = self
        var urlImage = mayGroupDetailsdata?.data?.detail?.groupDetail?.image ?? ""
        self.impTopHeader.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: "Ind"), options: .highPriority, context: nil)
         self.namelbl.text = mayGroupDetailsdata?.data?.detail?.groupDetail?.name
        self.name = mayGroupDetailsdata?.data?.detail?.groupDetail?.name ?? ""
        self.uploadImg.layer.cornerRadius = 15

        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.batteryLevelChanged),
            name: UIDevice.batteryLevelDidChangeNotification,
            object: nil)
        
        
    }
    
     //MARK: DELEGATE OF TEXTFIELD
    func textFieldDidEndEditing(_ textField: UITextField) {
    self.name = namelbl.text ?? ""
    }
    
    @objc private func batteryLevelChanged(notification: NSNotification){
        
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func saveBtn(_ sender: UIButton) {
    self.editapi()
    }
    
    @IBAction func uploadClick(_ sender: Any) {
        ImagePickerManager().pickImage(self){ image in
           self.impTopHeader.image = image
           self.groupImage = image
    }
    
}
}
extension UserCanEditGroupDetaisVc: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tblUserEditgroup.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.titlLbl.text = "ABOUT GROUP"
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tblUserEditgroup.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
            //cell.isAbout = true
            cell.listtxtview.isUserInteractionEnabled = true
            cell.callBAck = {
                aboutVal,isAbout in
                self.about = aboutVal ?? ""
                print("about\(self.about)")
                
                
            }
            cell.listtxtview.text = mayGroupDetailsdata?.data?.detail?.groupDetail?.about
            self.about = mayGroupDetailsdata?.data?.detail?.groupDetail?.about ?? ""
            return cell
        }
        else if  indexPath.row == 2
        {
            let cell = tblUserEditgroup.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.titlLbl.text = "WHY SHOULD YOU JOIN GROUP?"
            return cell
        }
        else
        {
            let cell = tblUserEditgroup.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
            cell.listtxtview.isUserInteractionEnabled = true
            cell.callBAck = {
                aboutVal,isAbout in
                self.reason = aboutVal ?? ""
                print("reason\(self.reason)")
                
            }
            self.reason = mayGroupDetailsdata?.data?.detail?.groupDetail?.joinReason ?? ""
            cell.listtxtview.text = mayGroupDetailsdata?.data?.detail?.groupDetail?.joinReason
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerUserEdit
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }
      func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footer
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  indexPath.row == 0
        {
            return  45
        }
        else if indexPath.row == 1
        {
            return UITableView.automaticDimension
        }
        else if indexPath.row == 2
        {
            return  45
        }
        else
        {
          return UITableView.automaticDimension
        }
    }
}



extension UserCanEditGroupDetaisVc
{
    func editapi() {
        var param: [String: Any] = [:]
        param["name"] = name
        param["about"] = self.about
        param["joinReason"] = self.reason
        param["groupId"] = self.grpId
        
        
        var imgParam: [String : UIImage] = [:]
        if let img = self.groupImage {
        imgParam["image"] = img
        }
        KRProgressHUD.show()
        ServiceManager.instance.uploadImageRequestWithDecodable(
            method: .post,
            apiURL: Api.editgroupDetail.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: EditDetail.self,
            imageParameters: imgParam) { success, response, Error in
                KRProgressHUD.dismiss()
                print(response)
             guard response?.Status == 200 else {
                if let message = response?.message {
                    }else {
                    self.createAlert(title: kAppName, message: "Something Went wrong")
                    }
                    return
             }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
                self.navigationController?.popToRootViewController(animated: false)
            }
    }
}

//MARK: MODEL
struct EditDetail: Codable {
    let code,Status : Int?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        case Status = "status"
        case message = "message"
        case code = "code"
    }
}
