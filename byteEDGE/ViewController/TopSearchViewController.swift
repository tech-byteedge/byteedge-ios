
//  TopSearchViewController.swift
//  byteEDGE
//  Created by Pawan Kumar on 27/01/22.

import UIKit
import Alamofire
import IQKeyboardManagerSwift
import AVFoundation
import Foundation
import Speech
import AVKit
import  KRProgressHUD

class TopSearchViewController: UIViewController,UITextFieldDelegate {
    let speechBtn = UIButton()
    var speechText = String()
    var speechTextField = UITextField()
    let speechRecognizer        = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    var recognitionRequest      = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask         : SFSpeechRecognitionTask?
    let audioEngine             = AVAudioEngine()
     var isRecognizing = Bool()
    let audioSession = AVAudioSession.sharedInstance()
    
    @IBOutlet weak var tblTopSearch: UITableView!
    @IBOutlet weak var txttopSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var imgMic: UIImageView!
    @IBOutlet weak var btnOnMic: UIButton!
    
    var dataOfSearchByuser: DataOfSearch?
    var callBAck:(()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        txttopSearch.delegate = self
        self.tblTopSearch.delegate = self
        self.tblTopSearch.dataSource = self
        imgMic.image = UIImage(named: "micIcon")
        self.txttopSearch.rightView = speechBtn
        self.txttopSearch.rightViewMode = .always
        setui()
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: AVAudioSession.Mode.measurement, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        audioEngine.inputNode.removeTap(onBus: 0)
    }
    
    @IBAction func dissmissBtn(_ sender: Any) {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    @IBAction func actionOnMic(_ sender: UIButton) {
        imgMic.loadGif(name: "mic")
        speechBtn.loadingIndicator(true)
        sender.isUserInteractionEnabled = false
        newstartRecording()
    }

    func setui()
    {
        self.txttopSearch.attributedPlaceholder = NSAttributedString(string: "Search Skills etc.",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.viewSearch.selectedborder()
        searchApi(text: "")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if txttopSearch.text == "" && dataOfSearchByuser?.searchData?.count == 0 {
            self.tblTopSearch.setEmptyMessageshow()
        }
        else{
            self.tblTopSearch.restore()
            self.searchApi(text: txttopSearch.text ?? "")
        }
        audioEngine.inputNode.removeTap(onBus: 0)
        self.tblTopSearch.reloadData()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if txttopSearch.text == "" && dataOfSearchByuser?.searchData?.count == 0 {
            self.tblTopSearch.setEmptyMessageshow()
        }
        else{
            self.tblTopSearch.restore()
            self.searchApi(text: txttopSearch.text ?? "")
        }
        audioEngine.inputNode.removeTap(onBus: 0)
        self.tblTopSearch.reloadData()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.searchApi(text: txttopSearch.text ?? "")
        audioEngine.inputNode.removeTap(onBus: 0)
    }
    
    //------------------------------------------------------------------------------
    //    func startRecording()
    //    {
    //        // Clear all previous session data and cancel task
    ////        if recognitionTask != nil {
    ////            recognitionTask?.cancel()
    ////            recognitionTask = nil
    ////        }
    //print(recognitionTask)
    //        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
    //        let inputNode = audioEngine.inputNode
    //        let recordingFormat = inputNode.outputFormat(forBus: 0)
    //        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
    //        self.recognitionRequest.append(buffer)
    //        }
    //       let recognitionRequest = recognitionRequest
    //        recognitionRequest.shouldReportPartialResults = true
    //        self.recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
    //            var isFinal = false
    //            if result != nil {
    //                self.speechTextField.text = result?.bestTranscription.formattedString
    //                isFinal = (result?.isFinal)!
    //            }
    //            if error != nil || isFinal {
    //                self.audioEngine.stop()
    //                inputNode.removeTap(onBus: 0)
    ////                self.recognitionRequest = nil
    //                self.recognitionTask = nil
    //                self.speechBtn.isEnabled = true
    //            }
    //        })
    //
    //
    //        self.audioEngine.prepare()
    //
    //        do {
    //            try self.audioEngine.start()
    //        } catch {
    //            print("audioEngine couldn't start because of an error.")
    //        }
    //
    //        self.speechTextField.text = "Say something, I'm listening!"
    //    }
    
    func newstartRecording() {
        isRecognizing = true
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        //Change / Edit Start
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        //Change / Edit Finished
        var recognitionRequestNew = SFSpeechAudioBufferRecognitionRequest()
        // Setup audio engine and speech recognizer
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.reset()
        node.removeTap(onBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            recognitionRequestNew.append(buffer)
        }
       
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
        
        // Analyze the speech
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequestNew, resultHandler: { result, error in
            if let result = result {

                self.speechText = result.bestTranscription.formattedString
                print("speechText:self.speechText \(result.isFinal)")
                if (result.isFinal){
                    self.isRecognizing = true
                    self.imgMic.image =  UIImage(named: "micIcon")
                    self.txttopSearch.text = self.speechText
                    self.searchApi(text: result.bestTranscription.formattedString)
                    self.audioEngine.stop()
                                    node.removeTap(onBus: 0)
//                                    self.recognitionRequest = nil
                                    self.recognitionTask = nil
                                    self.speechBtn.isEnabled = true
//                    self.recognitionRequest.endAudio()
                }
                NSLog(result.bestTranscription.formattedString)
                self.recognitionTask?.finish()
            } else if let error = error {
                print(error)
                self.imgMic.image =  UIImage(named: "micIcon")
                self.speechBtn.loadingIndicator(false)
                NSLog(error.localizedDescription)
            }
        })
    }
    
    func finishSpeech(){
        isRecognizing = false
        audioEngine.stop() //AVAudioEngine()
        recognitionTask?.cancel() //speechRecognizer?.recognitionTask
        recognitionRequest.endAudio()  //SFSpeechAudioBufferRecognitionRequest?
        audioEngine.inputNode.removeTap(onBus: 0)
    }

    
    func configurationTextField(textField: UITextField!){
        textField.placeholder = "Say something, I'm listening!"
        speechTextField = textField
        speechTextField.text = self.speechText
        speechTextField.delegate = self
    }
    
}


extension TopSearchViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailsViewController") as! CourseDetailsViewController
        //        if let id = self.dataOfSearchByuser?.searchData?[indexPath.row]._id {
        //        vc.categoryId = id
        //        }
        //        self.navigationController?.pushViewController(vc, animated: false)
        
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
        if let courseId = self.dataOfSearchByuser?.searchData?[indexPath.row]._id {
            vc.courseId = courseId
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  dataOfSearchByuser?.searchData?.count == 0
        {
            self.tblTopSearch.setEmptyMessageshow()
        }
        else
        {
            self.tblTopSearch.restore()
        }
        return dataOfSearchByuser?.searchData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblTopSearch.dequeueReusableCell(withIdentifier: "topSearchCell") as! topSearchCell
        cell.searchLbl.text = dataOfSearchByuser?.searchData?[indexPath.row].courseName
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


// MARK: API FOR SEACH LIST
extension TopSearchViewController
{
    func searchApi(text:String){
        var param : [String: Any] = [:]
        param["search"] = text
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.searchlist.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: SearchList.self,
                                        encoding : URLEncoding.queryString)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                self.dataOfSearchByuser = model?.data
                DispatchQueue.main.async {
                   
                    self.tblTopSearch.reloadData()
//
                    self.btnOnMic.isUserInteractionEnabled = true
                }
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}

extension TopSearchViewController: SFSpeechRecognizerDelegate {
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            self.speechBtn.isEnabled = true
        } else {
            self.speechBtn.isEnabled = false
        }
    }
}

// MARK: cell Class
class topSearchCell: UITableViewCell
{
    @IBOutlet weak var searchLbl: UILabel!
}

extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}



