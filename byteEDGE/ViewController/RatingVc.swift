
import UIKit
import Cosmos
import Alamofire
import  KRProgressHUD

class RatingVc: UIViewController {
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    
    var courseId = ""
    override func viewDidLoad() {
        ratingView.rating = 0
        super.viewDidLoad()
        print(courseId)
        ratingView.settings.fillMode = .half
//        self.ratingApi()
    }
    override func viewWillLayoutSubviews() {
    self.submitBtn.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    @IBAction func submitBtn(_ sender: UIButton) {
        self.ratingApi()
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    AppUtility.lockOrientation(.all)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    AppUtility.lockOrientation(.portrait)
    }
}

//MARK: RatingApi........
extension RatingVc
{
    func ratingApi()
       {
        var param: [String: Any] = [:]
           param["rating"] = self.ratingView.rating
        param ["courseId"] = self.courseId
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.ratingAdd.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: CommonResponseModel.self,
                                        encoding: JSONEncoding.default
        ) { (status, json, model, error) in
            let status = model?.status
            if status == 200
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refrseh"), object: nil)
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popToRootViewController(animated: true)
            }
            else {
            self.createAlert(title: AppName, message: model?.message ?? "")
            }
        }
    }
}



