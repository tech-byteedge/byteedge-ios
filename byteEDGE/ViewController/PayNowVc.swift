//
//  PayNowVc.swift
//  byteEDGE
//  Created by Pawan Kumar on 28/02/22.

import UIKit
import KRProgressHUD
import  Alamofire
import  Foundation
import  SDWebImage


class PayNowVc: UIViewController {
    @IBOutlet weak var imgpop: UIImage!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var seepreview: UIButton!
    @IBOutlet weak var paynow: UIButton!
    var courseData:LearningListData?
    var callBack:(()->())?
    var callBackofSeepreview:(()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismiss(animated: false, completion: nil)
    }
    
    
    private func setui()
    {
     self.seepreview.isselected(borderColor: .black)
     self.seepreview.tintColor = .black
    }
    
    @IBAction func seePreviewTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.callBackofSeepreview?()
                })
                }
    
        
    @IBAction func paynow(_ sender: Any) {
       
        self.dismiss(animated: true, completion: {
            self.callBack?()
                })
      
    }
}
    
    
