
import UIKit
import Alamofire
import Foundation

class LogoutpopUpVc: UIViewController {
    @IBOutlet weak var btnView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnView.clipsToBounds = true
        let view = CommonButtonView.fromNib()
        view.firstCallBack = {
            view.secondName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            view.firstView.backgroundColor = #colorLiteral(red: 0.1461495757, green: 0.1526317596, blue: 0.183000803, alpha: 1)
            view.secondView.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            view.img1st.isHidden = false
            view.img2nd.isHidden = true
            view.firstName.text = "No"
            view.secondName.text = "Yes"
            self.dismiss(animated: false, completion: nil)
        }
        view.secondCallBack = {
            
            view.img1st.isHidden = true
            view.img2nd.isHidden = false
            self.logout()
//            self.logoutApi()
        }
        view.initSetup(.LogoutpopUpVc)
        view.frame = btnView.bounds
        self.btnView.addSubview(view)
    }
}


//MARK: logoutApi

extension LogoutpopUpVc
{
func logoutApi()
{
    ServiceManager.instance.request(method: .post, apiURL: Api.logout.url, headers: CommonUtility.instance.headers , parameters: nil, decodable: Logout.self, encoding: JSONEncoding.default)
        {(status, json, model, error) in
            let status = model?.status
            if status ==  200
            {
            self.createAlertCallback(title: AppName, message: model?.message ?? "", cancelTitle: "cancel") { (value) in
            if value == true
          {
          CommonUtility.instance.homevc()
          }
        else
        {
                    
        }
                
            }
}
}
}
}


            







struct Logout: Codable
{
let Code:Int?
let status: Int?
let message: String?
}


