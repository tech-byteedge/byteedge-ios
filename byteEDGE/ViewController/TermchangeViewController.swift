//
//  TermchangeViewController.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 03/06/22.
//

import UIKit
import  Alamofire
import  Foundation

class TermchangeViewController: UIViewController {
    @IBOutlet weak var tvl_term: UITableView!
    @IBOutlet weak var topHeading: UILabel!
    
    var datamodelterm : TermModeldata?
    var type = Int()
    var discriptionofTerm = ""
    var titleMain = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvl_term.registerCell(type: DetailCellTableViewCell.self)
        self.topHeading.text = titleMain
        self.tvl_term.delegate = self
        self.tvl_term.dataSource = self
        self.tvl_term.backgroundColor = .clear
       
        
        
        DispatchQueue.main.async {
        self.termApi(type:self.type)
        }
    }
    
    @IBAction func back_Tap(_sender:UIButton)
    {
    self.navigationController?.popViewController(animated: false)
        
    }
    
}

extension TermchangeViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tvl_term.dequeueReusableCell(withIdentifier: "DetailCellTableViewCell") as! DetailCellTableViewCell
        cell.detaillbl.numberOfLines = 0
        cell.detaillbl.textColor = UIColor.white
        cell.detaillbl.backgroundColor = UIColor.black
        cell.selectionStyle = .none
        cell.detaillbl.text = self.datamodelterm?.data?.cms?.description?.html2String ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension TermchangeViewController
{
    func termApi(type:Int)
    {
        var param: [String: Any] = [:]
        param["type"] = type
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.term.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: TermModeldata.self,
                                        encoding: URLEncoding.default)
        
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                self.datamodelterm = model
                DispatchQueue.main.async {
                self.discriptionofTerm = self.datamodelterm?.data?.cms?.description ?? ""
                self.type = self.datamodelterm?.data?.cms?.type ?? 0
                   
                self.tvl_term.reloadData()
                }
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
            
        }
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}

struct TermModeldata: Codable {
    let Code, status: Int?
    let message: String?
    let data: DataofTermdata?
}
// MARK: - DataClass
struct DataofTermdata: Codable {
    let cms: CMSdata?
}
// MARK: - CMS
struct CMSdata: Codable {
    let _id: String?
    let type: Int?
    let description: String?
    let status: Bool?
    let createdAt, updatedAt: String?
}
