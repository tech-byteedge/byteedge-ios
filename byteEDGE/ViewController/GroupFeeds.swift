//
//  GroupFeeds.swift
//  byteEDGE
//  Created by Pawan Kumar on 07/03/22.

import UIKit
import  Alamofire
import CoreAudio
import KRProgressHUD
import  AVKit
import  AVFoundation
import GameKit
import FirebaseDynamicLinks

enum CallBAckTypeshow{
 case firstCallBack,secondCallBack
}

class GroupFeeds: UIViewController
{
    @IBOutlet weak var moveCreatpostVc: UIButton!
    @IBOutlet weak var tblCommunity: UITableView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet var titleheader: UIView!
    var maintype = ""
    var text = ""
    var selected = Int()
    var callBAckType : CallBAckTypeshow = .firstCallBack
    var objfeedlist: FeelList?
    let playerViewController = AVPlayerViewController()
    var _id = String()
    var section_type = Int()
    var selectedCell = Int()
    var idforFeed = ""
    var thumbnail = ""
    
     override func viewDidLoad() {
        super.viewDidLoad()
         self.imgPost.maskCircle()
        self.feedlistApi()
        print(idforFeed)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name:Notification.Name("NotificationIdentifier"), object: nil)
        
        //MARK: Referesh page
        let refreshControl = tblCommunity.addRefreshControl(target: self,
                                                            action: #selector(doRefresh(_:)))
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle =
        NSAttributedString(string: "",
                           attributes: [
                            NSAttributedString.Key.foregroundColor: UIColor.black,
                            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-UltraLight",
                                                                size: 36.0)! ])
    
        tblCommunity.isUserInteractionEnabled = true
        self.registedXib()
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbNailImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    
    //MARK: refresh page
    @objc func doRefresh(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.tintColor = self.view.backgroundColor // store color
            self.view.backgroundColor = UIColor.black
        }) { _ in
            self.feedlistApi()
            self.view.backgroundColor = self.view.tintColor // restore color
        }
    }
    
    @IBAction func movetopost(_ sender: Any) {
        
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier:
                                                                    "CreatePostVc") as! CreatePostVc
        vc.idforFeed = idforFeed
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.feedlistApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.feedlistApi()
    }
    
    //MARK: Register XibCell
    
    func registedXib()
    {
        self.tblCommunity.register(UINib(nibName: "Community", bundle: nil), forCellReuseIdentifier: "Community")
        self.tblCommunity.register(UINib(nibName: "MultipleImgCell", bundle: nil), forCellReuseIdentifier: "MultipleImgCell")
        tblCommunity.delegate = self
        tblCommunity.dataSource = self
    }
}

//MARK:  Delegate and DataSource
extension GroupFeeds: UITableViewDelegate,UITableViewDataSource
{

    func numberOfSections(in tableView: UITableView) -> Int {
        if callBAckType == .firstCallBack{
            return  1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if callBAckType == .firstCallBack{
            if  objfeedlist?.data?.feedList?.count ?? 0 == 0
            {
            self.tblCommunity.setemptyforfeed()
            }
            else
            {
             self.tblCommunity.restore()
            }
            return objfeedlist?.data?.feedList?.count ?? 0
        }
            else
            {
                return 0
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if callBAckType == .firstCallBack //Feed
         {
            let cell = self.tblCommunity.dequeueReusableCell(withIdentifier: "MultipleImgCell") as! MultipleImgCell
            cell.btnFeedOptions.isHidden = true
            cell.multiProfileImage.layer.masksToBounds = true
            cell.multiProfileImage.layer.cornerRadius = cell.multiProfileImage.frame.size.height/2;
            cell.multiProfileImage.layer.masksToBounds = true
            cell.multiProfileImage.layer.borderWidth=1
            cell.multiName.text = objfeedlist?.data?.feedList?[indexPath.row].userDetail?.name
            cell.multiDES.text = objfeedlist?.data?.feedList?[indexPath.row].description
            var url = objfeedlist?.data?.feedList?[indexPath.row].userDetail?.image ?? ""
//            self.imgPost.contentMode = .scaleAspectFit
            self.imgPost.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"prfImg")), options: .highPriority, context: nil)
            cell.multiProfileImage.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"prfImg")), options: .highPriority, context: nil)
            cell.multiName.text = objfeedlist?.data?.feedList?[indexPath.row].userDetail?.name ?? "pawan kumar"
            cell.timelbl.text =  CommonUtility.instance.getDateTIMEFromString(inputDate:(objfeedlist?.data?.feedList?[indexPath.row].createdAt)!)
            UserDefaults.standard.set(cell.timelbl.text, forKey: "Time")
           
            if objfeedlist?.data?.feedList?[indexPath.row].image?.count == 0  && ((objfeedlist?.data?.feedList?[indexPath.row].video) != nil)
            {
                
                DispatchQueue.main.async {
                    var urlImage  = self.objfeedlist?.data?.feedList?[indexPath.row].thumbnails ?? ""
                cell.singleImg.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                }
                
                cell.secodImg.isHidden = true
                cell.thirdImg.isHidden = true
                cell.singleImg.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = true
                if UIDevice.current.userInterfaceIdiom == .pad {
                cell.height.constant = 380
                }
                else
                {
                cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = false
                cell.countLbl.isHidden = true
                DispatchQueue.main.async { [self] in
            


                    
//                    if urlImage == ""{
//                        self.getThumbnailImageFromVideoUrl(url: URL(string: objfeedlist?.data?.feedList?[indexPath.row].video ?? "")!) { (thumbNailImage) in
//                            cell.singleImg.image = thumbNailImage
////                            self.yourImageView.image = thumbNailImage
//                        }
////                        cell.singleImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
//                    }else{
//                        cell.singleImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
//                    }
               
                    
                }
                cell.callBackvedio = {
                    if let url = URL(string: self.objfeedlist?.data?.feedList?[indexPath.row].video ?? "" ) {
                        let vedio_play = AVPlayer(url: url)
                        self.playerViewController.player = vedio_play
                        self.present(self.playerViewController, animated: true) {
                         vedio_play.play()
                        }
                    }
                }
            }
            if  objfeedlist?.data?.feedList?[indexPath.row].image?.count == 0 && objfeedlist?.data?.feedList?[indexPath.row].video == ""
            {
                cell.secodImg.isHidden = true
                cell.thirdImg.isHidden = true
                cell.singleImg.isHidden = true
                cell.fourthImage.isHidden = true
                cell.height.constant = 0
                cell.vedioPLAY.isHidden = true
                cell.stackViewImg.isHidden = true
                cell.twoImgStackView.isHidden = true
                cell.countLbl.isHidden = true
            }
            else if objfeedlist?.data?.feedList?[indexPath.row].image?.count == 1  {
                cell.secodImg.isHidden = true
                cell.thirdImg.isHidden = true
                cell.singleImg.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = true
                var urlImage = objfeedlist?.data?.feedList?[indexPath.row].image?[0] ?? ""
                cell.singleImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                if UIDevice.current.userInterfaceIdiom == .pad {
                cell.height.constant = 380
                }
                else
                {
                cell.height.constant = 230
                }
                cell.countLbl.isHidden = true
                cell.vedioPLAY.isHidden = true
            }

            else if objfeedlist?.data?.feedList?[indexPath.row].image?.count == 2
            {   cell.singleImg.isHidden = false
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = true
                cell.fourthImage.isHidden = true
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = true
                let urlImagefirst = objfeedlist?.data?.feedList?[indexPath.row].image?[0] ?? ""
                let urlImageSecond = objfeedlist?.data?.feedList?[indexPath.row].image?[1] ?? ""

                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
//                cell.height.constant = 175
                if UIDevice.current.userInterfaceIdiom == .pad {
                cell.height.constant = 380
                }
                else
                {
                cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = true
            }
            
            else if objfeedlist?.data?.feedList?[indexPath.row].image?.count == 3
            {   cell.singleImg.isHidden = false
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.fourthImage.isHidden = true
                cell.fourthImage.isHidden = true
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = true
                let urlImagefirst = objfeedlist?.data?.feedList?[indexPath.row].image?[0] ?? ""
                let urlImageSecond = objfeedlist?.data?.feedList?[indexPath.row].image?[1] ?? ""
                let urlImageThird = objfeedlist?.data?.feedList?[indexPath.row].image?[2] ?? ""
                
                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst ), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.thirdImg.sd_setImage(with: URL(string: urlImageThird), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
//                cell.height.constant = 175
                if UIDevice.current.userInterfaceIdiom == .pad {
                cell.height.constant = 380
                }
                else
                {
                cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = true
            }
            
            else if objfeedlist?.data?.feedList?[indexPath.row].image?.count == 4
            {   cell.singleImg.isHidden = false
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.fourthImage.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = true
                let urlImagefirst = objfeedlist?.data?.feedList?[indexPath.row].image?[0] ?? ""
                let urlImageSecond = objfeedlist?.data?.feedList?[indexPath.row].image?[1] ?? ""
                let urlImageThird = objfeedlist?.data?.feedList?[indexPath.row].image?[2] ?? ""
                let urlImageFourth = objfeedlist?.data?.feedList?[indexPath.row].image?[2] ?? ""
                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst ), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.thirdImg.sd_setImage(with: URL(string: urlImageThird), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.fourthImage.sd_setImage(with: URL(string: urlImageFourth), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
//                cell.height.constant = 175
                if UIDevice.current.userInterfaceIdiom == .pad {
                cell.height.constant = 380
                }
                else
                {
                cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = true
            }


            else if objfeedlist?.data?.feedList?[indexPath.row].image?.count ?? 0 > 4 && ((objfeedlist?.data?.feedList?[indexPath.row].video) == "")
            {
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.fourthImage.isHidden = false
                cell.singleImg.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = false
                cell.countLbl.textColor = .green
                if objfeedlist?.data?.feedList?[indexPath.row].image!.count ?? 0 > 3 {
                    let count  = objfeedlist?.data?.feedList?[indexPath.row].image!.count ?? 0
                    cell.countLbl.text =  "+" + String(count - 4)
                    cell.countLbl.textColor = .white
                }
                else{
                cell.countLbl.text = ""
                }
                let urlImagefirst = objfeedlist?.data?.feedList?[indexPath.row].image?[0] ?? ""
                let urlImageSecond = objfeedlist?.data?.feedList?[indexPath.row].image?[1] ?? ""
                let urlImageThird = objfeedlist?.data?.feedList?[indexPath.row].image?[2] ?? ""
                let urlImageFourth = objfeedlist?.data?.feedList?[indexPath.row].image?[2] ?? ""
                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                cell.thirdImg.sd_setImage(with: URL(string: urlImageThird), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                cell.fourthImage.sd_setImage(with: URL(string: urlImageFourth), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                if UIDevice.current.userInterfaceIdiom == .pad {
                cell.height.constant = 380
                }
                else
                {
                cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = true
            }
            
            if objfeedlist?.data?.feedList?[indexPath.row].ownLikes == true {
                cell.likeBtn.setImage(UIImage(named: "heart"), for: .normal)
            }else {
                cell.likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
            }
            ///for like count
            if let likeCount = objfeedlist?.data?.feedList?[indexPath.row].likes {
                likeCount == 0 ? (cell.likeBtn.setNormalTitle(normalTitle: "Like")) : (cell.likeBtn.setNormalTitle(normalTitle: "\(likeCount)" + " " +  "Like"))

            }else {
                cell.likeBtn.setNormalTitle(normalTitle: "Like")
            }

            ///for comment count
            if let commentCount = objfeedlist?.data?.feedList?[indexPath.row].comments {
                commentCount == 0 ? (cell.commetnBtn.setNormalTitle(normalTitle: "Comments")) : (cell.commetnBtn.setNormalTitle(normalTitle: "\(commentCount)" + " " +  "Comments"))

            }else {
                cell.commetnBtn.setNormalTitle(normalTitle: "Comments")
            }

               cell.commentsAction = {
                 print("pawan ::::::::::::::::::::: \(indexPath.row)")
                let vc = CommentsViewController.instantiate(fromAppStoryboard: .Main)
                if let postId = self.objfeedlist?.data?.feedList?[indexPath.row]._id {
                    vc.postId = postId
                    vc.group_id = self.idforFeed
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }

            cell.likeBtnAciton =  { status in
                print("likeaction ::::::::::::::::::::: \(indexPath.row)")
                if let id = self.objfeedlist?.data?.feedList?[indexPath.row]._id {
                    self.likePost(islike: status, postId: id, section: indexPath, groupid: self.idforFeed)
                }
            }
            
        
              cell.share =
               {
                var grpId = String()
                var  postID = String()
                   
                
                if let postId = self.objfeedlist?.data?.feedList?[indexPath.row]._id {
                  postID = postId
                   }
                   let urlStrig  = "https://byteedgetest2.page.link/dynamic_link?groupId=\(self.idforFeed)&feedId=\(postID)"
                guard let link = URL(string: urlStrig) else { return }
                let dynamicLinksDomainURIPrefix = "https://byteedgetest2.page.link"
                print(link)
                let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
                linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID:"com.bytedge1")
                linkBuilder?.iOSParameters?.appStoreID = "1631500899"
                linkBuilder?.iOSParameters?.minimumAppVersion = "1.3"
                linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.app.byteedge")
                linkBuilder?.androidParameters?.minimumVersion = 1
                linkBuilder?.androidParameters?.fallbackURL = URL.init(string: "https://play.google.com/store/apps/details?id=com.app.byteedge")
                linkBuilder?.iOSParameters?.fallbackURL = URL.init(string: "https://apps.apple.com/us/app/byteedge/id1631500899")
                linkBuilder?.iOSParameters?.customScheme = "com.bytedge1"
                guard let longDynamicLink = linkBuilder?.url else { return }
                print("The long URL is: \(longDynamicLink)")
               
                   
                let textToShare = [ longDynamicLink ] as [Any]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                // so that iPads won't crash
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                self.present(activityViewController, animated: true, completion: nil)

            }
            
            
            
            return cell
        }
        else
        {
       print("Nothing")
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return UITableView.automaticDimension
        }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if callBAckType == .firstCallBack {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            if let postId = self.objfeedlist?.data?.feedList?[indexPath.row]._id {
                vc.postId = postId
            }else {
                print("post Id not found")
            }
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            print(indexPath)
        }
        
    }
}

// MARK: Api for FeedList
extension GroupFeeds
{
    func feedlistApi()
    {
        var param: [String: Any] = [:]
        param["groupId"] = self.idforFeed
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.feedlist.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: FeelList.self,
                                        encoding: URLEncoding.default
        )
           {(status, json, model, error) in
            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                self.objfeedlist?.data?.feedList?.removeAll()
                self.objfeedlist = model
                print(model)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.tblCommunity.reloadData()
                }
                DispatchQueue.main.async {
                self.tblCommunity.reloadData()
                }
            }
            else
            {
             self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}

//MARK: -LIKE BUTTON API-
extension GroupFeeds
    {
    func likePost(islike: Bool, postId : String,section : IndexPath,groupid:String) {
        var param: [String: Any] = [:]
        param["postId"] = postId
        param["role"] = "postlike"
        param["groupId"] = self.idforFeed
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.objfeedlist?.data?.feedList?[section.row].ownLikes = islike
            guard let count =  self.objfeedlist?.data?.feedList?[section.row].likes else {
                return
            }
            
            
            if islike == true {
                self.objfeedlist?.data?.feedList?[section.row].likes = count+1
                
                
            }else {
                if  count == 0 {
                    self.objfeedlist?.data?.feedList?[section.row].likes = count
                }
                else {
                    self.objfeedlist?.data?.feedList?[section.row].likes = count-1
                }
                
                
            }
            
            DispatchQueue.main.async {
                self.tblCommunity.reloadData()
            }
            UIApplication.topViewController()?.view.isUserInteractionEnabled = true
        }
    }
}















