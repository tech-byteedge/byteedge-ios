import UIKit
import Alamofire
import SDWebImage
import  SKPhotoBrowser
import  KRProgressHUD


class AccountVc: UIViewController,UIGestureRecognizerDelegate {
    //MARK: OUTLET CONNECTION
    @IBOutlet weak var tblAccount: UITableView!
    @IBOutlet var headervView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var numProfile: UILabel!
    @IBOutlet weak var nameProfile: UILabel!
    @IBOutlet var footerViewLogin: UIView!
    
    //MARK: LIST
    var dicData = [
        ["tilte":"Personal info","leftIcon":"AccountUser",
         "rightIcon":"Icon"],
        ["tilte":"Favourite Reels","leftIcon":"book",
         "rightIcon":"Icon"],
//        ["tilte":"","leftIcon":"",
//         "rightIcon":""],
        ["tilte":"Terms","leftIcon":"term",
         "rightIcon":"Icon"],
        ["tilte":"About us","leftIcon":"about",
         "rightIcon":"Icon"],
        ["tilte":"Contact us","leftIcon":"ContactUs",
         "rightIcon":"Icon"],
        ["tilte":"FAQ's","leftIcon":"faq",
         "rightIcon":"Icon"],
    ]
    
    //MARK: VARIABLE
    var firstName = ""
    var lastName = ""
    var mob = ""
    var img =  ""
    var typeState = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAccount.separatorColor = UIColor.white.withAlphaComponent(0.5)
        self.setui()
        self.imgProfile.layer.masksToBounds = true
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2;
        imgProfile.layer.masksToBounds = true
        imgProfile.layer.borderWidth=1;
        self.loginData()
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
    self.loginData()
    }
    
    @IBAction func editTap(_ sender: UIButton) {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "PersonalnfoVcViewController") as! PersonalnfoVcViewController
          vc.typeClass = 0
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    @IBAction func photoTap(_sender:UIButton)
    {
     self.imagefull()
    }
    
    //MARK:  USERDEFAULT DATA
    func loginData()
    {
        let mobNum =  kUserDefaults.retriveString(.mobileNumber)
        self.mob = mobNum as? String ?? ""
        self.numProfile.text = self.mob
        
        let firsName =  kUserDefaults.retriveString(.firstName)
        self.firstName = firsName as?  String  ?? ""
        
        let lastname =  kUserDefaults.retriveString(.lastName)
        self.lastName = lastname as?  String  ?? ""
        self.nameProfile.text = self.firstName + " " +  self.lastName
        
         let urlImage =  kUserDefaults.retriveString(.image)
         self.imgProfile.sd_setImage(with: URL(string:urlImage as! String), placeholderImage: UIImage(named:"prfImg"), options: .highPriority, context: nil)
    }
    
    func imagefull()
    {
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImage(self.imgProfile.image!)// add some UIImage
        images.append(photo)
        // 2. create PhotoBrowser Instance, and present from your viewController.
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    private func  setui()
    {
        self.tblAccount.delegate = self
        self.tblAccount.dataSource = self
    }
    
    func showToast(controller: UIViewController, message : String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.red
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    
    @IBAction  func logout(_sender:UIButton)
    {
        kUserDefaults.set("4", forKey: "signFlow")
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"LogoutpopUpVc") as! LogoutpopUpVc
        vc.modalPresentationStyle = .fullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func badgeClick(_ sender: Any) {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CertificatedVc") as! CertificatedVc
        self.navigationController?.pushViewController(vc, animated: false)
        
//     showToast(controller: self, message: "Coming Soon", seconds: 2.0)
    }
    
    @IBAction func downloadTap(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        vc.typeint  = "1"
        self.navigationController?.pushViewController(vc, animated: true)
//        let purchaseId = (objPurchase?.data?.purchaseList[indexPath.row]._id)
//        vc.orderID = purchaseId ?? ""
//     showToast(controller: self, message: "Coming Soon", seconds: 2.0)
    }
    
    @IBAction func purchse_His(_ sender: Any) {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "SubcriptionVc") as! SubcriptionVc
       self.navigationController?.pushViewController(vc, animated: false)
    }
}

//@available(iOS 15.0, *)
extension AccountVc:UITableViewDelegate,UITableViewDataSource
{
    @objc func presentWebBrowser(sender:UIButton)
    {
        if sender.tag == 0
        {
            print("pawan")
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "PersonalnfoVcViewController") as! PersonalnfoVcViewController
            vc.typeClass = 1
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        else if sender.tag == 1
        {
            print("favReels")
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "BookMarksRealsaFav") as! BookMarksRealsaFav
           
            self.navigationController?.pushViewController(vc, animated: false)
            
//            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CertificatedVc") as! CertificatedVc
//            self.navigationController?.pushViewController(vc, animated: false)
//            createAlert(title: AppName, message: "Comming Soon")
        }
        
        else if sender.tag == 2
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "ProfileTermVcViewController") as! ProfileTermVcViewController
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        else if sender.tag == 3
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "TermchangeViewController") as! TermchangeViewController
            self.navigationController?.pushViewController(vc, animated: false)
            vc.type = 1
            vc.titleMain = "About Us"
        }
        
        else if sender.tag == 5
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "FAQsVC") as! FAQsVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if sender.tag == 4
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "ContactUS") as! ContactUS
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
            
        default:
            return dicData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch  indexPath.section {
        case 0:
            let cell = self.tblAccount.dequeueReusableCell(withIdentifier: "AccountCell") as! AccountCell
            cell.selectionStyle = .none
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            
            return cell
        default:
            let cell = self.tblAccount.dequeueReusableCell(withIdentifier: "AccountListCell") as! AccountListCell
            cell.selectionStyle = .none
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            cell.content_lbl.text = dicData[indexPath.row]["tilte"]
            cell.icon.image = UIImage(named: dicData[indexPath.item]["rightIcon"] ?? "")
            cell.img.image = UIImage(named: dicData[indexPath.item]["leftIcon"] ?? "")
            cell.cellTap.addTarget(self, action: #selector(presentWebBrowser), for: .touchUpInside)
            cell.cellTap.tag = indexPath.row
            return cell
        }
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
        return 100
        }
        else
        {
            if indexPath.section == 0
            {
            return 60
            }
            else
            {
            return  60
            }
       
        }
       
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0
        {
        return headervView
        }
        else
        {
        return nil
        }
       
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
        return 200
        }
        else
        {
            return 0.1
        }
        
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0
        {
        return nil
        }
        else
        {
        return footerViewLogin
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 0.1
        }
        else
        {
        return 60
        }
       
    }
}

//extension AccountVc
//{
//func certiFied()
//{
//    var param: [String:Any] = [:]
//    param["courseUuid"] = "courseUuid"
//    param["email"] = kUserDefaults.retriveString(.email)
//    param["firstName"] = kUserDefaults.retriveString(.firstName)
//    param["lastName"] =  kUserDefaults.retriveString(.lastName)
//    param["mobileNumber"] = kUserDefaults.retriveString(.mobileNumber)
//    param["countryCode"] = UserDefaults.standard.object(forKey:"CountryCode") as! String
//
//    print(CommonUtility.instance.headers)
//    ServiceManager.instance.request(method: .post,
//                                    apiURL: Api.userinvite.url,
//                                    headers: CommonUtility.instance.headers,
//                                    parameters: param,
//                                    decodable: UpdateProfile.self
//    ) { (status, json, model, error) in
//        if model?.status == 200
//        {
//            self.firstName = model?.data?.userDetail?.firstName ?? ""
//            self.lastName = model?.data?.userDetail?.lastName ?? ""
//            self.emailAdree = model?.data?.userDetail?.email ?? ""
//            self.dateVal = model?.data?.userDetail?.dob ?? ""
//            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ProfessionalDetailsViewController") as! ProfessionalDetailsViewController
//            self.navigationController?.pushViewController(vc, animated: false)
//            kUserDefaults.saveToDefault(obj: model?.data?.userDetail)
//
//
//        }
//        else
//        {
//            self.createAlert(title: AppName, message:model?.message ?? "")
//        }
//
//    }
//}
