
import UIKit
import Alamofire
import IQKeyboardManagerSwift
import AVFoundation
import Foundation
import Speech
import AVKit
import  KRProgressHUD

class SearchVc: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate {
    var dataOfTopSearch: DataOftopSearch?
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    @IBOutlet var headerView_top: UIView!
    @IBOutlet weak var searchCollection: UICollectionView!
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var topSearchLbl: UILabel!
    @IBOutlet weak var micTap: UIButton!
    @IBOutlet weak var miceImg: UIImageView!
    
    var refreshControl:UIRefreshControl!
    var totaltime = 5
    var timerTest : Timer?
    let speechBtn = UIButton()
    var speechText = String()
    var speechTextField = UITextField()
    let speechRecognizer        = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))
    var recognitionRequest      = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask         : SFSpeechRecognitionTask?
    let audioEngine             = AVAudioEngine()
    var isRecognizing = Bool()
    let audioSession = AVAudioSession.sharedInstance()
    var recent : RecentSearchModel?
    //----------------------------------------------
    var exchangeTbl =  false
    var heightForHeader = 0
    let columnLayout = CustomViewFlowLayoutsearch()
    var selected = Int()
    var obj: SearchCategoryList?
    var dataOfSearchByuser: DataOfSearch?
    var timer = Timer()
    var counter = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
        self.refreshapage()
        self.searchCollection.collectionViewLayout = columnLayout
        for family: String in UIFont.familyNames
                {
                    print(family)

                    for names: String in UIFont.fontNames(forFamilyName: family)
                    {
                        print("== \(names)")
                    }
                }
        
        
        
        var exchangeTbl =  false
        self.searchTable.separatorColor = UIColor.white.withAlphaComponent(0.5)
        DispatchQueue.main.async {
        self.topSearch()
        }
        txtSearch.delegate = self
        miceImg.image = UIImage(named: "micIcon")
        self.heightCons.constant = self.searchCollection.contentSize.height
        self.searchCollection.collectionViewLayout = columnLayout
        self.txtSearch.delegate = self
        self.txtSearch.rightViewMode = .always
        view.endEditing(true)
        searchTable.registerCell(type: SearchItem.self)
        _ = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 1)
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        //        do
        //        {
        //            try self.audioEngine.start()
        //        }
        //        catch
        //        {
        //                print("fail to start")
        //        }
        
        //        let node = audioEngine.inputNode
        //        node.reset()
        
        //        let audioSession = AVAudioSession.sharedInstance()
        //        do {
        //            try audioSession.setCategory(AVAudioSession.Category.soloAmbient, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        //            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        //        } catch {
        //            print("audioSession properties weren't set because of an error.")
        //        }
    }
    
    func refreshapage()
    {
        searchTable.alwaysBounceVertical = true
        searchTable.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.searchTable.addSubview(refreshControl)
    }
    
    @objc func didPullToRefresh() {
        self.topSearch()
        refreshControl?.endRefreshing()
    }
    
    
    
    func StartTimer()
    {
        timerTest =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.timeInterval), userInfo: nil, repeats: true)
    }
    
    @objc func timeInterval(_ timer1: Timer)
    {
        if totaltime == 0{
            //            KRProgressHUD.dismiss()
            self.stopTimerTest()
            DispatchQueue.main.async {
                if self.dataOfSearchByuser?.searchData?.count == 0{
                    self.exchangeTbl = false
                    self.searchTable.reloadData()
                }
            }
            
            self.miceImg.image =  UIImage(named: "micIcon")
            self.isRecognizing = true
            
            self.audioEngine.stop()
            self.recognitionTask = nil
            self.speechBtn.isEnabled = true
            self.recognitionTask?.finish()
            micTap.isUserInteractionEnabled = true
            
        }else{
            miceImg.loadGif(name: "mic")
            micTap.isUserInteractionEnabled = false
            totaltime -= 1
            print(totaltime)
            self.topSearch()
            self.searchTable.reloadData()
            
        }
    }
    
    func stopTimerTest() {
        if txtSearch.text == "" {
            self.exchangeTbl = false
        }else{
            self.exchangeTbl = true
        }
        
        timerTest?.invalidate()
        timerTest = nil
    }
    
    @IBAction func searchClickonMic(_ sender:UIButton)
    {
        miceImg.loadGif(name: "mic")
        self.totaltime = 4
        micTap.isUserInteractionEnabled = false
        newstartRecording()
    }
    
    
    func newstartRecording() {
        self.StartTimer()
        //        KRProgressHUD.show()
        isRecognizing = true
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }

        
        //Change / Edit Start
        do {
            try audioSession.setCategory(AVAudioSession.Category.record)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            
            print("audioSession properties weren't set because of an error.")
        }
        //Change / Edit Finished
        var recognitionRequestNew = SFSpeechAudioBufferRecognitionRequest()
        // Setup audio engine and speech recognizer
        let node = audioEngine.inputNode
        
        let recordingFormat = node.outputFormat(forBus: 0)
        node.reset()
        node.removeTap(onBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            recognitionRequestNew.append(buffer)
        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            return print(error)
        }
        
        // Analyze the speech
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequestNew, resultHandler: { result, error in
            if let result = result {
                self.speechText = result.bestTranscription.formattedString
                print("speechText:self.speechText \(result.isFinal)")
                if (result.isFinal){
                    print("TExt data : \(self.txtSearch.text ?? "")")
                    self.isRecognizing = true
                    self.miceImg.image =  UIImage(named: "micIcon")
                    self.txtSearch.text = self.speechText
                    self.exchangeTbl = true
                    self.searchApi(text: self.speechText)
                    self.audioEngine.stop()
                    node.removeTap(onBus: 0)
                    self.recognitionTask = nil
                    self.speechBtn.isEnabled = true
                }
                NSLog(result.bestTranscription.formattedString)
                self.recognitionTask?.finish()
                
            } else if let error = error {
                print(error)
                self.miceImg.image =  UIImage(named: "micIcon")
                self.speechBtn.loadingIndicator(false)
                self.exchangeTbl = false
                //                self.topSearch()
                NSLog(error.localizedDescription)
            }
        })
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField.text == "" && dataOfSearchByuser?.searchData?.count == 0 {
//            exchangeTbl = false
//            self.searchTable.setEmptyMessageshow()
//        }
//        else{
//            self.searchTable.restore()
//        }
//        return true
//    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        exchangeTbl = true
        print("textFieldShouldEndEditing")
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if /textField.text?.count > 0 {
            exchangeTbl = true
            self.searchApi(text: txtSearch.text ?? "")
        }else{
            exchangeTbl = false
            self.searchApi(text: "")
            searchTable.reloadData()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if /textField.text?.count > 0 {
            if txtSearch.text == "" && dataOfSearchByuser?.searchData?.count == 0 {
                exchangeTbl = true
                self.searchTable.setEmptyMessageshow()
            }
            else{
                self.searchTable.restore()
                self.searchApi(text: txtSearch.text ?? "")
            }
            self.searchTable.reloadData()
        }
        else
        {
            exchangeTbl = false
            searchTable.reloadData()
        }
    }
    
    func configurationTextField(textField: UITextField!){
        textField.placeholder = "Say something, I'm listening!"
        speechTextField = textField
        speechTextField.text = self.speechText
        speechTextField.delegate = self
    }
    
    func setui()
    {
        self.searchTable.delegate = self
        self.searchTable.dataSource = self
        self.searchCollection.delegate = self
        self.searchCollection.dataSource = self
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: "Search Skills etc.",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.topSearchLbl.setAppFontColor(.appColor(.white,alpha: 0.64), font: .ProductSans(.medium, size: .oneSeven))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        exchangeTbl = false
        self.txtSearch.text = ""
        self.topSearch()
        self.dataOfTopSearch?.topSearch?.removeAll()
        self.heightCons.constant = self.searchCollection.contentSize.height
        view.endEditing(true)
        //        let audioSession = AVAudioSession.sharedInstance()
        //        do {
        //            try audioSession.setCategory(AVAudioSession.Category.record, mode: AVAudioSession.Mode.measurement, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        //            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        //        } catch {
        //            print("audioSession properties weren't set because of an error.")
        //        }
        //        audioEngine.inputNode.removeTap(onBus: 0)
        
    }
}

//MARK: collectioncell
extension SearchVc: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataOfTopSearch?.topSearch?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.searchCollection.dequeueReusableCell(withReuseIdentifier: "searchCollectionCell", for: indexPath) as! searchCollectionCell
        if self.dataOfTopSearch?.topSearch?.count ?? 0 > 0
        {
            let courseName = self.dataOfTopSearch?.topSearch?[indexPath.item].courseName
            cell.lbl_Cell.text = courseName
        }
        cell.contentView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        cell.contentView.layer.borderWidth = 1
        cell.contentView.layer.cornerRadius = 20
        cell.contentView.backgroundColor = .black
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if kUserDefaults.bool(forKey: "isSubscribed") == true
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
            if /self.dataOfTopSearch?.topSearch?.count > 0
            {
            print(self.dataOfTopSearch?.topSearch?[indexPath.row])
            vc.playlistID = self.dataOfTopSearch?.topSearch?[indexPath.row].playlistId ?? ""
            vc.courseId = self.dataOfTopSearch?.topSearch?[indexPath.row]._id ?? ""
            vc.courseUuid = self.dataOfTopSearch?.topSearch?[indexPath.row].courseUuid ?? ""
            let quizStatus = self.dataOfTopSearch?.topSearch?[indexPath.row].isQuizExist ?? false
            vc.isQuizExist = quizStatus
            
            self.navigationController?.pushViewController(vc, animated: true)
            print(indexPath.row)
        }
        }
        
        else if kUserDefaults.bool(forKey: "isSubscribed") == false
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
            vc.viaHome = true
            let id  = self.dataOfTopSearch?.topSearch?[indexPath.row]._id ?? ""
            vc.courseId = id ?? ""
            vc.mediaID  = self.dataOfTopSearch?.topSearch?[indexPath.row].previewId ?? ""
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
        }
        searchCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.text = self.dataOfTopSearch?.topSearch?[indexPath.item].courseName
        label.sizeToFit()
        return CGSize(width:searchCollection.frame.width, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
        
    }
}

//TableView
extension SearchVc: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if exchangeTbl == false
        {
        let vc  = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailsViewController") as! CourseDetailsViewController
         vc.categoryId = obj?.data?.browseCategory?[indexPath.row]._id ?? ""
        self.navigationController?.pushViewController(vc, animated:false)
        }
        
         if exchangeTbl == true
        {
         if kUserDefaults.bool(forKey: "isSubscribed") == true
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
             
            vc.playlistID = self.dataOfSearchByuser?.searchData?[indexPath.row].playlistId ?? ""
            vc.courseId = self.dataOfTopSearch?.topSearch?[indexPath.row]._id ?? ""
             vc.courseUuid = self.dataOfTopSearch?.topSearch?[indexPath.row].courseUuid ?? ""
             
//            self.recentSearch(recentSearch:dataOfTopSearch?.topSearch?[indexPath.row].courseName ?? "")
            let quizStatus = self.dataOfTopSearch?.topSearch?[indexPath.row].isQuizExist ?? false
            vc.isQuizExist = quizStatus
             self.navigationController?.pushViewController(vc, animated: true)
             print(indexPath.row)
            }
            else if kUserDefaults.bool(forKey: "isSubscribed") == false
            {
                let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
                vc.viaHome = true
                let id  = self.dataOfTopSearch?.topSearch?[indexPath.row]._id ?? ""
                vc.courseId = id ?? ""
                vc.mediaID  = self.dataOfTopSearch?.topSearch?[indexPath.row].previewId ?? ""
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else{
                
            }
             DispatchQueue.main.async {
            self.recent(recentype:/self.dataOfSearchByuser?.searchData?[indexPath.row].courseName)
             }
            
             self.searchTable.reloadData()
//            else
//            {
//
//            let quizStatus = dataOfSearchByuser?.searchData?[indexPath.row].isQuizExist ?? false
//                vc.isQuizExist = quizStatus
//            }

            
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if exchangeTbl == false
        {
            if obj?.data?.browseCategory?.count ?? 0 > 0 {
                self.searchTable.restoredata()
                return obj?.data?.browseCategory?.count ?? 0
                
            }
            else
            {
                if obj?.data?.browseCategory?.count == 0
                {
                    
                    self.searchTable.setEmptyMessageshow()
                } else
                {
                    self.searchTable.restoredata()
                }
                
            }
        }
        else
        {
            if dataOfSearchByuser?.searchData?.count ?? 0 > 0 {
                self.searchTable.restoredata()
                return dataOfSearchByuser?.searchData?.count ?? 0
            }
            else{
                if dataOfSearchByuser?.searchData?.count == 0
                {
                    self.searchTable.setEmptyMessageshow()
                } else
                {
                    self.searchTable.restoredata()
                }
                
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if exchangeTbl == false
        {
            let cell = self.searchTable.dequeueReusableCell(withIdentifier: "SearchTableCell", for: indexPath) as! SearchTableCell
            cell.catagoryList.text = obj?.data?.browseCategory?[indexPath.row].categoryName
            cell.imgCategory.image = UIImage(named: "privacyPolicy")
            cell.imgCategory.clipsToBounds = true
            cell.selectionStyle = .none
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
        
        else
        {
            let cell = self.searchTable.dequeueReusableCell(withIdentifier: "SearchItem", for: indexPath) as! SearchItem
            if dataOfSearchByuser?.searchData?.count ?? 0 > 0
            {
                if (dataOfSearchByuser?.searchData?[indexPath.row].courseName) != nil
                {
                    cell.CourselBL.text = dataOfSearchByuser?.searchData?[indexPath.row].courseName
                }else{
                    cell.CourselBL.text = ""
                }
            }
            else{}
            cell.contentView.backgroundColor = .black
            cell.CourselBL.textColor = .white
            cell.selectionStyle = .none
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 80
        }
        else
        {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if exchangeTbl == false
        {
            return headerView_top
        }
        else
        {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if exchangeTbl == false
        {
            return self.searchCollection.contentSize.height + 200
        }
        else
        {
            return 0.01
        }
    }
    
}

//extension SearchVc{
//    var collectionViewLayout: UICollectionViewFlowLayout?
//    {
//        return searchCollection.collectionViewLayout as? UICollectionViewFlowLayout
//    }
//
//    func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize
//    {
//        view.layoutIfNeeded()
//        let size = self.searchCollection.contentSize
//        let newSize = CGSize(width: size.width + 10, height: size.height + 100)
//        return newSize
//    }
//}

extension SearchVc
{
    func categoryApi(){
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.usercategoriesSearch.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: SearchCategoryList.self)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                self.obj = model
                print(model?.data?.browseCategory?.count)
                DispatchQueue.main.async {
                    self.searchCollection.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
                        self.searchTable.reloadData()
                        
                    }
                    
                }
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
            
        }
    }
}

extension SearchVc: SFSpeechRecognizerDelegate {
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            self.speechBtn.isEnabled = true
        } else {
            self.speechBtn.isEnabled = false
        }
    }
}

//SuggestionApi....
extension SearchVc
{
    func searchApi(text:String){
        var param : [String: Any] = [:]
        param["search"] = text
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.searchlist.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: SearchList.self,
                                        encoding : URLEncoding.queryString)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                self.dataOfSearchByuser = model?.data
                DispatchQueue.main.async {
                    self.searchTable.reloadData()
                    self.micTap.isUserInteractionEnabled = true
                }
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}

// MARK: API FOR SEACH TOP 10
extension SearchVc
{
    func topSearch(){
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.topSearch.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: TopSearchList.self,
                                        encoding : URLEncoding.queryString)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                self.dataOfTopSearch = model?.data
                print("dataOfTopSearch\(self.dataOfTopSearch?.topSearch?.count ?? 0)")
                DispatchQueue.main.async {
                    self.categoryApi()
                    self.heightForHeader = Int(self.searchCollection.contentSize.height)
                    print(self.searchCollection.contentSize.height)
                }
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}

extension SearchVc
{
    func recent(recentype:String)
    {
    var param: [String: Any] = [:]
    param["recentSearch"] = recentype
    ServiceManager.instance.request(method: .post,
                                    apiURL: Api.recentSearch.url,
                                    headers: CommonUtility.instance.headers,
                                    parameters: param,
                                    decodable: RecentSearchModel.self,
                                    encoding: JSONEncoding.default
    )
    { (status, json, model, error) in
        print(json)
        if model?.status == 200
        {
            self.exchangeTbl = false
            DispatchQueue.main.async {
           }
        }
        else
        {
        self.createAlert(title: AppName, message:model?.message ?? "")
        }
    }
}
}

//MARK: - Custom class for headeing
class TitleofBrouse: UITableViewCell
{
    @IBOutlet weak var titleLbl: UILabel!
    
}

class CustomViewFlowLayoutsearch: UICollectionViewFlowLayout {
let cellSpacing: CGFloat = 10.0
override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    let attributes = super.layoutAttributesForElements(in: rect)
    var leftMargin = sectionInset.left
    var maxY: CGFloat = -1.0
    attributes?.forEach { layoutAttribute in
        if layoutAttribute.frame.origin.y >= maxY {
            leftMargin = sectionInset.left
        }
        
        layoutAttribute.frame.origin.x = leftMargin
        leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
        maxY = max(layoutAttribute.frame.maxY , maxY)
    }
    
    return attributes
}
}
