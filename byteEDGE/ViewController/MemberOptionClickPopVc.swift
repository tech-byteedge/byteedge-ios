
//  MemberOptionClickPopVc.swift
//  byteEDGE
//  Created by Pawan Kumar on 20/01/22.

import UIKit
import Alamofire
import KRProgressHUD

class MemberOptionClickPopVc: UIViewController {
    @IBOutlet weak var popMemberoptionTbl: UITableView!
    @IBOutlet weak var mainViewoption: UIView!
    var groupDetail = ""
    var mayGroupDetailsdata: MyGroupDetail?
    var option = ["Edit","Remove","Block"]
    var selected = Int()
    var grpID = ""
    var memeberId = ""
    var grouAdmin = ""
    var icon = [UIImage(named: "editImg"),UIImage(named: "delete"),UIImage(named: "blockImg")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
    }
    
    func setui()
    {
    self.popMemberoptionTbl.delegate = self
    self.popMemberoptionTbl.dataSource = self
    
    
    }
    
   
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    dismiss(animated: false, completion: nil)
    }
}

extension MemberOptionClickPopVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return option.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = popMemberoptionTbl.dequeueReusableCell(withIdentifier: "OptionMemberCell", for: indexPath) as! OptionMemberCell
        cell.celllbloption.text = option[indexPath.row]
        cell.cellimgption.image = icon[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            dismiss(animated: false, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier:
                                                                            "MemberAllowPopVc") as! MemberAllowPopVc
            vc.groupId = self.grpID
            vc.membeid  = self.grouAdmin
            vc.groupDetail =  self.groupDetail
            vc.modalPresentationStyle = .fullScreen
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .fullScreen
            nav.providesPresentationContextTransitionStyle = true
            nav.definesPresentationContext = true
            nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
            nav.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
            UIApplication.topViewController()?.present(nav, animated: true, completion: nil)
                
                
        }
        }
        else if indexPath.row == 1
        {
        print(indexPath.row)
        self.removeAndBlockedApi(role: "deleted", groupID:self.grpID)
        }
        else
        {
        print(indexPath.row)
        self.removeAndBlockedApi(role: "blocked", groupID:self.grpID)
        }
        self.popMemberoptionTbl.reloadData()
    }
}


//MARK: api
extension MemberOptionClickPopVc
{
    func removeAndBlockedApi(role: String,groupID:String)
    {
        var param: [String: Any] = [:]
        param["groupMemberId"] = self.grpID
        param["role"] = role
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .put,
                                        apiURL: Api.removeandBlock.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: CommonResponseModel.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
            self.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshValue"), object: nil)
                DispatchQueue.main.async {
                self.popMemberoptionTbl.reloadData()
                }
                self.navigationController?.popViewController(animated: false)
                }
            else
            {
            self.createAlert(title:AppName, message: model?.message ?? "")
            }
        }
    }
}



