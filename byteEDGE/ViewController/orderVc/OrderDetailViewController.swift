
import UIKit
import Alamofire
import  KRProgressHUD

class OrderDetailViewController: UIViewController {
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var vcTitleLbl: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: -VARIABLES
    var objTranscation:TransactionModel?
    var objCurr: CurrentModel?
    var objTranscationcurrent:TransactionModel?
    var orderID = ""
    var typeint = String()
    
    //MARK: -VIEW LIFE CYCLE-
    override func viewDidLoad() {
        super.viewDidLoad()
        if typeint == "1"
        {
        self.currentPerHis()
        }
        else
        {
        orderApoCall()
        }
        setVc()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnBackBtn(_ sender: Any) {
    self.navigationController?.popViewController(animated: false)
    }
    //MARK: -FUNCTIONS-
        func setVc() {
        view.backgroundColor = .black
        //set title
        vcTitleLbl.text = "Subscription Details"
        ///register of tableView cell
        tableView.registerCell(type: SubcriptionCellTableViewCell.self)
        tableView.registerCell(type: OrderIdDateTableViewCell.self)
        
        tableView.registerCell(type: DownloadInvoiceTableViewCell.self)
        tableView.registerCell(type: PaymentSummaryTableViewCell.self)
        tableView.registerCell(type: PaymentMethodTableViewCell.self)
        tableView.registerCell(type: RateUsThisProductTableViewCell.self)
            
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        seperatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
    }
}

//MARK: -TABLEVIEW DELEGATE-
extension OrderDetailViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print(indexPath.row)
    }
}


//MARK: -TABLEVIEW DATA SOURCE-
extension OrderDetailViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return section == 0 ? 3 : 6
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if typeint == "1"
        {
            if indexPath.section == 0{
                switch indexPath.row {
                 case 0:
                    guard let cell = tableView.dequeueCell(withType: SubcriptionCellTableViewCell.self, for: indexPath) as? SubcriptionCellTableViewCell else {
                     fatalError("SubcriptionCellTableViewCell is not initialize...")
                    }
                    
                   let  subscriptionType =  Int(objCurr?.data?.purchaseList?.subscriptionType ?? 0)
                    cell.subcriptionPlaneLbl.text = "("  +  " for" +  " "  +  String(subscriptionType) + " " + " " + "month" + ")"
                    return cell
                case 1:
                    guard let cell = tableView.dequeueCell(withType: OrderIdDateTableViewCell.self, for: indexPath) as? OrderIdDateTableViewCell else {
                        fatalError("OrderIdDateTableViewCell is not initialize...")
                    }
                     cell.orderIdLbl.text = objCurr?.data?.purchaseList?.transactionId ?? ""
                    
                    cell.orderDateLbl.text =   CommonUtility.instance.getDateTIMEFromString(inputDate: objCurr?.data?.purchaseList?.createdAt ?? "" )
                    return cell
                case 2:
                    guard let cell = tableView.dequeueCell(withType: DownloadInvoiceTableViewCell.self, for: indexPath) as? DownloadInvoiceTableViewCell else {
                        fatalError("DownloadInvoiceTableViewCell is not initialize...")
                    }
                    return cell
                default: return UITableViewCell()
                }
            }
            else {
                if indexPath.row == 4 {
                    guard let cell = tableView.dequeueCell(withType: PaymentMethodTableViewCell.self, for: indexPath) as? PaymentMethodTableViewCell else {
                    fatalError("PaymentMethodTableViewCell is not initialize...")
                    }
                    cell.upperSepatorView.isHidden = false
                    cell.lowerSepratorView.isHidden = false
                    cell.paymentMethodLbl.text = "Payment Method"
                    cell.cardTypeLbl.text = objCurr?.data?.purchaseList?.paymentType
                    
                    return cell
                }else if indexPath.row == 5 {
                    guard let cell = tableView.dequeueCell(withType: RateUsThisProductTableViewCell.self, for: indexPath) as? RateUsThisProductTableViewCell else {
                        fatalError("RateUsThisProductTableViewCell is not initialize...")
                    }
                    
                    return cell
                }else {
                    guard let cell = tableView.dequeueCell(withType: PaymentSummaryTableViewCell.self, for: indexPath) as? PaymentSummaryTableViewCell else {
                        fatalError("PaymentSummaryTableViewCell is not initialize...")
                    }
                    switch indexPath.row {
                    case 0 :
                        cell.headingLbl.text = "Price"
                        let  amount = Int(objCurr?.data?.purchaseList?.amount ?? 0)
                        cell.valueLbl.text = String(amount)
                        cell.upperSepatorView.isHidden = true
                        cell.lowerSepratorView.isHidden = true
                    case 1 :
                        cell.headingLbl.text = "Additional fee"
                        cell.valueLbl.text = "₹ 0"
                        cell.upperSepatorView.isHidden = true
                        cell.lowerSepratorView.isHidden = true
                    case 2 :
                        cell.headingLbl.text = "Discount"
                        cell.valueLbl.text = "₹0"
                        cell.upperSepatorView.isHidden = true
                        cell.lowerSepratorView.isHidden = true
                    case 3 :
                        cell.headingLbl.text = "Total Amount Paid"
                        let  amount = Int(objCurr?.data?.purchaseList?.amount ?? 0)
                        //                    let amont =  Int(objTranscation?.data.orderDetail.amount ?? 0)
                        cell.valueLbl.text =  String(amount)
                        cell.upperSepatorView.isHidden = false
                        cell.lowerSepratorView.isHidden = true
                        
                    default : print("nil")
                    }
                    return cell
                }
        }
        }
        else
        {
        if indexPath.section == 0{
            switch indexPath.row {
             case 0:
                guard let cell = tableView.dequeueCell(withType: SubcriptionCellTableViewCell.self, for: indexPath) as? SubcriptionCellTableViewCell else {
                 fatalError("SubcriptionCellTableViewCell is not initialize...")
                }
                
               let  subscriptionType =  Int(objTranscation?.data.orderDetail.subscriptionType ?? 0)
                cell.subcriptionPlaneLbl.text = "("  +  " for" +  " "  +  String(subscriptionType) + " " + " " + "month" + ")"
                return cell
            case 1:
                guard let cell = tableView.dequeueCell(withType: OrderIdDateTableViewCell.self, for: indexPath) as? OrderIdDateTableViewCell else {
                    fatalError("OrderIdDateTableViewCell is not initialize...")
                }
                cell.orderIdLbl.text = objTranscation?.data.orderDetail.orderID ?? ""
                
                cell.orderDateLbl.text =   CommonUtility.instance.getDateTIMEFromString(inputDate: objTranscation?.data.orderDetail.createdAt ?? "" )
                return cell
            case 2:
                guard let cell = tableView.dequeueCell(withType: DownloadInvoiceTableViewCell.self, for: indexPath) as? DownloadInvoiceTableViewCell else {
                fatalError("DownloadInvoiceTableViewCell is not initialize...")
                }
                return cell
            default: return UITableViewCell()
            }
            
        }else {
            if indexPath.row == 4 {
                guard let cell = tableView.dequeueCell(withType: PaymentMethodTableViewCell.self, for: indexPath) as? PaymentMethodTableViewCell else {
                    fatalError("PaymentMethodTableViewCell is not initialize...")
                }
                cell.upperSepatorView.isHidden = false
                cell.lowerSepratorView.isHidden = false
                cell.paymentMethodLbl.text = "Payment Method"
                cell.cardTypeLbl.text = objTranscation?.data.orderDetail.paymentType
                
                return cell
            }else if indexPath.row == 5 {
                guard let cell = tableView.dequeueCell(withType: RateUsThisProductTableViewCell.self, for: indexPath) as? RateUsThisProductTableViewCell else {
                    fatalError("RateUsThisProductTableViewCell is not initialize...")
                }
                
                return cell
            }else {
                guard let cell = tableView.dequeueCell(withType: PaymentSummaryTableViewCell.self, for: indexPath) as? PaymentSummaryTableViewCell else {
                    fatalError("PaymentSummaryTableViewCell is not initialize...")
                }
                switch indexPath.row {
                case 0 :
                    cell.headingLbl.text = "Price"
                    let  amount = Int(objTranscation?.data.orderDetail.amount ?? 0)
                    cell.valueLbl.text = String(amount)
                    cell.upperSepatorView.isHidden = true
                    cell.lowerSepratorView.isHidden = true
                case 1 :
                    cell.headingLbl.text = "Additional fee"
                    cell.valueLbl.text = "₹ 0"
                    cell.upperSepatorView.isHidden = true
                    cell.lowerSepratorView.isHidden = true
                case 2 :
                    cell.headingLbl.text = "Discount"
                    cell.valueLbl.text = "₹0"
                    cell.upperSepatorView.isHidden = true
                    cell.lowerSepratorView.isHidden = true
                case 3 :
                    cell.headingLbl.text = "Total Amount Paid"
                    let  amount = Int(objTranscation?.data.orderDetail.amount ?? 0)
                    //                    let amont =  Int(objTranscation?.data.orderDetail.amount ?? 0)
                    cell.valueLbl.text =  String(amount)
                    cell.upperSepatorView.isHidden = false
                    cell.lowerSepratorView.isHidden = true
                    
                default : print("nil")
                }
                return cell
            }
        }
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.backgroundView?.backgroundColor = .black
            view.textLabel!.backgroundColor = .clear
            view.textLabel!.textColor = .appColor(.white, alpha: 0.64)
            view.textLabel!.font = .ProductSans(.regular, size: .oneFive)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 5 : 0.01
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 1 ? "PAYMENT SUMMARY" : nil
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension OrderDetailViewController
{
    func orderApoCall()
        {
            var param: [String: Any] = [:]
            param["orderId"] = self.orderID
            KRProgressHUD.show()
            ServiceManager.instance.request(method: .get,
                                            apiURL: Api.orderDetailsafterPurchase.url,
                                            headers: CommonUtility.instance.headers,
                                            parameters: param,
                                            decodable: TransactionModel.self,
                                            encoding: URLEncoding.default
            )

            { (status, json, model, error) in
                KRProgressHUD.dismiss()
                if model?.status == 200
                {
                    self.objTranscation = model
                    print(model)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.tableView.reloadData()
                    }
                }
                else
                {
                self.createAlert(title: AppName, message:model?.message ?? "")
                }
            }
        }
    
    
        func currentPerHis()
         {
//            var param: [String: Any] = [:]
//            param["orderId"] = self.orderID
            KRProgressHUD.show()
            ServiceManager.instance.request(method: .get,
                                            apiURL: Api.paymentcurrentpurchasehistory.url,
                                            headers: CommonUtility.instance.headers,
                                            parameters: nil,
                                            decodable: CurrentModel.self,
                                            encoding: URLEncoding.default
            )
             
            { (status, json, model, error) in
                KRProgressHUD.dismiss()
                if model?.status == 200
                {
                    self.objCurr = model
                    print(model)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.tableView.reloadData()
                    }
                }
                else
                {
                }
            }
        }
    
    }




