
import UIKit
import  Alamofire
import  KRProgressHUD
import  SDWebImage

protocol  SubCriptionprotocol {
 func getid(index:Int)
}

enum CourseState
{
    case none, yourCourse,couese,favouriteCourse,recommendedCourse,recommendedCoursesetdata,trendingCourse,popular
}

class LearningGoalsViewController: UIViewController,UIGestureRecognizerDelegate {
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var vcTitleLbl: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: -VARIABLES-
    var learningListData: LearningListData?
    var recommended_obj: RecommnndedModel?
    var trend_obj: TrendingModel?
    var popular_obj: Popular?
    var objTracklearning: LearningTracModel?
    var courseID = ""
    var objGraphlist: GrapglistModel?
    var delegate: SubCriptionprotocol?
    var callBAckID:((_ id: String)->())?
    var selectedtype = ""
    var objpercentge: GrapglistpercentageModel?
    let group = DispatchGroup()
    //MARK: -VIEW LIFE CYCLE-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        group.enter()
        group.enter()
        group.enter()
       //  group.enter()
        
        
        
        DispatchQueue.main.async {
        self.learningList()
        self.recomendApi()
        self.trendingApi()
        self.popularApi()
        }
 
        selectedtype = "week"
        //self.trackApi()
        NotificationCenter.default.addObserver(self,selector: #selector(courseUpdate),name: NSNotification.Name ("refrseh"),object: nil)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        let refreshControl = tableView.addRefreshControl(target: self, action: #selector(doRefresh(_:)))
        refreshControl.tintColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        refreshControl.attributedTitle = NSAttributedString(string: "",attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-UltraLight",size: 36.0)! ])
        
   
          setVc()
//         self.graphList(selectedType:"week" )
    }

    override func viewWillAppear(_ animated: Bool) {
//        AppUtility.lockOrientation(.all)
        self.trackApi()
        self.graphListpercentage(selectedType: selectedtype)
        
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        AppUtility.lockOrientation(.portrait)
//    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    
    @objc func doRefresh(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.tintColor = self.view.backgroundColor // store color
            self.view.backgroundColor = UIColor.black
        }) { (Bool) in
            self.viewDidLoad()
       // self.learningList()
        self.view.backgroundColor = self.view.tintColor // restore color
        }
    }
    
    @objc func courseUpdate(notification: Notification) {
        self.learningList()
        self.trackApi()
        self.tableView.restoredata()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnBackBtn(_ sender: Any) {
     self.navigationController?.popViewController(animated: false)
    }
    //MARK: -FUNCTIONS-
    func setVc() {
        view.backgroundColor = .black
        //set title
        vcTitleLbl.text = "My Learnings"
        //register of tableView cell
        tableView.registerCell(type: StackWeekMonthYearTableViewCell.self)
        tableView.registerCell(type: CircularSliderTableViewCell.self)
        tableView.registerCell(type: LearningProgressTableViewCell.self)
        tableView.registerCell(type: GraphLearningGoalsTableViewCell.self)
        tableView.registerCell(type: FriendsCollectionTableViewCell.self)
        tableView.registerCell(type: ProfileHeaderTableViewCell.self)
        
        ///connector to tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        ///seperator view
      seperatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
    }
}
//MARK: -TABLEVIEW DELEGATE-
extension LearningGoalsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2 {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
            vc.playlistID = self.objTracklearning?.data?.trackedCourse[indexPath.row]?.courseDetail?.playlistId ?? ""
            vc.courseId =  self.objTracklearning?.data?.trackedCourse[indexPath.row]?.courseDetail?._id ?? ""
            vc.courseUuid = self.objTracklearning?.data?.trackedCourse[indexPath.row]?.courseDetail?.courseUuid ?? ""
            self.navigationController?.pushViewController(vc, animated: false)
            
            }else {
            print(indexPath.row)
        }
    }
}

//MARK: -TABLEVIEW DATA SOURCE-
extension LearningGoalsViewController : UITableViewDataSource,SubsCriptionCourse {
    func getIndex(index: Int) {
        self.courseID = self.learningListData?.yourCourse?[index]._id ?? ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
     return 11
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 2
        case 1: return 1
        case 2: return  /objTracklearning?.data?.trackedCourse.count
//            ?? 0 <= 0 ?  0 : 5

        case 3: return  learningListData?.yourCourse?.count ?? 0 <= 0 ?  0 : 2
        case 4: return  learningListData?.favouriteCourse?.count ?? 0 <= 0 ?  0 :2
        case 5: return  trend_obj?.data?.trending.count ?? 0 <= 0 ?  0 : 2
        case 6: return  recommended_obj?.data?.recommend.count ?? 0 <= 0 ?  0 : 2
        case 7: return  popular_obj?.data?.Popular?.count ?? 0 <= 0 ?  0 : 2  // popular
        case 8: return  recommended_obj?.data?.newCourseCount.count ?? 0 <= 0 ?  0 : 2
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0 :
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueCell(withType: StackWeekMonthYearTableViewCell.self, for: indexPath) as? StackWeekMonthYearTableViewCell else {
                    fatalError("StackWeekMonthYearTableViewCell is not initialize...")
                }
                
                cell.weekBtnAction = {
                print("week")
              self.selectedtype = "week"
              //self.graphList(selectedType: "week")
              self.graphListpercentage(selectedType: "week")
                }
                
                cell.monthBtnAction = {
                    self.selectedtype = "month"
                //self.graphList(selectedType: "month")
                self.graphListpercentage(selectedType: "month")
                }
                cell.yearBtnAction = {
                self.selectedtype = "year"
               // self.graphList(selectedType: "year")
                self.graphListpercentage(selectedType: "year")
                }
                return cell
               }else {
                guard let cell = tableView.dequeueCell(withType: CircularSliderTableViewCell.self, for: indexPath) as? CircularSliderTableViewCell else {
                fatalError("CircularSliderTableViewCell is not initialize...")
                }
                if selectedtype == "week"
                {
                let perc = Int(self.objpercentge?.data?.totalPercentage ?? 0)
                cell.percentageLbl.text = String(perc)
                cell.circularSlider.angle = Double(perc)
                return cell
                }
                   
                else if selectedtype == "month"
                {
                let perc = Int(self.objpercentge?.data?.totalPercentage ?? 0)
                cell.percentageLbl.text = String(perc)
                return cell
                }
                else
                {
                let perc = Int(self.objpercentge?.data?.totalPercentage ?? 0)
                cell.percentageLbl.text = String(perc)
                return cell
                }
            }
            case 1:
            guard let cell = tableView.dequeueCell(withType: GraphLearningGoalsTableViewCell.self, for: indexPath) as? GraphLearningGoalsTableViewCell else {
            fatalError("GraphLearningGoalsTableViewCell is not initialize...")
            }
            if selectedtype == "week"
            {
            cell.objGraphlist = self.objGraphlist
            cell.reloadInputViews()
            print(cell.objGraphlist)
            cell.selectedtype = selectedtype
            cell.userdata = objGraphlist?.data?.userData ?? []
            let dataEntries = cell.generateRandomEntries()
            cell.lineChart.dataEntries = dataEntries
            cell.lineChart.isCurved = false
            print(cell.userdata)
            cell.lineChart.backgroundColor = .black
            return cell
            }
            else if selectedtype == "month"
            {
            cell.objGraphlist = self.objGraphlist
            cell.reloadInputViews()
            print(cell.objGraphlist)
                cell.selectedtype = selectedtype
            cell.userdata = objGraphlist?.data?.userData ?? []
            let dataEntries1 = cell.monthGraphData()
            cell.lineChart.dataEntries = dataEntries1
            cell.lineChart.isCurved = false
            print(cell.userdata)
            cell.lineChart.backgroundColor = .black
            return cell
            }
            else{
                cell.objGraphlist = self.objGraphlist
                cell.reloadInputViews()
                cell.selectedtype = selectedtype
                print(cell.objGraphlist)
                cell.userdata = objGraphlist?.data?.userData ?? []
                let dataEntries1 = cell.yearGraphData()
                cell.lineChart.dataEntries = dataEntries1
                cell.lineChart.isCurved = false
                print(cell.userdata)
                cell.lineChart.backgroundColor = .black
                return cell
            }
            
            case 2 :
            guard let cell = tableView.dequeueCell(withType: LearningProgressTableViewCell.self, for: indexPath) as? LearningProgressTableViewCell else {
            fatalError("LearningProgressTableViewCell is not initialize...")
            }
            
            if /self.objTracklearning?.data?.trackedCourse.count > 0
            {
            let courseName =  self.objTracklearning?.data?.trackedCourse[indexPath.row]?.courseDetail?.courseName
            cell.goadHeadingLbl.text = self.objTracklearning?.data?.trackedCourse[indexPath.row]?.courseDetail?.courseName
            cell.byUserNameLbl.text = self.objTracklearning?.data?.trackedCourse[indexPath.row]?.courseDetail?.Description

            let urlImage = self.objTracklearning?.data?.trackedCourse[indexPath.row]?.courseDetail?.thumbnails ?? ""
            cell.goalsImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named:"logo"), options: .highPriority, context: nil)
            let perc = Int(self.objTracklearning?.data?.trackedCourse[indexPath.row]?.percentage ?? 0)
            cell.progessCompeleteLbl.text = String(perc) + " " +  "%" + "" +  " " + "completed"
            let pec = Float(perc) / 100
            cell.progressView.setProgress(pec, animated: false)
//            cell.progressView.trackTintColor = .appColor(.pink, alpha:1.0)
        }
            else
            {
                print(" Nothing to do")
            }
            return cell
            
            case 3 : // Your Course
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueCell(withType: ProfileHeaderTableViewCell.self, for: indexPath) as? ProfileHeaderTableViewCell else {
                fatalError("ProfileHeaderTableViewCell is not initialize...")
                }
              
                cell.headingLbl.text = "My Courses"
                cell.viewAll.isHidden = false
                cell.viewAllBtnTapped = {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "LessonOverviewViewController") as! LessonOverviewViewController
//                let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "LessonOverviewViewController") as        LessonOverviewViewControll
                self.navigationController?.pushViewController(vc, animated: false)
                }
                return cell
            }else {
                guard let cell = tableView.dequeueCell(withType: FriendsCollectionTableViewCell.self, for: indexPath) as? FriendsCollectionTableViewCell else {
                    fatalError("FriendsCollectionTableViewCell is not initialize...")
                }
               
                
                cell.courseData = self.learningListData
                cell.cellState = .yourCourse
                cell.selectedAction = {
                    (index) -> Void in
                    print(index)
                    
                    if self.learningListData?.yourCourse?.count != 0
                    {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                    if let playlistID = self.learningListData?.yourCourse?[index].playlistId {
                    vc.playlistID = playlistID
                    vc.courseId = self.learningListData?.yourCourse?[index]._id ?? ""
                        let quizStatus = self.learningListData?.yourCourse?[index].isQuizExist ?? false
                    vc.isQuizExist = quizStatus
                    vc.courseUuid =  self.learningListData?.yourCourse?[index].courseUuid ?? ""
                    }
                        
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                self.navigationController?.pushViewController(vc, animated: false)
                }
                }
                cell.likeAction = {
                self.learningList()
                }
                cell.friendsCollectionView.reloadData()
                return cell
            }
            case 7 :
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueCell(withType: ProfileHeaderTableViewCell.self, for: indexPath) as? ProfileHeaderTableViewCell else {
                fatalError("ProfileHeaderTableViewCell is not initialize...")
                }
                cell.headingLbl.text = "Popular Courses"
                cell.viewAll.isHidden = true
//                if learningListData?.course?.count ?? 0 < 8
//                {
//                cell.viewAll.isHidden = true
//                }
//                else
//                {
//                cell.viewAll.isHidden = false
//                }
                cell.viewAllBtnTapped = {
                    let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LearningViewAll") as! LearningViewAll
                    vc.typerole = 2
                    vc.courseData = self.learningListData
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                return cell
            }
            else {
                guard let cell = tableView.dequeueCell(withType: FriendsCollectionTableViewCell.self, for: indexPath) as? FriendsCollectionTableViewCell else {
                    fatalError("FriendsCollectionTableViewCell is not initialize...")
                }
                
                cell.popular_ob = self.popular_obj
                cell.cellState = .popular
                  cell.selectedAction = {
                    (index) -> Void in
                      if kUserDefaults.bool(forKey: "isSubscribed") == true
                      {
                      let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                          vc.playlistID = self.popular_obj?.data?.Popular?[index].playlistId ?? ""
                          vc.courseId = self.popular_obj?.data?.Popular?[index]._id ?? ""
                          vc.courseUuid = self.popular_obj?.data?.Popular?[index].courseUuid ?? ""
                          

                          let quizStatus = self.popular_obj?.data?.Popular?[index].isQuizExist ?? false
                            vc.isQuizExist = quizStatus
                          
                          
                          self.navigationController?.pushViewController(vc, animated: true)
                      }
                      else if  kUserDefaults.bool(forKey: "isSubscribed") == false
                      {
                     let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"SubsCriptionPlane") as! SubsCriptionPlane
                      vc.callBackofSeepreviewofSub = {
                      let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
                        print(indexPath.row)
                          if let id = self.popular_obj?.data?.Popular?[index]._id {
                        vc.courseId = id
                        vc.mediaID = self.popular_obj?.data?.Popular?[index].previewId ?? ""
                        }
                        else
                        {
//                        self.createAlert(title: kAppName, message: "Something went wrong")
                        }
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                          
                    vc.callBAck = {
                        objTransactionModel in

                        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                        vc.objTranscation = objTransactionModel
                        self.navigationController?.pushViewController(vc, animated: false)

                    }
                          
                    vc.modalPresentationStyle = .fullScreen
                    vc.providesPresentationContextTransitionStyle = true
                    vc.definesPresentationContext = true
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                    vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2
                    )
                    self.present(vc, animated: true, completion: nil)
                }
                      else
                      {
                        print("Nothing to do")
                      }
                      
                  }
                
                cell.friendsCollectionView.reloadData()
                cell.likeAction = {
                self.learningList()
                }
                 return cell
            }
            case 4 :
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueCell(withType: ProfileHeaderTableViewCell.self, for: indexPath) as? ProfileHeaderTableViewCell else {
                fatalError("ProfileHeaderTableViewCell is not initialize...")
                }
                cell.headingLbl.text = "Favourites"
//                if learningListData?.favouriteCourse?.count ?? 0 < 8
//                {
//                cell.viewAll.isHidden = true
//                }
//                else
//                {
//                cell.viewAll.isHidden = false
//                }
                cell.viewAllBtnTapped = {
                    let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LearningViewAll") as! LearningViewAll
                    vc.typerole = 3
                    vc.courseData = self.learningListData
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                return cell
            }else {
                guard let cell = tableView.dequeueCell(withType: FriendsCollectionTableViewCell.self, for: indexPath) as? FriendsCollectionTableViewCell else {
                fatalError("FriendsCollectionTableViewCell is not initialize...")
                }
                cell.courseData = self.learningListData
                cell.cellState = .favouriteCourse
                cell.delegate = self
                cell.selectedAction = {
                    (index) -> Void in
                    if kUserDefaults.bool(forKey: "isSubscribed") == true
                    {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                        vc.playlistID = self.learningListData?.favouriteCourse?[index].playlistId ?? ""
                        vc.courseId = self.learningListData?.favouriteCourse?[index].courseId ?? ""
                        vc.courseUuid = self.learningListData?.favouriteCourse?[index].courseUuid ?? ""
                        
                        let quizStatus = self.learningListData?.favouriteCourse?[index].isQuizExist ?? false
                        vc.isQuizExist = quizStatus
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else if kUserDefaults.bool(forKey: "isSubscribed") == false
                    {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"SubsCriptionPlane") as! SubsCriptionPlane
                    vc.callBackofSeepreviewofSub = {
                        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as!CoursePreviewViewController
                        if let id = self.learningListData?.favouriteCourse?[0].courseId {
                        vc.courseId = id
                        vc.mediaID = self.learningListData?.course?[index].previewId ?? ""
                        }
                        else
                        {
//                        self.createAlert(title: kAppName, message: "Something went wrong")
                        }
                        self.navigationController?.pushViewController(vc, animated: false)

                    }
                    
                    vc.callBAck = {
                        objTransactionModel in

                        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                        vc.objTranscation = objTransactionModel
                        self.navigationController?.pushViewController(vc, animated: false)

                    }
                    vc.modalPresentationStyle = .fullScreen
                    vc.providesPresentationContextTransitionStyle = true
                    vc.definesPresentationContext = true
                    vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                    vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2
                    )
                    self.present(vc, animated: true, completion: nil)
                }
                    else {
                        
                    }
                }
                
                cell.friendsCollectionView.reloadData()
                cell.likeAction = {
                self.learningList()
                }
               return cell
            }
            
            case 8 :
              if indexPath.row == 0 {
              guard let cell = tableView.dequeueCell(withType: ProfileHeaderTableViewCell.self, for: indexPath) as? ProfileHeaderTableViewCell else {
              fatalError("ProfileHeaderTableViewCell is not initialize...")
              }
              cell.headingLbl.text = "New Courses"
             cell.viewAll.isHidden = true
//              if recommended_obj?.data?.newCourseCount.count ?? 0 < 8
//              {
//              cell.viewAll.isHidden = true
//              }
//              else
//              {
//              cell.viewAll.isHidden = false
//              }
              
              cell.viewAllBtnTapped = {
                  let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LearningViewAll") as! LearningViewAll
//                  vc.typerole = 3
//                  vc.courseData = self.learningListData
                  self.navigationController?.pushViewController(vc, animated: false)
              }
              return cell
           }
            else {
              guard let cell = tableView.dequeueCell(withType: FriendsCollectionTableViewCell.self, for: indexPath) as? FriendsCollectionTableViewCell else {
              fatalError("FriendsCollectionTableViewCell is not initialize...")
              }
              cell.recom_courseData = self.recommended_obj
              cell.cellState = .recommendedCourse
              cell.delegate = self
              cell.selectedAction = {
                  (index) -> Void in
                  if kUserDefaults.bool(forKey: "isSubscribed") == true
                  {
                  let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                      vc.playlistID = self.recommended_obj?.data?.newCourseCount[index].playlistId ?? ""
                      vc.courseId = self.recommended_obj?.data?.newCourseCount[index]._id ?? ""
                      vc.courseUuid = self.recommended_obj?.data?.newCourseCount[index].courseUuid ?? ""
                      let quizStatus = self.recommended_obj?.data?.newCourseCount[index].isQuizExist ?? false;             vc.isQuizExist = quizStatus
                      
                      self.navigationController?.pushViewController(vc, animated: true)
                  }

                  else if kUserDefaults.bool(forKey: "isSubscribed") == false
                  {
                  let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"SubsCriptionPlane") as! SubsCriptionPlane
                  vc.callBackofSeepreviewofSub = {
                      let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as!CoursePreviewViewController
                      if let id = self.recommended_obj?.data?.newCourseCount[index]._id {
                      vc.courseId = id
                      vc.mediaID = self.recommended_obj?.data?.newCourseCount[index].previewId ?? ""
                      }
                      else
                      {
//                      self.createAlert(title: kAppName, message: "Something went wrong")
                      }
                     self.navigationController?.pushViewController(vc, animated: false)
                  }
                      
                   vc.callBAck = {
                      objTransactionModel in  
                      let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                      vc.objTranscation = objTransactionModel
                      self.navigationController?.pushViewController(vc, animated: false)
                  }
                  vc.modalPresentationStyle = .fullScreen
                  vc.providesPresentationContextTransitionStyle = true
                  vc.definesPresentationContext = true
                  vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                  vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2
                  )
                  self.present(vc, animated: true, completion: nil)
              }
                  else {
                  }
              }
              cell.friendsCollectionView.reloadData()
              cell.likeAction = {
              self.learningList()
              }
             return cell
          }
             case 6 :
              if indexPath.row == 0 {
              guard let cell = tableView.dequeueCell(withType: ProfileHeaderTableViewCell.self, for: indexPath) as? ProfileHeaderTableViewCell else {
              fatalError("ProfileHeaderTableViewCell is not initialize...")
              }
              cell.headingLbl.text = "Recommended Courses"
                cell.viewAll.isHidden  = true
//              if recommended_obj?.data?.recommend.count ?? 0 < 8
//              {
//              cell.viewAll.isHidden = true
//              }
//              else
//              {
//              cell.viewAll.isHidden = false
//              }
                   
              cell.viewAllBtnTapped = {
                  let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LearningViewAll") as! LearningViewAll
//                  vc.typerole = 3
//                  vc.courseData = self.learningListData
                  self.navigationController?.pushViewController(vc, animated: false)
              }
              return cell
           }
            else {
              guard let cell = tableView.dequeueCell(withType: FriendsCollectionTableViewCell.self, for: indexPath) as? FriendsCollectionTableViewCell else {
              fatalError("FriendsCollectionTableViewCell is not initialize...")
              }
              cell.recom_courseData = self.recommended_obj
              cell.cellState = .recommendedCoursesetdata
              cell.delegate = self
              cell.selectedAction = {
                  (index) -> Void in
                  if kUserDefaults.bool(forKey: "isSubscribed") == true
                  {
                  let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                      vc.playlistID = self.recommended_obj?.data?.recommend[index].playlistId ?? ""
                      vc.courseId = self.recommended_obj?.data?.recommend[index]._id ?? ""
                      vc.courseUuid = self.recommended_obj?.data?.recommend[index].courseUuid ?? ""
                      
                      let quizStatus = self.recommended_obj?.data?.recommend[index].isQuizExist ?? false
                      vc.isQuizExist = quizStatus
                      self.navigationController?.pushViewController(vc, animated: true)
                  }

                  else if kUserDefaults.bool(forKey: "isSubscribed") == false
                  {
                  let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"SubsCriptionPlane") as! SubsCriptionPlane
                  vc.callBackofSeepreviewofSub = {
                      let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as!CoursePreviewViewController
                      if let id = self.recommended_obj?.data?.recommend[index]._id {
                      vc.courseId = id
                      vc.mediaID = self.recommended_obj?.data?.recommend[index].previewId ?? ""
                      }
                      else
                      {
//                      self.createAlert(title: kAppName, message: "Something went wrong")
                      }
                     self.navigationController?.pushViewController(vc, animated: false)
                  }
                      
                   vc.callBAck = {
                      objTransactionModel in

                      let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                      vc.objTranscation = objTransactionModel
                      self.navigationController?.pushViewController(vc, animated: false)
                  }
                  vc.modalPresentationStyle = .fullScreen
                  vc.providesPresentationContextTransitionStyle = true
                  vc.definesPresentationContext = true
                  vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                  vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2
                  )
                  self.present(vc, animated: true, completion: nil)
              }
                  else {
                  }
              }
              cell.friendsCollectionView.reloadData()
              cell.likeAction = {
              self.learningList()
              }
             return cell
          }
            
        case 5 :
          if indexPath.row == 0 {
          guard let cell = tableView.dequeueCell(withType: ProfileHeaderTableViewCell.self, for: indexPath) as? ProfileHeaderTableViewCell else {
          fatalError("ProfileHeaderTableViewCell is not initialize...")
          }
          cell.headingLbl.text = "Trending Courses"
          if trend_obj?.data?.trending.count ?? 0 < 8
          {
          cell.viewAll.isHidden = true
          }
          else
          {
           cell.viewAll.isHidden = false
          }
              
          cell.viewAllBtnTapped = {
              let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LearningViewAll") as! LearningViewAll
//                  vc.typerole = 3
//                  vc.courseData = self.learningListData
              self.navigationController?.pushViewController(vc, animated: false)
          }
          return cell
       }
        else {
          guard let cell = tableView.dequeueCell(withType: FriendsCollectionTableViewCell.self, for: indexPath) as? FriendsCollectionTableViewCell else {
          fatalError("FriendsCollectionTableViewCell is not initialize...")
          }
          cell.trend_obj = self.trend_obj
          cell.cellState = .trendingCourse
          cell.delegate = self
          cell.selectedAction = {
              (index) -> Void in
              if kUserDefaults.bool(forKey: "isSubscribed") == true
              {
              let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                  vc.playlistID = self.trend_obj?.data?.trending[index].playlistId ?? ""
                  vc.courseId = self.trend_obj?.data?.trending[index]._id ?? ""
                  vc.courseUuid = self.trend_obj?.data?.trending[index].courseUuid ?? ""
                  
                  let quizStatus = self.trend_obj?.data?.trending[index].isQuizExist ?? false
                  vc.isQuizExist = quizStatus
                  
                  
                  
                  
                  
                  self.navigationController?.pushViewController(vc, animated: true)
              }
              
              else if kUserDefaults.bool(forKey: "isSubscribed") == false
              {
              let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"SubsCriptionPlane") as! SubsCriptionPlane
              vc.callBackofSeepreviewofSub = {
                  let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as!CoursePreviewViewController
                  if let id = self.trend_obj?.data?.trending[index]._id {
                  vc.courseId = id
                  vc.mediaID = self.trend_obj?.data?.trending[index].previewId ?? ""
                  }
                  else
                  {
//                  self.createAlert(title: kAppName, message: "Something went wrong")
                  }
                 self.navigationController?.pushViewController(vc, animated: false)
              }

               vc.callBAck = {
                  objTransactionModel in

                  let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                  vc.objTranscation = objTransactionModel
                  self.navigationController?.pushViewController(vc, animated: false)
              }
              vc.modalPresentationStyle = .fullScreen
              vc.providesPresentationContextTransitionStyle = true
              vc.definesPresentationContext = true
              vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
              vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2
              )
              self.present(vc, animated: true, completion: nil)
          }
              else {
              }
          }
          cell.friendsCollectionView.reloadData()
          cell.likeAction = {
          self.learningList()
          }
         return cell
      }
              
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.backgroundView?.backgroundColor = .black
            view.textLabel!.backgroundColor = .clear
            view.textLabel!.textColor = .appColor(.white, alpha: 0.64)
            view.textLabel!.font = .ProductSans(.regular, size: .oneFive)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1{
            return UITableView.automaticDimension
        }
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "COURSES STARTED"
        case 1: return "WEEKLY LEARNING RATIO"
            
            
            
            
        case 2: return nil
        default: return nil
        }
        
    }
}

//MARK: -API-
extension LearningGoalsViewController {
    func learningList() {
        
        ServiceRequest.instance.learningList() { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
//                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                 return
             }
            
           // self.group.leave()
            self.learningListData = response?.data
            
            DispatchQueue.main.async {
           
               // self.recomendApi()
            }
           
            print(response)
            
      
            self.tableView.reloadData()
            
            
        }
    }
}

// Recommended Api
extension LearningGoalsViewController {
    func recomendApi() {
        ServiceRequest.instance.recomCourse() { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
//                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.group.leave()
            self.recommended_obj = response
            print(self.recommended_obj)
            DispatchQueue.main.async {
               // self.trendingApi()
            }
            self.tableView.reloadData()
        }
    }
}

extension LearningGoalsViewController
{
    func trendingApi() {
        ServiceRequest.instance.trendingcourse() { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
//                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.group.leave()
            self.trend_obj = response
            print(self.trend_obj)
//            DispatchQueue.main.async {
//           // self.popularApi()
//            }
            self.tableView.reloadData()
        }
    }

    func popularApi()
    {
        ServiceRequest.instance.popularCourse() { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
//                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.group.leave()
            self.popular_obj = response
            print(self.popular_obj)
            DispatchQueue.main.async {
            self.tableView.reloadData()
            }
        }
        
    }
}

extension LearningGoalsViewController
{
    func trackApi()
    {
//        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.trackingLearning.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: LearningTracModel.self,
                                        encoding: URLEncoding.default
        )
        { (status, json, model, error) in
//            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                self.objTracklearning = model
                print(model)
                DispatchQueue.main.async {
                   // self.tableView.reloadSections([3], with: .none)
                }
                self.tableView.reloadData()
            }
            else
            {
            }
        }
    }
    
    func graphList(selectedType:String)
    {
//    KRProgressHUD.show()
    var param: [String: Any] = [:]
    param["dateLabels"] = selectedType
    param["timezoneOffset"] = "+5:30"
    ServiceManager.instance.request(method: .post,
                                    apiURL: Api.graphList.url,
                                    headers: CommonUtility.instance.headers,
                                    parameters: param,
                                    decodable: GrapglistModel.self,
                                    encoding: JSONEncoding.default
    )
    { (status, json, model, error) in
//        KRProgressHUD.dismiss()
        print(json)
        if model?.status == 200
        {
        //DispatchQueue.main.async {
            self.objGraphlist = model
            self.trackApi()
//            DispatchQueue.main.async {
//                self.tableView.reloadSections([2], with: .none)
//            }
            self.tableView.reloadData()
           // self.graphListpercentage(selectedType: selectedType)
            print(self.objGraphlist)
           }
        else
        {
//        self.createAlert(title: AppName, message:model?.message ?? "")
        }
    }
}
    
    //MARK: percentage showGraph
    func graphListpercentage(selectedType:String)
    {
   // KRProgressHUD.show()
    var param: [String: Any] = [:]
    param["dateLabels"] = selectedType
    param["timezoneOffset"] = "+5:30"
    ServiceManager.instance.request(method: .post,
                                    apiURL: Api.graphcourselistperc.url,
                                    headers: CommonUtility.instance.headers,
                                    parameters: param,
                                    decodable: GrapglistpercentageModel.self,
                                    encoding: JSONEncoding.default
    )
        
    { (status, json, model, error) in
      //  KRProgressHUD.dismiss()
        print(json)
        if model?.status == 200
        {
            self.objpercentge = model
             
            print(model?.data?.totalPercentage)
            self.graphList(selectedType: selectedType)
//            DispatchQueue.main.async {
//               // self.tableView.reloadSections([1], with: .none)
//            //self.tableView.reloadData()
//            }
//              self.trackApi()
             print(self.objpercentge)
           }
        else
        {
//        self.createAlert(title: AppName, message:model?.message ?? "")
        }
    }
}
}

