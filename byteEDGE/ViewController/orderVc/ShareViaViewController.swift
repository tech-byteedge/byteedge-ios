
//  ShareViaViewController.swift
//  byteEDGE
//  Created by gaurav on 03/02/22.

import UIKit

class ShareViaViewController: UIViewController {
    //MARK: -VARIABLES-
    var icons = ["whatsapp", "linkedin", "instagram_icon", "copy_link"]
    
//    var icons = ["whatsappIcon", "linkedInIcon", "skypeIcon"
    var callBack : ((Int)->())?
    //MARK: -OUTLETS-
   
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    //MARK: -VIEW LIFE CYCLE-
    override func viewDidLoad() {
        super.viewDidLoad()
        setVc()
    }
    //MARK: -ACTIONS-
  
    
    //MARK: -FUNCTIONS-
    func setVc() {
        view.backgroundColor = .black
        ////outer view
//        outerView.makeRoundCornerwithborder(12.0, bordercolor: .appColor(.white), borderwidth: 1.0)
        outerView.backgroundColor = .appColor(.white, alpha: 0.5)
        view.backgroundColor = .clear
        ///Register of cells
        collectionView.registerCell(type: ShareCollectionViewCell.self)
        ///connectors
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.init(white: 0.0, alpha: 0.51)
        collectionView.isScrollEnabled = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(swipeDownAction))
        gesture.numberOfTapsRequired = 1
                self.outerView.isUserInteractionEnabled = true
                self.outerView.addGestureRecognizer(gesture)
    }
    
    @objc func swipeDownAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

extension ShareViaViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let action = callBack {
            action(indexPath.row)
        }
    }
    
}

extension ShareViaViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return icons.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(withType: ShareCollectionViewCell.self, for: indexPath) as? ShareCollectionViewCell else {
            fatalError("ShareCollectionViewCell is not initialize...")
        }
        cell.iconImg.image = UIImage(named: self.icons[indexPath.row])
        return cell
    }
    
    
}

extension ShareViaViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
