
import UIKit
import PhotosUI
import AVFoundation
import Alamofire
import SDWebImage
import SwiftUI

enum commentReplyState  {
    case none, comment, commentReply
}

class CommentsViewController: UIViewController,UITextViewDelegate{
    //MARK: -VARIABLES-
//    let placeholder = "Type message here..."
    var selectedImage: UIImage?
    var commentListData : CommentModelData?
    var postId = String()
    var commentState : commentReplyState = .comment
    var commentId : String = ""
    var group_id = String()
    var placeholderLabel : UILabel!

    
    
    
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var vcTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var writeCommentView: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var emojiBtn: UIButton!
    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var seperatorView: UIView!

    //MARK: -VIEW LIFE CYCLE-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addplaceholder()
        setVc()
        commentList(loader: true)
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func tappedOnEmojiBtn(_ sender: Any) {
    createAlert(title: kAppName, message: "Under Development")
    }
    @IBAction func tappedOnImgBtn(_ sender: Any) {
        self.presentCommentImageActionSheet()
    }
    @IBAction func tappedOnSendBtn(_ sender: Any) {
        self.view.endEditing(true)
//        self.commentTextView.placeholder = "Type message here..."
//        sendBtn.isUserInteractionEnabled = false
        if self.commentState == .comment {
        self.comment(postId: self.postId, text: self.commentTextView.text, groupId: self.group_id)
//            self.commentTextView.text = nil
        }else{
            self.commentReply(postId: self.postId, commentId: self.commentId, Text: self.commentTextView.text, groupId: self.group_id)
//            self.commentTextView.text = nil
            self.commentState = .comment
        }
    }
    
    
    func addplaceholder()
    {
        self.commentTextView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Write something here......"
        placeholderLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        placeholderLabel.sizeToFit()
        commentTextView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (commentTextView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !commentTextView.text.isEmpty
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
           placeholderLabel.isHidden = !commentTextView.text.isEmpty
       }
    
    
    //MARK: -FUNCTIONS-
    func setVc() {
        view.backgroundColor = .black
        ///register of tableView cell
        tableView.registerCell(type: CommentsTableViewCell.self)
        ///Header View register
        let nibName = UINib(nibName: "CommentsHeaderView", bundle: nil)
        self.tableView.register(nibName, forHeaderFooterViewReuseIdentifier: "CommentsHeaderView")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        
        ///set vc title
        vcTitleLbl.text = "Comments"
        seperatorView.backgroundColor = .appColor(.gray, alpha: 0.27)
        writeCommentView.backgroundColor = .black
        commentTextView.backgroundColor = .clear
        commentTextView.textColor = .appColor(.white)
        commentTextView.font = .ProductSans(.regular, size: .oneFive)
//        commentTextView.placeholder = placeholder
        
        sendBtn.backgroundColor = .appColor(.pink)
        sendBtn.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        sendBtn.setTitle("Send", for: .normal)
        self.sendBtn.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        imgBtn.isHidden = true
        
        
        
    }
}

//MARK: -TABLEVIEW DELEGATE-
extension CommentsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
}


//MARK: -TABLEVIEW DATA SOURCE-
extension CommentsViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return commentListData?.commentList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentListData?.commentList?[section].reply?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueCell(withType: CommentsTableViewCell.self, for: indexPath) as? CommentsTableViewCell else {
            fatalError("PurchaseHistoryTableViewCell is not initialize...")
        }
        //set user image
        if let image = commentListData?.commentList?[indexPath.section].reply?[indexPath.row].replyUser?.image, let url = URL(string: image) {
            
            cell.userImg.layer.masksToBounds = true
            cell.userImg.layer.cornerRadius = cell.userImg.frame.size.height/2;
            cell.userImg.layer.masksToBounds = true
            cell.userImg.layer.borderWidth=1
            
            cell.userImg.sd_setImage(with: url, placeholderImage: UIImage(named: "kIconprofileperson") )
            
        }else {
            cell.userImg.image = UIImage(named: kIconprofileperson)
        }///set user Name
        ///
        ///set reply message and user name
        if let replyMessage = commentListData?.commentList?[indexPath.section].reply?[indexPath.row].reply, let userName = commentListData?.commentList?[indexPath.section].reply?[indexPath.row].replyUser?.name {
            cell.commentLbl.text = replyMessage
            cell.userNameLbl.text = userName
        }else {
            cell.commentLbl.text = "--"
            cell.userNameLbl.text = "User Name"
        }///set time
        if let time = commentListData?.commentList?[indexPath.section].reply?[indexPath.row].createdAt {
            let timer =  CommonUtilities.shared.differnceTime(sendTime: time)
            cell.timeLbl.text = timer +  " " + kAgo
        }else {
        cell.timeLbl.text = kTime
        }
        
        //set like count
        if let likeCount = commentListData?.commentList?[indexPath.section].reply?[indexPath.row].totalLikeOnReply {
            likeCount == 0 ? (cell.likeButtonLbl.text = "Like") : (cell.likeButtonLbl.text = "\(likeCount) Like")
            
        }else{
            cell.likeButtonLbl.text = "Like"
        }
        
        //set like icon
        if commentListData?.commentList?[indexPath.section].reply?[indexPath.row].isLiked == true {
            cell.likeBtn.setImage(UIImage(named: "heart"), for: .normal)
        }else {
            cell.likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
        }
        
        cell.likeBtnAciton = { status in
            if let commentReplyId = self.commentListData?.commentList?[indexPath.section].reply?[indexPath.row].id, let commentId = self.commentListData?.commentList?[indexPath.section].reply?[indexPath.row].commentId  {
                self.likeReplyComment(postId: commentReplyId, commentId: commentId, groupId: self.group_id)
            }else {
            }
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentsHeaderView")  as! CommentsHeaderView
        view.tintColor = .clear
        //set user image
        if let image = commentListData?.commentList?[section].commentUser?.image, let url = URL(string: image) {
            view.userImg.sd_setImage(with: url, placeholderImage: UIImage(named: "kIconprofileperson") )
            
        }else {
            view.userImg.image = UIImage(named: kIconprofileperson)
        }
        
        ///set reply message and user name
        if let replyMessage = commentListData?.commentList?[section].comment, let userName = commentListData?.commentList?[section].commentUser?.name{
            view.commentLbl.text = replyMessage
            view.userNameLbl.text = userName
        }else {
            view.commentLbl.text = "--"
            view.userNameLbl.text = "User Name"
        }///set time
        if let time = commentListData?.commentList?[section].createdAt {
        let timer =  CommonUtilities.shared.differnceTime(sendTime: time)
        view.timeLbl.text = timer +  " " + kAgo
        }else {
            view.timeLbl.text = kTime
        }
        //set like count
        if let likeCount = commentListData?.commentList?[section].totalLikeOnComment {
            likeCount == 0 ? (view.likeButtonLbl.text = "Like") : (view.likeButtonLbl.text = "\(likeCount) Like")
            
        }else{
            view.likeButtonLbl.text = "Like"
        }
        
        //set like icon
        if commentListData?.commentList?[section].isLiked == true {
            view.likeBtn.setImage(UIImage(named: "heart"), for: .normal)
        }else {
            view.likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
        }
        
         view.likeBtnAciton = { status in
            if let commentReplyId = self.commentListData?.commentList?[section].id {
                self.likeComment(postId: commentReplyId, groupId: self.group_id,postID :self.postId)
            }else {
            }
            
        }
        
        view.commentBtnAction =  {
            self.commentState = .commentReply
            self.commentTextView.becomeFirstResponder()
            if let commentId = self.commentListData?.commentList?[section].id {
                self.commentId = commentId
            }else {
            }
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return UITableView.automaticDimension
    }
}

//MARK: -COMMENT LIST API-
extension CommentsViewController  {
    
    func commentList(loader: Bool){
        if loader  {
            Loader.isLoader(show: true)
        }else {
            Loader.isLoader(show: false)
        }
        
        var param: [String: Any] = [:]
    
        param["postId"] = self.postId
        param["groupId"] = self.group_id
        

        ServiceManager.instance.request(
            method: .get,
            apiURL: Api.commentList.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: CommentModel.self,
            encoding: URLEncoding.queryString)
        { (status, json, model, error) in
            
            guard model?.status == 200 else {
                print(model?.status as Any)
                Loader.isLoader(show: false)
            //    self.createAlert(title: kAppName, message:model?.message ?? "")
                return
            }
            
            self.commentListData = model?.data
            
            DispatchQueue.main.async {
                
                self.tableView.reloadData()
                Loader.isLoader(show: false)
            }
          
        }
    }
    
    
}
//MARK: -COMMENT LIKE-
extension CommentsViewController {
    func likeComment(postId : String,groupId: String,postID:String) {
        var param: [String: Any] = [:]
        param["commentId"] = postId
        param["postId"] = self.postId
        param["role"] = "commentlike"
        param["groupId"] = self.group_id
        
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
//                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.commentList(loader: false)

        }
    }
}
//MARK: -REPLY LIKE-
extension CommentsViewController {
    func likeReplyComment(postId : String, commentId : String,groupId: String) {
        var param: [String: Any] = [:]
        param["replyId"] = postId
        param["role"] = "replylike"
        param["commentId"] = commentId
        param["groupId"] = self.group_id
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
//                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.commentList(loader: false)

        }
    }
}

//MARK: -COMMENT-
extension CommentsViewController {
    func comment(postId : String,text : String,groupId: String) {
        var param: [String: Any] = [:]
        param["postId"] = postId
        param["role"] = "comment"
        param["comment"] = text
        param[""] = text
        param["groupId"] = self.group_id
        
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
//                self.commentTextView.placeholder = self.placeholder
                self.addplaceholder()
                return
            }
            self.commentList(loader: false)
            self.commentTextView.text = nil
            self.addplaceholder()
//            self.commentTextView.placeholder = self.placeholder
//            self.setVc()
//            self.sendBtn.isUserInteractionEnabled = true

        }
    }
}

//MARK: -COMMENT REPLY-
extension CommentsViewController {
    func commentReply(postId : String, commentId : String, Text : String,groupId: String) {
        var param: [String: Any] = [:]
        param["postId"] = postId
        param["role"] = "reply"
        param["commentId"] = commentId
        param["reply"] = Text
        param["groupId"] = self.group_id
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
                return
            }
            self.commentList(loader: false)
            self.commentTextView.text = ""
            self.addplaceholder()
//            self.commentTextView.placeholder = self.placeholder

        }
    }
}

//MARK: -IMAGE PICKER DELEGATE-
extension CommentsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func presentCommentImageActionSheet() {
        let actionSheet = UIAlertController(
            title: "Comments",
            message: "How would you like to select a Comments picture?",
            preferredStyle: .actionSheet
        )
        actionSheet.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .cancel,
                handler: nil
            )
        )
        actionSheet.addAction(
            UIAlertAction(
                title: "Take Photo",
                style: .default,
                handler: { [weak self] _ in
                    
                    self?.checkCamraPermission { (isAuthrosized) in
                        if isAuthrosized {
                            DispatchQueue.main.async {
                                self?.presentCamera()
                            }
                        }
                    }
                }
            )
        )
        actionSheet.addAction(UIAlertAction(
            title: "Choose Photo",
            style: .default,
            handler: { [weak self] _ in
                
                self?.checkGalleryPermission { (isAuthrosized) in
                    if isAuthrosized {
                        DispatchQueue.main.async {
                            self?.presentPhotoPicker()
                        }
                    }
                }
            }
        )
        )
        DispatchQueue.main.async {
            self.present(actionSheet, animated: true)
        }
    }
    
    func presentCamera() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        DispatchQueue.main.async {
            self.present(vc, animated: true)
        }
    }
    
    func presentPhotoPicker() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        DispatchQueue.main.async {
            self.present(vc, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        self.selectedImage = selectedImage
//        self.commentImgOuterView.isHidden = false
//        self.commentImg.image = selectedImage
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func checkCamraPermission(closure: @escaping (Bool)-> Void) {
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: // The user has previously granted access to the camera.
            closure(true)
            
        case .notDetermined: // The user has not yet been asked for camera access.
            AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                if granted {
                    closure(true)
                } else {
                    self?.failedCameraAccess()
                    closure(false)
                }
            }
            
        case .denied: // The user has previously denied access.
            failedCameraAccess()
            closure(false)
            
        case .restricted: // The user can't grant access due to restrictions.
            failedCameraAccess()
            closure(false)
            
        @unknown default:
            failedCameraAccess()
            closure(false)
        }
    }
    
    func checkGalleryPermission(closure: @escaping (Bool)-> Void) {
        
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            closure(true)
            
        case .denied:
            failedGalleyAccess()
            closure(false)
            
        case .limited:
            failedGalleyAccess()
            closure(false)
            
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ [weak self]status in
                if status == .authorized{
                    closure(true)
                } else {
                    self?.failedGalleyAccess()
                    closure(false)
                }
            })
            
        case .restricted:
            failedGalleyAccess()
            closure(false)
            
        @unknown default:
            failedGalleyAccess()
            closure(false)
        }
    }
    
    private func failedCameraAccess() {
        DispatchQueue.main.async {
            self.presentAlertViewWithOneButton(
                alertTitle: kAppName,
                alertMessage: kCameraPermissionMsg,
                btnOneTitle: kOK) { (alert) in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }
    }
    
    private func failedGalleyAccess() {
        DispatchQueue.main.async {
            self.presentAlertViewWithOneButton(
                alertTitle: kAppName,
                alertMessage: kGalleryPersissionMSg,
                btnOneTitle: kOK) { (alert) in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }
    }
}
