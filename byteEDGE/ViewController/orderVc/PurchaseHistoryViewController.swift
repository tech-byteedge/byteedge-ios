//
//  PurchaseHistoryViewController.swift
//  byteEDGE
//
//  Created by gaurav on 20/01/22.
//

import UIKit

class PurchaseHistoryViewController: UIViewController {
    
    //MARK: -VARIABLES-
    
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var vcTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //MARK: -VIEW LIFE CYCLE-
    override func viewDidLoad() {
        super.viewDidLoad()
        setVc()
    }
    //MARK: -ACTIONS-
    @IBAction func tappedOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: -FUNCTIONS-
    func setVc() {
        view.backgroundColor = .black
        ///register of tableView cell
        tableView.registerCell(type: PurchaseHistoryTableViewCell.self)
        ///connector to tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
    }
}
//MARK: -TABLEVIEW DELEGATE-
extension PurchaseHistoryViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
}


//MARK: -TABLEVIEW DATA SOURCE-
extension PurchaseHistoryViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueCell(withType: PurchaseHistoryTableViewCell.self, for: indexPath) as? PurchaseHistoryTableViewCell else {
            fatalError("PurchaseHistoryTableViewCell is not initialize...")
        }
        return cell
    }
}
