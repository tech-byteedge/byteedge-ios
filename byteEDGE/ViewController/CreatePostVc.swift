
import UIKit
import IBAnimatable
import MobileCoreServices
import UniformTypeIdentifiers
import AVKit
import DBAttachmentPickerController
import Fusuma
import AVFoundation
import Photos
import  SDWebImage

enum IsType{
case photo,video
}

class CreatePostVc: UIViewController,FusumaDelegate{
    //MARK: - OutLet's
    @IBOutlet weak var prfname: UILabel!
    @IBOutlet weak var prfimg: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var post: UIButton!
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var lblAddMedia: UILabel!
    @IBOutlet weak var AddImage: AnimatableButton!
    @IBOutlet weak var AddDocuments: AnimatableButton!
    @IBOutlet weak var AddVideo: AnimatableButton!
    @IBOutlet weak var botttomCons: NSLayoutConstraint!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    
    
    //MARK: - Property's
    var disclaimerHasBeenDisplayed = false
    var placeholderLabel : UILabel!
    var details:[UIImage] = []
    var doc : URL?
    var imagPickUp : UIImagePickerController!
   // var SelectedAssets = [PHAsset]()
    var mediaType : String?
    var isType:IsType = .photo
    var selectedVideo : URL?
    var imagepost = [UIImage]()
    var generatThumbsNail = UIImage()
    var tapclick = false
    var idforFeed = ""
    
    private lazy var imagePicker = ImagePicker(presentationController: self, delegate: self)

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        self.addplaceholder()
        super.viewDidLoad()
        self.setprofile()
        self.showpopup()
        imagPickUp = self.imageAndVideos()
        self.view.backgroundColor = .black
        self.viewOuter.backgroundColor = .black
        initSetup()
        if(details.count == 0){
            heightCollectionView.constant = 0
        }
    }
    
    
    
    
    
//    override func viewDidAppear(_ animated: Bool) {
//        if disclaimerHasBeenDisplayed == false {
//            disclaimerHasBeenDisplayed = true
//            let alert = UIAlertController(title: "bytEdge", message: "You are under strict vigilance please adhere to community guidelines to help us maintain a good learning community environment.be curious and keep learning", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .default,handler:{(_) in
//                }
//                ))
//
//            alert.addAction(UIAlertAction(title:"Cancel", style: .cancel,handler:{(_)
//                in
//                self.navigationController?.popViewController(animated:false)
//            }))
//           self.navigationController?.present(alert,animated:true)
//          }
//    }
    
    func showpopup()
    {
        if !UserDefaults.standard.bool(forKey: "wasAlreadyShown") {
            UserDefaults.standard.set(true, forKey: "wasAlreadyShown")
            let alert = UIAlertController(title: "bytEdge", message: "You are under strict vigilance please adhere to community guidelines to help us maintain a good learning community environment.be curious and keep learning", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: .default,handler:{(_) in
                }
                ))

            alert.addAction(UIAlertAction(title:"cancel", style: .cancel,handler:{(_)
                in
                self.navigationController?.popViewController(animated:false)
            }))
          self.navigationController?.present(alert,animated:true)
        
    }
        }
    
    
    
    func setprofile()
    {
        self.prfimg.layer.masksToBounds = true
        self.prfimg.layer.cornerRadius = self.prfimg.frame.size.height/2;
        self.prfimg.layer.masksToBounds = true
        self.prfimg.layer.borderWidth=1
        
        let name = kUserDefaults.retriveString(.firstName)
        let lastName = kUserDefaults.retriveString(.lastName)
        self.prfname.text = name + " " + lastName
        if kUserDefaults.retriveString(.image) == ""
        {
        self.prfimg.image = UIImage(named: "prfImg")
        }
        else
        {
        let urlImage =  kUserDefaults.retriveString(.image)
        self.prfimg.sd_setImage(with: URL(string:urlImage as! String), placeholderImage: UIImage(named:"prfImg"), options: .highPriority, context: nil)
    
        }
    }
    
    //MARK: - Action's
    @IBAction func btnBack(_ sender: Any) {
    self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnPost(_ sender: UIButton) {
        
        if details.count == 0 && textView.text.count == 0 && doc == nil {
        createAlert(title: AppName, message: kPlzAddSomthng)
            return
        }
       if mediaType == "3" || mediaType == "4" {
        self.details.removeAll()
        }
        
        if Request.shared.postColorId == nil{
        Request.shared.postColorId = "#98CA98"
        }
        self.creapost()
    }
    
    @IBAction func btnAddImage(_ sender: Any) {
        if details.count == 5
        {
        createAlert(title: AppName, message: "You only select 5  images  ")
        }
        else
        {
        self.chooseImage()
        self.isType = .photo
        }}
    
    @IBAction func btnAddVideo(_ sender: Any) {
        self.tapclick = true
        addVideo()
        self.isType = .video
    }
    
    override func viewWillLayoutSubviews() {
    self.post.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
}
//MARK: - Extension
extension CreatePostVc{
    private func initSetup(){
        
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        textView.layer.cornerRadius = 12
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1).cgColor
        viewOuter.roundUpperBothCorner(cornerRadius: 30)
        textView.delegate = self
    }
}
//MARK: - Extension UITextViewDelegate

extension CreatePostVc : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        Request.shared.isColored = ""
        mediaType = "1"
//        lblAddMedia.isHidden = true
        AddImage.isHidden = true
        AddVideo.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardwillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        AddImage.isHidden = false
        AddVideo.isHidden = false
//        lblAddMedia.isHidden = false
        //     ViewBtnBack.isHidden = true
    }
    
    @objc func keyboardwillShow(notification:NSNotification){
        if let keyboardFrame:NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            //            botttomCons.constant = keyboardHeight + 0
        }
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode, metaData: [ImageMetadata]) {
       // stackForColor.isUserInteractionEnabled = false
        //stackForSmallColor.isUserInteractionEnabled = false
        self.details = images
        if self.details.count > 0{
                  self.heightCollectionView.constant = 85
                  self.imageCollectionView.reloadData()
              }
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode, metaData: ImageMetadata) {
//        self.details.append(image)
//        if self.details.count > 0{
//                  self.heightCollectionView.constant = 85
//                  self.imageCollectionView.reloadData()
//              }
//        stackForColor.isUserInteractionEnabled = false
//        stackForSmallColor.isUserInteractionEnabled = false
    }
    
    func fusumaImageSelected(_ image: UIImage, source: FusumaMode) {
//        self.details.append(image)
//        if self.details.count > 0{
//                  self.heightCollectionView.constant = 85
//                  self.imageCollectionView.reloadData()
//              }
//        stackForColor.isUserInteractionEnabled = false
//        stackForSmallColor.isUserInteractionEnabled = false
    }
    
    func fusumaVideoCompleted(withFileURL fileURL: URL) {
       
    }
    
    func fusumaCameraRollUnauthorized() {
      
    }
    
    func fusumaMultipleImageSelected(_ images: [UIImage], source: FusumaMode) {
    }
    func addplaceholder()
    {
        self.textView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Write something here......"
        placeholderLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        placeholderLabel.sizeToFit()
        textView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
        
    }
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
}






//MARK: - Extension
extension CreatePostVc
{
    func hide(){
        AddImage.isEnabled = false
        AddVideo.isEnabled = false
        //        AddImageMini.isEnabled = false
        //        AddvideoMini.isEnabled = false
        //        AddDocumentMini.isEnabled = false
        textView.textColor = UIColor.white
    }
    func show(){
        AddImage.isEnabled = true
        AddVideo.isEnabled = true
        textView.textColor = #colorLiteral(red: 0.06666666667, green: 0.1294117647, blue: 0.1333333333, alpha: 1)
        
    }
}


extension CreatePostVc: UICollectionViewDelegate{
}

//MARK: - Extension collection view Data source
extension CreatePostVc:  UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if details.count != 0
        {
            if  self.isType == .video{
            self.AddImage.isUserInteractionEnabled = false
            }

        self.AddVideo.isUserInteractionEnabled = false
        }
        else
        {
        self.AddVideo.isUserInteractionEnabled = true
        }
        return details.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreatePostCell", for: indexPath) as? CreatePostCell else{
            fatalError("wrong")
        }
        
        cell.delegate = self
        cell.removeBtn.tag = indexPath.row
        cell.AddImage.image = (details[indexPath.row])
        return cell
    }
}

//MARK: - Extension collection view Delegate Flow Layout
extension CreatePostVc: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize(width: 120, height: 85)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

//MARK: - Extension ImagePickerDelegate
extension CreatePostVc:ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        if let images = image {
            details.append(images)
            imageCollectionView.reloadData()
            
        }
    }
}

//MARK: - Extension MyProtocol
extension CreatePostVc :MyProtocol{
    func delete(type: Int) {
        details.remove(at: type)
        self.imageCollectionView.reloadData()
        if(details.count == 0){
            heightCollectionView.constant = 0
            self.AddImage.isUserInteractionEnabled = true
            self.AddVideo.isUserInteractionEnabled = true
        }
    }
}

//MARK: - Extension
extension CreatePostVc{
    func imageAndVideos()-> UIImagePickerController{
        if(imagPickUp == nil){
            imagPickUp = UIImagePickerController()
            imagPickUp.delegate = self
            imagPickUp.allowsEditing = true
            imagPickUp.videoQuality = .typeHigh
        }
        return imagPickUp
    }
}

//MARK: - Extension imagePickerController
extension CreatePostVc{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            self.details.append(chosenImage)
            imageCollectionView.reloadData()
        }
        
        else if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.details.append(chosenImage)
            imageCollectionView.reloadData()
        }
        
        if self.details.count > 0{
            self.heightCollectionView.constant = 85
            self.imageCollectionView.reloadData()
        }

        
        dismiss(animated: true, completion: nil)
         let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL
         if let url = videoURL{
            let data = try? Data(contentsOf: url as URL, options: [])
            let videoMb = Double(data?.count ?? 0) / (1048576.0)
            print(videoMb)
            print(videoURL!)
            self.selectedVideo = url as URL?
            self.doc = videoURL as URL?
            if let thumbnailImage = getThumbnailImage(forUrl: url as URL) {
            self.generatThumbsNail = thumbnailImage
            print(self.generatThumbsNail)
            details.append(thumbnailImage)
            }
            
             
            if self.details.count > 0{
            self.heightCollectionView.constant = 85
            self.imageCollectionView.reloadData()
            }
        }
        
        imagPickUp.dismiss(animated: true, completion: { () -> Void in
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagPickUp.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
}

      extension CreatePostVc {
      func addImage()  {
        let fusuma = FusumaViewController()
                self.mediaType = "2"
               fusuma.delegate = self
        
               fusuma.allowMultipleSelection = true
               fusuma.availableModes = [.library]
               fusuma.photoSelectionLimit = 5
               present(fusuma, animated: true, completion: nil)

    }
    
//    func addImage()  {
//        let allAssets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
//        var evenAssets = [PHAsset]()
//        let imagePicker = ImagePickerController(selectedAssets: SelectedAssets)
//        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
//        imagePicker.settings.selection.max = 5
//        imagePicker.settings.theme.selectionStyle = .numbered
//        imagePicker.settings.selection.unselectOnReachingMax = true
//        self.presentImagePicker(imagePicker, select: { (asset) in
//            print("Selected: \(asset)")
//            if asset.mediaType == .video {
//                if self.selectedVideo == nil {
//                    asset.getURL { url in
//                        self.selectedVideo = url
//                    }
//                } else {
//
//                    imagePicker.deselect(asset: asset)
//                }
//            } else {
//                if let img = CommonUtilities.shared.getAssetThumbnail(assets: [asset]).first {
//                    self.details.append(img)
//                }
//            }
//            evenAssets.append(asset)
//        }, deselect: { (asset) in
//            //evenAssets.remove(at: asset)
//            print("Deselected: \(asset)")
//        }, cancel: { (assets) in
//
//            print("Canceled with selections: \(assets)")
//        }, finish: { (assets) in
//            self.SelectedAssets.removeAll()
//            self.details.removeAll()
//            print("Finished with selections: \(assets)")
//            for i in 0..<assets.count
//            {
//                self.SelectedAssets.append(assets[i])
//
//            }
//            self.mediaType = "2"
//            self.convertAssetToImages()
//
//        })
//    }
    func addVideo(){
        self.mediaType = "3"
        let ActionSheet = UIAlertController(title: nil, message: "Select Video", preferredStyle: .actionSheet)
        let cameraPhoto = UIAlertAction(title: "Make Video", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagPickUp.mediaTypes = ["public.movie"]
                self.imagPickUp.sourceType = UIImagePickerController.SourceType.camera;
                self.present(self.imagPickUp, animated: true, completion: nil)
            }
            else
            {
                UIAlertController(title: "iOSDevCenter", message: "No Camera available.", preferredStyle: .alert).show(self, sender: nil)
            }
            
        })
        
        let PhotoLibrary = UIAlertAction(title: "Video Library", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.imagPickUp.mediaTypes = ["public.movie"]
                self.imagPickUp.sourceType = UIImagePickerController.SourceType.photoLibrary;
                self.present(self.imagPickUp, animated: true, completion: nil)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        ActionSheet.addAction(cameraPhoto)
        ActionSheet.addAction(PhotoLibrary)
        ActionSheet.addAction(cancelAction)
        if let popoverController = ActionSheet.popoverPresentationController {
                    popoverController.sourceView = self.view
                    popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    popoverController.permittedArrowDirections = []
                }

            self.present(ActionSheet, animated: true, completion: nil)
        
    }
    func camera()
    {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
        
    }
    
}

//Variable class
class Request{
    static var shared = Request()
    
    var coverPhoto: UIImage?
    var profilePhoto: UIImage?
    var name: String?
    var profession: String?
    var dob : String?
    var language: String?
    //    var education:String?
    //    var university:String?
    //    var educationId:String?
    //    var universityId:String?
    var about: String?
    var intrest: String?
    var selectedCountryName: String?
    var selectedCountryCode :Int?
    var selectedStateId: String?
    var selectedStateName: String?
    var selectedCityId: String?
    var selectedCityName: String?
    
    var postColorId:String?
    var isColored: String?
}

//MARK: Api Class
extension CreatePostVc
{
    func creapost() {
        var param: [String: Any] = [:]
        param["description"] = self.textView.text
        param["groupId"] = self.idforFeed
        ServiceRequest.instance.addPost( param, imagArry: self.details, videoURL: self.selectedVideo, thumbnails: self.generatThumbsNail ) {  (isSucess, response, error) in
            guard response?.Status == 200 else {
            self.createAlert(title: kAppName, message: "Something Went wrong")
             return
            }
               self.presentAlertViewWithOneButton(
                alertTitle: kAppName,
                alertMessage: response?.message,
                btnOneTitle: kOK) { (alert) in
                    self.selectedVideo = nil
                    self.imagepost.removeAll()
                    self.navigationController?.popViewController(animated: false)
                }
        }
    }
}

//extension PHAsset {
//    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
//        if self.mediaType == .image {
//            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
//            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
//                return true
//            }
//            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
//                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
//            })
//        } else if self.mediaType == .video {
//            let options: PHVideoRequestOptions = PHVideoRequestOptions()
//            options.version = .original
//            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
//                if let urlAsset = asset as? AVURLAsset {
//                    let localVideoUrl: URL = urlAsset.url as URL
//                    completionHandler(localVideoUrl)
//                } else {
//                    completionHandler(nil)
//                }
//            })
//        }
//    }
//}

extension CreatePostVc: UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func chooseImage(){
        let ActionSheet = UIAlertController(title: nil, message: "Select Image", preferredStyle: .actionSheet)
         let cameraPhoto = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagPickUp.mediaTypes = ["public.image"]
                self.imagPickUp.sourceType = UIImagePickerController.SourceType.camera;
                self.present(self.imagPickUp, animated: true, completion: nil)
            }
            else
            {
             UIAlertController(title: "iOSDevCenter", message: "No Camera available.", preferredStyle: .alert).show(self, sender: nil)
            }
        })
        
        let PhotoLibrary = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
         self.addImage()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
        (alert: UIAlertAction) -> Void in
        })
        ActionSheet.addAction(cameraPhoto)
        ActionSheet.addAction(PhotoLibrary)
        ActionSheet.addAction(cancelAction)
        if let popoverController = ActionSheet.popoverPresentationController {
        popoverController.sourceView = self.view
        popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        popoverController.permittedArrowDirections = []
        }
        self.present(ActionSheet, animated: true, completion: nil)
    }
    
}




extension URL {
    func generateThumbnail() -> UIImage? {
        do {
            let asset = AVURLAsset(url: self)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            
            // Swift 5.3
            let cgImage = try imageGenerator.copyCGImage(at: .zero,
                                                         actualTime: nil)

            return UIImage(cgImage: cgImage)
        } catch {
            print(error.localizedDescription)

            return nil
        }
    }
}
