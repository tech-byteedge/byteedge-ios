
import UIKit
import  KRProgressHUD
import  Alamofire

class FAQsVC: UIViewController
{
    var collapse = false
    var isSelected = -1
    var selected = Int()
    var datamodelfaq: FaqModelData?
    
    @IBOutlet weak var vw_Header: UIView!
    @IBOutlet weak var lblHeaderTxt: UILabel!
    @IBOutlet weak var tblVw_FAQ: UITableView!
    @IBOutlet weak var searchview: UIView!
    @IBOutlet weak var searchtefld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.faqApi()
        vw_Header.layer.cornerRadius = 10
        vw_Header.dropShadow()
        tblVw_FAQ.dataSource = self
        tblVw_FAQ.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblVw_FAQ.reloadData()
        self.faqApi()
    }
    
    @IBAction func tap_BackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Delegat and DataSou
extension FAQsVC:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
    return datamodelfaq?.data?.faqList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == isSelected {
         return  2
        }
        else{
         return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let ncell = tableView.dequeueReusableCell(withIdentifier: "FaqQuestionCell", for: indexPath) as! FaqQuestionCell
            ncell.lbl_Question.text =  " Q." + " "  +  (datamodelfaq?.data?.faqList?[indexPath.section].question ?? "")
           if  indexPath.section == isSelected {
           ncell.imgDropDown.image = #imageLiteral(resourceName: "upArrow")
           ncell.bottomQuestion.constant = -6
            }
            else{
                
                ncell.bottomQuestion.constant = 10
                ncell.vw_main.layer.cornerRadius = 10
                ncell.imgDropDown.image = #imageLiteral(resourceName: "droparrow")

            }

            ncell.selectionStyle = .none
            return ncell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FaqAnswerCell", for: indexPath) as! FaqAnswerCell
            cell.lbl_Answer.text =  datamodelfaq?.data?.faqList?[indexPath.section].answer
            //
            if  indexPath.section == isSelected && collapse == true
            {
                cell.vw_main.roundParticularCorners(cornerRadius: 10)
            }
            
            else{
                cell.vw_main.layer.cornerRadius = 10
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if indexPath.row == 0{
            if self.isSelected == indexPath.section{
                self.isSelected = -1
                collapse = false
            }
            else
            {
                collapse = true
                isSelected = indexPath.section
            }
            tableView.reloadData()
        }
    }
}
//MARK: QuestionCell
class FaqQuestionCell: UITableViewCell{
    @IBOutlet weak var lblLine:UILabel!
    @IBOutlet weak var bottomQuestion:NSLayoutConstraint!
    @IBOutlet weak var vw_main: UIView!
    @IBOutlet weak var lbl_Question: UILabel!
    @IBOutlet weak var imgDropDown:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        vw_main.layer.masksToBounds = true
        vw_main.layer.cornerRadius = 10
    }
}
//MARK: AnswerCell
class FaqAnswerCell: UITableViewCell{
    @IBOutlet weak var vw_main: UIView!
    @IBOutlet weak var lbl_Answer: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK: API intargration
extension FAQsVC
{
    func faqApi()
    {
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.faq.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: FaqModelData.self
        ) { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                self.datamodelfaq = model
                DispatchQueue.main.async {
                    self.tblVw_FAQ.reloadData()
                }
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
            
        }
    }
    
}
