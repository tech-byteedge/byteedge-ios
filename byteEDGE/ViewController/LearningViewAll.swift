
import UIKit
import IBAnimatable
import Alamofire
import KRProgressHUD

class LearningViewAll: UIViewController
{
 //MARK: - IBOutlets
    @IBOutlet weak var listViewAllcollection: UICollectionView!
    //MARK: - Variables
    let columnLayout = CustomViewFlowLayout()
    var typerole = Int()
    var viewlearnig :LearningListViewAll?
    var selectedAction : (()->())?
    var courseData: LearningListData?
    var currentPage : Int = 0
    var isLoadingList : Bool = false
    var pageCount = 1
    var limit = 0
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.listViewAllcollection.register(UINib(nibName: "ScrollView", bundle: nil),forCellWithReuseIdentifier: "ScrollView")
        self.listViewAllcollection.collectionViewLayout = columnLayout
        rigisteredCell()
        setup()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        pageCount = 1
        learnongAPi(role: self.typerole, page: self.pageCount,isAppend: false)
    }
    
    //MARK: - IBAction
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func  rigisteredCell()
    {
    self.listViewAllcollection.registerCell(type: CourseCollectionViewCell.self)
    }
}

//MARK: - Extension SearchCuisineVC
  extension LearningViewAll{
    private func setup(){
        listViewAllcollection.delegate = self
        listViewAllcollection.dataSource = self
    }
}

//MARK: - Extension UICollectionViewDataSource
extension LearningViewAll: UICollectionViewDataSource,UICollectionViewDelegate{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0
        {
        if typerole == 1
        {
        return viewlearnig?.data?.course?.count ?? 0
        }
        else if typerole == 2{
        return  viewlearnig?.data?.course?.count ?? 0
        }
        else if typerole == 3
        {
        return viewlearnig?.data?.favouriteCourse?.count ?? 0
        }
        else
        {
        return 0
        }
    }
        else if section == 1 && isLoadingList
        {
         return 1
        }
        return 0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0
        {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CourseCollectionViewCell", for: indexPath) as! CourseCollectionViewCell
        cell.viewlearnig = self.viewlearnig
        cell.starView.isUserInteractionEnabled = false
            
        if typerole == 1
        {
            cell.courseNameLbl.text = viewlearnig?.data?.course?[indexPath.row].courseName
            cell.courseDiscLbl.text = viewlearnig?.data?.course?[indexPath.row].Description
            cell.ratingsLbl.text  =  String(viewlearnig?.data?.course?[indexPath.row].rating ?? 0)
            
            let val =  Double(self.viewlearnig?.data?.course?[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
            
            cell.typeCourse = self.typerole
            var urlImg = viewlearnig?.data?.course?[indexPath.row].thumbnails ?? ""
            cell.courseImg.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
            cell.callBackOflike = {
            if let id = self.courseData?.yourCourse?[indexPath.row]._id
//                if let id = self.viewlearnig?.data?.course?[indexPath.row]._id
                {
               self.favooriteApi(id: id, state:.yourCourse, index: indexPath)
                }
            }
        
        }
            
        else if typerole == 2
        {
            cell.courseNameLbl.text = viewlearnig?.data?.course?[indexPath.row].courseName
            cell.courseDiscLbl.text = viewlearnig?.data?.course?[indexPath.row].Description
            cell.ratingsLbl.text  =  String(viewlearnig?.data?.course?[indexPath.row].rating ?? 0)
            cell.typeCourse = self.typerole
            let val =  Double(self.viewlearnig?.data?.course?[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
//          let ratingshow =  String(viewlearnig?.data?.course?[indexPath.row].rating)
//          cell.ratingsLbl.text = String(viewlearnig?.data?.course?[indexPath.row].rating)
            var urlImg = viewlearnig?.data?.course?[indexPath.row].thumbnails ?? ""
            cell.courseImg.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
//            if courseData?.course?[indexPath.row].isFavourite ==  true
            if viewlearnig?.data?.course?[indexPath.row].isFavourite ==  true
            {
            cell.likeBtn.isSelected = true
            }
            else
            {
            cell.likeBtn.isSelected = false
            }
            
            cell.callBackOflike = {
                if let id = self.courseData?.course?[indexPath.row]._id{
                self.favooriteApi(id: id, state: .couese,index: indexPath)
                }
            }
        }

        else if   typerole == 3
        {
            cell.likeBtn.isHidden = true
            cell.courseNameLbl.text = viewlearnig?.data?.favouriteCourse?[indexPath.row].courseName
//           cell.courseDiscLbl.text = viewlearnig?.data?.favouriteCourse?[indexPath.row].Description
            cell.ratingsLbl.text  =  String(viewlearnig?.data?.favouriteCourse?[indexPath.row].rating ?? 0)
            cell.typeCourse = self.typerole
            let val =  Double(self.viewlearnig?.data?.favouriteCourse?[indexPath.row].rating ?? 0) ?? 0.0
            cell.starView.rating = val
            
            var urlImg = viewlearnig?.data?.favouriteCourse?[indexPath.row].thumbnails ?? ""
            cell.courseImg.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
            
//            if courseData?.favouriteCourse?[indexPath.row].isFavourite ==  true {
//            cell.likeBtn.isSelected = true
//            }else {
//            cell.likeBtn.isSelected = false
//            }
            cell.callBackOflike = {
                if let id = self.courseData?.favouriteCourse?[indexPath.row]._id {
                self.favooriteApi(id: id, state: .favouriteCourse, index: indexPath)
                }
            }
        }

        else
        {
         return  UICollectionViewCell()
        }
        
        return cell
    }
        else if indexPath.section == 1
        {
            let cell =  listViewAllcollection.dequeueReusableCell(withReuseIdentifier: "scrollIndicatorCellCollectionViewCell", for: indexPath) as! scrollIndicatorCellCollectionViewCell
             cell.indicator.startAnimating()
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           let offset = scrollView.contentOffset.y
           let contenHeight = scrollView.contentSize.height
        if offset > contenHeight - scrollView.frame.height && viewlearnig?.data?.count ?? 0 > viewlearnig?.data?.course?.count ?? 0{
               if !isLoadingList{
                   isLoadingList = true
                   pageCount = pageCount + 1
                   learnongAPi(role: self.typerole, page: pageCount,isAppend:  true)
               }
           }
           
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch typerole // course
        {
        case 3:
            if kUserDefaults.bool(forKey: "isSubscribed") == true
            {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                vc.playlistID = self.viewlearnig?.data?.favouriteCourse?[indexPath.item].playlistId ?? ""
                vc.courseId = self.viewlearnig?.data?.favouriteCourse?[indexPath.item]._id ?? ""
                
                
                vc.courseId = self.viewlearnig?.data?.favouriteCourse?[indexPath.item]._id ?? ""
                let quizStatus = self.viewlearnig?.data?.favouriteCourse?[indexPath.item].isQuizExist ?? false
                  vc.isQuizExist = quizStatus
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            else if kUserDefaults.bool(forKey: "isSubscribed") == false
            {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"SubsCriptionPlane") as! SubsCriptionPlane

                vc.callBackofSeepreviewofSub = {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
                    if let id = self.viewlearnig?.data?.favouriteCourse?[indexPath.row]._id {
                        vc.courseId = id
                            }
                         else
                          {
                            self.createAlert(title: kAppName, message: "Something went wrong")
                            }
                            self.navigationController?.pushViewController(vc, animated: false)
                                    }

                  vc.callBAck = {
                  objTransactionModel in
                 let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                vc.objTranscation = objTransactionModel
                self.navigationController?.pushViewController(vc, animated: false)

            }
                
                vc.modalPresentationStyle = .fullScreen
                vc.providesPresentationContextTransitionStyle = true
                vc.definesPresentationContext = true
                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2
                )
                self.present(vc, animated: true, completion: nil)
        }
            else
            {
              print("Nothing to do")
            }
       
        default:
            print("Nothing to do")
        }
}
}


//MARK: - Extension UICollectionViewDelegateFlowLayout
extension LearningViewAll: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
        let size = (collectionView.frame.size.width - 30) / 2
        return CGSize(width: size, height:320)
        }
        else
        {
        let size = (collectionView.frame.size.width - 30) / 2
        return CGSize(width: size, height:250)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    
    
    
    
    
}
  

extension LearningViewAll
{
    func learnongAPi(role:Int?,page:Int? , isAppend: Bool?)
    {
        var param: [String: Any] = [:]
        param["role"] = role
        param["page"] = page
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.learningViewAll.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: LearningListViewAll.self,
                                        encoding: URLEncoding.queryString)
        
        {(status, json, model, error) in
            self.isLoadingList = false
            if model?.status == 200
            {
                
                if isAppend == true{
                    if model?.data?.course?.count ?? 0 > 0{
                        for i in 0...(model?.data?.course?.count ?? 0) - 1{
                            self.viewlearnig?.data?.course?.append((model?.data?.course?[i])!)
                        }
                    }
                    
                     else if model?.data?.yourCourse?.count ?? 0 > 0{
                        for i in 0...(model?.data?.yourCourse?.count ?? 0) - 1{
                            self.viewlearnig?.data?.yourCourse?.append((model?.data?.yourCourse?[i])!)
                        }
                    }
                    
                    else if model?.data?.favouriteCourse?.count ?? 0 > 0{
                       for i in 0...(model?.data?.favouriteCourse?.count ?? 0) - 1{
                           self.viewlearnig?.data?.favouriteCourse?.append((model?.data?.favouriteCourse?[i])!)
                       }
                   }
                    
                  
                }

                else{
                    self.viewlearnig = model
                }
               print(self.viewlearnig?.data?.favouriteCourse)
                DispatchQueue.main.async {
                    self.listViewAllcollection.reloadData()
                }
            }
        }
    }
}


extension LearningViewAll {
    func favooriteApi(id: String,  state:CourseState, index : IndexPath) {
        var param : [String: Any] = [:]
        param["courseId"] = id
        ServiceRequest.instance.favoriteCourseApi(param){ (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
                UIApplication.topViewController()?.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            
            if response?.Code == 1 { // for like
                switch state {
                case .none:
                    print("none")
                case .yourCourse:
                self.viewlearnig?.data?.course?[index.row].isFavourite = true
                case .couese:
                self.viewlearnig?.data?.course?[index.row].isFavourite = true
                case .favouriteCourse:
                    print("favorite like work")
                case .recommendedCourse:
                    print("favorite like work")
                  
                case .trendingCourse:
                    print("favorite like work")
                case .recommendedCoursesetdata:
                    print("favorite like work")
                case .popular:
                print("favorite like work")
                }
             }
             else { // for unlike
                switch state {
                case .none:
                    print("none")
                case .yourCourse:
                self.viewlearnig?.data?.course?[index.row].isFavourite = false
                case .couese:
                self.viewlearnig?.data?.course?[index.row].isFavourite = false
                case .favouriteCourse:
                    print("favorite like work")
                case .recommendedCourse:
                    print("favorite like work")
                    
                case .trendingCourse:
                    print("favorite like work")
                case .recommendedCoursesetdata:
                    print("favorite like work")
                case .popular:
                    print("favorite like work")
                    
                }
            }
            
            DispatchQueue.main.async {
                self.listViewAllcollection.reloadData()
            }
        }
    }
}



