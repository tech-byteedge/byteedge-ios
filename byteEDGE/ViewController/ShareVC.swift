
import UIKit
import Alamofire
import  KRProgressHUD
class ShareVC: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var likeTap: UIButton!
    @IBOutlet weak var pinkCaption: UIButton!
    @IBOutlet weak var teasrName: UIButton!
    
    var particular_obj: ShareReals?
    var player_obj = Jwplayer()
    var id = String()
    let navigat = UINavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shareReels()
        self.setui()
    }
    
    func setui()
    {
        self.pinkCaption.layer.borderWidth = 0.3
        self.pinkCaption.layer.cornerRadius = 15
        self.pinkCaption.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func backTap(_ sender: UIButton) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC

                let nav_obj = UINavigationController(rootViewController: vc)
                nav_obj.navigationBar.isHidden = true
                UIApplication.shared.windows.first?.rootViewController = nav_obj
                UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    @IBAction func likeAction(_ sender: UIButton) {
        likeClick()
    }
    
    @IBAction func captionClick(_ sender: UIButton) {
        print("caption")
        self.teasureClick()
//        if kUserDefaults.bool(forKey: "isSubscribed") == true
//        {
//            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
//            vc.playlistID = self.particular_obj?.data?.reelList?.courseData?.playlistId ?? ""
//            vc.courseId =  self.particular_obj?.data?.reelList?.courseData?._id ?? ""
//            vc.back = true
           // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openCourseDetailshowVc"), object: nil, userInfo: ["Value":particular_obj])
            
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openCourseDetailshowVc"), object: nil)
            
//            let rootViewController:UIViewController = (UIApplication.shared.keyWindow?.rootViewController)!
//                    if (rootViewController.presentedViewController != nil) {
//                        rootViewController.dismiss(animated: true, completion: {
//                            //completion block.
////                            rootViewController.navigationController?.pushViewController(vc, animated: true)
//
//                        })
//                    }
            
            
            
            
//            self.navigationController?.pushViewController(vc, animated: false)
//            navigat.pushViewController(vc, animated: false)
//            self.present(vc, animated: false, completion: nil)
         }
        
//          else if kUserDefaults.bool(forKey: "isSubscribed") == false
//          {
//          }
//          }
//           {
//            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
//            vc.viaHome = true
//            let id  = self.particular_obj?.data?.reelList?.courseData?._id
//            vc.courseId = id ?? ""
//            vc.mediaID  = self.particular_obj?.data?.reelList?.courseData?.previewId ?? ""
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
//        else{
//
//        }
//
//    }
    
    func teasureClick()
    {
        if kUserDefaults.bool(forKey: "isSubscribed") == true
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
            vc.playlistID = self.particular_obj?.data?.reelList?.courseData?.playlistId ?? ""
            vc.courseId =  self.particular_obj?.data?.reelList?.courseData?._id ?? ""
            //            let quizStatus = self.particular_obj?.data?.reelList?.courseData?.isQuizExist ?? false
            //            vc.isQuizExist = quizStatus
            self.navigationController?.pushViewController(vc, animated: true)
         }
          else if kUserDefaults.bool(forKey: "isSubscribed") == false
          {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
            vc.viaHome = true
            let id  = self.particular_obj?.data?.reelList?.courseData?._id
            vc.courseId = id ?? ""
            vc.mediaID  = self.particular_obj?.data?.reelList?.courseData?.previewId ?? ""
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else{
            
        }
    }
    
    private func likeClick ()
    {
        
        var  count = self.particular_obj?.data?.reelList?.likes ?? 0
        let status = self.particular_obj?.data?.reelList?.isLiked
        let idlike = self.particular_obj?.data?.reelList?._id ?? ""
        if status == false
        {
            count += 1
            self.likePost(islike: status ?? false, postId:idlike)
            self.likeTap.setImage(UIImage(named: "selected-1"), for: .normal)
            self.particular_obj?.data?.reelList?.likes = count
            self.particular_obj?.data?.reelList?.isLiked = true
            self.countLbl.text = "\(count)"
        }
        
        else
        {
            count -= 1
            self.likePost(islike: status ?? true, postId: idlike)
            self.likeTap.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
            self.particular_obj?.data?.reelList?.likes = count
            self.particular_obj?.data?.reelList?.isLiked = false
            self.countLbl.text = "\(count)"
        }
    }
}



extension ShareVC
{
    func shareReels(){
        var param: [String: Any] = [:]
        param["videoId"] = id
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.shareReals.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: ShareReals.self,
                                        encoding : URLEncoding.queryString)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                
                self.particular_obj = model
                var  count = self.particular_obj?.data?.reelList?.likes ?? 0
                self.countLbl.text = "\(count)"
                self.likeClick()
                let mediaId =  model?.data?.reelList?.video
                self.player_obj.providesPresentationContextTransitionStyle = true
                self.player_obj.definesPresentationContext = true
                self.player_obj.mediaID = mediaId ?? ""
                self.addChild(self.player_obj)
                self.player_obj.view.frame = self.mainView.bounds
                self.mainView.addSubview(self.player_obj.view)
                self.player_obj.setUpPlayer(isautostart: true , volume: 5.0)
                let teasureName =  model?.data?.reelList?.teasureName
                self.teasrName.setTitle(teasureName, for: .normal)

                if self.particular_obj?.data?.reelList?._id == "" || self.particular_obj?.data?.reelList?.courseData == nil || self.particular_obj?.data?.reelList?._id == nil{
                  self.pinkCaption.isHidden = true
               }else{
                self.pinkCaption.isHidden = false
              }
            }
            
            else
            {
                
            }
            
        }
    }
}

extension ShareVC {
    func likePost(islike: Bool, postId : String) {
        var param: [String: Any] = [:]
        param["teasureId"] = postId
        param["role"] = "teasurelike"
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                self.createAlert(title: kAppName, message: response?.message ?? "")
                return
            }
        }
    }
}



extension UINavigationController {
    var rootViewController: UIViewController? {
        return viewControllers.first
    }

}




