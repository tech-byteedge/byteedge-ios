
//  SubsCriptionPlane.swift
//  byteEDGE
//  Created by Pawan Kumar on 02/03/22.

import UIKit
import  KRProgressHUD
import  Alamofire
import  Razorpay
import  SafariServices

class SubsCriptionPlane: UIViewController {
    
    //MARK: variable
    var isServiceHit = Bool()
    var objPayment: PaymentMaodel?
    var razorpayObj :RazorpayCheckout? = nil
    var selected: Int = -1
    var objSub:SubcriptionModel?
    var callBackofSeepreviewofSub:(()->())?
    var didSelection: ((Int)->())?
    var selectedAmount = ""
    var orderId = ""
    var callBAck:((_ obj :TransactionModel?)->())?
    var subCriptionType = Int()
    var objTransaction: TransactionModel?
    var viaHome = false
    @IBOutlet weak var subsTbl: UITableView!
    @IBOutlet weak var subcTap: UIButton!
    @IBOutlet weak var seePreviewBtn: UIButton!
    @IBOutlet weak var subcbtn: UIButton!
    
     override func viewDidLoad() {
        super.viewDidLoad()
        self.subsTbl.delegate = self
        self.subsTbl.dataSource = self
        self.seePreviewBtn.setTitleColor(UIColor.white, for: .normal)
        razorpayObj = RazorpayCheckout.initWithKey("rzp_test_5lgCveSCx6BtnX", andDelegate: self)
//        var razorpayKey = "rzp_test_5lgCveSCx6BtnX"
//        let razorpayKey = "rzp_test_5lgCveSCx6BtnX"
        self.subcriptionapi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    subcTap.isUserInteractionEnabled  = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
    subcTap.isUserInteractionEnabled  = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    subcTap.isUserInteractionEnabled  = true
        
    }
    
    override func viewWillLayoutSubviews() {
    self.subcTap.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    @IBAction func subscribeBtn(_ sender: UIButton) {

        if selected == -1
        {
            isServiceHit = false
            sender.isUserInteractionEnabled = true
            createAlert(title:AppName, message: "please select plan")
        }
        else
        {
            if viaHome{
            self.dismiss(animated: false, completion: nil)
            }
            isServiceHit = true
            sender.isUserInteractionEnabled = false
            
            let when = DispatchTime.now() + 1
                    DispatchQueue.main.asyncAfter(deadline: when, execute: { () -> Void in
                self.paymentApi(amount:self.selectedAmount)
        
    })
                                                  }
       
                                                  }
    
    @IBAction func seepreview(_ sender: UIButton) {
    self.dismiss(animated: true, completion: {
    self.callBackofSeepreviewofSub?()
                })
    }
    
    @IBAction func dismissBtn(_sender:UIButton)
    {
    dismiss(animated: false, completion: nil)
    }
    func createOrderID(){
    }
}

extension SubsCriptionPlane: RazorpayPaymentCompletionProtocol {
    func onPaymentError(_ code: Int32, description str: String) {
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        kUserDefaults.bool(forKey:"isSubscribed")
    }
    
    func onPaymentSuccess(_ payment_id: String) {
        print("success: Payment save", payment_id)
        UserDefaults.standard.set(payment_id, forKey: "PaymentID")
        self.transactionApi(paymentId: payment_id)
//        self.dismiss(animated: false, completion: nil)
//        let alertView = UIAlertController(title: title, message: "Payment Succeeded", preferredStyle: .alert)
//        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
//        self.navigationController?.popToRootViewController(animated: false)
//        })
//        alertView.addAction(action)
//        self.present(alertView, animated: true, completion: nil)
    }
    }

extension SubsCriptionPlane: RazorpayPaymentCompletionProtocolWithData {
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", code)
        self.presentAlert(withTitle: "Alert", message: str)
        kUserDefaults.bool(forKey:"isSubscribed")
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", payment_id)
        print(response)
        self.presentAlert(withTitle: "Success", message: "Payment Succeeded")
    }
}

extension PayNowVc : SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
    controller.dismiss(animated: true, completion: nil)
    }
}

extension PayNowVc {
    func presentAlert(withTitle title: String?, message : String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Okay", style: .default)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

//MARK:  TableView delegate and DataSource
extension SubsCriptionPlane: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  objSub?.data.subscriptionList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.subsTbl.dequeueReusableCell(withIdentifier: "SubCriptionCell", for: indexPath) as! SubCriptionCell
        cell.selectionStyle = .none
        cell.subAmount.text =  "Rs" + " " + (objSub?.data.subscriptionList[indexPath.row].price)!
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
        cell.selectionStyle = .none
        
        // MARK: SUBCRIPTION MONTH-
        if  let sub = objSub?.data.subscriptionList[indexPath.row].subscriptionType
        {
          cell.monthplane.text =   String(sub) +  " " + "Month"
          cell.monthplane.textColor = .white
        }
        else
        {
        }
        if selected == indexPath.row
        {
            cell.subCriptionView.layer.borderWidth = 1
            cell.subCriptionView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.subAmount.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.IconImg.image = UIImage(named: "selectedplane")
            self.selectedAmount = (objSub?.data.subscriptionList[indexPath.row].price)!
            self.subCriptionType = (objSub?.data.subscriptionList[indexPath.row].subscriptionType)!
        }
        
        else
        {
            cell.subCriptionView.layer.borderWidth = 1
            cell.subCriptionView.layer.borderColor = #colorLiteral(red: 0.1461495757, green: 0.1526317596, blue: 0.183000803, alpha: 1)
            cell.subAmount.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.IconImg.image = UIImage(named: "unselectedplane")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = indexPath.row
        self.subcTap.isUserInteractionEnabled = true
        subsTbl.reloadData()
    }
}

//MARK: APISubcriptionlist
extension SubsCriptionPlane
{
    func subcriptionapi()
    {
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.subscriptionPlane.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: SubcriptionModel.self)
        
        { (status, json, model, error) in
            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                self.objSub = model
                print(model)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.subsTbl.reloadData()
                }
            }
            else
            {
            self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}

//MARK: API generate orderID -
extension SubsCriptionPlane{
    func paymentApi(amount:String){
        var param: [String: Any] = [:]
        param["amount"] =  amount
        //        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.payment.url,
                                        headers:CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable:PaymentMaodel.self
        ) { (status, json, model, error) in
            //            KRProgressHUD.dismiss()
            self.objPayment = model
            self.orderId = self.objPayment?.data?.orderId?.id ?? ""
            UserDefaults.standard.set(self.orderId, forKey: "OrderID")
            if self.isServiceHit{
            self.openRazorpayCheckout(order_id: self.objPayment?.data?.orderId?.id ?? "")
              
            }
        }
    }
    
    private func openRazorpayCheckout(order_id: String) {
        let amount  = Int(self.selectedAmount)!
                let amount_get  = (amount * 100)
                let options: [String:Any] = [
                    "amount": amount_get,
                    "currency": "INR",//We support more that 92 international currencies.
                    "description": "purchase description",
                    "order_id": [
                    "order id": order_id
                    ],
                    //"image": "https://url-to-image.png",
                    "name": "byteEdge",
                    "international": true,
                    "prefill": [
                    "contact": "132435467890",
                    "email": "pk62654@gmail.com"
                    ],
                    "theme": [
                    "color": "#1F69E0"
                    ],
                ]
         if let rzp = self.razorpayObj {
             rzp.open(options)
         } else {
            print("Unable to initialize")
        }
        
    }
    
}

//MARK: transactionsave Api -
extension SubsCriptionPlane{
    func transactionApi(paymentId:String){
        var param: [String: Any] = [:]
        param["orderId"] = self.orderId
        param["amount"] = self.selectedAmount
        param["subscriptionType"] = self.subCriptionType
        param["paymentId"] = paymentId
        param["paymentType"] = "UPI"
       
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.transactionpaymentSave.url,
                                        headers:CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable:TransactionModel.self
        ) { (status, json, model, error) in
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            self.objTransaction = model
            if status == 200
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "success"), object: nil)
            let isSubscribed  = kUserDefaults.set(true, forKey: "isSubscribed")
            print(UserDefaults.standard.value(forKey: "isSubscribed"))
            self.callBAck?(self.objTransaction)
            }
            else
            {
            self.createAlert(title: AppName, message: message ?? "")
            }
        }
    }
}

//MARK: ALert
extension SubsCriptionPlane {
    func presentAlert(withTitle title: String?, message : String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Okay", style: .default)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
