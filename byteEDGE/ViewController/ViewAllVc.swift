
import UIKit
import IBAnimatable
import Alamofire
import KRProgressHUD
import SwiftUI
import  SDWebImage

class ViewAllVc: UIViewController,JoinDelegateofViewAll {
    //MARK: - IBOutlets
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var cuisineCollectionView: UICollectionView!
    @IBOutlet weak var thirdImg: UIImageView!
    @IBOutlet weak var secondImg: UIImageView!
    @IBOutlet weak var firstImg: UIImageView!
    @IBOutlet weak var imgStackview: UIStackView!
    
    @IBOutlet weak var lbblHeading: UILabel!
    
    var typeofViewAll = Int()
    var objViewAll: ViewAll?
    var _id = String()
    var joinSatatus = false
    var currentPage : Int = 0
    var isLoadingList : Bool = false
    var pageCount = 1
    var limit = 0
    
    //MARK: - Variables
    var filterdata:[String]!
    var temp = Int()
    var length = Int()
    let columnLayout = CustomViewFlowLayout()
//    let callBackID:((typeofViewAll:Int,ID)->())?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        lbblHeading.setAppFontColor(.appColor(.white), font: .ProductSans(.bold, size: .oneSix))
        if typeofViewAll == 2{
            lbblHeading.text = "Groups you may like"
        }else if typeofViewAll == 3{
            lbblHeading.text = "Most Popular"
            
        }else{
            lbblHeading.isHidden = true
        }
        self.viewAllApi(groupType:typeofViewAll, page: pageCount, isAppend: false)
        self.cuisineCollectionView.collectionViewLayout = columnLayout
        setup()
    }
//    override func viewWillAppear(_ animated: Bool) {
//    self.viewAllApi(groupType:typeofViewAll, page: pageCount, isAppend: false)
//    }
    
    //MARK: - IBAction
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func joinIndex(index: Int, type: Int) {
    if typeofViewAll == 1
    {
     self.joinApiofViewAll(type:1)
      }
        else if typeofViewAll == 2
        {
        self._id = objViewAll?.data?.mayLikeGroup?[index]._id ?? ""
        self.joinApiofViewAll(type:2)
            
        }
        else
        {
       print("Populgroup")
        self._id = objViewAll?.data?.popularGroup?[index]._id ?? ""
        self.joinApiofViewAll(type:3)
        }
      
    }
}

//MARK: - Extension SearchCuisineVC
extension ViewAllVc{
    private func setup(){
        cuisineCollectionView.delegate = self
        cuisineCollectionView.dataSource = self
       
    }
}

//MARK: - Extension UICollectionViewDataSource
extension ViewAllVc: UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if typeofViewAll == 1
        {
        return objViewAll?.data?.myGroup?.count ?? 0
        }
        else if typeofViewAll == 2 {
        return objViewAll?.data?.mayLikeGroup?.count ?? 0
        }
        else if typeofViewAll == 3 {
        return objViewAll?.data?.popularGroup?.count ?? 0
        }
        else
        {
        return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CuisineCell", for: indexPath) as! CuisineCell
        
        cell.firstImg.layer.masksToBounds = true
        cell.firstImg.layer.cornerRadius = cell.firstImg.frame.size.height/2;
        cell.firstImg.layer.masksToBounds = true
        cell.firstImg.layer.borderWidth=1;

        cell.secondImg.layer.masksToBounds = true
        cell.secondImg.layer.cornerRadius = cell.secondImg.frame.size.height/2;
        cell.secondImg.layer.masksToBounds = true
        cell.secondImg.layer.borderWidth=1;

        cell.thirdImg.layer.masksToBounds = true
        cell.thirdImg.layer.cornerRadius = cell.thirdImg.frame.size.height/2;
        cell.thirdImg.layer.masksToBounds = true
        cell.thirdImg.layer.borderWidth=1;

        if typeofViewAll == 1 //Mygroup
        {
            cell.titleLabel.text = objViewAll?.data?.myGroup?[indexPath.row].name
            var urlImg = objViewAll?.data?.myGroup?[indexPath.row].image ?? ""
            cell.titleImage.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: "logobyte"), options: .highPriority, context: nil)
            cell.joinTap.isHidden = false
            cell.joinTap.isUserInteractionEnabled = false
            cell.joinTap.backgroundColor = .black
            cell.joinTap.setTitle("member", for:.normal)
            cell.joinTap.tintColor = .white
    
            if objViewAll?.data?.myGroup?[indexPath.row].memberDetails?.count == 1
            {
            var urlImage1 = objViewAll?.data?.myGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
            cell.firstImg.sd_setImage(with: URL(string: urlImage1 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
            cell.secondImg.isHidden = true
            cell.thirdImg.isHidden = true
            cell.firstImg.isHidden = false
                
            }
            else if objViewAll?.data?.myGroup?[indexPath.row].memberDetails?.count == 2
            {
            var urlImage3 = objViewAll?.data?.myGroup?[indexPath.row].memberDetails?[1].memberImage ?? ""
            cell.secondImg.sd_setImage(with: URL(string: urlImage3 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
            cell.firstImg.isHidden = false
            cell.secondImg.isHidden = false
            cell.thirdImg.isHidden = true
            }
            else if  objViewAll?.data?.myGroup?[indexPath.row].memberDetails?.count == 3
            {
                var urlImage3 = objViewAll?.data?.myGroup?[indexPath.row].memberDetails?[2].memberImage ?? ""
                cell.thirdImg.sd_setImage(with: URL(string: urlImage3 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                cell.firstImg.isHidden = true
                cell.secondImg.isHidden = true
                cell.thirdImg.isHidden = true
            }
            else
            {
                
            }
        }
        
        else if typeofViewAll == 2 //Mylike
        {
            cell.titleLabel.text = objViewAll?.data?.mayLikeGroup?[indexPath.row].name
            cell.joinTap.isHidden = false
            cell.typeOfviewAll = typeofViewAll
            cell.delegateofViewAll  = self
            cell.joinTap.tag  = indexPath.row
            var urlImg = objViewAll?.data?.mayLikeGroup?[indexPath.row].image ?? ""
            cell.titleImage.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: "logobyte"), options: .highPriority, context: nil)
            
            
            if objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?.count == 1
            {
                var urlImage = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                cell.firstImg.isHidden = false
                cell.secondImg.isHidden = true
                cell.thirdImg.isHidden = true
                
            }
            else if objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?.count == 2
            {
                
                var urlImage = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                
            var urlImage1 = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[1].memberImage ?? ""
            cell.secondImg.sd_setImage(with: URL(string: urlImage1 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
            cell.firstImg.isHidden = false
            cell.secondImg.isHidden = false
            cell.thirdImg.isHidden = true
            }
            
            else if  objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?.count == 3
            {
                
                var urlImage = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                
            var urlImage1 = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[1].memberImage ?? ""
            cell.secondImg.sd_setImage(with: URL(string: urlImage1 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                
                var urlImage3 = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[2].memberImage ?? ""
                cell.thirdImg.sd_setImage(with: URL(string: urlImage3 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                cell.thirdImg.sd_setImage(with: URL(string: urlImage3 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                cell.firstImg.isHidden = false
                cell.secondImg.isHidden = false
                cell.thirdImg.isHidden = false
            }
            
             else if /objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?.count > 3
              {
                cell.firstImg.isHidden = false
                cell.secondImg.isHidden = false
                cell.thirdImg.isHidden = false
                 
                var urlImage = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:nil)
                                            
                                            
                var urlImage1 = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[1].memberImage ?? ""
                cell.secondImg.sd_setImage(with: URL(string: urlImage1 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:  nil)
                
                var urlImage2 = objViewAll?.data?.mayLikeGroup?[indexPath.row].memberDetails?[2].memberImage ?? ""
                cell.thirdImg.sd_setImage(with: URL(string: urlImage2 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:  nil)
               }
            
            //MARK: join modification
              if objViewAll?.data?.mayLikeGroup?[indexPath.row].isJoin == false  &&
                    objViewAll?.data?.mayLikeGroup?[indexPath.row].joinStatus == "pending"
                     {
                         cell.joinTap.isHidden = false
                         cell.joinTap.isUserInteractionEnabled = false
                         cell.joinTap.setTitle("Pending", for: .normal)
                     
                     }
                     else if objViewAll?.data?.mayLikeGroup?[indexPath.row].isJoin == false  &&
                                objViewAll?.data?.mayLikeGroup?[indexPath.row].joinStatus == ""
                     {
                         
                         cell.joinTap.isHidden = false
                         cell.joinTap.isUserInteractionEnabled = true
                         cell.joinTap.setTitle("Join", for: .normal)
                         }
                     
                     
            else if objViewAll?.data?.mayLikeGroup?[indexPath.row].isJoin ==  true  &&
                    objViewAll?.data?.mayLikeGroup?[indexPath.row].joinStatus == ""
                     {
                         cell.joinTap.isHidden = true
                         cell.joinTap.isUserInteractionEnabled = true
                     }
                    else{}
        
               }
        
         else if typeofViewAll == 3 //popular
         {
            cell.titleLabel.text = objViewAll?.data?.popularGroup?[indexPath.row].name
            cell.joinTap.isHidden = false
            cell.typeOfviewAll = typeofViewAll
            cell.delegateofViewAll  = self
            cell.joinTap.tag  = indexPath.row
            cell.titleLabel.text = objViewAll?.data?.popularGroup?[indexPath.row].name ?? ""
            var urlImg = objViewAll?.data?.popularGroup?[indexPath.row].image ?? ""
            cell.titleImage.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: "logobyte"), options: .highPriority, context: nil)
            
            if objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?.count == 1
            {
                var urlImage = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                cell.secondImg.isHidden = true
                cell.thirdImg.isHidden = true
                cell.firstImg.isHidden = false
                
            }
            else if objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?.count == 2
            {
           
                var urlImage1 = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage1 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                
                var urlImage2 = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[1].memberImage ?? ""
                cell.secondImg.sd_setImage(with: URL(string: urlImage2 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                
                cell.secondImg.isHidden = false
                cell.thirdImg.isHidden = true
                cell.firstImg.isHidden = false
                
            }
            
            else if objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?.count == 3
            {
                cell.secondImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.firstImg.isHidden = false
                
                var urlImage3 = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[2].memberImage ?? ""
                cell.thirdImg.sd_setImage(with: URL(string: urlImage3 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context: nil)
                
                
                
                var urlImage = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:nil)
                                            
                                            
                var urlImage1 = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[1].memberImage ?? ""
                cell.secondImg.sd_setImage(with: URL(string: urlImage1 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:  nil)
            }
            
            else if /objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?.count > 3
            {
                cell.secondImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.firstImg.isHidden = false
                
                var urlImage = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[0].memberImage ?? ""
                cell.firstImg.sd_setImage(with: URL(string: urlImage ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:nil)
                                            
                                            
                var urlImage1 = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[1].memberImage ?? ""
                cell.secondImg.sd_setImage(with: URL(string: urlImage1 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:  nil)
                
                var urlImage2 = objViewAll?.data?.popularGroup?[indexPath.row].memberDetails?[2].memberImage ?? ""
                cell.thirdImg.sd_setImage(with: URL(string: urlImage2 ), placeholderImage:UIImage(named:"grpIcon"),options: .highPriority, context:  nil)
            
                
            }
                        
            
            //MARK: JOIN MODIFICATION :
            if objViewAll?.data?.popularGroup?[indexPath.row].isJoin == false  &&
                  objViewAll?.data?.popularGroup?[indexPath.row].joinStatus == "pending"
                   {
                
                        cell.joinTap.isHidden = false
                        cell.joinTap.tag  = indexPath.row
                       cell.joinTap.isUserInteractionEnabled = false
                        cell.joinTap.setTitle("Pending", for: .normal)
                   
                   }
                   else if objViewAll?.data?.popularGroup?[indexPath.row].isJoin == false  &&
                              objViewAll?.data?.popularGroup?[indexPath.row].joinStatus == ""
                   {
                       
                       cell.joinTap.isHidden = false
                       cell.joinTap.isUserInteractionEnabled = true
                       cell.joinTap.setTitle("Join", for: .normal)
                       }
                   
                   
          else if objViewAll?.data?.popularGroup?[indexPath.row].isJoin ==  true  &&
                  objViewAll?.data?.popularGroup?[indexPath.row].joinStatus == ""
                   {
                       cell.joinTap.isHidden = true
                       cell.joinTap.isUserInteractionEnabled = true
                   }
                  else{}
        }
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           let offset = scrollView.contentOffset.y
           let contenHeight = scrollView.contentSize.height
        if offset > contenHeight - scrollView.frame.height - scrollView.frame.height && objViewAll?.data?.count ?? 0 > objViewAll?.data?.myGroup?.count ?? 0 {
               if !isLoadingList{
                   isLoadingList = true
                   pageCount = pageCount + 1
                   self.viewAllApi(groupType: self.typeofViewAll , page:pageCount, isAppend: true)
               }
           }
        
         if offset > contenHeight - scrollView.frame.height - scrollView.frame.height && objViewAll?.data?.count ?? 0 > objViewAll?.data?.mayLikeGroup?.count ?? 0 {
               if !isLoadingList{
                   isLoadingList = true
                   pageCount = pageCount + 1
                   self.viewAllApi(groupType: self.typeofViewAll , page:pageCount, isAppend: true)
                   self.cuisineCollectionView.reloadData()
               }
           }
        
           else if offset > contenHeight - scrollView.frame.height - scrollView.frame.height && objViewAll?.data?.count ?? 0 > objViewAll?.data?.popularGroup?.count ?? 0 {
               if !isLoadingList{
                   isLoadingList = true
                   pageCount = pageCount + 1
                   self.viewAllApi(groupType: self.typeofViewAll , page:pageCount, isAppend: true)
               }
             }
        else
        {}
       }
}

//MARK: - Extension UICollectionViewDelegateFlowLayout
extension ViewAllVc: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
         if UIDevice.current.userInterfaceIdiom == .pad
        {
        let size = (collectionView.frame.size.width - 20) / 2
             return CGSize(width: size, height: 300)
         }
        else{
        let size = (collectionView.frame.size.width - 20) / 2
            return CGSize(width: size, height:collectionView.frame.size.height/2.8)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if typeofViewAll == 1
        {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "EditGroupDetails") as! EditGroupDetails
        vc._id = objViewAll?.data?.myGroup?[indexPath.row]._id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else if typeofViewAll == 2
        {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "JoinViewController") as! JoinViewController
        vc._id = objViewAll?.data?.mayLikeGroup?[indexPath.row]._id ?? ""
         self.navigationController?.pushViewController(vc, animated: true)
        }
        else if typeofViewAll == 3
        {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "JoinViewController") as! JoinViewController
         vc._id = objViewAll?.data?.popularGroup?[indexPath.row]._id ?? ""
         self.navigationController?.pushViewController(vc, animated: true)
        }
//        self.cuisineCollectionView.reloadData()
    }
}

//MARK: API OF ViewAll

extension ViewAllVc
{
    func viewAllApi(groupType:Int?,page:Int?,isAppend: Bool?)
    {
        var param: [String: Any] = [:]
        param["groupType"] = groupType
        param["page"] = page
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.ViewAllApi.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: ViewAll.self,
                                        encoding: URLEncoding.queryString)
        
        {(status, json, model, error) in
          
            if model?.status == 200
            {
                
                self.isLoadingList = false
                if isAppend == true{
                    if model?.data?.myGroup?.count ?? 0 > 0{
                        for i in 0...(model?.data?.myGroup?.count ?? 0) - 1{
                        self.objViewAll?.data?.myGroup?.append((model?.data?.myGroup?[i])!)
                        }
                    }
                    
                    
                     else if model?.data?.mayLikeGroup?.count ?? 0 > 0{
                        for i in 0...(model?.data?.mayLikeGroup?.count ?? 0) - 1{
                        self.objViewAll?.data?.mayLikeGroup?.append((model?.data?.mayLikeGroup?[i])!)
                        }
                        
                    }
                    
                    else if model?.data?.popularGroup?.count ?? 0 > 0{
                       for i in 0...(model?.data?.popularGroup?.count ?? 0) - 1{
                           self.objViewAll?.data?.popularGroup?.append((model?.data?.popularGroup?[i])!)
                       }
                   }

                }
                
                else{
                self.objViewAll = model
                }
                DispatchQueue.main.async {
                self.cuisineCollectionView.reloadData()
                }
            }
        }
    }
}


extension ViewAllVc
{
    func joinApiofViewAll(type:Int)
    {
        var param: [String: Any] = [:]
        param["groupId"] = self._id
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.joinGroup.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: JoinMember.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
              self.createAlertCallback(title: AppName, message:message ?? "" , cancelTitle: "Cancel") { (value)in
                    if value == true
                    {
                    self.cuisineCollectionView.reloadData()
                    }
                    
                    else
                    {
                    print("Nothing")
                    }
                }
                DispatchQueue.main.async {
                self.viewAllApi(groupType: self.typeofViewAll, page: self.pageCount, isAppend: false)
                self.cuisineCollectionView.reloadData()
                    
                }
            }
            else
            {
                self.createAlertCallback(title: AppName, message:message ?? "" , cancelTitle: "Cancel") { (value) in
                    if value == true
                    {
//                    self.grouplistApi()
                    self.cuisineCollectionView.reloadData()
                    }
                    else
                    {
                        print("Nothing")
                    }
                }
            }
        }
    }
}

