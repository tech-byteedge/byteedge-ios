
//  LessonOverviewViewController.swift
//  byteEDGE
//  Created by gaurav on 26/01/22.

import UIKit
import  Photos
import  Alamofire
import  AVFoundation
import KRProgressHUD

enum LessonOverViewState {
case none, lesson, overview
}

class LessonOverviewViewController: UIViewController {
    //MARK: -VARIABLES-
    var vcState: LessonOverViewState = .lesson
    var refreshControl:UIRefreshControl!
    
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lessonView: UIView!
    @IBOutlet weak var lessonLbl: UILabel!
    @IBOutlet weak var lessonDotImg: UIImageView!
    @IBOutlet weak var overviewView: UIView!
    @IBOutlet weak var overviewBtn: UIButton!
    @IBOutlet weak var overviewDotImg: UIImageView!
    @IBOutlet weak var overviewlbl: UILabel!
    @IBOutlet weak var lessonBtn: UIButton!
    
    var objModel : LearningListViewAll?
    var objtracCourse:CompleteCourseModel?
    var bookMark_ob: BookmarkVideo?
    var currentPage : Int = 0
    var isLoadingList : Bool = false
    var pageCount = 1
    var limit = 0
    var FavReels = Int()
    var  openquz = Bool()
    
    
    //MARK: -VIEW LIFE CYCLE-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiCall(role:1)
         self.setVc()
       
//        if self.FavReels == 1
//        {
//            self.favReels()
//            self.stackView.isHidden = true
////            self.overviewlbl.isHidden = true
////            self.lessonBtn.isHidden = true
//
//        }
//        else
//        {
//
//
//        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.apiCall(role:1)
//        refreshapage()
    }
    
    func refreshapage()
    {
        tableView.alwaysBounceVertical = true
        tableView.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc func didPullToRefresh() {
    self.apiCall(role:1)
    refreshControl?.endRefreshing()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnBackBtn(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedOnLessonBtn(_ sender: Any) {
        setLabelFont(selectedBtn: lessonLbl, unselectedBtn: overviewlbl)
        self.apiCall(role:1)
        lessonDotImg.isHidden = false
        overviewDotImg.isHidden = true
        setViewBackground(selectedView: lessonView, unselectedview: overviewView)
        vcState = .lesson
        DispatchQueue.main.async {
            self.tableView.reloadData()
       }
     }
    
     @IBAction func tappedOnMonthBtn(_ sender: Any) {
        setLabelFont(selectedBtn: overviewlbl, unselectedBtn: lessonLbl)
        lessonDotImg.isHidden = true
        overviewDotImg.isHidden = false
        self.comppletedCourseApi()
        setViewBackground(selectedView: overviewView, unselectedview: lessonView)
        vcState = .overview
        DispatchQueue.main.async {
        self.tableView.reloadData()
//       self.tableView.reloadData()
          }
    }
    
    //MARK: -FUNCTIONS-
    func setVc() {
        view.backgroundColor = .black
        ///register of tableView cell
        tableView.registerCell(type: LessonTableViewCell.self)
        tableView.registerCell(type: OverviewTableViewCell.self)
        ///connector to tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        
        setLabelFont(selectedBtn: lessonLbl, unselectedBtn: overviewlbl)
        setViewBackground(selectedView: lessonView, unselectedview: overviewView)
        lessonLbl.text = "Course"
        overviewlbl.text = "Completed"
        
        lessonDotImg.isHidden = false
        overviewDotImg.isHidden = true
        lessonView.layer.borderWidth = 1
        lessonView.layer.borderColor = UIColor.appColor(.blackLight).cgColor
        overviewView.layer.borderWidth = 1
        overviewView.layer.borderColor = UIColor.appColor(.blackLight).cgColor
       
    }
    
    ///FUNCTION TO MANAGE SEGMENT BTN
    private func setLabelFont(selectedBtn: UILabel, unselectedBtn: UILabel) {
        selectedBtn.setAppFontColor(.appColor(.white), font: .ProductSans(.regular, size: .oneSeven))
        unselectedBtn.setAppFontColor(.appColor(.white, alpha: 0.64), font: .ProductSans(.regular, size: .oneSeven))
    }
    private func setViewBackground(selectedView: UIView, unselectedview: UIView) {
        selectedView.backgroundColor = .appColor(.blackLight)
        unselectedview.backgroundColor = .black
    }
}

//MARK: -TABLEVIEW DATA SOURCE-
extension LessonOverviewViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if FavReels == 1
//        {
//         self.bookMark_ob?.data?.bookmark?.count ?? 0
//        }
//        else
//        {
        if vcState == .overview
        {
          if self.objtracCourse?.data?.complete.count == 0
            {
             self.tableView.setEmptyMessageshow()
            }
            else
            {
              self.tableView.restoredata()
            }
        }
        return vcState == .lesson ? self.objModel?.data?.yourCourse?.count ?? 0 : self.objtracCourse?.data?.complete.count ?? 0
        }
//        return self.bookMark_ob?.data?.bookmark?.count ?? 0
        
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//       /if FavReels == 1
//        {
//           let cell = tableView.dequeueCell(withType: LessonTableViewCell.self, for: indexPath) as! LessonTableViewCell
//           cell.videoDescLbl.text =  self.bookMark_ob?.data?.bookmark?[indexPath.row].courseData?.Description
//           cell.videoTitleLbl.text = self.bookMark_ob?.data?.bookmark?[indexPath.row].courseData?.courseName
////          cell.videoTimeLbl.text = CommonUtility.instance.getDateTIMEFromString(inputDate:objModel?.data?.yourCourse?[indexPath.row].createdAt ?? "")
//           var urlImg = /bookMark_ob?.data?.bookmark?[indexPath.row].courseData?.thumbnails
//           cell.videoPreviewImg.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
//            cell.downloadBtnAction =
//           {
//           self.createAlert(title: AppName, message: "UnderDevelopment")
//           }
//           return cell
//
//        }
//        else
//        {
           if vcState == .lesson {
             let cell = tableView.dequeueCell(withType: LessonTableViewCell.self, for: indexPath) as! LessonTableViewCell
            cell.videoDescLbl.text =  self.objModel?.data?.yourCourse?[indexPath.row].Description
            cell.videoTitleLbl.text = self.objModel?.data?.yourCourse?[indexPath.row].courseName
            cell.videoTimeLbl.text = CommonUtility.instance.getDateTIMEFromString(inputDate:objModel?.data?.yourCourse?[indexPath.row].createdAt ?? "")
            
//            CommonUtility.instance.getDateTIMEFromString(inputDate: objTranscation?.data.orderDetail.createdAt ?? "" )
            
            var urlImg = /objModel?.data?.yourCourse?[indexPath.row].thumbnails
            cell.videoPreviewImg.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
              cell.downloadBtnAction =
             {
             self.createAlert(title: AppName, message: "UnderDevelopment")
             }
            return cell
            }
        
           else{
            let cell = tableView.dequeueCell(withType: OverviewTableViewCell.self, for: indexPath) as! OverviewTableViewCell
            cell.titleLbl.text = self.objtracCourse?.data?.complete[indexPath.row].courseDetail?.courseName
            cell.percentageLbl.text =  "\(/self.objtracCourse?.data?.complete[indexPath.row].percentage)" + "" + "%"
            cell.lessonDescLbl.text = self.objtracCourse?.data?.complete[indexPath.row].courseDetail?.Description
              
            var urlImg = /objtracCourse?.data?.complete[indexPath.row].courseDetail?.thumbnails
            cell.projectImg.sd_setImage(with: URL(string: urlImg ), placeholderImage: UIImage(named: ""), options: .highPriority, context:nil)
            return cell
            }
          
      }
    
        
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            switch vcState {
            case .overview:
                let vc  = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as!  CourseDetailshowVc
                vc.playlistID = self.objtracCourse?.data?.complete[indexPath.row].courseDetail?.playlistId ?? ""
                vc.isQuizExist = self.objtracCourse?.data?.complete[indexPath.row].openQuiz ?? false
                vc.courseUuid = self.objtracCourse?.data?.complete[indexPath.row].courseDetail?.courseUuid  ?? ""
                vc.courseId = self.objtracCourse?.data?.complete[indexPath.row].courseDetail?._id  ?? ""
              //  vc.openQuiz = self.objtracCourse?.data?.complete[indexPath.row].openQuiz ?? false
                vc.viewCompleted = true
                self.navigationController?.pushViewController(vc, animated: false)
                break
                default:
                let vc  = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as!  CourseDetailshowVc
                 vc.playlistID = self.objModel?.data?.yourCourse?[indexPath.row].playlistId ?? ""
                 vc.courseId = self.objModel?.data?.yourCourse?[indexPath.row]._id ?? ""
//                 vc.courseUuid = self.objtracCourse?.data?.complete[indexPath.row].courseDetail?.courseUuid  ?? ""
                self.navigationController?.pushViewController(vc, animated: false)
                break
            }
            
//            let vc  = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as!  CourseDetailshowVc
//            vc.playlistID = self.objModel?.data?.yourCourse?[indexPath.row].playlistId ?? ""
//            self.navigationController?.pushViewController(vc, animated: false)
            self.tableView.reloadData()
      }
}

//MARK: Api implemented
extension LessonOverviewViewController
{
 func  apiCall(role:Int?)
    {
        var param: [String: Any] = [:]
        param["role"] = role
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.learningViewAll.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: LearningListViewAll.self,
                                        encoding: URLEncoding.queryString)
        
        {(status, json, model, error) in
            if model?.status == 200
            {
            self.objModel = model
                print(json)
            DispatchQueue.main.async {
            self.tableView.reloadData()
            }
            }
            else
            {
            self.createAlert(title: AppName, message: /model?.message)
            }
            
            }
        }
    }

extension LessonOverviewViewController
{
    func comppletedCourseApi()
    {
//        CustomLoader.sharedInstance.startAnimation()
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.ComppleteCourse.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: CompleteCourseModel.self,
                                        encoding: URLEncoding.default
        )
        { (status, json, model, error) in
            KRProgressHUD.dismiss()
            CustomLoader.sharedInstance.stopAnimation()
            if model?.status == 200
            {
               self.objtracCourse = model
                print(model)
                DispatchQueue.main.async {
                self.tableView.reloadData()
                }
            }
            else
            {
           // self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }

}

struct CommonRes: Codable
{
     let  Code: Int?
     let status: Int
     let message: String?
     let data: DataUpdate?
}

struct DataUpdate:Codable
{
}
