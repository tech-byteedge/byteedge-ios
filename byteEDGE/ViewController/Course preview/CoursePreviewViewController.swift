
import UIKit
import AVKit
import AVFoundation
import JWPlayerKit
import Alamofire
import  CoreData
import  Cosmos
import SDWebImage

class CoursePreviewViewController: UIViewController
{
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var vcTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    var viaHome = false
    //MARK: -VARIABLES-
    var playerViewController: AVPlayerViewController!
    var coursePreviewDetailData : CoursePreviewDetailData?
    var courseId : String = ""
    var categoryId : String = ""
    var mediaID = ""
    var videoUrlString = ""
    var managedObjectContext: NSManagedObjectContext?
    var player_ob = Jwplayer()
    
    
    //MARK: -VIEW LIFE CYCLE-
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,selector:#selector(success),name: NSNotification.Name("success"),object: nil)
                                               
       
        setVc()
        if viaHome{
        redirect()
        }
        coursePreviewDetail()
    }
    
    @objc func success(note: NSNotification){
        print("pawan")
        
    }
    
    
      func redirect(){
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier:"SubsCriptionPlane") as! SubsCriptionPlane
          vc.viaHome = self.viaHome
//          kUserDefaults.set(false, forKey: "isSubscribed")
           vc.callBackofSeepreviewofSub = {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
             vc.courseId = self.courseId
             vc.mediaID  = self.mediaID
            self.navigationController?.pushViewController(vc, animated: false)
        }
        vc.callBAck = {
            objTransactionModel in
            vc.viaHome = self.viaHome
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
            vc.objTranscation = objTransactionModel
            self.navigationController?.pushViewController(vc, animated: false)
        }
          
        vc.modalPresentationStyle = .fullScreen
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2
        )
        self.present(vc, animated: true, completion: nil)
       
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnBackBtn(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    //MARK: -JW player Integration-
    
    
    //MARK: -FUNCTIONS-
        func setVc() {
            
           
        view.backgroundColor = .black
        ///register of tableView cell
        tableView.registerCell(type: AboutCourseTableViewCell.self)
        tableView.registerCell(type: CourseNameAndRatingsTableViewCell.self)
        //connector to tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        ///set vc title
        vcTitleLbl.text = "Course Preview"
        videoView.backgroundColor = .white
    }
}

//MARK: -TABLEVIEW DELEGATE-
extension CoursePreviewViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print(indexPath)
    }
}

//MARK: -TABLEVIEW DATA SOURCE-
extension CoursePreviewViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            
            
            
            guard let cell = tableView.dequeueCell(withType: CourseNameAndRatingsTableViewCell.self, for: indexPath) as? CourseNameAndRatingsTableViewCell else {
             fatalError("CourseNameAndRatingsTableViewCell is not initialize...")
            }
            
            cell.shareBtn.isHidden = true
            
//            cell.callback =
//            {
//                let text = "This is some text that I want to share."
//                let someText:String = "Hello want to share text also"
//                let objectsToShare:URL = URL(string: "https://byteedgetest2.page.link")!
//                let textToShare = [ text,objectsToShare ] as [Any]
//                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
//                activityViewController.popoverPresentationController?.sourceView = self.view
//                // so that iPads won't crash
//                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
//                self.present(activityViewController, animated: true, completion: nil)
//
//            }
            
            if let courseName = self.coursePreviewDetailData?.coursePreview?.courseName {
                cell.courseNameLbl.text = courseName
            }else {
            cell.courseNameLbl.text = ""
            }
            if let rating = self.coursePreviewDetailData?.coursePreview?.rating {
            cell.ratingLbl.text = String(rating)
            cell.ratingLbl.textColor = .black
            }else {
            cell.ratingLbl.text = ""
            }
            
            return cell
        }else {
            guard let cell = tableView.dequeueCell(withType: AboutCourseTableViewCell.self, for: indexPath) as? AboutCourseTableViewCell else {
                fatalError("AboutCourseTableViewCell is not initialize...")
            }
            if let desc = self.coursePreviewDetailData?.coursePreview?.Description{
                cell.aboutCourseLbl.text = desc
            }else {
                cell.aboutCourseLbl.text = ""
            }
            return cell
        }
    }

    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.backgroundColor = .clear
            view.textLabel!.backgroundColor = .clear
            view.textLabel!.textColor = .appColor(.white, alpha: 0.64)
            view.textLabel!.font = .ProductSans(.regular, size: .oneSeven)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? nil : "ABOUT COURSE"
    }
}

//MARK: -API-
extension CoursePreviewViewController {
    func coursePreviewDetail() {
        var param : [String: Any] = [:]
        param["courseId"] = self.courseId
        ServiceRequest.instance.coursePreviewDetail(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
                return
            }
            self.coursePreviewDetailData = response?.data
            print(self.coursePreviewDetailData)
            let playid = self.coursePreviewDetailData?.coursePreview?.previewId ?? ""
            self.player_ob.providesPresentationContextTransitionStyle = true
            self.player_ob.definesPresentationContext = true
            self.player_ob.mediaID = playid
            self.addChild(self.player_ob)
            self.player_ob.view.frame = self.videoView.bounds
            self.videoView.addSubview(self.player_ob.view)
            self.player_ob.setUpPlayer(isautostart: true, volume: 5.0)
            DispatchQueue.main.async {
            self.tableView.reloadData()
            }
        }
    }
}
    
    
    
    
    
