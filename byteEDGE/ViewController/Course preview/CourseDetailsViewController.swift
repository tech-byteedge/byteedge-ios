
//  CourseDetailsViewController.swift
//  byteEDGE
//  Created by gaurav on 31/01/22.


import UIKit
import SDWebImage
import KRProgressHUD
import Alamofire

class CourseDetailsViewController: UIViewController {
    //MARK: -VARIABLES-
    var categoryId = String()
    var categoryListData : CategoryListData?
    
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var vcTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seperatorView: UIView!
   
    //MARK: -VIEW LIFE CYCLE-
//     var courseId = ""
     var CourseDetailshowVc_ob = CourseDetailshowVc()
     var CoursePreviewvc_ob = CoursePreviewViewController()
      override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.8)
        setVc()
        categoryList()
    }
    
    //MARK: -ACTIONS-
    @IBAction func tappedOnBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: -FUNCTIONS-
        func setVc() {
        view.backgroundColor = .black
        ///register of tableView cell
        tableView.registerCell(type: CourseDetailsTableViewCell.self)
        ///connector to tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        vcTitleLbl.text = "Course Details"
    }
}
//MARK: -TABLEVIEW DELEGATE-
extension CourseDetailsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if kUserDefaults.bool(forKey: "isSubscribed") == true
        {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
//        print(self.dataOfTopSearch?.topSearch?[indexPath.row])
            vc.playlistID = self.categoryListData?.courseList?[indexPath.row].playlistId ?? ""
            let id  = self.categoryListData?.courseList?[indexPath.row]._id ?? ""
            vc.courseId = id ?? ""
         self.navigationController?.pushViewController(vc, animated: true)
//        print(indexPath.row)
        }

        else if kUserDefaults.bool(forKey: "isSubscribed") == false
        {
         let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
            CoursePreviewvc_ob.viaHome = true
          let id  = self.categoryListData?.courseList?[indexPath.row]._id ?? ""
            vc.courseId = id ?? ""
            vc.mediaID  = self.categoryListData?.courseList?[indexPath.row].previewId ?? ""
        self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
        print("Nothing to do")
        }
    }
}


//MARK: -TABLEVIEW DATA SOURCE-
extension CourseDetailsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if categoryListData?.courseList?.count  == 0
        {
        self.tableView.setEmptyMessageshownotification()
        }
        else
        {
         self.tableView.restoredata()
        }
        return self.categoryListData?.courseList?.count ?? 0
    }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueCell(withType: CourseDetailsTableViewCell.self, for: indexPath) as? CourseDetailsTableViewCell else {
                fatalError("CourseDetailsTableViewCell is not initialize...")
            }
            cell.selectionStyle = .none
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            DispatchQueue.main.async {
                if let image = self.categoryListData?.courseList?[indexPath.row].thumbnails, let url = URL(string: image) {
                cell.courseImg.sd_setImage(with: url, placeholderImage: UIImage(named: "logobyte") )
                    
                }else {
                cell.courseImg.image = UIImage(named: "logobyte")
                }
            }
            
            cell.courseBtnAction = {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
            if let courseId = self.categoryListData?.courseList?[indexPath.row]._id {
            vc.courseId = courseId
            }
            self.navigationController?.pushViewController(vc, animated: false)
        }
           
        if let courseName = self.categoryListData?.courseList?[indexPath.row].courseName {
         cell.courseNameLbl.text = courseName
        }else {
            cell.courseNameLbl.text = "Course Name"
        }
        return cell
    }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return UITableView.automaticDimension
    }
}

//MARK: -API-
   extension CourseDetailsViewController {
//       func categoryList(){
//           var param : [String: Any] = [:]
//           param["categoryId"] = self.categoryId
//
//           ServiceManager.instance.request(method: .get,
//                                           apiURL: Api.categoryList.url,
//                                           headers: CommonUtility.instance.headers,
//                                           parameters: param,
//                                           decodable: CategoryListModel.self)
//
//           { (status, json, model, error) in
//               print(model)
//               if model?.status == 200
//               {
//                   self.categoryListData = model?.data
//                   DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                   }
//               }
//               else
//               {
//               }
//           }
//       }
       
//   }

//    func categoryList() {
//        var param : [String: Any] = [:]
//        param["categoryId"] = self.categoryId
//        ServiceRequest.instance.categoryList(param) { (isSucess, response, error) in
//            guard response?.status == 200 else {
//                print(response?.status as Any)
//                return
//             }
//            self.createAlert(title: AppName, message: response?.message ?? "")
//            print(response?.message ?? "" )
//            self.categoryListData = response?.data
//
//            DispatchQueue.main.async {
//             self.tableView.reloadData()
//            }
//            }
//         }
       
       func categoryList()
       {
           var param : [String: Any] = [:]
           param["categoryId"] = self.categoryId
           KRProgressHUD.show()
           ServiceManager.instance.request(method: .get,
                                           apiURL: Api.categoryList.url,
                                           headers: CommonUtility.instance.headers,
                                           parameters: param,
                                           decodable: CategoryListModel.self,
                                           encoding: URLEncoding.default
           ) { (status, json, model, error) in
               print(model)
               KRProgressHUD.dismiss()
               if model?.status == 200
               {
                   self.categoryListData = model?.data
                   DispatchQueue.main.async {
                    self.tableView.reloadData()
                   }
               }
               else
               {
                
               }
               
           }
       }
       
      }
