//
//  PostDetailViewController.swift
//  byteEDGE
//  Created by gaurav on 04/02/22.

import UIKit
import SDWebImage
import AVFoundation
import AVKit
import SDWebImage
import UIKit
import  Alamofire
import CoreAudio
import KRProgressHUD
import  AVKit
import  AVFoundation
import GameKit
import IQKeyboardManagerSwift
import  SDWebImage
import  FirebaseDynamicLinks

class PostDetailViewController: UIViewController {
    //MARK: -VARIABLES-
    var postId = ""
    var grpID = ""
    var feedDetailData : PostDetailModelData?
    var obj_ModelFeed: FeedDataModel?
    let playerViewController = AVPlayerViewController()
    //MARK: -OUTLETS-
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var vcTitleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seperatorView: UIView!
    //MARK: -VIEW LIFE CYCLE-
    var image = ""
    var viaSplash = false
    override func viewDidLoad() {
        super.viewDidLoad()
        playerViewController.player?.automaticallyWaitsToMinimizeStalling = false
        setVc()
        postDetail()
    }
    
    //MARK: -ACTIONS-
    
    @IBAction func tappedOnBackBtn(_ sender: Any) {
        if viaSplash
        {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            vc.isDynamicLink = true
                    let nav_obj = UINavigationController(rootViewController: vc)
                    nav_obj.navigationBar.isHidden = true
                    UIApplication.shared.windows.first?.rootViewController = nav_obj
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
        else
        {
        self.navigationController?.popViewController(animated: true)
        }
    
    }
    
    //MARK: -FUNCTIONS-
    func setVc() {
        view.backgroundColor = .black
        ///register of tableView cell
        tableView.registerCell(type: PostDetailImageTableViewCell.self)
        tableView.registerCell(type: PostCaptionTableViewCell.self)
        self.tableView.register(UINib(nibName: "MultipleImgCell", bundle: nil), forCellReuseIdentifier: "MultipleImgCell")
        ///connector to tableview
        ///
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        
        ///set vc title
        vcTitleLbl.text = "Post Details"
    }
    
    func getThumbnailFrom(path url: URL) -> UIImage? {
        let request = URLRequest(url: url)
        let cache = URLCache.shared
        
        
        if let cachedResponse = cache.cachedResponse(for: request),
           let image = UIImage(data: cachedResponse.data)
        {
            return image
        }
        
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        imageGenerator.maximumSize = resolutionSizeForLocalVideo(url: url) ?? CGSize(width: 350, height: 200)//CGSize(width: 250, height: 120)
        
        var time = asset.duration
        time.value = min(time.value, 1)
        var image: UIImage?
        
        do {
            let cgImage = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            image = UIImage(cgImage: cgImage)
        } catch { }
        
        if
            let image = image,
            let data = image.pngData(),
            let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        {
            let cachedResponse = CachedURLResponse(response: response, data: data)
            
            cache.storeCachedResponse(cachedResponse, for: request)
        }
        return image
    }
    
    func resolutionSizeForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
    func playVideo(url: URL) {
        let videoURL =  url
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
         playerViewController.player!.play()
        }
    }
}


//MARK: -TABLEVIEW DELEGATE-
extension PostDetailViewController : UITableViewDelegate {
    
}

//MARK: -TABLEVIEW DATA SOURCE-
extension PostDetailViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else {
            if self.feedDetailData?.feedDetail?.video == "" {
                return self.feedDetailData?.feedDetail?.image?.count ?? 0
            }else {
                return self.feedDetailData?.feedDetail?.image?.count == 0 ? 1 :  (self.feedDetailData?.feedDetail?.image?.count ?? 0) + 1
            }
        }
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let image  = kUserDefaults.retriveString(.image)
            self.image = image
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "MultipleImgCell") as! MultipleImgCell
            cell.linelbl.isHidden = true
            cell.countLbl.isHidden = true
            cell.btnFeedOptions.isHidden = true
            let settime = UserDefaults.standard.object(forKey:"Time")
            //cell.timelbl.text =  CommonUtility.instance.getDateTIMEFromString(inputDate:(settime) as! String)
//            cell.multiName.text = kUserDefaults.retriveString(.firstName)
            cell.multiName.text = self.feedDetailData?.feedDetail?.userDetail?.name
            let time =  CommonUtility.instance.getDateTIMEFromString(inputDate:(self.feedDetailData?.feedDetail?.createdAt ?? "")!)
            cell.timelbl.text = time
            
            var url = feedDetailData?.feedDetail?.userDetail?.image ?? ""
            cell.multiProfileImage.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named: "prfImg")), options: .highPriority, context: nil)
            cell.multiDES.text = feedDetailData?.feedDetail?.description
            cell.selectedCell = 0
            cell.multiProfileImage.layer.masksToBounds = true
            cell.multiProfileImage.layer.cornerRadius = cell.multiProfileImage.frame.size.height/2;
            cell.multiProfileImage.layer.masksToBounds = true
            cell.multiProfileImage.layer.borderWidth = 1
            
//            if kUserDefaults.retriveString(.image) == ""
//            {
//             cell.multiProfileImage.image = UIImage(named:"prfImg" )
//            }
//            else
//            {
//                let imageprofile = kUserDefaults.retriveString(.image)
//                cell.multiProfileImage.sd_setImage(with: URL(string: imageprofile as? String ?? "prfImg"), placeholderImage: UIImage(named:""), options:.highPriority, context: nil)
//            }
            
            cell.height.constant = 0.01
            cell.likecommentheight.constant = 0.01
            cell.viewBiottom.isHidden = true
            cell.vedioPLAY.isHidden = true
            cell.countLbl.isHidden = true
            cell.secodImg.isHidden = true
            cell.thirdImg.isHidden = true
            cell.singleImg.isHidden = true
            
            return cell
        }
        
        else {
            guard let cell = tableView.dequeueCell(withType: PostDetailImageTableViewCell.self, for: indexPath) as? PostDetailImageTableViewCell else {
                fatalError("PostDetailImageTableViewCell is not initialize...")
            }
        print(self.feedDetailData?.feedDetail)

            if ((self.feedDetailData?.feedDetail?.video ?? "") != "") &&  self.feedDetailData?.feedDetail?.image?.count == 0 {
                if let image = UIImage(named: "playVedio") {
                cell.playBtn.setImage(image, for: .normal)
                }
                cell.playBtn.isHidden = false
            }
            
            else if (self.feedDetailData?.feedDetail?.description?.count != 0) && ((self.feedDetailData?.feedDetail?.video ?? "") != "")
            {
                if let image = UIImage(named: "playVedio") {
                cell.playBtn.setImage(image, for: .normal)
                }
                cell.playBtn.isHidden = false
            }
            else
            {
            if let image = UIImage(named: "") {
            cell.playBtn.setImage(image, for: .normal)
             }
                cell.playBtn.isHidden = true
             }

            if self.feedDetailData?.feedDetail?.video == "" {
//                cell.playBtn.isHidden = true
                if let image = self.feedDetailData?.feedDetail?.image?[indexPath.row], let url = URL(string: image) {
                cell.postImg.sd_setImage(with: url, placeholderImage: UIImage(named: "SplashScreenone") )
                }
                else
                {
                cell.postImg.image = UIImage(named: "SplashScreenone")
                }
             }
             else {
                    DispatchQueue.main.async { [self] in
                    var urlImage = self.feedDetailData?.feedDetail?.thumbnails ?? ""
                    var img = createThumbnailOfVideoFromRemoteUrl(url: "https://byteedge.s3.ap-south-1.amazonaws.com/" + (self.feedDetailData?.feedDetail?.video ?? ""))
                    cell.postImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: img, options: .highPriority, context: nil)
                }

                if indexPath.row == 0{
                    cell.playBtnAction =  {
//
                        if let video = self.feedDetailData?.feedDetail?.video,
                           let url = URL(string: video) {
                            if let url = URL(string: (self.feedDetailData?.feedDetail?.video ?? "")) {
                                DispatchQueue.main.async {
                                let vedio_play = AVPlayer(url: url)
                                self.playerViewController.player = vedio_play
                                self.present(self.playerViewController, animated: true) {
                                    self.playerViewController.player?.playImmediately(atRate: 1.0)
                                  vedio_play.play()
                                }
                            }
                            }

                        }
                       }
                   }
                  else{
                    if let image = self.feedDetailData?.feedDetail?.image?[indexPath.row - 1], let url = URL(string: image) {
                    cell.postImg.sd_setImage(with: url, placeholderImage: UIImage(named: "SplashScreenone") )
                    }else {
                        cell.postImg.image = UIImage(named: "SplashScreenone")
                    }
                }
            }
            return cell
        }
        }
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        if let vurl = url as? String{
            if vurl != ""{
            let asset = AVAsset(url: URL(string: vurl)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        //assetImgGenerate.maximumSize = CGSize(width,height)
        let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
          print(error.localizedDescription)
          return nil
        }
            
        }
            else{
                return nil
            }
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: -API-
extension PostDetailViewController {
    func postDetail() {
        var param : [String: Any] = [:]
        param["feedId"] = self.postId
        ServiceRequest.instance.postDetail(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
          
                print(response?.status as Any,"response")
                
                return
            }
            print(response?.status as Any,"response")
            self.feedDetailData = response?.data
            DispatchQueue.main.async {
                
                self.tableView.reloadData()
            }
        }
    }
    
    
        func likePost(islike: Bool, postId : String,section : IndexPath,groupid:String) {
            var param: [String: Any] = [:]
            param["postId"] = postId
            param["role"] = "postlike"
            param["groupId"] = groupid
            ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
                guard response?.status == 200 else {
                    print(response?.status as Any,"response")
                    self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                    return
                }
                self.obj_ModelFeed?.data?.feedList[section.row].ownLikes = islike
                guard let count =  self.obj_ModelFeed?.data?.feedList[section.row].likes else {
                    return
                }
                
                
                if islike == true {
                    self.obj_ModelFeed?.data?.feedList[section.row].likes = count+1
                    
                    
                }else {
                    if  count == 0 {
                        self.obj_ModelFeed?.data?.feedList[section.row].likes = count
                    }
                    else {
                        self.obj_ModelFeed?.data?.feedList[section.row].likes = count-1
                    }
                    
                    
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                UIApplication.topViewController()?.view.isUserInteractionEnabled = true
            }
        }
    
    
    
       
        
}
