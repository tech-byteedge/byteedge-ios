//
//  PaymentViewController.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 25/02/22.

import UIKit
import Razorpay
import  Alamofire
import  KRProgressHUD
import  SafariServices

class PaymentViewController: UIViewController,UITextFieldDelegate
{
@IBOutlet weak var amouttxt: UITextField!
   
    var razorpayObj : RazorpayCheckout? = nil
    var amount = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.amouttxt.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//     self.paymentApi(with:amouttxt.text!)
        }
    
    @IBAction func paytap(_ sender: UIButton) {
        if amouttxt.text?.count != nil
        {
//           paymentApi(amount: self.amount)
           self.openRazorpayCheckout()
        }
        else
        {
        }
    }
    private func openRazorpayCheckout() {
        // 1. Initialize razorpay object with provided key. Also depending on your requirement you can assign delegate to self. It can be one of the protocol from RazorpayPaymentCompletionProtocolWithData, RazorpayPaymentCompletionProtocol.
        razorpayObj = RazorpayCheckout.initWithKey("rzp_test_5lgCveSCx6BtnX", andDelegate: self)
        let options: [AnyHashable:Any] = [
            "prefill": [
                "contact": "1234567890",
                "email": "a@a.com"
            ],
            "image": "",
            "amount" : 100,
            "name":"",
            "theme": [
             "color": ""
            ]
            // follow link for more options - https://razorpay.com/docs/payment-gateway/web-integration/standard/checkout-form/
        ]
        if let rzp = self.razorpayObj {
            rzp.open(options)
        } else {
            print("Unable to initialize")
        }
    }
}
    
    //MARK: TextFieldDelegare
    extension PaymentViewController
    {
        func textFieldDidEndEditing(_ textField: UITextField) {
        self.amount = self.amouttxt.text ?? ""
        }
    }

    extension PaymentViewController: RazorpayPaymentCompletionProtocolWithData {
        func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
            print("error: ", code)
            self.presentAlert(withTitle: "Alert", message: str)
        }
        
        func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
            print("success: ", payment_id)
            self.presentAlert(withTitle: "Success", message: "Payment Succeeded")
        }
    }

  extension PaymentViewController : SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
    extension PaymentViewController {
        func presentAlert(withTitle title: String?, message : String?) {
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "Okay", style: .default)
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }


    
    
   
    
   
    
    
