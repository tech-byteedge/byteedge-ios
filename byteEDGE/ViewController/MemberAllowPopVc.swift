
//  MemberAllowPopVc.swift
//  byteEDGE
//  Created by Pawan Kumar on 20/01/22.

import UIKit
import  Alamofire
import  KRProgressHUD

class MemberAllowPopVc: UIViewController {
    @IBOutlet weak var popMemberallowTbl: UITableView!
    @IBOutlet weak var mainViewallow: UIView!
    
    var groupId = ""
    var  membeid = ""
    var groupDetail = ""
    
    var option = ["Make user admin","Allow user to contribute","Remove admin rights","Remove contribution rights"]
    var selected = Int()
    
    
    var icon = [UIImage(named: "editImg"),UIImage(named: "delete"),UIImage(named: "blockImg"),UIImage(named: "delete"),UIImage(named: "blockImg")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
    }
    func setui()
    {
        self.popMemberallowTbl.delegate = self
        self.popMemberallowTbl.dataSource = self
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismiss(animated: false, completion: nil)
    }
}

extension MemberAllowPopVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = popMemberallowTbl.dequeueReusableCell(withIdentifier: "AllowMemberCell", for: indexPath) as! AllowMemberCell
        cell.celllblallow.text = option[indexPath.row]
        cell.cellimgallow.image = icon[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
        self.grpAdmin(role: "makeAdmin", groupID: self.groupId)
        }
        
        else if indexPath.row == 1
        {
        self.grpAdmin(role: "allowContribute", groupID: self.groupId)
        }
        
        
        else if indexPath.row == 2
        {
        self.grpAdmin(role: "removeAdmin", groupID: self.groupId)
        }
        
        else if indexPath.row == 3
        {
        self.grpAdmin(role: "removeContribute", groupID: self.groupId)
        }
    }
}

//MARK: API
extension MemberAllowPopVc
{
    func grpAdmin(role: String,groupID:String)
    {
        var param: [String: Any] = [:]
        param["groupId"] = self.groupDetail
        param["memberId"] = self.membeid
        param["role"] = role
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .put,
                                        apiURL: Api.groupAdmin.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: CommonResponseModel.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
         
                self.createAlertCallbackpop(title: AppName, message:message ?? "") { (value)in
                     if value == true
                    {
                         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshValue"), object: nil)
                             DispatchQueue.main.async {
                             self.popMemberallowTbl.reloadData()
                             }
                         self.dismiss(animated: true) {
                             self.navigationController?.popViewController(animated: false)
                         }
                    }
                    else
                    {
                     print("Nothing")
                    }
          
            }
           
                }
            else
            {
                self.createAlertCallbackpop(title: AppName, message:message ?? "") { (value)in
                     if value == true
                    {
                        self.dismiss(animated: true) {
                        self.navigationController?.popViewController(animated: false)
                         }
                    }
                    else
                    {
                     print("Nothing")
                    }
          
            }
        }
    }
    }}
