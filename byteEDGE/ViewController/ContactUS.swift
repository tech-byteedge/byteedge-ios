
import UIKit
import IBAnimatable
import MobileCoreServices
import UniformTypeIdentifiers
import AVKit
import DBAttachmentPickerController
import AVFoundation
import Photos
import OpalImagePicker

class ContactUS: UIViewController, Myprotocol {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var titltlbl: UILabel!
    @IBOutlet weak var txttiltle: UITextField!
    @IBOutlet weak var emaillbl: UILabel!
    @IBOutlet weak var emltxt: UITextField!
    @IBOutlet weak var msglbl: UILabel!
    @IBOutlet weak var txtmsg: UITextField!
    @IBOutlet weak var tblviewContact: UITableView!
    @IBOutlet weak var addViewBorder: UIView!
    @IBOutlet weak var footerview: UIView!
    @IBOutlet weak var addimgTap: UIButton!
    @IBOutlet weak var collectionImg: UICollectionView!
    @IBOutlet weak var heightCos: NSLayoutConstraint!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var addImagebView: NSLayoutConstraint!
    
    var popup : Custopop!
    var details:[UIImage] = []
    var doc : URL?
    var imagPickUp : UIImagePickerController!
    var SelectedAssets = [PHAsset]()
    var mediaType : String?
    var isType:IsType = .photo
    var selectedVideo : URL?
    var imagepost = [UIImage]()
    var thumnail = UIImage()
    let columnLayout = CustomViewFlowLayout()
    let imagePicker = OpalImagePickerController()
    var numberOfImage  = 3
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblviewContact.isPagingEnabled = false
        self.collectionImg.collectionViewLayout = columnLayout
        imagPickUp = self.imageAndVideos()
        self.setui()
        headerView.backgroundColor = UIColor.black
        UITextField.appearance().tintColor = .white
        if(details.count == 0){
        heightCos.constant = 0
        imagePicker.delegate = self
        }
    }
    
    @IBAction func addbuttonImg(_ sender: Any) {
        if details.count == 3
        {
        createAlert(title: AppName, message: "Only select 3 Screenshots")
        }
        else
        {
        self.chooseImage()
         }
        print("dsfSf")
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated:false)
    }
    
    private func setui()
    {
     self.txttiltle.attributedPlaceholder = NSAttributedString(string: "Enter Title",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        
        self.emltxt.attributedPlaceholder = NSAttributedString(string: "Enter email address",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.txtmsg.attributedPlaceholder = NSAttributedString(string: "Write message here...",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.5373, green: 0.5373, blue: 0.5373, alpha: 1.0)])
        self.tblviewContact.delegate = self
        self.tblviewContact.dataSource = self
        self.collectionImg.delegate = self
        self.collectionImg.dataSource = self
        self.addViewBorder.addDashedBorder()
        addViewBorder.backgroundColor = UIColor.black
        footerview.backgroundColor = UIColor.black
        self.collectionImg.isScrollEnabled = true
        if let layout = collectionImg.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
        }
        self.submit.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    @IBAction func submitClick(_ sender: Any) {
        
        if validation()
        {
        self.contactUsApicall()
        }
        else
        {
            
        }
    }
}
extension ContactUS:UITextFieldDelegate
{
    @objc func okbtnclick(_ sender:UIButton)
    {
     self.popup.removeFromSuperview()
    }
    
    func validation() -> Bool{
        
        self.popup = Custopop(frame: self.view.frame,inview:self)

        
        guard let first = txttiltle.text, !first.isBlank else{
            self.popup.titlelbl.text = "Please Enter title"
            popup.okTap.addTarget(self, action: #selector(self.okbtnclick(_:)), for: .touchUpInside)
            self.popup.cusView.layer.cornerRadius = 12
            self.popup.okTap.layer.cornerRadius =  12
            self.view.addSubview(popup)
            
//            createAlert(title: AppName, message: "Please Enter title")
            return false
        }
        
         guard let email = emltxt.text, !email.isBlank else{
            self.popup.titlelbl.text =  "Enter emailAdress"
            popup.okTap.addTarget(self, action: #selector(self.okbtnclick(_:)), for: .touchUpInside)
            self.popup.cusView.layer.cornerRadius = 12
                  self.popup.okTap.layer.cornerRadius = 12
            self.view.addSubview(popup)
//             createAlert(title: AppName, message: "Enter emailAdress")
            return false
        }
        
        if !(emltxt.text?.isValidEmail ?? true){
            self.popup.titlelbl.text = kAlertValidEmail
            popup.okTap.addTarget(self, action: #selector(self.okbtnclick(_:)), for: .touchUpInside)
            self.popup.cusView.layer.cornerRadius = 12
                  self.popup.okTap.layer.cornerRadius = 12
            self.view.addSubview(popup)
//            createAlert(title: AppName, message:kAlertValidEmail)
            return false
        }
        
        guard let last = txtmsg.text, !last.isBlank else{
            self.popup.titlelbl.text = "please Enter message"
            popup.okTap.addTarget(self, action: #selector(self.okbtnclick(_:)), for: .touchUpInside)
            self.popup.cusView.layer.cornerRadius = 12
                self.popup.okTap.layer.cornerRadius = 12
            self.view.addSubview(popup)
            return false
        }
        
       
        
        
        
        if(details.count == 0){
            self.popup.titlelbl.text = kAlertImage
            popup.okTap.addTarget(self, action: #selector(self.okbtnclick(_:)), for: .touchUpInside)
            self.popup.cusView.layer.cornerRadius = 25
                  self.popup.okTap.layer.cornerRadius = 25
            self.view.addSubview(popup)
//            createAlert(title: AppName, message: kAlertImage)
            return false
        }
        
        
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txttiltle) {
            emltxt.becomeFirstResponder()
            return false
        }
        if(textField == emltxt) {
            txtmsg.becomeFirstResponder()
            return false
        }
        return true
    }
}

extension ContactUS
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            self.details.append(chosenImage)
            collectionImg.reloadData()
        }
        
        else if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            self.details.append(chosenImage)
            collectionImg.reloadData()
        }
        
        if self.details.count > 0{
            self.heightCos.constant = 85
            self.collectionImg.reloadData()
        }
        
        dismiss(animated: true, completion: nil)
        let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL
        if let url = videoURL{
            let data = try? Data(contentsOf: url as URL, options: [])
            let videoMb = Double(data?.count ?? 0) / (1048576.0)
            print(videoMb)
            print(videoURL!)
            self.selectedVideo = url as URL?
            self.doc = videoURL as URL?
            if let thumbnailImage = getThumbnailImage(forUrl: url as URL) {
                details.append(thumbnailImage)
            }
            if self.details.count > 0{
                self.heightCos.constant = 85
                self.collectionImg.reloadData()
            }
        }
        
        imagPickUp.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagPickUp.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
}

//MARK: - Extension
extension ContactUS{
    func imageAndVideos()-> UIImagePickerController{
        if(imagPickUp == nil){
            imagPickUp = UIImagePickerController()
            imagPickUp.delegate = self
            imagPickUp.allowsEditing = true
            imagPickUp.videoQuality = .typeLow
        }
        return imagPickUp
    }
}

//MARK: - Extension MyProtocol
extension ContactUS :MyProtocol{
    func delete(type: Int) {
        details.remove(at: type)
        SelectedAssets.remove(at: type)
        self.collectionImg.reloadData()
        if(details.count > 3){
            self.addViewBorder.isHidden = true

        }
        self.addViewBorder.isHidden = false
        self.collectionImg.reloadData()
        
    }
    
      
}

//MARK: COLLECTION DELEGATE AND DATASOURCE

extension ContactUS: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return details.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  self.collectionImg.dequeueReusableCell(withReuseIdentifier: "ContactUSCellCollectionViewCell", for: indexPath) as! ContactUSCellCollectionViewCell
        cell.delegate = self
        cell.removeImg.tag = indexPath.row
        cell.imgContact.image = (details[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize(width: width/3.5, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
}

//MARK: TABLEVIEW DELEGATE AND DATASOURCE

extension ContactUS: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 850
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return footerview
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
}

extension UIView {
    func addDashedBorder() {
        
        let color = UIColor.gray.cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        self.layer.addSublayer(shapeLayer)
    }
}

extension ContactUS:ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        if let images = image {
        details.append(images)
        }
        collectionImg.reloadData()
    }
}

extension ContactUS
{
//    func addImage()  {
//        let allAssets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
//        var evenAssets = [PHAsset]()
//        let imagePicker = ImagePickerController()
//        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
//        imagePicker.settings.selection.max = 3
//        imagePicker.settings.theme.selectionStyle = .numbered
//        imagePicker.settings.selection.unselectOnReachingMax = true
//        self.presentImagePicker(imagePicker, select: { (asset) in
//            print("Selected: \(asset)")
//            if asset.mediaType == .video {
//                if self.selectedVideo == nil {
//                    asset.getURL { url in
//                    self.selectedVideo = url
//                    }
//                } else {
//                    imagePicker.deselect(asset: asset)
//                }
//            } else {
//                if let img = CommonUtilities.shared.getAssetThumbnail(assets: [asset]).first {
//                 self.details.append(img)
//                }
//            }
//
//            evenAssets.append(asset)
//        }, deselect: { (asset) in
//            print("Deselected: \(asset)")
//        }, cancel: { (assets) in
//            print("Canceled with selections: \(assets)")
//        }, finish: { (assets) in
//            self.SelectedAssets.removeAll()
//            self.details.removeAll()
//            print("Finished with selections: \(assets)")
//            for i in 0..<assets.count
//            {
//                self.SelectedAssets.append(assets[i])
//            }
//            self.mediaType = "2"
         //   self.convertAssetToImages()
//        })
//    }
    
}
extension ContactUS: UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func chooseImage(){
        let ActionSheet = UIAlertController(title: nil, message: "Select Image", preferredStyle: .actionSheet)
        let cameraPhoto = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagPickUp.mediaTypes = ["public.image"]
                self.imagPickUp.sourceType = UIImagePickerController.SourceType.camera;
                self.present(self.imagPickUp, animated: true, completion: nil)
            }
            else
            {
                UIAlertController(title: AppName, message: "No Camera available.", preferredStyle: .alert).show(self, sender: nil)
            }
        })
        let PhotoLibrary = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            let imagePicker = OpalImagePickerController()
            self.details.removeAll()
            imagePicker.maximumSelectionsAllowed = self.numberOfImage - self.details.count
            self.viewController?.presentOpalImagePickerController(imagePicker, animated: true,
                    select: { (assets) in
            self.SelectedAssets.append(contentsOf: assets)
                self.details = self.getAssetThumbnail(assets: assets)
                
                if self.SelectedAssets.count > 0 {
                    if UIDevice.current.userInterfaceIdiom == .phone
                    {
                    self.heightCos.constant = 85
                    self.addViewBorder.isHidden = true
                    self.collectionImg.reloadData()
                    }
                    else
                    {
                        self.heightCos.constant = 150
                        self.addViewBorder.isHidden = true
                        self.collectionImg.reloadData()
                    }
                }
                else
                {
                self.addViewBorder.isHidden = false
                }
                                self.collectionImg.reloadData()
                            self.tblviewContact.reloadData()
                            self.viewController?.dismiss(animated: true, completion: nil)
                            }, cancel: {
                            //Cancel
                            })
           // self.addImage()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        ActionSheet.addAction(cameraPhoto)
        ActionSheet.addAction(PhotoLibrary)
        ActionSheet.addAction(cancelAction)
        if let popoverController = ActionSheet.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(ActionSheet, animated: true, completion: nil)
    }
}

//MARK: Api Class
extension ContactUS
{
    func contactUsApicall() {
        var param: [String: Any] = [:]
        param["title"] = self.txttiltle.text
        param["email"] = self.emltxt.text
        param["message"] = self.txtmsg.text
        ServiceRequest.instance.contactUs( param, imagArry: self.details, videoURL: self.selectedVideo, thumbnails:self.thumnail) {  (isSucess, response, error) in
            guard response?.Status == 200 else {
                self.createAlert(title: kAppName, message: response?.message ?? "")
                return
            }
            self.createAlertCallbackpop(title: AppName, message:"Submitted successfully") { (value)in
                 if value == true
                {
                self.navigationController?.popViewController(animated: true)
                }
                else
                {
                 print("Nothing")
                }
            }
//            self.presentAlertViewWithOneButton(
//                alertTitle: kAppName,
//                alertMessage: response?.message,
//                btnOneTitle: kOK) { (alert) in
//                    self.selectedVideo = nil
//                    self.imagepost.removeAll()
//                    self.navigationController?.popViewController(animated: true)
//
//                }
        }
    }
}


extension UIResponder {
    
    /// This Property is used to getting the ParentViewController(UIViewController) of any UIView.
    var viewController:UIViewController? {
        
        if self.next is UIViewController {
            return self.next as? UIViewController
        } else {
            guard self.next != nil else { return nil }
            return self.next?.viewController
        }
    }
    
}
