
import UIKit
import  IQKeyboardManagerSwift
import  Alamofire

class Setup_profile: UIViewController {
    @IBOutlet weak var tvl_Sethome:UITableView!
    @IBOutlet weak var btnNext_view:UIView!
    @IBOutlet weak var nextBtn: UIButton!
    
    var firstName = ""
    var lastName = ""
    var emailAdree = ""
    var dob: Date? // =  ""
    var dateVal = ""
    var mob = ""
    var week = ""
    var data = [
        ["user": "First name*","image": ""],
        ["user":"Last name*","image": ""],
        ["user":"Email Address*","image": ""],
        ["user":"Date of Birth*", "image" :"dob"],
    ]
    let datePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
        setui()
    }
    
    private func setui()
    {
        self.btnNext_view.clipsToBounds = true
        btnNext_view.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        self.nextBtn.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        self.tvl_Sethome.delegate = self
        self.tvl_Sethome.dataSource = self
    }
    
    @IBAction func back_tap(_sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func next_tap(_sender:UIButton)
    {
       
        if  validate(){
            var value = Int()
            value = 0
            kUserDefaults.set(value, forKey: "profileCompleted")
            print(kUserDefaults.set(0, forKey: "profileCompleted"))
            let mobNum = UserDefaults.standard.object(forKey: "MOBILENUM")
            self.mob = mobNum as! String
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ProfessionalDetailsViewController") as! ProfessionalDetailsViewController
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            print("")
        }
        
    }
    func validate() -> Bool {
        if  firstName.trim == ""{
            self.createAlert(title: AppName, message: kAlertFirstName)
            return false
        }
        else if lastName.trim == ""{
            self.createAlert(title: AppName, message: kAlertLastName)
            return false
        }
        
        else if emailAdree.trim == ""{
            self.createAlert(title: AppName, message: kAlertEmail)
            return false
        }
        else if !emailAdree.isValidEmail{
            createAlert(title: AppName, message:kAlertValidEmail)
            return false
        }
        
        else if dateVal.trim == ""{
            self.createAlert(title: AppName, message: kAlertSpouseDOB)
            return false
        }
        
        else{
            kUserDefaults.saveString(firstName, forKey: .firstName)
            kUserDefaults.saveString(lastName, forKey: .lastName)
            kUserDefaults.saveString(emailAdree, forKey: .email)
            kUserDefaults.saveString(dateVal, forKey: .dob)
            kUserDefaults.saveString(week, forKey: .week)
            return true
        }
    }
}

extension Setup_profile: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tvl_Sethome.dequeueReusableCell(withIdentifier: "SethomeCell") as! SethomeCell
        cell.user_lbl.text = data[indexPath.row]["user"]
        cell.selectionStyle = .none
        
        if indexPath.row == 3 {
            cell.calender_img.isHidden = false
            cell.sub_tilte.isUserInteractionEnabled = false
        } else {
            cell.calender_img.isHidden = true
            cell.sub_tilte.isUserInteractionEnabled = true
        }
        
        switch indexPath.row {
        case 0:
            cell.sub_tilte.text = self.firstName.trim
            
            cell.sub_tilte.attributedPlaceholder = NSAttributedString(string: "Enter first name",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
            
            break
        case 1:
            cell.sub_tilte.text = self.lastName.trim
            cell.sub_tilte.attributedPlaceholder = NSAttributedString(string: "Enter last name",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
            
            
            
            break
        case 2:
            cell.sub_tilte.text = self.emailAdree.trim
            cell.sub_tilte.attributedPlaceholder = NSAttributedString(string: "Enter email address",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
            break
        case 3:
           
            cell.sub_tilte.text = self.dob?.dateToString(format: "YYYY-MM-dd") ?? ""
            self.dateVal = cell.sub_tilte.text!.trim
            cell.sub_tilte.attributedPlaceholder = NSAttributedString(string: "Select Date of Birth",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
            
        default:
            print("")
        }
        
           cell.callBack = {
            data in
            switch indexPath.row {
            case 0:
                self.firstName = data
            case 1:
                self.lastName = data
            case 2:
            self.emailAdree = data
            default:
                print("index")
            }
        }
     return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3 {
            DispatchQueue.main.async {
               let picker = DatePickerDialog()
                let calendar = Calendar.current
                picker.show("DOB", maximumDate:calendar.date(byAdding: .year, value: -10, to: Date())! , datePickerMode:.date) { (date) in
                if let selectedDate = date {
                        self.dob = selectedDate
//                        self.dob = Date().description(with: .current)
                        DispatchQueue.main.async {
                        tableView.reloadRows(at: [indexPath], with: .none)
                        }

                    } else {
                    }

                }
            }
            
//            let indexPath = IndexPath(item: 3, section: 0)
//
//            let cell = tvl_Sethome.cellForRow(at: indexPath) as? SethomeCell
//
//            let toolbar = UIToolbar()
//            toolbar.sizeToFit()
//            let doneButtons = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePickers))
//            toolbar.setItems([doneButtons], animated: false)
//            toolbar.isUserInteractionEnabled = true
//            // toolbar.tintColor = UIColor(red: 88/255, green: 29/255, blue: 93/255, alpha: 1.0)
//            toolbar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
//            toolbar.sizeToFit()
//            cell?.sub_tilte.inputAccessoryView = toolbar
//            cell?.sub_tilte.inputView = datePicker
//
//            toolbar.isUserInteractionEnabled = true
//            datePicker.datePickerMode = .date
//            datePicker.locale = Locale(identifier: "en")
//            datePicker.datePickerMode = .date
//            datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -10, to: Date())
//            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
//
//        }
//
//
//    }
//    @objc func donePickers() {
//          let formatter = DateFormatter()
//          formatter.dateStyle = .medium
//          formatter.timeStyle = .none
//          formatter.dateFormat = "d MMMM yyyy"
//          formatter.locale = Locale(identifier: "en")
//          print(datePicker.date)
//          let dateString = formatter.string(from: datePicker.date)
//          print(dateString)
//          //dobTxt.text = formatter.string(from: datePicker.date)
//          view.endEditing(true)
//      }
 }
    }
}

extension UITextField {
    enum Direction {
        case Left
        case Right
    }
    // add image to textfield
    func withImage(direction: Direction, image: UIImage, colorSeparator: UIColor, colorBorder: UIColor){
        let mainView = UIView(frame: CGRect(x: 20, y: 0, width:  20, height: 20))
        mainView.layer.cornerRadius = 5
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 45))
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.layer.cornerRadius = 5
        view.layer.borderWidth = CGFloat(0.5)
        view.layer.borderColor = colorBorder.cgColor
        mainView.addSubview(view)
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0.0, y: 5.0, width: 20.0, height: 20.0)
        view.addSubview(imageView)
        
        let seperatorView = UIView()
        seperatorView.backgroundColor = colorSeparator
        mainView.addSubview(seperatorView)
        
        if(Direction.Left == direction){ // image left
            seperatorView.frame = CGRect(x: 45, y: 0, width: 5, height: 45)
            self.leftViewMode = .always
            self.leftView = mainView
        } else { // image righ
            seperatorView.frame = CGRect(x: -2, y: 5, width: 20, height: 20)
            self.rightViewMode = .always
            self.rightView = mainView
        }
        
        self.layer.borderColor = colorBorder.cgColor
        self.layer.borderWidth = CGFloat(0.5)
        self.layer.cornerRadius = 5
    }
}

     extension Setup_profile{
    func hitServiceForUpdateProfile()
    {
        let param = ["mobileNumber":mob,"firstName":firstName,"lastName":lastName,"email":emailAdree,"dob":dateVal ] as [String : Any]
        print(CommonUtility.instance.headers)
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.updateProfile.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: UpdateProfile.self
        ) { (status, json, model, error) in
            if model?.status == 200
            {
                self.firstName = model?.data?.userDetail?.firstName ?? ""
                self.lastName = model?.data?.userDetail?.lastName ?? ""
                self.emailAdree = model?.data?.userDetail?.email ?? ""
                self.dateVal = model?.data?.userDetail?.dob ?? ""
                let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ProfessionalDetailsViewController") as! ProfessionalDetailsViewController
                self.navigationController?.pushViewController(vc, animated: false)
                kUserDefaults.saveToDefault(obj: model?.data?.userDetail)
              
                
            }
            else
            {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
           
        }
    }

}

