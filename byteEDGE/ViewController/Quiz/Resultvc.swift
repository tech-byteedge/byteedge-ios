
//  Resultvc.swift
//  byteEDGE
//  Created by Pawan Kumar on 25/05/22.

import UIKit
import  Foundation
import Alamofire
import SwiftUI
import SDWebImage

class Resultvc: UIViewController
 {
    @IBOutlet weak var tblResult: UITableView!
    @IBOutlet var resultHeaderView: UIView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var courseImg: UIImageView!
    @IBOutlet weak var usrName_lbl: UILabel!
    @IBOutlet weak var courseName_lbl: UILabel!
    @IBOutlet weak var yourResultName_lbl: UILabel!
    @IBOutlet weak var totalQus_Score_lbl: UILabel!
    @IBOutlet weak var scoreCorrectlbl: UILabel!
    @IBOutlet weak var wronglblScore: UILabel!
    @IBOutlet weak var skipQues_lbl: UILabel!
    @IBOutlet weak var mainview_Score: UIView!
    @IBOutlet weak var finalScore_lbl: UILabel!
    @IBOutlet weak var gallarybtn: UIButton!
    var courseID = ""
    
    var ansCount = Int()
    var WrongCount = Int()
    lazy var totalque = Int()
    lazy var skipQus = Int()
     var courseUuid = String()
    
    
    var obj_parti:ParticularCoursemodel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultApi()
        self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = false;
        self.setinit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userInfo()
    
    }
    
    @IBAction func back_click(_sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func downloadCertificatebtb(_ sender: Any) {
    self.downloadcerti()
    }
    
    @IBAction func back_tap(_ sender: UIButton) {
    self.navigationController?.popToRootViewController(animated: false)
        
    }
    
    private func setinit()
    {
        self.tblResult.delegate = self
        self.tblResult.dataSource = self
        self.tblResult.backgroundColor = .black
        self.gallarybtn.layer.cornerRadius = 15
        self.resultHeaderView.backgroundColor = .clear
        self.tblResult.dataSource = self
        self.userImg.layer.masksToBounds = true
        self.userImg.layer.cornerRadius = userImg.bounds.width / 2
        self.userImg.image = UIImage(named:"profile_unselected")
        
        
        self.courseImg.layer.masksToBounds = true
        self.courseImg.layer.cornerRadius = courseImg.bounds.width / 2
        self.courseImg.image = UIImage(named:"profile_unselected")
        
    }
    
    private func userInfo()
    {
        DispatchQueue.main.async {
        self.scoreCorrectlbl.text  = String(self.ansCount)
        self.wronglblScore.text  = String(self.WrongCount)
        self.totalQus_Score_lbl.text = String(self.totalque)
            
        let skipques = self.totalque - (self.ansCount + self.WrongCount)
        self.skipQues_lbl.text = String(skipques)
        self.finalScore_lbl.text = String(self.ansCount)
        }
        
        let name = kUserDefaults.retriveString(.firstName)
        let lastname = kUserDefaults.retriveString(.lastName)
        self.usrName_lbl.text = name + lastname
        self.yourResultName_lbl.text =  name + lastname
        
        let urlImage =  kUserDefaults.retriveString(.image)
        self.userImg.sd_setImage(with: URL(string:urlImage as! String), placeholderImage: UIImage(named:"prfImg"), options: .highPriority, context: nil)
          self.userImg.maskCircle()
    }
}

extension Resultvc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return resultHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return UITableView.automaticDimension
        }
        else
        {
         return 0.1
        }
        
    }
    
}

extension Resultvc
{
    func resultApi(){
        var param: [String: Any] = [:]
        param["courseId"] = self.courseID
        print("courseid\(self.courseID)")
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.particularCourse.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: ParticularCoursemodel.self,
                                        encoding : URLEncoding.queryString)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                self.obj_parti = model
                let urlImage = self.obj_parti?.data?.courseDetail.thumbnails ?? ""
                self.courseImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named:""), options: .highPriority, context: nil)
                self.courseName_lbl.text = self.obj_parti?.data?.courseDetail.courseName ?? ""
                self.courseImg.maskCircle()
                DispatchQueue.main.async
                {
                    self.tblResult.reloadData()
                }
                
            }
            else
            {
//             self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}


extension Resultvc
{
  func downloadcerti()
   {
    var param: [String:Any] = [:]
    param["courseUuid"] = self.courseUuid
    param["email"] = kUserDefaults.retriveString(.email)
    param["firstName"] = kUserDefaults.retriveString(.firstName)
    param["lastName"] =  kUserDefaults.retriveString(.lastName)
    param["mobileNumber"] = kUserDefaults.retriveString(.mobileNumber)
    param["countryCode"] = UserDefaults.standard.object(forKey:"CountryCode") as! String

    print(CommonUtility.instance.headers)
    ServiceManager.instance.request(method: .post,
                                    apiURL: Api.userinvite.url,
                                    headers: CommonUtility.instance.headers,
                                    parameters: param,
                                    decodable: CommonResponseModel.self
    ) { (status, json, model, error) in
        if model?.status == 200
        {
        self.createAlert(title: AppName, message:model?.message ?? "")
        }
        else
        {
            self.createAlert(title: AppName, message:model?.message ?? "")
        }

    }
}
}



 extension UIImageView {
  public func maskCircle() {
      self.contentMode = UIView.ContentMode.scaleAspectFill
    self.layer.cornerRadius = self.frame.height / 2
    self.layer.masksToBounds = false
    self.clipsToBounds = true
  }
}



