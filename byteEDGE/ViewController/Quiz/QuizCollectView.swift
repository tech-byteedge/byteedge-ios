
//  QuizCollectView.swift
//  byteEDGE
//  Created by Pawan Kumar on 24/05/22

import UIKit
import  Alamofire
import  KRProgressHUD

protocol Questionprotocol {
  func indexAnswer(index: Int,status:Bool)
}
class QuizCollectView:UICollectionViewCell {
    
    @IBOutlet weak var tblquiz: UITableView!
    @IBOutlet weak var mainView: UIView!
    
    var callback: ((Int)->())?
    var callbacks: ((Int,Int,Bool)->())?
    var select = [Bool]()
    var color = [UIColor]()
    var selectedCells = [Int]()
    var selectedQues = -1
    var selectedvalues = false
    var ques_obofQuiz:QuesModel?
    var delegateque : Questionprotocol?
    var single_ob =  ""
    var courseId = ""
     var answer_id = ""
     var ques_id = ""
    var Index = Int()
    var match_QuizOb:Mathques?
    var justiFy = false
    var isShowAnswerDetails = false
    
    
    var ansCount = 0
    var wrongCount =  0
    var skipCount =  0
    var TotalCount = Int()
    var shouldShowRightAnswer = false
    let rightAnswer = 0
    var selectedAnswer: Int?
    
    var selectedIndexes = [[IndexPath.init(row: -1, section: 0)], [IndexPath.init(row: -1, section: 1)]]

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tblquiz.isScrollEnabled = true
        self.tblquiz.delegate = self
        self.tblquiz.dataSource = self
        self.tblquiz.estimatedRowHeight = 80;
        self.tblquiz.rowHeight = UITableView.automaticDimension;
        self.tblquiz.isPagingEnabled = false
        self.registercell()
    }
    
    func registercell()
    {
     tblquiz.registerCell(type: DetailCellTableViewCell.self)
    }
}


extension QuizCollectView: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerGivenIndex == true{
            return 2
        }else{
            return 1
        }
 
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
        return 2 + /ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].option.count
        }
        else
        {
         return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.section == 0
        {
        if indexPath.row == 0
        {
            let cell = self.tblquiz.dequeueReusableCell(withIdentifier: "questionCel", for: indexPath) as! questionCel
            cell.selectionStyle = .none
            cell.lblques.text = "QUESTIONS \(self.tblquiz.tag + 1 )/\(ques_obofQuiz?.data?.questionlist.count ?? 0)"
            cell.lblques.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.58)
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = self.tblquiz.dequeueReusableCell(withIdentifier: "questionCel", for: indexPath) as! questionCel
            cell.selectionStyle = .none
            cell.lblques.text = ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].question
            cell.lblques.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            self.tblquiz.reloadData()
            return cell
        }
        else
        {
            let cell = tblquiz.dequeueReusableCell(withIdentifier: "QuizAnswer", for: indexPath) as! QuizAnswer
            cell.answerlbl.text = self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].option[indexPath.row - 2].option
            cell.selectionStyle = .none
            cell.iconImg.isHidden = true
//            cell.imgQUIZ.image = UIImage(named:"questionUnselected")
            let correctAnswer = self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].correctAnswer ?? ""
            let selectedAnswer = self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].option[indexPath.row - 2]._id
            cell.answerView.backgroundColor =  #colorLiteral(red: 0.2078702748, green: 0.2136184275, blue: 0.2371024489, alpha: 1)
            cell.imgQUIZ.image = UIImage(named:"questionUnselected")
            
      
            if indexPath.row - 2 == ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerSelected{
                if self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].option[indexPath.row - 2]._id == self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].correctAnswer{
//                    ansCount += 1
                    cell.answerView.backgroundColor =  #colorLiteral(red: 0.01176470588, green: 0.7333333333, blue: 0.2784313725, alpha: 1)
                    cell.iconImg.isHidden = false
                    cell.iconImg.image = UIImage(named:"correctIcon")
                   
                }else{
//                    wrongCount += 1
                  
                    cell.answerView.backgroundColor =  #colorLiteral(red: 0.8901960784, green: 0.01960784314, blue: 0.07843137255, alpha: 1)
                    cell.iconImg.isHidden = false
                    cell.imgQUIZ.image = UIImage(named:"questionSelected")
                    cell.iconImg.image = UIImage(named:"incorrectIcon")
                    
                }
            }else{
              cell.answerView.backgroundColor =  #colorLiteral(red: 0.2078702748, green: 0.2136184275, blue: 0.2371024489, alpha: 1)
                
            }
            
            if indexPath.row - 2 == ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerCorrect{
//                cell.isUserInteractionEnabled = false
                 ansCount += 1
                cell.answerView.backgroundColor =  #colorLiteral(red: 0.01176470588, green: 0.7333333333, blue: 0.2784313725, alpha: 1)
                cell.iconImg.isHidden = false
                cell.imgQUIZ.image = UIImage(named:"questionSelected")
                cell.iconImg.image = UIImage(named:"correctIcon")
            }
            if self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerGivenIndex == true {
                cell.isUserInteractionEnabled = false
            }
            else{
                cell.isUserInteractionEnabled = true
            }
            return cell
         }
    }
        else if indexPath.section == 1 &&  self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerGivenIndex == true
        {
            
        let detailCell = self.tblquiz.dequeueReusableCell(withIdentifier: "DetailCellTableViewCell") as! DetailCellTableViewCell
            detailCell.detaillbl.text = self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].justify
            detailCell.detaillbl.backgroundColor = UIColor.black
//            detailCell.detaillbl.textColor = UIColor.red
            detailCell.detaillbl.numberOfLines = 0
            
            
            
            return detailCell
        }
        else{
            
        let detailCell = self.tblquiz.dequeueReusableCell(withIdentifier: "DetailCellTableViewCell") as! DetailCellTableViewCell
            detailCell.detaillbl.text = self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].justify
            detailCell.detaillbl.backgroundColor = UIColor.black
            detailCell.detaillbl.textColor = UIColor.red
            
            
            
            return detailCell
        }
        
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerGivenIndex == true{
           return
          }else{
            self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerGivenIndex = true
          }
          
          for i in 0...(self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].option.count ?? 0) - 1{
              if self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].option[i]._id == self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].correctAnswer{
                  self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerCorrect = i
                  self.ansCount += 1
                  
              }
              
          }
          
         self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerSelected = indexPath.row - 2
          callbacks?(indexPath.row - 2,self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerCorrect ?? 0, self.ques_obofQuiz?.data?.questionlist[self.tblquiz.tag].answerGivenIndex ?? true)
              tblquiz.reloadData()
          }
}

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}


