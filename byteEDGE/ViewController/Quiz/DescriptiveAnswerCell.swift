//
//import UIKit
//import IQKeyboardManagerSwift

//class DescriptiveAnswerCell: UITableViewCell {
//    func statusChanged(state: Bool, sender: String) {
//    }
//
//    @IBOutlet weak var mainView: UIView!
//    @IBOutlet weak var txtDescriptive: KMPlaceholderTextView!
//    var selectedIndexPath = -1
//    weak var viewController : IncomeDocumentVC?
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//        mainView.layer.borderWidth = 0.5
//        mainView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        mainView.clipsToBounds = true
//        txtDescriptive.autocapitalizationType = .words
//        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "done".localized()
//    }
//
//    func populatedata(visible_question_index : Int)
//    {
//        selectedIndexPath = visible_question_index
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
//}
//
//extension DescriptiveAnswerCell: UITextViewDelegate {
//
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//
//        if(text == "\n") {
//            txtDescriptive.resignFirstResponder()
//            return false
//        }
//        return true
//    }
//
//    func textViewDidChange(_ textView: UITextView)
//    {
//        DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].user_descriptive_answer = textView.text
//    }
//}
//
