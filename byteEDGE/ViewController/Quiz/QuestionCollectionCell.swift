
import UIKit
import Localize_Swift

protocol StatusDelegate {
    func statusChanged(questiontype: QuestionType.RawValue, sender: [PassAnswers])
}


class QuestionCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var optionTable: UITableView!
    var selectedIndexPath = 0
    var statusDelegate : StatusDelegate?
    
    //weak var viewController : IncomeDocumentVC?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        optionTable.rowHeight = UITableView.automaticDimension
        optionTable.estimatedRowHeight = 65//UITableView.automaticDimension
        
        optionTable.sectionHeaderHeight = UITableView.automaticDimension
        optionTable.estimatedSectionHeaderHeight = 70
        optionTable.register(UINib(nibName: "DescriptiveAnswerCell", bundle: nil), forCellReuseIdentifier: "DescriptiveAnswerCell")
    }
    
    func populatedata(visible_question_index : Int)
    {
        DataIncomeDocument.shared.is_visible = visible_question_index
        selectedIndexPath = visible_question_index
        optionTable.reloadData()
    }
}


extension QuestionCollectionCell:UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell") as! QuestionCell
        headerCell.lblQuestion.text = "\(DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].question)"
        headerCell.lblDescription.text = DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].description
        return headerCell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].question_type == .InputChoice
        ? 1 : DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let question_type = DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].question_type
        if(question_type == .InputChoice)
        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptiveAnswerCell") as? DescriptiveAnswerCell
//            cell?.populatedata(visible_question_index: selectedIndexPath)
//            cell?.txtDescriptive.text = DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].user_descriptive_answer
//            cell?.txtDescriptive.placeholder = "enter_your_answer".localized()
//
//            return cell ?? UITableViewCell()
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.reuseID) as? AnswerCell
            cell?.lblAnswer.text = DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[indexPath.row].answer
            if(DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[indexPath.row].isSelected)
            {
                cell?.lblAnswer.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell?.btnSelect.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.4078431373, blue: 0.5490196078, alpha: 1)
            }
            else
            {
                cell?.lblAnswer.textColor = #colorLiteral(red: 0.4156862745, green: 0.4156862745, blue: 0.4901960784, alpha: 1)
                cell?.btnSelect.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            
            return cell ?? UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let question_type = DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].question_type
        if (question_type == .SingleChoice)
        {
            if let row = DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options.firstIndex(where: {$0.isSelected == true})
            {
                DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[row].isSelected = false
                let previous_cell = tableView.cellForRow(at: IndexPath(row: row, section: 0) ) as? AnswerCell
                previous_cell?.lblAnswer.textColor = #colorLiteral(red: 0.4156862745, green: 0.4156862745, blue: 0.4901960784, alpha: 1)
                previous_cell?.btnSelect.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[indexPath.row].isSelected = !DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[indexPath.row].isSelected
            let cell = tableView.cellForRow(at: indexPath) as? AnswerCell
            cell?.lblAnswer.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell?.btnSelect.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.4078431373, blue: 0.5490196078, alpha: 1)
        }
        else if (question_type == .MultipleChoice || question_type == .TagChoice )
        {
            DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[indexPath.row].isSelected = !DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[indexPath.row].isSelected
            let cell = tableView.cellForRow(at: indexPath) as? AnswerCell
            if(DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].options[indexPath.row].isSelected)
            {
                cell?.lblAnswer.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                cell?.btnSelect.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.4078431373, blue: 0.5490196078, alpha: 1)
            }
            else
            {
                cell?.lblAnswer.textColor = #colorLiteral(red: 0.4156862745, green: 0.4156862745, blue: 0.4901960784, alpha: 1)
                cell?.btnSelect.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return DataIncomeDocument.shared.arrayQuestion[selectedIndexPath].question_type == .InputChoice
            ? 120
            : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}


