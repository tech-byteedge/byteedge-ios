

import UIKit
import  Alamofire
import  KRProgressHUD

class QuizVC: UIViewController{
    @IBOutlet weak var previosbtn: UIButton!
    @IBOutlet weak var nextbtn: UIButton!
    @IBOutlet weak var collectionQuiz:UICollectionView!
    
    var ques_ob:QuesModel?
    var QuestionDictCount = 6
    var moveToIndexAt = IndexPath(item: 0, section: 0)
    var index = 0
    var ques_id = ""
    var answer_id = ""
    var CourseIdQuiz = ""
    var match_QuizOb:Mathques?
    var courseID = String()
    var courseUid = String()
    
    var correctAnsCount = 0
    var wrongAnsCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        quizQuesApi()
        setui()
    }
    
    func setui()
    {
        collectionQuiz.delegate = self
        collectionQuiz.dataSource = self
        collectionQuiz .isPagingEnabled = false
        self.previosbtn.isHidden = true
    }
    
    @IBAction func backTap(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
  
    
    @IBAction func nextTap(_ sender: UIButton) {
//        if /ques_ob?.data?.questionlist.count > 0
//        {
        if /ques_ob?.data?.questionlist.count - 1 != index
        {
             self.ques_id = /ques_ob?.data?.questionlist[index]._id
            print("quesID : \(ques_id)")
            print(self.ques_id)
            index += 1
            self.previosbtn.isHidden = false
            collectionQuiz.scrollToItem(at: IndexPath(item: index, section: 0), at: .right, animated: true)
        }else {
            let vc = AppStoryboard.Quiz.instance.instantiateViewController(withIdentifier: "Resultvc") as! Resultvc
            vc.courseID = self.CourseIdQuiz
            vc.ansCount = self.correctAnsCount
            vc.WrongCount = self.wrongAnsCount
            vc.totalque = self.ques_ob?.data?.questionlist.count ?? 0
            vc.courseID = CourseIdQuiz
            vc.courseUuid = courseUid
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    @IBAction func previousTap(_ sender: UIButton)
    {
        if index == 0 {
            index = 0
            self.nextbtn.isHidden = false
            self.previosbtn.isHidden = true
        }else{
            index -= 1
            if index == 0
            {
                self.previosbtn.isHidden = true
                self.nextbtn.isHidden = false
            }
            
           
        }
        collectionQuiz.scrollToItem(at: IndexPath(item: index, section:0), at: .left, animated: true)
      //  collectionQuiz.reloadData()
    }
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionQuiz.contentOffset.y ,width : self.collectionQuiz.frame.width,height : self.collectionQuiz.frame.height)
        self.collectionQuiz.scrollRectToVisible(frame, animated: true)
      }
}

extension QuizVC : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIScrollViewDelegate
{
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     return ques_ob?.data?.questionlist.count ?? 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
          {
              let pageWidth = scrollView.frame.width
             let currentIndex =  Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
              let indexPath = IndexPath(item: currentIndex, section: 0)
              let vc = collectionQuiz.cellForItem(at: indexPath) as? QuizCollectView
              vc?.tblquiz.reloadData()
              
             
          }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionQuiz.dequeueReusableCell(withReuseIdentifier: "QuizCollectView", for: indexPath) as! QuizCollectView
        cell.ques_obofQuiz = self.ques_ob
        cell.courseId = self.CourseIdQuiz
        cell.contentView.backgroundColor = .clear
        cell.Index = index
        cell.tblquiz.tag = indexPath.row
//        cell.tblquiz.reloadData()
        cell.callbacks = {
            index,select,status in
            print("indexpr : \(index)")
            let questionId = /self.ques_ob?.data?.questionlist[indexPath.row]._id
            print("quesID : \(questionId)")
            let andId = /self.ques_ob?.data?.questionlist[indexPath.row].option[index]._id
            print("AnswerID : \(andId)")
            self.questionMatch(quesId: questionId, ansId: andId)
            self.ques_ob?.data?.questionlist[indexPath.row].answerSelected = index
            self.ques_ob?.data?.questionlist[indexPath.row].answerCorrect = select
            self.ques_ob?.data?.questionlist[indexPath.row].answerGivenIndex = status
        }
        
//        cell.callback = { index in
//            let questionId = /self.ques_ob?.data?.questionlist[indexPath.row]._id ?? ""
//            print("quesID : \(questionId)")
//            let andId = /self.ques_ob?.data?.questionlist[indexPath.row].option[index]._id ?? ""
//            print("AnswerID : \(andId)")
//
//            if self.ques_ob?.data?.questionlist[indexPath.row].correctAnswer == self.ques_ob?.data?.questionlist[indexPath.row].option[index]._id{
//
//                self.questionMatch(quesId: questionId, ansId: andId)
//            }
//            else
//            {
//                self.questionMatch(quesId: questionId, ansId: andId)
//                print("not match")
//            }
//            self.questionMatch(quesId:questionId ?? "", ansId: andId)
//        }
        
        
        print(self.ques_ob?.data?.questionlist[indexPath.row].answerSelected)
        cell.tblquiz.reloadData()
            
        print(indexPath.item)
           return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.collectionQuiz.bounds.width, height:self.collectionQuiz.bounds.height)
    }
}

extension UICollectionView {
 func scrollToNextItemset() {
     let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
     self.moveToFrame(contentOffset: contentOffset)
 }
 func scrollToPreviousItemset() {
     let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
     self.moveToFrame(contentOffset: contentOffset)
 }

 func moveToFrameset(contentOffset : CGFloat) {
     self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
 }
    
    func moveToFrameq(contentOffset : CGFloat) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: true)
    }
}

//Mark: Api implementation >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
extension QuizVC
{
           func quizQuesApi(){
               var param: [String: Any] = [:]
               param["courseId"] = CourseIdQuiz
               ServiceManager.instance.request(method: .get,
                                            apiURL: Api.QuizQues.url,
                                            headers: CommonUtility.instance.headers,
                                            parameters: param,
                                            decodable: QuesModel.self,
                                            encoding : URLEncoding.queryString)
            { (status, json, model, error) in
                print(model)
                if model?.status == 200
                {
                    self.ques_ob = model
                    if self.ques_ob?.data?.questionlist.count ?? 0 - 1 > 0
                    {
                    for i in 0...(self.ques_ob?.data?.questionlist.count ?? 0) - 1{
                        self.ques_ob?.data?.questionlist[i].answerCorrect = -1
                        
                        self.ques_ob?.data?.questionlist[i].answerSelected = -1
                        self.ques_ob?.data?.questionlist[i].answerGivenIndex = false
                    }
                    }
                    
                    print(self.ques_ob)
                    DispatchQueue.main.async {
                    self.collectionQuiz.reloadData()
                    }
                }
                else
                {
               // self.createAlert(title: AppName, message:model?.message ?? "")
                }
            }
        }
    }




extension QuizVC
{
    func questionMatch(quesId : String, ansId: String){
               var param: [String: Any] = [:]
               param["questionId"] = quesId
               param["courseId"] = CourseIdQuiz
               param["selectAnswer"] = ansId

               ServiceManager.instance.request(method: .post,
                                               apiURL: Api.matchQue.url,
                                               headers:CommonUtility.instance.headers,
                                               parameters: param,
                                               decodable: Mathques.self
               ) { (status, json, model, error) in
                   print(json)
                   if model?.Status == 200
                   {
                       self.correctAnsCount += 1
                       print("Yeah! correctAnsCount Answer\(self.correctAnsCount)")
                       self.match_QuizOb = model
                       DispatchQueue.main.async {
                        self.collectionQuiz.reloadData()
                       }
                   }
                   else if model?.Status == 400{
                       
                       self.wrongAnsCount += 1
                       print("Yup! Wrong Answer\(self.wrongAnsCount)")
                   }
                  
                   else {
                   print(model?.message ?? "")
                   }
               }
        }
    }

    
    






