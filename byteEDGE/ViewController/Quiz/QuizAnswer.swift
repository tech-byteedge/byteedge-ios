//
//  AnswerTVCellTableViewCell.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 24/05/22.
//

import UIKit

class QuizAnswer: UITableViewCell {
    
    
    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var answerlbl: UILabel!
    
    @IBOutlet weak var imgQUIZ: UIImageView!
    @IBOutlet weak var iconImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }

}
