
import UIKit
import Alamofire
import KRProgressHUD


enum Constants {
    static let spacing: CGFloat = 16
    static let borderWidth: CGFloat = 0.5
    static let reuseID = "AnswerCell"
    static let reuseQuestionID = "QuestionCollectionCell"
}

enum QuestionType: Int {
    case MultipleChoice = 1, TagChoice, InputChoice, SingleChoice
}

class QuizVcViewController: UIViewController {
    
   
    //var arrayQuestion = [Question]()
    //var nextIndex = 0
    var nextButtonActive:Bool = false
    var selectedAnswerArray = [PassAnswers]()
    
    @IBOutlet weak var lblDocument: UILabel!
    //@IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var questionCollection: UICollectionView!
    @IBOutlet weak var previousBtn: UIButton!
    
    @IBOutlet weak var nextBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
//        questionListAPI()
        questionCollection.contentInsetAdjustmentBehavior = .never
    }
    
    func Localizable() {
        previousBtn.setTitle("previous_question", for: .normal)
        nextBtn.setTitle("next", for: .normal)
    }
    
    func statusChanged(questiontype: QuestionType.RawValue, sender: [PassAnswers]) {
            print("questiontype = \(questiontype) , sender = \(sender)")
            selectedAnswerArray = sender
        }

    
//    @IBAction func backAction(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "backToUpload"), object: nil)
//    }
    
//    @IBAction func previousAction(_ sender: UIButton) {
//        let collectionBounds = self.questionCollection.bounds
//        let contentOffset = CGFloat(floor(self.questionCollection.contentOffset.x - collectionBounds.size.width))
//        self.moveCollectionToFrame(contentOffset: contentOffset)
//
//        if DataIncomeDocument.shared.is_visible == DataIncomeDocument.shared.arrayQuestion.count{
//            DataIncomeDocument.shared.is_visible = DataIncomeDocument.shared.arrayQuestion.count
//        }
//    }
    
    @IBAction func previosAction(_ sender: UIButton) {
        
        
        
    }
    
    @IBAction func nextAction(_ sender:  UIButton) {
        
        
    }
    
    
//    @IBAction func nextAction(_ sender: UIButton)
//    {
//            if let visibleItems = self.questionCollection.indexPathForItem(at: self.view.convert(self.view.center, to: questionCollection))
//            {
//                let nextItem: IndexPath = IndexPath(item: visibleItems.item + 1, section: 0)
//
//                if visibleItems.row + 1  == DataIncomeDocument.shared.arrayQuestion.count {
//                    print(DataIncomeDocument.shared.is_visible)
//                }
//                else
//                {
//                    self.questionCollection.scrollToItem(at: nextItem, at: .right, animated: true)
//                }
//            }
//        }
    }
//
//    func statusChanged(questiontype: QuestionType.RawValue, sender: [PassAnswers]) {
//        print("questiontype = \(questiontype) , sender = \(sender)")
//        selectedAnswerArray = sender
//    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : self.questionCollection.contentOffset.y ,width : self.questio.frame.width,height : self.questionCollection.frame.height)
            questionCollection.scrollRectToVisible(frame, animated: true)
    }


extension QuizVcViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return DataIncomeDocument.shared.arrayQuestion.count
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = questionCollection.dequeueReusableCell(withReuseIdentifier: Constants.reuseQuestionID, for: indexPath) as! QuestionCollectionCell
        cell.populatedata(visible_question_index: indexPath.row)
//        cell.statusDelegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: questionCollection.frame.width, height: questionCollection.frame.width)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        //nextBtn.isHidden = (indexPath.row == collectionView.numberOfItems(inSection: 0) - 1)
//        previousBtn.isHidden = (indexPath.row == 0)
//        leftImg.isHidden = (indexPath.row == 0)
        if(DataIncomeDocument.shared.arrayQuestion.count == 0)
        {
            previousBtn.isHidden = true
//            leftImg.isHidden = true
        }
        else{
            previousBtn.isHidden = (indexPath.row == 0)
//            leftImg.isHidden = (indexPath.row == 0)
        }
    }
}

//extension IncomeDocumentVC {
//
//    func questionListAPI()
//    {
//        let token = UserDefaults.standard.object(forKey: "access_token") as? String ?? ""
//        print(token)
//        let langEnable = UserDefaults.standard.string(forKey: "Language") ?? ""
//        print(langEnable)
//
//        let headers : HTTPHeaders = [
//            "Authorization": "Bearer " + token,
//            "Content-Type": "application/json"
//        ]
//
//        let parameters: Parameters = [
//            "language": langEnable,//"1",//langEnable,//
//            "doc_type": doc_type,
//        ]
//
//        NSLog("parameters %@", parameters)
//
//        KRProgressHUD.show()
//
//        ApiManager.instance.request(method: .post, URLString: "DocumentQuestion", parameters: parameters as [String:AnyObject] , encoding: JSONEncoding.default, headers: headers)
//        { (success, dictionary, error) in
//            print(dictionary as Any)
//            KRProgressHUD.dismiss()
//
//            if(success)!
//            {
//                if let data = dictionary?["data"] as? Dictionary<String,AnyObject>{
//                    let status = data["status"] as? Int
//                    if status == 1
//                    {
//                        DataIncomeDocument.shared.resetallvalues()
//                        self.galleryArray.removeAll()
//                        self.gallerySampleArray.removeAll()
//
//                        if let result = data["result"] as? [String:AnyObject] {
//
//                            if let descriptionDict = result["description"] as? [String:AnyObject] {
//                                let description = descriptionDict["description"] as? String ?? ""
//                                let doc_type = descriptionDict["doc_type"] as? String ?? ""
//                                print(doc_type)
//                                print(description)
//
//                                //self.lblDescription.text = description
//                                self.lblDocument.text = self.document_name
//                            }
//
//                            let questionArr = result["questions"] as? [[String:AnyObject]]
//                            print(questionArr as Any)
//
//                            for value in questionArr ?? [[:]] {
//                                let ObjArray = Question()
//                                ObjArray.id = String(value["id"] as? Int ?? 0)
//                                ObjArray.question = value["question"] as? String ?? ""
//                                if let question_type_value = value["question_type"] as? Int
//                                {
//                                    if question_type_value > 0, question_type_value < 4{
//                                        let value  = QuestionType(rawValue: question_type_value)
//                                        ObjArray.question_type = value!
//                                    }
//                                }
//                                        // ObjArray.question_type = String(value["question_type"] as? Int ?? 0)
//                                ObjArray.questionAnswerType = String(value["QuestionAsnsweType"] as? Int ?? 0)
//                                ObjArray.language_type = String(value["language_type"] as? Int ?? 0)
//                                ObjArray.is_user_answered = data["is_user_answered"] as? Bool ?? false
//                                ObjArray.user_descriptive_answer = value["user_descriptive_answer"] as? String ?? ""
//                                ObjArray.description = value["description"] as? String ?? ""
//
//                                DataIncomeDocument.shared.arrayQuestion.append(ObjArray)
//
//                                let question_option = value["question_option"]  as? [[String:AnyObject]]
//                                for data in question_option ?? [[:]] {
//                                    let ObjAnswer = Options()
//                                    ObjAnswer.id = String(data["id"] as? Int ?? 0)
//                                    ObjAnswer.answer = data["answer"] as? String ?? ""
//                                    ObjAnswer.question_id = String(data["question_id"] as? Int ?? 0)
//                                    ObjAnswer.isSelected = data["is_answer"] as? Bool ?? false
//                                    ObjArray.options.append(ObjAnswer)
//                                }
//                            }
//
//                            let uploadedDocumentArr = result["user_uploaded_document"] as? [[String:AnyObject]]
//                            print(uploadedDocumentArr as Any)
//
//                            for value in uploadedDocumentArr ?? [[:]] {
//                                let objGallery = GalleryModal()
//                                objGallery.id = value["id"] as? Int ?? 0
//                                objGallery.doc_type = value["doc_type"] as? String ?? ""
//                                objGallery.documet_file = value["documet_file"] as? String ?? ""
//                                self.galleryArray.append(objGallery)
//                            }
//
//                            let sampleDocumentArr = result["sample_document"] as? [[String:AnyObject]]
//                            print(sampleDocumentArr as Any)
//
//                            for value in sampleDocumentArr ?? [[:]] {
//                                let objGallery = GalleryModal()
//                                objGallery.id = value["id"] as? Int ?? 0
//                                objGallery.doc_type = value["doc_type"] as? String ?? ""
//                                objGallery.documet_file = value["document"] as? String ?? ""
//                                self.gallerySampleArray.append(objGallery)
//                            }
//
//                            if(DataIncomeDocument.shared.arrayQuestion.count == 0)
//                            {
//                                self.previousBtn.isHidden = true
//                                self.leftImg.isHidden = true
//                            }
//
//                            self.questionCollection.reloadData()
//                        }
//                    }
//                    else
//                    {
//                        self.createAlert(title: AppName, message: data["message"] as? String ?? "")
//                    }
//                }
//            }
//        }
//    }
//}


// MARK: MODAL CLASS
class Question {
    var id = ""
    var question = ""
    var question_type : QuestionType = .SingleChoice
    var questionAnswerType = ""
    var language_type = ""
    var is_user_answered = Bool()
    var user_descriptive_answer = ""
    var description = ""
    var isMutiple: Bool = false
    var options = [Options]()
}

class Options {
    var id = ""
    var question_id = ""
    var answer = ""
    var is_answer = Bool()
    var isSelected: Bool = false
}

class PassQuestionModal {
    var question_id = ""
    var question_type = ""
    var language_type = ""
    var other_doc_comment = ""
    var descriptive_answer = ""
    var answer = [PassAnswers]()
}

class PassAnswers {
    var answer_id = ""
}


class GalleryModal {
    var id = Int()
    var doc_type = ""
    var documet_file = ""
}



class DataIncomeDocument
{
    static let shared = DataIncomeDocument()
    var arrayQuestion = [Question]()
    var is_visible = 0
    
    func resetallvalues()
    {
        arrayQuestion = [Question]()
    }
}




