//
//  StartQuiz.swift
//  byteEDGE
//
//  Created by Pawan_Kumar_Mac on 21/06/22.
//

import UIKit

class StartQuiz: UIViewController {
    
    @IBOutlet weak var disLbl:UILabel!
    @IBOutlet weak var disLblbottom:UILabel!
    @IBOutlet weak var Startbtn: UIButton!
    var QUIZid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setinit()
    }
    
    func setinit()
    {
        disLbl.setAppFontColor(.appColor(.white, alpha: 1.0), font: .ProductSans(.regular, size: .oneSeven))
        disLblbottom.setAppFontColor(.appColor(.white, alpha: 1.0), font: .ProductSans(.regular, size: .oneSeven))
        
        disLblbottom.setAppFontColor(.appColor(.white, alpha: 1.0), font: .ProductSans(.medium, size: .oneSeven))
    }

    @IBAction func backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func startbtn(_ sender: UIButton) {
        
        let vc  = AppStoryboard.Quiz.instance.instantiateViewController(withIdentifier: "QuizVC") as! QuizVC
        vc.CourseIdQuiz = QUIZid
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
