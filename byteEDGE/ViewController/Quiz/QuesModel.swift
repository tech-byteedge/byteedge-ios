
//  QuesModel.swift
//  byteEDGE
//  Created by Pawan Kumar on 25/05/22.

import Foundation
// MARK: - Welcome
struct QuesModel: Codable {
    let Code, status: Int
    let message: String
    var data: DataQuiz?
}

// MARK: - DataClass
struct DataQuiz: Codable {
    let count: Int?
    var questionlist: [Questionlist]
    
}

// MARK: - Questionlist
 struct Questionlist: Codable {
    let _id, question: String
    let option: [Option]
    var correctAnswer: String?
    let isDeleted, status: Bool
    let createdAt, courseName, image: String
    var answerGivenIndex:Bool?
    var answerCorrect:Int?
    var answerSelected:Int?
    var justify: String?
   // var selectoption = Bool
 
    
}

// MARK: - Option
struct Option: Codable {
    let option, _id: String?
  
}





