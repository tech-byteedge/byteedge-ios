
import UIKit

class AnswerCell: UITableViewCell {

//    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var answerView: UIView!
    //    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerView.clipsToBounds = true
        answerView.addViewBorder(borderColor: #colorLiteral(red: 0.8038416505, green: 0.8039775491, blue: 0.8038237691, alpha: 1), borderWith: 0.5)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
