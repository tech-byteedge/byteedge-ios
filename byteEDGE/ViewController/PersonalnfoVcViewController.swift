
import UIKit
import Foundation
import iOSDropDown
import  Alamofire
import KRProgressHUD
import  Photos
import SDWebImage
import  IQKeyboardManagerSwift

class PersonalnfoVcViewController: UIViewController,UITextFieldDelegate,UIGestureRecognizerDelegate{
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var mobTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var dobTxt: UITextField!
    @IBOutlet weak var genderTxt: DropDown!
    @IBOutlet weak var nationalityTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var setLearningTxr: DropDown!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var cameraTap: UIButton!
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lastNametxt: UITextField!
    @IBOutlet weak var txtSkills: UIButton!
    @IBOutlet weak var txtSkillText: UITextField!

    @IBAction func txtSkills(_ sender: Any) {
    }
    //MARK: Variable
    var image = [UIImage]()
    var profileImage = String()
    let picker  = UIImagePickerController()
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    let datePicker = UIDatePicker()
    
    var firstName = ""
    var lastName = ""
    var emailAdree = ""
    var weekselected : Int = 0
    var genderselected : Int = 0
    var idofSkills = [String]()
    
    var mob = ""
    var dateVal = ""
    var nationality = ""
    var dob :Date?
    var imagPickUp : UIImagePickerController!
    var objuser: UpdateProfile?
    var obj: SkillsChoose?
    var typeClass = Int()
    var skillStr = ""
    
    var weekSelect = ["20Mins - Per Week",
                      "30Mins - Per Week",
                      "60Mins - Per Week",
                      "2HR - Per Week",
                      "3HR - Per Week",
                      "4HR - Per Week",
                      "5HR Per Week"]
    
    var textfield = UITextField()
    
    var genderselect = ["Male",
                        "Female",
                        "Other"]
    
    var groupImage: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
//        self.userUpdate()
        self.loginData()
        self.setDropdown()
        self.profileImg.layer.masksToBounds = true
        profileImg.layer.cornerRadius = profileImg.frame.size.height/2;
        profileImg.layer.masksToBounds = true
        profileImg.layer.borderWidth=1;
        createDatePicker()
        
        self.userIntractionOfTextfied()
        self.dobTxt.delegate = self
        self.dobTxt.tintColor = .clear
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    
    @IBAction func saveData(_ sender: UIButton) {
        if typeClass == 0
        {
            self.userUpdate()
        }
        else
        {
            self.userUpdate()
        }
//        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewWillLayoutSubviews() {
        self.saveBtn.clipsToBounds = true
        self.saveBtn.backgroundColor =  #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive press: UIPress) -> Bool {
        return false
    }
    
    private  func userIntractionOfTextfied()
    {
        if typeClass == 1
        {
            self.nameTxt.isUserInteractionEnabled = true
            self.lastNametxt.isUserInteractionEnabled = true // txtfld
            self.mobTxt.isUserInteractionEnabled = false
            self.emailTxt.isUserInteractionEnabled = true
            self.dobTxt.isUserInteractionEnabled = true
            self.nationalityTxt.isUserInteractionEnabled = true
            self.setLearningTxr.isUserInteractionEnabled = true
        }
        else if typeClass == 0
        {
            self.nameTxt.isUserInteractionEnabled = true
            self.mobTxt.isUserInteractionEnabled = false
            self.emailTxt.isUserInteractionEnabled = true
            self.dobTxt.isUserInteractionEnabled = true
            self.nationalityTxt.isUserInteractionEnabled = true
            self.setLearningTxr.isUserInteractionEnabled = true
        }
        else
        {
            createAlert(title: AppName, message: "someThing went wrong")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setDropdown()
        self.hitServiceForSkillschoose(param: [:])

    }
    
    @IBAction func editSkills(_ sender: UIButton) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ChooseSkillVc") as! ChooseSkillVc
        vc.callBack = {
            id in
            self.idofSkills = id ?? [""]
            print(self.idofSkills)
        }
        
        vc.idofSkills = self.idofSkills
        print(self.idofSkills)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    private func setui()
    {
        
        if let skills = kUserDefaults.object(forKey:"SelectedSkill")
        {
            self.idofSkills = skills as! [String]
        }
        
        self.nameTxt.attributedPlaceholder = NSAttributedString(string: "Enter Name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        self.lastNametxt.attributedPlaceholder = NSAttributedString(string: "Enter Last Name",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])

        self.mobTxt.attributedPlaceholder = NSAttributedString(string: "Enter MobileNumber",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        self.emailTxt.attributedPlaceholder = NSAttributedString(string: "Enter Email",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        self.dobTxt.attributedPlaceholder = NSAttributedString(string: "Enter Dob ",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        self.genderTxt.attributedPlaceholder = NSAttributedString(string: "Enter Gender",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        self.nationalityTxt.attributedPlaceholder = NSAttributedString(string: "Enter Nationality",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        self.cityTxt.attributedPlaceholder = NSAttributedString(string: "Enter City",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        self.setLearningTxr.attributedPlaceholder = NSAttributedString(string: "Select Learning",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.7)])
        
        self.cameraView.clipsToBounds = true
        self.cameraView.makeRounded()
        self.pinkView.makeRounded()
        self.profileImg.contentMode = .scaleToFill
        mobTxt.keyboardType = .phonePad
    }
    
    func createDatePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButtons = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePickers))
        //           doneButtons.tintColor = .white
        toolbar.setItems([doneButtons], animated: false)
        toolbar.isUserInteractionEnabled = true
        // toolbar.tintColor = UIColor(red: 88/255, green: 29/255, blue: 93/255, alpha: 1.0)
        toolbar.tintColor = .white
        toolbar.sizeToFit()
        dobTxt.inputAccessoryView = toolbar
        dobTxt.inputView = datePicker
        
        toolbar.isUserInteractionEnabled = true
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: "en")
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -10, to: Date())
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
    }
    
    @objc func donePickers() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.dateFormat = "yyyy- MM-dd"
        //          formatter.dateFormat = "d MMMM yyyy"
        formatter.locale = Locale(identifier: "en")
        print(datePicker.date)
        let dateString = formatter.string(from: datePicker.date)
        print(dateString)
        dobTxt.text = formatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    //MARK: DROPDown list
    func setDropdown()
    {
        self.genderTxt.optionArray = self.genderselect
        self.genderTxt.didSelect{(selectedText , index ,id) in
            self.genderTxt.text = "\(selectedText)"
            self.genderselected = (index + 1)
        }
        self.genderTxt.listWillAppear
        {
            self.genderTxt.resignFirstResponder()
            if(self.textfield == self.genderTxt) {
                self.genderTxt.resignFirstResponder()
            }
        }
        
        self.setLearningTxr.optionArray = self.weekSelect
        self.setLearningTxr.didSelect{(selectedText , index ,id) in
            print(index)
            self.setLearningTxr.text = "\(selectedText)"
            self.weekselected = index
            
            print(selectedText)
            //        kUserDefaults.saveString(selectedText, forKey: .week)
        }
        self.setLearningTxr.listWillAppear
        {
            self.setLearningTxr.resignFirstResponder()
            if(self.textfield == self.setLearningTxr) {
                self.setLearningTxr.resignFirstResponder()
            }
        }
        //           self.genderTxt.listWillAppear
        //            {
        //                self.genderTxt.resignFirstResponder()
        //                if(self.textfield == self.genderTxt) {
        //                self.genderTxt.resignFirstResponder()
        //                }
        //            }
        
    }
    @IBAction func backClick(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func cameraTap(_ sender: Any)
    {
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    print("OKAY")
                } else {
                    print("NOT OKAY")
                }
            })
        }
        checkLibrary()
        checkPermission()
        print("sfdsdssfs")
    }
    
    func loginData()
    {
        kUserDefaults.retriveString(.firstName)
        let mobNum = kUserDefaults.retriveString(.mobileNumber)
        self.mob = mobNum as! String
        self.mobTxt.text =  self.mob
        
        let firsName = kUserDefaults.retriveString(.firstName)
        //        let lastName = kUserDefaults.retriveString(.lastName)
        self.nameTxt.text = firsName
        
        let lastname = kUserDefaults.retriveString(.lastName)
        self.lastNametxt.text = lastname
        
        let email = kUserDefaults.retriveString(.email)
        self.emailAdree = email as?  String ?? ""
        self.emailTxt.text = self.emailAdree
        
        let nationality = kUserDefaults.retriveString(.nationality)
        self.nationalityTxt.text  = nationality as? String ?? ""
        
        let city = kUserDefaults.retriveString(.city)
        self.cityTxt.text  = city as? String ?? ""
        
        let dob = kUserDefaults.retriveString(.dob)
        self.dateVal = dob as?  String ?? ""
        self.dobTxt.text = self.dateVal
        
        
        
        let week = kUserDefaults.retriveString(.week)
        self.setLearningTxr.text  = week as? String ?? ""
        
        
        let gender = kUserDefaults.retriveString((.gender))
        self.genderTxt.text = String(gender)
        
        
        if kUserDefaults.retriveString(.image) == ""
        {
            self.profileImg.image = UIImage(named:"prfImg" )
        }
        else
        {
            let imageprofile = kUserDefaults.retriveString(.image)
            self.profileImg.sd_setImage(with: URL(string: imageprofile as? String ?? "prfImg"), placeholderImage: UIImage(named:""), options: .highPriority, context: nil)
            self.profileImg.makeRounded()
        }
    }
}

extension PersonalnfoVcViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UNUserNotificationCenterDelegate
{
    func displayUploadImageDialog() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        let alertController = UIAlertController(title: "", message: "Upload profile photo?".localized(), preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel action"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            alertController.dismiss(animated: true) {() -> Void in
            }
        })
        alertController.addAction(cancelAction)
        let cameraRollAction = UIAlertAction(title: NSLocalizedString("Open library".localized(), comment: "Open library action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UI_USER_INTERFACE_IDIOM() == .pad {
                OperationQueue.main.addOperation({() -> Void in
                    picker.sourceType = .photoLibrary
                    picker.toolbar.backgroundColor = .white
                    self.present(picker, animated: true) {() -> Void in }
                })
            }
            else {
                picker.sourceType = .photoLibrary
                self.present(picker, animated: true) {() -> Void in }
            }
        })
        let cameraRollAction1 = UIAlertAction(title: NSLocalizedString("Open camera".localized(), comment: "Open library action"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UI_USER_INTERFACE_IDIOM() == .pad {
                OperationQueue.main.addOperation({() -> Void in
                    picker.sourceType = .camera
                    self.present(picker, animated: true) {() -> Void in }
                })
            }
            else {
                self.profileImg.maskCircle()
                picker.sourceType = .camera
                self.present(picker, animated: true) {() -> Void in }
            }
        })
        
        alertController.addAction(cameraRollAction)
        alertController.addAction(cameraRollAction1)
        alertController.view.tintColor = .black
        alertController.view.backgroundColor = .white
        
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.backgroundColor = .red
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        present(alertController, animated: true) {() -> Void in }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.profileImg.image = image
        self.groupImage = image
        self.profileImg.maskCircle()
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func checkPermission() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized:
            self.displayUploadImageDialog()
        case .denied:
            print("Error")
        default:
            break
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func checkLibrary() {
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .authorized {
            switch photos {
            case .authorized:
                self.displayUploadImageDialog()
            case .denied:
                print("Error")
            default:
                break
            }
        }
    }
}

extension PersonalnfoVcViewController
{
    
    func hitServiceForSkillschoose(param: [String:Any]){
        
        
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.skillList.url,
                                        headers: nil,
                                        parameters: nil,
                                        decodable: SkillsChoose.self
        ) { (status, json, model, error) in
            if model?.status == 200
            {
                self.obj = model
                if let skills = kUserDefaults.value(forKey: "SelectedSkill") as? [String] {
                    self.skillStr = ""
                    for idskill in skills {
                        if let arr = self.obj?.data?.skillList {
                            for skil in arr {
                                if let data = skil.skillType {
                                    for typ in data {
                                        if idskill == typ._id {
                                            self.skillStr += "\(typ.skills ?? ""),"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                self.txtSkillText.text = self.skillStr
            }
            else {
                self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
    
    func userUpdate() {
        let idofSkillsData = try! JSONSerialization.data(withJSONObject: self.idofSkills, options: .prettyPrinted)
        let idofSkillstring = String(data: idofSkillsData, encoding: .utf8)!
        var param = [String:Any]()
        if typeClass == 1
        {
            param = ["mobileNumber": kUserDefaults.retriveString(.mobileNumber),
                     "firstName": kUserDefaults.retriveString(.firstName),
                     "city": cityTxt.text,
                     "lastName": lastNametxt.text,
                     "email":  emailTxt.text ?? "",
                     //                     "gender": genderTxt.text ?? "",
                     "gender": String(genderselected),
                     
                     "dob":  dobTxt.text ?? "",
                     "week":  String(weekselected),
                     "skills":idofSkillstring
            ]
            print("param")
            print(param)
        }
        else
        {
            param =  [
                "firstName": self.nameTxt.text ?? "",
                "city": cityTxt.text,
                //                "mobileNumber": mobTxt.text ?? "",
                //                "email":  emailTxt.text ?? "",
                "dob":  dobTxt.text ?? "",
                //               "gender": genderTxt.text ?? "",
                "gender": String(genderselected),
                "week":  String(weekselected),
                //                "skills":self.idofSkills
            ]
            print("param")
            print(param)
        }
        
        
        var imgParam: [String : UIImage] = [:]
        if let img = self.groupImage {
            imgParam["image"] = img
        }
        
        KRProgressHUD.show()
        ServiceManager.instance.uploadImageRequestWithDecodable(
            method: .post,
            apiURL: Api.updateProfile.url,
            headers: CommonUtility.instance.headers,
            parameters: param,
            decodable: UpdateProfile.self,
            imageParameters: imgParam) { success, response, Error in
                KRProgressHUD.dismiss()
                guard response?.status == 200 else {
                    if let message = response?.message {
                    }else {
                        self.createAlert(title: kAppName, message: "Something Went wrong")
                    }
                    return}
                print(response)
                
                self.idofSkills = response?.data?.userDetail?.skills ?? [""]
                self.nationality = response?.data?.userDetail?.nationality ?? ""
                print(self.nationality)
                kUserDefaults.set(self.idofSkills, forKey: "SelectedSkill")
                self.profileImage = response?.data?.userDetail?.image ?? ""
                kUserDefaults.saveString(self.profileImage, forKey: .image)
                //                kUserDefaults.saveString(self.dob, forKey: .dob)
                kUserDefaults.set(self.profileImage, forKey: "Image")
                kUserDefaults.saveString(self.nameTxt.text, forKey: .firstName)
                kUserDefaults.saveString(self.lastNametxt.text, forKey: .lastName)
                //                kUserDefaults.saveString(self.nameTxt.text, forKey: .lastName)
                kUserDefaults.saveString(self.nationalityTxt.text, forKey: .nationality)
                kUserDefaults.saveString(self.dobTxt.text, forKey: .dob)
                kUserDefaults.saveString(self.emailTxt.text, forKey: .email)
                kUserDefaults.saveString(self.cityTxt.text, forKey: .city)
                //                kUserDefaults.saveString(response?.data?.userDetail?.gender, forKey: .gender)
                //                kUserDefaults.saveInt(self.genderselect[response?.data?.userDetail?.gender ?? 0],forKey:.gender)
                if (response?.data?.userDetail?.gender ?? 1) - 1 > 0
                {
                    let val = (response?.data?.userDetail?.gender ?? 1) - 1 ?? 0
                    kUserDefaults.saveString(self.genderselect[val], forKey: .gender)
                }
                kUserDefaults.saveString(self.weekSelect[response?.data?.userDetail?.week ?? 0], forKey: .week)
                kUserDefaults.saveString(self.setLearningTxr.text, forKey: .week)
                kUserDefaults.saveString(self.genderTxt.text, forKey:.gender)
                self.nationalityTxt.text = self.nationality
                self.navigationController?.popViewController(animated: false)
                DispatchQueue.main.async {
                    self.loginData()
                    self.scrollView.reloadInputViews()
                }
            }
    }
}

extension UITextField {
    override open func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return (action == #selector(UIResponderStandardEditActions.copy(_:))
                || action == #selector(UIResponderStandardEditActions.paste(_:))) ?
        false : super.canPerformAction(action, withSender: sender)
    }
}



