//
//  BeforeSplash.swift
//  byteEDGE
//  Created by Pawan_Kumar_Mac on 09/08/22.

import UIKit
import  Foundation
import Alamofire
class BeforeSplash: UIViewController {
    var  id = String()
     var groupid = String()
    var postID = ""
    var type = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
          
            if(self.postID != "")
            {
                self.hitServiceForGrpVAlidation(groupId: self.groupid)
//                self.tabBarController?.tabBar.isHidden = false
//                vc.hidesBottomBarWhenPushed = false
            }
             else
             {
                let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ShareVC") as! ShareVC
                vc.id = self.id
                self.navigationController?.pushViewController(vc, animated: false)
            }          
        }
    }
}

extension BeforeSplash
{
    func hitServiceForGrpVAlidation(groupId:String)
    {
       
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.userExit.url + "?groupId=\(groupId)",
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: UserExitModel.self,
                                        encoding: URLEncoding.default
        )
        {(status, json, model, error) in
            print((status, json, model, error))
            if model?.status == 200
            {
                
                if model?.message != "user Exist In Group" {
                    let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                    vc.isDynamicLink = true
                            let nav_obj = UINavigationController(rootViewController: vc)
                            nav_obj.navigationBar.isHidden = true
                            UIApplication.shared.windows.first?.rootViewController = nav_obj
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                }
                else{
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                    vc.postId = self.postID
                    vc.grpID = self.groupid
                    vc.viaSplash = true
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
            }
            else
            {
                let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                vc.isDynamicLink = true
                        let nav_obj = UINavigationController(rootViewController: vc)
                        nav_obj.navigationBar.isHidden = true
                        UIApplication.shared.windows.first?.rootViewController = nav_obj
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
            }
        }
    }
}

struct UserExitModel:Codable
{
    var Code : Int?
    var status:Int?
    var message: String?
    var data: DataUserExit?
}

struct DataUserExit: Codable
{
 var group: GroupData?
}

struct GroupData:Codable
{
    var id: String?
    var isJoin: Bool?
}



