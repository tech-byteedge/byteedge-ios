
import UIKit

class TabBarVC: UITabBarController,UITabBarControllerDelegate {
    var btn_home : UITabBarItem!
    var btn_search : UITabBarItem!
    var btnComm:UITabBarItem!
    var btnlearning: UITabBarItem!
    var btnProfile: UITabBarItem!
    var pushdata = Int()
    var courseId = ""
    var mediaId = ""
    var playlistId = ""
    var isDynamicLink = false
    var viaCourse = false
    
     override func viewDidLoad()
     {
        super.viewDidLoad()
        btn_home = UITabBarItem()
        btn_home.title = "bytes"
        btn_search = UITabBarItem()
        btn_search.title = "search"
        btnComm = UITabBarItem()
        btnComm.title = "communinty"
        btnlearning = UITabBarItem()
        btnlearning.title = "learning"
        btnProfile = UITabBarItem()
        btnProfile.title = "profile"
        if pushdata == 1
        {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ShareVC") as! ShareVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
         
        self.tabBar.barTintColor = UIColor.black
        setUpTabBarElements()
         
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC1 = storyboard.instantiateViewController(withIdentifier: "VedioplayViewController") as! VedioplayViewController
        let VC2 = storyboard.instantiateViewController(withIdentifier: "SearchVc") as! SearchVc
        let VC3 = storyboard.instantiateViewController(withIdentifier: "CommunityVc") as! CommunityVc
        let VC4 = storyboard.instantiateViewController(withIdentifier:  "LearningGoalsViewController") as! LearningGoalsViewController
        let VC5 = storyboard.instantiateViewController(withIdentifier: "AccountVc") as! AccountVc
        
        VC1.tabBarItem = btn_home
        VC2.tabBarItem = btn_search
        VC3.tabBarItem = btnComm
        VC4.tabBarItem = btnlearning
        VC5.tabBarItem = btnProfile
        self.viewControllers = [VC1,VC2,VC3,VC4,VC5]
        NotificationCenter.default.addObserver(self,selector: #selector(openCourse),name: NSNotification.Name ("openCourseDetailshowVc"),object: nil)
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 4)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 255, green: 255, blue: 255, alpha: 1.0)], for: .selected)
            //            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 0.7059, green: 0.7059, blue: 0.7451, alpha: 1.0)], for: .normal)
            self.selectedIndex = 0
            
        }
        else
        {
            UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 4, vertical: -4)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 255, green: 255, blue: 255, alpha: 1.0)], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 0.7059, green: 0.7059, blue: 0.7451, alpha: 1.0)], for: .normal)
            self.selectedIndex = 0
        }
        
       if let value = UserDefaults.standard.object(forKey: "SwitchStatus") as? Bool{
       }
        if isDynamicLink{
         self.selectedIndex = 2
        }
         
        if viaCourse{
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
            vc.viaDeepLink = true
            vc.courseId = self.courseId
            vc.playlistID = self.playlistId
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
      @objc func openCourse(note: NSNotification)
       {
        if let value = note.userInfo?["Value"] as? ShareReals {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
            vc.playlistID = value.data?.reelList?.courseData?.playlistId ?? ""
            vc.courseId =  value.data?.reelList?.courseData?._id ?? ""
            self.dismiss(animated: false)
            self.navigationController?.pushViewController(vc, animated: false)
                }
        }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func setNeedsStatusBarAppearanceUpdate() {
        super.setNeedsStatusBarAppearanceUpdate()
    }
    override func viewWillLayoutSubviews()
    {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
        var newTabBarFrame = tabBar.frame
        let newTabBarHeight: CGFloat = 75
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight
        tabBar.bottomAnchor.constraint(equalToSystemSpacingBelow: view.bottomAnchor, multiplier:0)
        tabBar.layer.cornerRadius = 20
        tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        tabBar.frame = newTabBarFrame
        }
        else
        {
            var newTabBarFrame = tabBar.frame
            let newTabBarHeight: CGFloat = 500
            newTabBarFrame.size.height = newTabBarHeight
            newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight
            tabBar.bottomAnchor.constraint(equalToSystemSpacingBelow: view.bottomAnchor, multiplier:0)
            tabBar.layer.cornerRadius = 20
            tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            tabBar.frame = newTabBarFrame
        }
    }
    
    func setUpTabBarElements()
    {
        self.btn_home.imageInsets = UIEdgeInsets(top:0, left:0, bottom: 2, right: 5)
        self.btn_search.imageInsets = UIEdgeInsets(top:0, left:0, bottom: 2, right: 5)
        self.btnComm.imageInsets = UIEdgeInsets(top:0, left:0, bottom: 2, right: 5)
        self.btnComm.imageInsets = UIEdgeInsets(top:0, left:0, bottom: 2, right: 5)
        self.btnProfile.imageInsets = UIEdgeInsets(top: 0, left:0, bottom: 2, right:5)
        
        
        
        self.btn_home.image = #imageLiteral(resourceName: "home_unselected").withRenderingMode(.alwaysOriginal)
        self.btn_search.image = #imageLiteral(resourceName: "search_unselected").withRenderingMode(.alwaysOriginal)
        self.btnComm.image = #imageLiteral(resourceName: "community_unselected").withRenderingMode(.alwaysOriginal)
        self.btnlearning.image = #imageLiteral(resourceName: "learning_unselected").withRenderingMode(.alwaysOriginal)
        self.btnProfile.image = #imageLiteral(resourceName: "profile_unselected").withRenderingMode(.alwaysOriginal)
        
        
        self.btn_home.selectedImage = #imageLiteral(resourceName: "homeselected").withRenderingMode(.alwaysOriginal)
        self.btn_search.selectedImage = #imageLiteral(resourceName: "search_selected").withRenderingMode(.alwaysOriginal)
        self.btnComm.selectedImage = #imageLiteral(resourceName: "Communityseleceted").withRenderingMode(.alwaysOriginal)
        self.btnlearning.selectedImage = #imageLiteral(resourceName: "learningseected").withRenderingMode(.alwaysOriginal)
        self.btnProfile.selectedImage = #imageLiteral(resourceName: "profile_selected").withRenderingMode(.alwaysOriginal)
        }
}

