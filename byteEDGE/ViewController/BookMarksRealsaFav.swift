
//  BookMarksReals.swift
//  byteEDGE
//  Created by Pawan_Kumar_Mac on 02/08/22.

import UIKit
import SDWebImage
import AVKit
import AVFoundation
import Firebase
import FirebaseDynamicLinks
import JWPlayerKit
import Alamofire
import KRProgressHUD

class BookMarksRealsaFav: UIViewController {
    
    @IBOutlet weak var mainCollection: UICollectionView!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    // MARK: -Variable
    
    public var isPlaying: Bool = false
//     var homeDataList: HomeData?
//    var dashBoardList = [DashBoardListData]()
//    var bookMark_ob: BookmarkVideo?
    var bookMark_ob = [BookmarkListData]()
    var totalObj : BookmarkData?
    var videoUrl : URL?
    var videoRow : Int = 0
    var postid: String = ""
    var filetye = Int()
    var previous_node = Jwplayer()
    var current_node =  Jwplayer()
    var next_node = Jwplayer()
    var currnetposssition  = 0
    var version: String?
    var isLoadingList : Bool = false
    var pageCount = 1
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.current_node.player.pause()
        self.previous_node.player.pause()
        self.next_node.player.pause()
        //       self.homeDataList?.dashBoardList?.removeAll()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.current_node.player.pause()
        self.previous_node.player.pause()
        self.next_node.player.pause()
    }
    
    var isCollapse = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageCount = 1
        self.favReels(pageNum: pageCount, isAppend: false)
//        self.homeData(pageNum: self.pageCount, isAppend: false)
        self.addChild(current_node)
        self.addChild(previous_node)
        self.addChild(next_node)
        current_node.didMove(toParent: self)
        previous_node.didMove(toParent: self)
        next_node.didMove(toParent: self)
        self.registerCell()
        setui()
        //        refreshpage()
    }
    
    func registerCell()
    {
     self.mainCollection.registerCell(type:ScrollCollectionIndicator.self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.pageCount = 1
        self.favReels(pageNum: pageCount, isAppend: false)
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    func refreshpage()
    {
        let refreshControl = mainCollection.addRefreshControl(target: self,action: #selector(doRefreshhome(_:)))
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle =
        NSAttributedString(string: "",attributes: [
        NSAttributedString.Key.foregroundColor: UIColor.black,
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-UltraLight",size: 36.0)! ])
    }
    
    @objc func doRefreshhome(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.tintColor = self.view.backgroundColor // store color
            self.view.backgroundColor = UIColor.black
        }) { _ in
//            self.favReels()
//            self.homeData(pageNum: self.pageCount, isAppend: false)
            self.view.backgroundColor = self.view.tintColor // restore color
        }
    }
    //MARK: -ACTIONS-
    @IBAction func notificationTap(_ sender: UIButton) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func backTap(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: false)
    }
    
    
    
    private  func setui()
    {
        self.mainCollection.isPagingEnabled = true
        mainCollection.delegate = self
        mainCollection.dataSource = self
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
    }
}

//MARK: -COLLECTION VIEW DATASOURCE-
extension BookMarksRealsaFav:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate , UIScrollViewDelegate
{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0 {
            return bookMark_ob.count ?? 0
        }
        
        else if section == 1 && self.isLoadingList == true
        {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if indexPath.section == 0
        {
            let cell = mainCollection.dequeueReusableCell(withReuseIdentifier: "HomeVedioRealsCellCollectionViewCell", for: indexPath) as! HomeVedioRealsCellCollectionViewCell
            print("byte path ::::::::::::::::::::: \(indexPath.row)")
            cell.layoutIfNeeded()
            cell.captionTap.layer.borderWidth = 0.3
            cell.captionTap.layer.cornerRadius = 15
            cell.captionTap.layer.borderColor = UIColor.white.cgColor
            
          //  if self.bookMark_ob[indexPath.row].courseId == "" || self.bookMark_ob[indexPath.row].courseData == nil || self.bookMark_ob[indexPath.row].courseId == nil{
                cell.captionTap.isHidden = true
          //
            
            if self.bookMark_ob[indexPath.row].isBookMark ?? false
            {
            cell.bookmarkTap.setImage(UIImage(named: "book"), for: .normal)
            }
            else{
            cell.bookmarkTap.setImage(UIImage(named: "unbook"), for: .normal)
            }
            
              cell.callBackbookmark =
              {
                let videoId = self.bookMark_ob[indexPath.row].videoId ?? ""
                var statusBookmark = self.bookMark_ob[indexPath.row].isBookMark // true
                  if statusBookmark == true
                  {
                   print("statusBookmark\(statusBookmark)")
                     self.addmark1(postId: videoId)
                    cell.bookmarkTap.setImage(UIImage(named: "unbook"), for: .normal)
                    self.bookMark_ob[indexPath.row].isBookMark = false
                  }
                  
                 
                  
                else
                {
                    print("statusBookmark\(statusBookmark)")
                    self.addmark1(postId: videoId)
                    cell.bookmarkTap.setImage(UIImage(named: "book"), for: .normal)
                    self.bookMark_ob[indexPath.row].isBookMark = true
                }
             }
            
            cell.callBackofTreasure = {
                if kUserDefaults.bool(forKey: "isSubscribed") == true
                {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                    vc.playlistID = self.bookMark_ob[indexPath.row].courseData?.playlistId ?? ""
                    vc.courseId =  self.bookMark_ob[indexPath.row].courseData?._id ?? ""
//                    let quizStatus = self.bookMark_ob[indexPath.row].courseData?.isQuizExist ?? false
//                    vc.isQuizExist = quizStatus
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if kUserDefaults.bool(forKey: "isSubscribed") == false
                {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
                    vc.viaHome = true
                    let id  = self.bookMark_ob[indexPath.row].courseData?._id
                    vc.courseId = id ?? ""
                    vc.mediaID  = self.bookMark_ob[indexPath.row].courseData?.previewId ?? ""
                    
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else{
                    
                }
            }
            
            
            
            cell.courseHeadinglbl.textColor = UIColor.white
            cell.lblDescription.textColor = UIColor.white
            //        print(cell.lblDescription.numberOfLines)
            
            if(indexPath.row == 0)
            {
                if UIDevice.current.userInterfaceIdiom == .pad {
                 cell.videoView.addSubview(current_node.view)
                }
                else
                {
                    self.current_node.mediaID =  self.bookMark_ob[currnetposssition].video ?? ""
                    cell.videoView.addSubview(current_node.view)
                    self.current_node.setUpPlayer(isautostart: true,volume: 0.0)
                }
            }

            
            self.postid = bookMark_ob[indexPath.row]._id ?? ""
            cell.courseHeadinglbl.text = self.bookMark_ob[indexPath.row].teasureName ?? ""
            cell.lblDescription.text = self.bookMark_ob[indexPath.row].description ?? ""
            if let setcount = (self.bookMark_ob[indexPath.row].likes)
            {
                cell.counlikeLbl.text =  String(setcount)
            }
            if  self.bookMark_ob[indexPath.row].isLiked == true{
                cell.heartbtn.setImage(UIImage(named: "selected-1"), for: .normal)
            }else{
                cell.heartbtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
            }
            
            //MARK: ShareButton
            cell.callBackofsharebtn = {
                guard let link = URL(string: "https:byteedgetest2.page.link/id=2") else { return }
                let dynamicLinksDomainURIPrefix = "https://byteedgetest2.page.link"
                let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
                linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID:"com.bytedge1")
                linkBuilder?.iOSParameters?.fallbackURL = URL.init(string: "https://apps.apple.com/us/app/byteedge/id1631500899")
                linkBuilder?.options = DynamicLinkComponentsOptions()
                guard let longDynamicLink = linkBuilder?.url else { return }
                print("The long URL is: \(longDynamicLink)")
                
                let textToShare = [ longDynamicLink ] as [Any]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                // so that iPads won't crash
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                self.present(activityViewController, animated: true, completion: nil)
                
                //            linkBuilder?.shorten() { url, warnings, error in
                //                print("ERRRRRRR")
                //                print(error)
                //                print(warnings)
                //              guard let url = url, error != nil else { return }
                //              print("The short URL is: \(url)")
                //                let textToShare = [ url ] as [Any]
                //                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                //                activityViewController.popoverPresentationController?.sourceView = self.view
                //                // so that iPads won't crash
                //                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                //                self.present(activityViewController, animated: true, completion: nil)
                //
                //            }
                
                
                
                //            if UIDevice.current.userInterfaceIdiom == .pad {
                //                let text = "This is some text that I want to share."
                //                let someText:String = "Hello want to share text also"
                //                let objectsToShare:URL = URL(string: "https://byteedgetest2.page.link/\(self.dashBoardList[indexPath.row].video)")!
                //                let textToShare = [ text, objectsToShare ] as [Any]
                //                let activityVC = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                //                let nav = UINavigationController(rootViewController: activityVC)
                //                nav.modalPresentationStyle = UIModalPresentationStyle.popover
                //                let popover = nav.popoverPresentationController as UIPopoverPresentationController?
                //                popover?.sourceView = self.view
                //                popover?.sourceRect = //self.frame
                //                CGRect(x: self.view.bounds.midX, y: self.view.bounds.midX,width: 500,height: 0)
                //                self.present(nav, animated: true, completion: nil)
                
                //              }
                //              else
                //              {
                //                let text = "ByteEdge"
                //                let someText:String = "Hello want to share text also"
                //                let objectsToShare:URL = URL(string: "https://byteedgetest2.page.link")!
                //
                //
                //                let textToShare = [ text,objectsToShare] as [Any]
                //                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                //                activityViewController.popoverPresentationController?.sourceView = self.view
                //                // so that iPads won't crash
                //                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                //                self.present(activityViewController, animated: true, completion: nil)
                //            }
            }
            
            cell.callBackofLike =
            {
                var count = self.bookMark_ob[indexPath.row].likes ?? 0
                let status = self.bookMark_ob[indexPath.row].isLiked
                if status == false
                {
                    count += 1
                    self.likePost(islike: status ?? false, postId: self.postid)
                    cell.heartbtn.setImage(UIImage(named: "selected-1"), for: .normal)
                    self.bookMark_ob[indexPath.row].likes = count
                    self.bookMark_ob[indexPath.row].isLiked = true
                    cell.counlikeLbl.text = "\(count)"
                }
                
                else
                {
                    count -= 1
                    self.likePost(islike: status ?? true, postId: self.postid)
                    cell.heartbtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
                    self.bookMark_ob[indexPath.row].isLiked = false
                    self.bookMark_ob[indexPath.row].likes = count
                    cell.counlikeLbl.text = "\(count)"
                }
            }
            return  cell
        }
        else
        {
            let cell = mainCollection.dequeueReusableCell(withReuseIdentifier: "ScrollCollectionIndicator", for: indexPath) as! ScrollCollectionIndicator
            cell.indicator.startAnimating()
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let contenHeight = scrollView.contentSize.height
        if offset > contenHeight - scrollView.frame.height && self.totalObj?.count ?? 0 > bookMark_ob.count{

            if !self.isLoadingList{
                self.isLoadingList = true
                let indexSet = IndexSet(integer: 0)
                self.mainCollection.reloadSections(indexSet)
                DispatchQueue.main.async {
                    self.pageCount = self.pageCount + 1
                    self.favReels(pageNum: self.pageCount, isAppend: true)
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        findCenterIndex()
        print("scroll current position =====> \(currnetposssition)")
        let vcell = self.mainCollection.visibleCells
        if (vcell.count > 0)
        {
            print("current position =====> \(currnetposssition)")
            if let previous_cell = self.mainCollection.cellForItem(at: IndexPath(row: currnetposssition - 1, section: 0)) as? HomeVedioRealsCellCollectionViewCell
            {
                self.previous_node.mediaID =  self.bookMark_ob[currnetposssition - 1].video ?? "https://cdn.jwplayer.com/manifests/Zlt6Luyf.m3u8"
                previous_cell.videoView.addSubview(previous_node.view)
                self.previous_node.setUpPlayer(isautostart: false, volume:0.0)
            }
            
            if let current_cell = self.mainCollection.cellForItem(at: IndexPath(row: currnetposssition, section: 0)) as? HomeVedioRealsCellCollectionViewCell
            {
                self.current_node.mediaID =  self.bookMark_ob[currnetposssition].video ?? "https://cdn.jwplayer.com/manifests/Zlt6Luyf.m3u8"
                current_cell.videoView.addSubview(current_node.view)
                self.current_node.setUpPlayer(isautostart: true, volume: 0.0)
            }
            
            if let next_cell = self.mainCollection.cellForItem(at: IndexPath(row: currnetposssition + 1, section: 0)) as? HomeVedioRealsCellCollectionViewCell
            {
                self.next_node.mediaID =  bookMark_ob[currnetposssition + 1].video ?? "https://cdn.jwplayer.com/manifests/Zlt6Luyf.m3u8"
                next_cell.videoView.addSubview(next_node.view)
                self.next_node.setUpPlayer(isautostart: false,volume: 0.0)
            }
        }
    
    }
    
    func findCenterIndex()
    {
        let center = self.view.convert(self.mainCollection.center, to: self.mainCollection)
        let index = mainCollection.indexPathForItem(at: center)
        currnetposssition = index? .row ?? 0
        print(index ?? "index not found")
    }
    
    // Called when the cell is displayed
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("end display ========> \(indexPath.row)")
        print("end current position =====> \(currnetposssition)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isLoadingList {
            if indexPath.section == 0{
                return CGSize.init(width: self.mainCollection.frame.size.width, height: self.mainCollection.frame.size.height - 48)
            }else{
             return CGSize.init(width: self.mainCollection.frame.size.width, height: 48)
            }
            
        }else{
            if UIDevice.current.userInterfaceIdiom == .pad
            {
            return CGSize.init(width: self.mainCollection.frame.size.width, height: self.mainCollection.frame.size.height)
            }
            else
            {
             return CGSize.init(width: self.mainCollection.frame.size.width, height: self.mainCollection.frame.size.height)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
}

// MARK: -HIT HOME API-
extension BookMarksRealsaFav {
    
    func favReels(pageNum:Int,isAppend:Bool?)
    {
        var param: [String: Any] = [:]
        param["page"] = pageNum
       KRProgressHUD.show()
       ServiceManager.instance.request(method: .get,
                                       apiURL: Api.favReels.url,
                                       headers: CommonUtility.instance.headers,
                                       parameters: nil,
                                       decodable: BookmarkVideo.self,
                                       encoding: URLEncoding.default
       )
       { (status, json, model, error) in
           KRProgressHUD.dismiss()
           if model?.status == 200
           {
               print(model)
               self.isLoadingList = false
//               self.bookMark_ob.removeAll()
               if isAppend == true
               {
                   if model?.data?.bookmark?.count ?? 0 > 0{
                       for i in 0...(model?.data?.bookmark?.count ?? 0) - 1{
                       self.bookMark_ob.append((model?.data?.bookmark?[i])!)
                       }
                   }
               }
               
               else
               {
                   self.bookMark_ob = model?.data?.bookmark ?? []
                   self.totalObj = model?.data
               }

//               self.isLoadingList = false
               DispatchQueue.main.async {
               self.mainCollection.reloadData()
               }
           }
       }
   }
    
    func addmark1(postId :String)
    {
        var param: [String: Any] = [:]
        param["videoId"] = postId
//        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.addBookmark.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: CommonResponseModel.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
               print(message)
            }
            else
            {
            
            }
        }
    }
    
//    func addBookMark(postId :String) {
//        var param: [String: Any] = [:]
//        param["videoId"] = postId
//        ServiceRequest.instance.addBookMark(param) { (isSucess, response, error) in
//            guard response?.status == 200 else {
//                return
//            }
//        }
//    }
    
    
    
    
    
}

//    func homeData(pageNum:Int,isAppend:Bool?){
//        var param: [String: Any] = [:]
//        param["page"] = "1"
//        ServiceManager.instance.request(
//            method: .get,
//            apiURL: Api.homeData.url + "?page=\(pageNum)",
//            headers: CommonUtility.instance.headers,
//            parameters: nil,
//            //parameters: param,
//            decodable: Home.self)
//        { (status, json, model, error) in
//            if model?.status == 200 {
//                kUserDefaults.set("2", forKey: "signFlow")
//            }
//            guard model?.status == 200 else {
//                print(model)
//                return
//            }
//
//            if isAppend == true{
//                if model?.data?.dashBoardList?.count ?? 0 > 0{
//                    for i in 0...(model?.data?.dashBoardList?.count ?? 0) - 1{
//                        self.dashBoardList.append((model?.data?.dashBoardList?[i])!)
//                    }
//                }
//
//            }
//
//            else
//            {
//                self.dashBoardList = model?.data?.dashBoardList ?? []
//                self.homeDataList = model?.data
//            }
//
//            self.isLoadingList = false
//            //            Loader.isLoader(show: false)
//            DispatchQueue.main.async {
//                self.mainCollection.reloadData()
//            }
//
//        }
//    }
//}

//MARK: -LIKE BUTTON API-
extension BookMarksRealsaFav {
    func likePost(islike: Bool, postId : String) {
        var param: [String: Any] = [:]
        param["teasureId"] = postId
        param["role"] = "teasurelike"
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                //                print(response?.status as Any)
                self.createAlert(title: kAppName, message: response?.message ?? "")
                return
            }
        }
    }
}



