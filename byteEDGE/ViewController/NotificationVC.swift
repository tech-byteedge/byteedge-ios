
import UIKit
import  KRProgressHUD
import  Alamofire
import KRProgressHUD
import SwiftUI
import SDWebImage

class NotificationVC: UIViewController {
    @IBOutlet weak var tbl_noti: UITableView!
    @IBOutlet weak var top_view: UIView!
    
    var array :[Int] = [0]
    var page_no = Int()
    var total = Int()
    var lastpage = Int()
    var notificationModel: NotificationModel?
    override func viewDidLoad() {
        super.viewDidLoad()
       self.notificationApi()
        setui()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        dateobj = ""
        page_no = 0
        tbl_noti.reloadData()
      self.notificationApi()
    }
    
    private func setui()
    {
        tbl_noti.delegate = self
        tbl_noti.dataSource = self
    }
    
    @IBAction func backTap(_sender: UIButton)
    {
    self.navigationController?.popViewController(animated: false)
    }
}

extension NotificationVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if notificationModel?.data?.notificationList?.count == 0
        {
        self.tbl_noti.setEmptyMessageshownotification()
        }
        else
        {
         self.tbl_noti.restoredata()
        }
        return notificationModel?.data?.notificationList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return notificationModel?.data?.notificationList?[section].notification?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return CommonUtility.instance.getDate(inputDate: notificationModel?.data?.notificationList?[section].id ?? "")
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .black
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 20, y: 5, width: 320, height: 20)
        myLabel.font = UIFont.boldSystemFont(ofSize: 16)
        myLabel.textColor = #colorLiteral(red: 1, green: 0.9999999404, blue: 0.9999999404, alpha: 1)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        let headerView = UIView()
        headerView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        headerView.addSubview(myLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbl_noti.dequeueReusableCell(withIdentifier:"NotificationCell", for: indexPath) as! NotificationCell
               if notificationModel?.data?.notificationList?[indexPath.section].count == 0
               {
                cell.height_constant.constant = 0
                cell.bottam_cons.constant = -10
                cell.cell_view.roundSpecificCorners(corners: [.allCorners], radius: 0)
            }   else if notificationModel?.data?.notificationList?[indexPath.section].count == 1 {
                cell.height_constant.constant = 10
                cell.bottam_cons.constant = 10
                DispatchQueue.main.async {
                cell.cell_view.roundSpecificCorners(corners: [.allCorners], radius: 16)
                }
            }

              else
               {
                    if indexPath.row == 0 {
                    cell.height_constant.constant = 10 // top
                    cell.bottam_cons.constant = -10
                        DispatchQueue.main.async {
                    cell.cell_view.roundSpecificCorners(corners: [.topLeft, .topRight], radius: 16)
                        }

                    } else if indexPath.row == (notificationModel?.data?.notificationList?[indexPath.section].count)! - 1 {
                    DispatchQueue.main.async {
                    cell.height_constant.constant = 0
                    }
                    cell.bottam_cons.constant = 10
                    DispatchQueue.main.async {
                    cell.cell_view.roundSpecificCorners(corners: [.bottomLeft, .bottomRight], radius: 16)
                    }
                }else {

                    cell.height_constant.constant = 0
                    cell.bottam_cons.constant = 0
                    DispatchQueue.main.async {
                    cell.cell_view.roundSpecificCorners(corners:[.allCorners], radius: 0)
                    }
                }
            }
        
        cell.timeNotification.text = CommonUtility.instance.gettimeConversion12(time24: notificationModel?.data?.notificationList?[indexPath.section].notification?[indexPath.row].createdAt ?? "")
        cell.title.text = notificationModel?.data?.notificationList?[indexPath.section].notification?[indexPath.row].title ?? ""
        cell.subtitle.text = notificationModel?.data?.notificationList?[indexPath.section].notification?[indexPath.row].description ?? ""
        let urlImage = notificationModel?.data?.notificationList?[indexPath.section].notification?[indexPath.row].image ?? ""
        cell.img.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
        
           cell.img.layer.cornerRadius = cell.img.frame.size.height/2;
           cell.img.layer.masksToBounds = true
           cell.img.clipsToBounds = true
          cell.img.layer.borderWidth=1;
            cell.cell_view.dropShadow()
            cell.title.numberOfLines = 0
            return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
//}

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.section == notificationModel?.data?.notificationList!.count ?? 0 - 1 && page_no <= lastpage else {return}
        page_no = page_no + 1
        notificationApi()

    }
    
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//                let deleteAction = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
//                    self.array.remove(at: indexPath.section)
//                    tableView.deleteRows(at: [indexPath], with: .fade)
//                    self.tbl_noti.reloadData()
//                    completion(true)
//                }
//            deleteAction.image = UIImage(named: "deleted")
//            deleteAction.backgroundColor = UIColor.systemPink
//            deleteAction.title = "Delete"
//            return UISwipeActionsConfiguration(actions: [deleteAction])
//            }
    
}


//MARK: API----
extension NotificationVC
{
 func notificationApi()
  {
    ServiceManager.instance.request(method: .get,
    apiURL: Api.notification.url,
    headers: CommonUtility.instance.headers,
    parameters: nil,
    decodable: NotificationModel.self,
    encoding: URLEncoding.default)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
            self.notificationModel = model!
            DispatchQueue.main.async {
            self.tbl_noti.reloadData()
                    }
            }
            else
            {
            self.createAlert(title: AppName, message:model?.message ?? "")
            }

        }
    }
}


