
//  CourseDetailshowVc.swift
//  byteEDGE
//  Created by Pawan Kumar on 03/03/22.

import UIKit
import  KRProgressHUD
import  Alamofire
 import  SDWebImage
import  Foundation
import  JWPlayerKit
import FirebaseDynamicLinks

class CourseDetailshowVc: UIViewController {
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var courseTblView: UITableView!
    @IBOutlet weak var disCription: UILabel!
    @IBOutlet weak var playerview: UIView!
    @IBOutlet weak var courselb: UILabel!
    @IBOutlet weak var imgthumbnail: UIImageView!
    @IBOutlet weak var heightCon: NSLayoutConstraint!
    @IBOutlet weak var startQuiz: UIButton!
    @IBOutlet weak var rating_lbl: UILabel!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var ratingView: UIView!
    
    var selectedIndex = 0
    var viaDeepLink = false
    var objSub: PurchaseCourseModel?
    var playlistID = ""
    var viaSplashCourseDetail = false
    var  courseId = ""
    var back = false
    var couseName = ""
    var courseDis = ""
    var videoUrlString = ""
    var mediaiD = ""
    var titileSet = ""
    var image = ""
    var isGetNotify = false
    var isQuizExist = true
    var courseUuid = String()
    var viewCompleted = false
    var disCriptionSet = ""
    var callbackindex:(()->())?
    var player_obj = Jwplayer()
    var particularCourse_obj : ParticularCoursemodel?
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.registercell()
        self.subsCriptionApi()
        self.particularcourse()
        NotificationCenter.default.addObserver(self, selector: #selector(CourseDetailshowVc.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(courseUpdate),name: NSNotification.Name ("refrseh"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.jwplayerPlaylistHasCompleted(notification:)), name: Notification.Name("jwplayerPlaylistHasCompleted"), object: nil)
        self.setui()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    AppUtility.lockOrientation(.all)
//        self.rotated()
//        rotationchange()
   
    }
    
    private func setui()
    {
        self.shareView.layer.borderWidth = 1
        self.shareView.layer.cornerRadius = 12
        self.ratingView.layer.cornerRadius = 12
        self.shareView.layer.borderColor = UIColor.white.cgColor
    }
    
    
    private func rotationchange()
    {
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
           //add the observer
           NotificationCenter.default.addObserver(
               self,
               selector: #selector(orientationChanged(notification:)),
               name: UIDevice.orientationDidChangeNotification,
               object: nil)
        
    }
    
    
    @objc func orientationChanged(notification : NSNotification) {
            print("orientationChanged")
        }
    
    @objc func jwplayerPlaylistHasCompleted(notification: Notification) {

            if ((self.objSub?.playlist.count ?? 0) - 1) != self.selectedIndex
            {
                

            self.selectedIndex = self.selectedIndex + 1
            
            let mediaId =  self.objSub?.playlist[self.selectedIndex].mediaid ?? ""
            self.player_obj.mediaID = mediaId
            self.player_obj.setUpPlayer(isautostart: true, volume: 0.0)
                
                DispatchQueue.main.async {
                self.updatecourse(courseId: self.courseId, playlistId: self.playlistID, mediaId: mediaId)
                }
                
        self.updatecourse(courseId: self.courseId, playlistId: self.playlistID, mediaId: mediaId)
        let title =  self.objSub?.playlist[self.selectedIndex ].title ?? ""
        self.titileSet = title
        DispatchQueue.main.async {
        self.courselb.text = self.titileSet
        let description =  self.objSub?.playlist[self.selectedIndex].description ?? ""
        self.disCriptionSet = description
//        self.disCription.text = self.disCriptionSet
        }
        }
        else
        {
            let mediaId =  self.objSub?.playlist[0].mediaid ?? ""
            self.player_obj.mediaID = mediaId
            self.player_obj.setUpPlayer(isautostart: true, volume: 0.0)
            let title =  self.objSub?.playlist[0].title ?? ""
            self.titileSet = title
                    DispatchQueue.main.async {
            self.courselb.text = self.titileSet
            let description =  self.objSub?.playlist[0].description ?? ""
            self.disCriptionSet = description
//            self.disCription.text = self.disCriptionSet
                    }
        }
    }
    
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            print(selectedIndex)
        }

        if UIDevice.current.orientation.isPortrait {
            print("Portrait")
            print(selectedIndex)
        }
    }
        
        
//        if ((objSub?.playlist.count ?? 0) - 1) != selectedIndex{
//        DispatchQueue.main.async {
//        var indexPath = IndexPath(row: self.selectedIndex+1, section: 0)
//
//        self.courseTblView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
//        self.courseTblView.delegate?.tableView!(self.courseTblView, didSelectRowAt: indexPath)
//        }
//        }
//         else{
//            DispatchQueue.main.async {
//              //  AppUtility.lockOrientation(.portrait)
//            var indexPath = IndexPath(row: 0, section: 0)
//            self.courseTblView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
//            self.courseTblView.delegate?.tableView!(self.courseTblView, didSelectRowAt: indexPath)
//            }
//        }
        
    
    override func viewWillDisappear(_ animated: Bool) {
     AppUtility.lockOrientation(.portrait)
    }
    
    
    @objc func courseUpdate(notification: Notification) {
        self.particularcourse()
        self.courseTblView.restoredata()
    }
    func Animation()
    {
        UIView.animate(withDuration: 7.0, delay: 0.0, options:( [.curveEaseInOut,.repeat]), animations: {
            self.courselb.transform = CGAffineTransform(translationX: self.courselb.bounds.origin.x + 400, y: self.courselb.bounds.origin.y)
            self.courselb.transform = CGAffineTransform(translationX: self.courselb.bounds.origin.x-self.view.bounds.size.width, y: self.courselb.bounds.origin.y)

                    }, completion: { (finished: Bool) -> Void in
            self.courselb.transform = CGAffineTransform(translationX:   self.courselb.bounds.origin.x - 800, y: self.courselb.bounds.origin.y)
                      });
    }
    
    private func registercell()
    {
        //MARK: delegate & datasouce
        self.courseTblView.delegate = self
        self.courseTblView.dataSource = self
        courseTblView.registerCell(type: PurchaseHistoryTableViewCell.self)
    }
    
    
    func showToast(controller: UIViewController, message : String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.red
        alert.view.alpha = 0.4
        alert.view.layer.cornerRadius = 15
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
        alert.dismiss(animated: true)
        }
    }
    
    //MARK: Action
    @IBAction func shareBtn(_ sender:UIButton){
        
        let urlStrig  = "https://byteedgetest2.page.link/dynamic_link?courseId=\(courseId)&mediaId=\(playlistID)"
        guard let link = URL(string: urlStrig) else { return }
        let dynamicLinksDomainURIPrefix = "https://byteedgetest2.page.link"
        print(link)
        let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID:"com.bytedge1")
        linkBuilder?.iOSParameters?.appStoreID = "1631500899"
        linkBuilder?.iOSParameters?.minimumAppVersion = "1.3"
        linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.app.byteedge")
        linkBuilder?.androidParameters?.minimumVersion = 1
        linkBuilder?.androidParameters?.fallbackURL = URL.init(string: "https://play.google.com/store/apps/details?id=com.app.byteedge")
        linkBuilder?.iOSParameters?.fallbackURL = URL.init(string: "https://apps.apple.com/us/app/byteedge/id1631500899")
        linkBuilder?.iOSParameters?.customScheme = "com.bytedge1"
        guard let longDynamicLink = linkBuilder?.url else { return }
        print("The long URL is: \(longDynamicLink)")
       
           
        let textToShare = [ longDynamicLink ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        // so that iPads won't crash
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func back(_sender: UIButton)
    {
        if viaDeepLink{
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
           
                    let nav_obj = UINavigationController(rootViewController: vc)
                    nav_obj.navigationBar.isHidden = true
                    UIApplication.shared.windows.first?.rootViewController = nav_obj
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
      
        
    }
    
    @IBAction func playVideo(_ sender: Any) {
        
    let vc = AppStoryboard.Quiz.instance.instantiateViewController(withIdentifier: "QuizVC") as! QuizVC
    vc.CourseIdQuiz = courseId
    vc.courseUid = courseUuid
     self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    @IBAction func downloadVideoTap(_ sender: UIButton) {
//    showToast(controller: self, message: "UnderDevelopment", seconds: 1.0)
//    }
    
}
extension CourseDetailshowVc: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return   objSub?.playlist.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueCell(withType: PurchaseHistoryTableViewCell.self,for: indexPath) as!PurchaseHistoryTableViewCell
        cell.lessonLbl.isHidden = true
        cell.lessonLbl.backgroundColor = UIColor.black
        cell.lessonLbl.textColor = UIColor.white
        cell.lessonLbl.isUserInteractionEnabled = true
        cell.titleLbl.text = objSub?.playlist[indexPath.row].title
        cell.byUserLbl.text = objSub?.playlist[indexPath.row].description
        let urlImage =  objSub?.playlist[indexPath.row].image ?? ""
        cell.projectImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named:"logo"), options: .highPriority, context: nil)
        cell.dateLbl.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        selectedIndex = indexPath.row
        print("selectedIndex\(selectedIndex)")
        let mediaId =  objSub?.playlist[indexPath.row].mediaid ?? ""
        self.mediaiD = mediaId
        player_obj.mediaID = mediaId
        player_obj.setUpPlayer(isautostart: true, volume: 5.0)
        
        DispatchQueue.main.async {
        self.updatecourse(courseId: self.courseId, playlistId: self.playlistID, mediaId: mediaId)
        }
        
        let title =  objSub?.playlist[indexPath.row].title ?? ""
        self.titileSet = title
        DispatchQueue.main.async {
        self.courselb.text = self.titileSet
        }
        let description =  objSub?.playlist[indexPath.row].description ?? ""
        self.disCriptionSet = description
//        self.disCription.text = disCriptionSet
//        self.courseTblView.reloadData()
        
        if (indexPath.row > 2)
        {
               if UIDevice.current.orientation.isLandscape {
                let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "RatingVc") as! RatingVc
                  vc.courseId = self.courseId
                  vc.modalPresentationStyle = .automatic
                  vc.providesPresentationContextTransitionStyle = true
                  vc.definesPresentationContext = true
                  vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                  vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2)
                  self.present(vc, animated: true, completion: nil)
              
            }
           else
           {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "RatingVc") as! RatingVc
              vc.courseId = self.courseId
              vc.modalPresentationStyle = .automatic
              vc.providesPresentationContextTransitionStyle = true
              vc.definesPresentationContext = true
              vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
              vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.2)
              self.present(vc, animated: true, completion: nil)
            
        }
    }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK: API--
extension CourseDetailshowVc
{
 func subsCriptionApi()
        {
            KRProgressHUD.show()
            ServiceManager.instance.request(method: .get,
                                            apiURL:"https://cdn.jwplayer.com/v2/playlists/\(self.playlistID)",
                                            headers: CommonUtility.instance.headers,
                                            parameters: nil,
                                            decodable: PurchaseCourseModel.self,
                                            encoding: URLEncoding.default
            )
            { (status, json, model, error) in
                KRProgressHUD.dismiss()
                self.objSub = model
                let mediaId =  self.objSub?.playlist.first?.mediaid ?? ""
                self.mediaiD = mediaId
                self.player_obj.providesPresentationContextTransitionStyle = true
                self.player_obj.definesPresentationContext = true
                self.player_obj.mediaID = mediaId
                self.addChild(self.player_obj)
                self.player_obj.view.frame = self.playerview.bounds
                self.playerview.addSubview(self.player_obj.view)
                self.player_obj.setUpPlayer(isautostart: true , volume: 5.0)
                
                self.courselb.text = self.objSub?.playlist.first?.title ?? "title"
//                self.disCription.text = self.objSub?.playlist[0].description ?? ""
                DispatchQueue.main.async {
                self.updatecourse(courseId: self.courseId, playlistId: self.playlistID, mediaId: mediaId)
                self.courseTblView.reloadData()
                }
            }
        }
    
    func updatecourse(courseId:String,playlistId:String,mediaId:String)
    {
        var param: [String: Any] = [:]
        param["courseId"] = courseId;
        param["playlistId"] = playlistId;
        param["mediaId"] = mediaId
//        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.updateCourse.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable:  CommonRes.self,
                                        encoding: JSONEncoding.default
        )
        { (status, json, model, error) in
//            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                print(model)
                DispatchQueue.main.async {
                self.courseTblView.reloadData()
                }
            }
            else
            {
            }
        }
    }
    
    func particularcourse()
    {
        var param: [String: Any] = [:]
        param["courseId"] = courseId;
//        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.particularCourse.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable:  ParticularCoursemodel.self,
                                        encoding: URLEncoding.default
        )
        { (status, json, model, error) in
//            KRProgressHUD.dismiss()
            if model?.status == 200
            {
             self.particularCourse_obj = model
            let rating = Double(/model?.data?.courseDetail.rating)
            self.rating_lbl.text = String(rating)
             print(model)
            }
            else
            {
            }
            if self.particularCourse_obj?.data?.courseDetail.openQuiz == true{
                self.startQuiz.isHidden = false
                self.heightCon.constant = 60
            }else{
                self.startQuiz.isHidden = true
                self.heightCon.constant = 5
            }
        }
    }
}

