
import UIKit
import SDWebImage
import AVKit
import AVFoundation
import Firebase
import FirebaseDynamicLinks
class Homevc: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: -VARIABLES-
    var homeDataList: HomeData?
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var videoUrl : URL?
    var videoRow : Int = 0
    var postid: String = ""
    // MARK: -OUTLETS
    @IBOutlet weak var viewForVideo: UIView!
    @IBOutlet weak var centreButton: UIButton!
    @IBOutlet weak var ScrollCollection: UICollectionView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var shareBlurView: UIView!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setui()
     //  self.homeData()
        print(kUserDefaults.retriveString(.token))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        homeData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.pause()
        centreButton.isHidden = false
        player?.seek(to: .zero)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.layer.zPosition =  UIDevice.current.orientation.isLandscape ? -1 : 0
            
            self.playerLayer?.frame = self.view.bounds
            self.playerLayer?.videoGravity = .resize
        }
    }
    
    //MARK: -ACTIONS-
    
    @IBAction func notificationTap(_ sender: UIButton) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func tappedOnShareBtn(_ sender: Any) {
        
        //        guard let link = URL(string: "https://www.byteedge.com/my-page") else { return }
        //        let dynamicLinksDomainURIPrefix = "https://byteedgetest2.page.link"
        //        let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
        //        linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.bytedge1")
        //        guard let longDynamicLink = linkBuilder?.url else { return }
        //        print("The long URL is: \(longDynamicLink)")
        //        DynamicLinkComponents.shortenURL(longDynamicLink, options: nil) { url, warnings, error in
        //            if let url = url {
        //                print("The short URL is: \(url)")
        //                let vc = ShareViaViewController.instantiate(fromAppStoryboard: .Account)
        //                vc.modalPresentationStyle = .popover
        //                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //                //                vc.outerView.backgroundColor = UIColor.init(white: 0.0, alpha: 0.51)
        //                vc.callBack = { index in
        //                    switch index{
        //                    case 0 :
        //                    self.openWhatsApp()
        //                    case 1 :
        //                        self.openLinkedIn()
        //                    case 2 :
        //                        self.openInsta()
        //
        //                    case 3 :
        //                        UIPasteboard.general.string = "\(url)"
        //                        self.showtoast()
        //
        //
        //                    default :
        //                        UIPasteboard.general.string = "\(url)"
        //                        self.showtoast()
        //                    }
        //                    self.dismiss(animated: true)
        //                }
        //                self.present(vc, animated: false, completion: nil)
        //
        //            }
        //
        //
        //        }
        
                   

        if UIDevice.current.userInterfaceIdiom == .pad {
            let text = "This is some text that I want to share."
            let someText:String = "Hello want to share text also"
            let objectsToShare:URL = URL(string: "https://byteedgetest2.page.link")!
            let textToShare = [ text, objectsToShare ] as [Any]
                       let activityVC = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)

                       let nav = UINavigationController(rootViewController: activityVC)
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController as UIPopoverPresentationController?

            popover?.sourceView = self.view
            popover?.sourceRect = (sender as AnyObject).frame
            //CGRect(x: self.view.bounds.midX, y: 500,width: 1000,height: 0)
           // (sender as AnyObject).frame

            self.present(nav, animated: true, completion: nil)
        }
        else{
            let text = "This is some text that I want to share."
            let someText:String = "Hello want to share text also"
            let objectsToShare:URL = URL(string: "https://byteedgetest2.page.link")!
            let textToShare = [ text,objectsToShare ] as [Any]
               let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
               activityViewController.popoverPresentationController?.sourceView = self.view

// so that iPads won't crash
               activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
               self.present(activityViewController, animated: true, completion: nil)
        }


    }
    
    func showtoast() {
        DispatchQueue.main.async {
        self.showToast(message: "Link copied", font: .systemFont(ofSize: 12.0))
        }
        
    }
    
    @IBAction func tappedOnCenterBtn(_ sender: Any) {
        player?.play()
        centreButton.isHidden = true
    }
    @IBAction func tappedOnLikeBtn(_ sender: Any) {
        
        UIApplication.topViewController()?.view.isUserInteractionEnabled = false
        if likeBtn.imageView?.image == UIImage(named: "likeHomeIcon") {
            likeBtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
            likePost(islike: false, postId: self.postid, section: videoRow)
            
            
        }else {
            likeBtn.setImage(UIImage(named: "likeHomeIcon"), for: .normal)
            likePost(islike: true, postId: self.postid, section: videoRow)
        }
    }
    
    private  func setui()
    {
        viewForVideo.backgroundColor = .clear
        self.ScrollCollection.delegate = self
        self.ScrollCollection.dataSource = self
        bottomView.clipsToBounds = true
        bottomView.layer.cornerRadius = 20
        bottomView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        ////by gaurav for blur
        shareBlurView.makeRounded()
        bottomView.backgroundColor = .clear
        shareBlurView.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.backgroundColor = .clear
        blurEffectView.frame = bottomView.bounds
        blurEffectView.alpha = 60.0
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bottomView.addSubview(blurEffectView)
        
        
        let blurEffectShareView = UIVisualEffectView(effect: blurEffect)
        blurEffectShareView.backgroundColor = .clear
        blurEffectShareView.frame = shareBlurView.bounds
        blurEffectShareView.alpha = 60.0
        
        shareBlurView.addSubview(blurEffectShareView)
        
        
        ScrollCollection.clipsToBounds = true
        ScrollCollection.layer.cornerRadius = 20
        ScrollCollection.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        ///gesture for play and pause video
        ///let gestureForRightView = UITapGestureRecognizer(target: self,
        let longPressGestureForLeftView = UILongPressGestureRecognizer(target: self, action: #selector(tapPlayPauseAction))
        longPressGestureForLeftView.minimumPressDuration = 0.1
        longPressGestureForLeftView.delaysTouchesBegan = true
        longPressGestureForLeftView.delegate = self
        viewForVideo.addGestureRecognizer(longPressGestureForLeftView)
        
        //like button
        likeBtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
        likeCountLbl.setAppFontColor(.appColor(.white), font:  .ProductSans(.regular, size: .oneSeven))
        likeCountLbl.text = "0"
        shareBtn.setImage(UIImage(named: "shareBtnIcon"), for: .normal)
        shareBtn.makeRounded()
        
    }
    
    @objc func tapPlayPauseAction(gestureReconizer: UILongPressGestureRecognizer) {
        
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            //When lognpress is start or running
            player?.pause()
        }else {
            //When lognpress is finish
            player?.play()
            centreButton.isHidden = true
            
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
        self.centreButton.isHidden = false
        player?.seek(to: .zero)
    }
    
    func addVideoPlayer(videoUrl: URL, to view: UIView) {
        view.layer.sublayers?
            .filter { $0 is AVPlayerLayer }
            .forEach { $0.removeFromSuperlayer() }
        player = AVPlayer(url: videoUrl)
        playerLayer = AVPlayerLayer(player: player)
        player?.automaticallyWaitsToMinimizeStalling = false
        playerLayer?.backgroundColor = UIColor.black.cgColor
        playerLayer?.frame.size.height = view.frame.size.height
        playerLayer?.frame.size.width = view.frame.size.width
        playerLayer?.videoGravity = .resize
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        if let tempLayer = playerLayer {
            view.layer.addSublayer(tempLayer)
        }
    }
    
}

//MARK: -COLLECTIONVIEW DELEGATE-
extension Homevc : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        self.centreButton.isHidden = false
        player?.pause()
        player?.seek(to: .zero)
        if let videoUrl = self.homeDataList?.dashBoardList?[indexPath.row].video, let url = URL(string: videoUrl), let postId = self.homeDataList?.dashBoardList?[indexPath.row]._id {
            self.addVideoPlayer(videoUrl: url, to: self.viewForVideo)
            self.videoRow = indexPath.row
            self.postid = postId
            if homeDataList?.dashBoardList?[indexPath.row].isLiked == true  {
                self.likeBtn.setImage(UIImage(named: "likeHomeIcon"), for: .normal)
            }else {
                self.likeBtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
            }
            if homeDataList?.dashBoardList?.count != 0 {
                if let count = homeDataList?.dashBoardList?[0].likes  {
                    if count == 0 {
                        self.likeCountLbl.text = ""
                    }else {
                        self.likeCountLbl.text = "\(count)"
                    }
                }else {
                    self.likeCountLbl.text = ""
                }
            }else {
                print("array is empty")
            }
           
        }else {
            print("video not found")
        }
    }
}
//MARK: -COLLECTION VIEW DATASOURCE-
extension Homevc:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homeDataList?.dashBoardList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = ScrollCollection.dequeueReusableCell(withReuseIdentifier: "VedioScrollCellCollectionViewCell", for: indexPath) as! VedioScrollCellCollectionViewCell
        cell.celI_mg.makeRounded()
        if let image = self.homeDataList?.dashBoardList?[indexPath.row].thumbnails, let url = URL(string: image) {
         cell.celI_mg.sd_setImage(with: url , placeholderImage: UIImage(named: "SplashThird"), options: SDWebImageOptions.continueInBackground )
        }else {
            cell.celI_mg.image = UIImage(named: "SplashThird")
        }
        cell.callBack = {
            self.centreButton.isHidden = false
            self.player?.pause()
            self.player?.seek(to: .zero)
            if let videoUrl = self.homeDataList?.dashBoardList?[indexPath.row].video, let url = URL(string: videoUrl), let postId = self.homeDataList?.dashBoardList?[indexPath.row]._id {
                self.addVideoPlayer(videoUrl: url, to: self.viewForVideo)
                self.videoRow = indexPath.row
                self.postid = postId
                if self.homeDataList?.dashBoardList?[indexPath.row].isLiked == true  {
                    self.likeBtn.setImage(UIImage(named: "likeHomeIcon"), for: .normal)
                }else {
                    self.likeBtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
                }
                
                if let count = self.homeDataList?.dashBoardList?[indexPath.row].likes  {
                    if count == 0 {
                        self.likeCountLbl.text = ""
                    }else {
                        self.likeCountLbl.text = "\(count)"
                    }
                }else {
                self.likeCountLbl.text = ""
                }
            }else {
                print("video not found")
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionWidth = collectionView.bounds.width
        let count:  CGFloat = UIDevice.current.orientation.isLandscape ? 10 : 5
        return CGSize(width: collectionWidth/count, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return -5
    }
}

//MARK: -HIT HOME API-
extension Homevc {
    
    func homeData(){
        Loader.isLoader(show: true)
        ServiceManager.instance.request(
            method: .get,
            apiURL: Api.homeData.url,
            headers: CommonUtility.instance.headers,
            parameters: nil,
            decodable: Home.self)
        { (status, json, model, error) in
            
            if model?.status == 200 {
                kUserDefaults.set("2", forKey: "signFlow")
            }
            guard model?.status == 200 else {
                print(model?.status as Any)
                Loader.isLoader(show: false)
               // self.createAlert(title: kAppName, message:model?.message ?? "")
                return
            }
           
          
            guard model?.data?.dashBoardList?.count != 0  else{
                self.centreButton.isHidden = true
                self.ScrollCollection.isHidden = true
                self.bottomView.isHidden = true
                Loader.isLoader(show: false)
                return
            }
            if let videoUrl = self.homeDataList?.dashBoardList?[0].video,
                let url = URL(string: videoUrl) {
                self.addVideoPlayer(videoUrl: url, to: self.viewForVideo)
            }else {
                print("video not found")
            }
            if model?.data?.dashBoardList?[0].isLiked == true  {
                self.likeBtn.setImage(UIImage(named: "likeHomeIcon"), for: .normal)
            }else {
                self.likeBtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
            }
            if let count = model?.data?.dashBoardList?[0].likes  {
                if count == 0 {
                    self.likeCountLbl.text = ""
                }else {
                    self.likeCountLbl.text = "\(count)"
                }
            }else {
                self.likeCountLbl.text = ""
            }
            
            self.videoRow = 0
            if let postId = self.homeDataList?.dashBoardList?[0]._id  {
                self.postid = postId
            }else {
                self.postid = ""
            }
            
            
            DispatchQueue.main.async {
                
                kUserDefaults.set("2", forKey: "signFlow")
                self.ScrollCollection.reloadData()
                Loader.isLoader(show: false)
            }
        }
    }
}
//MARK: -LIKE BUTTON API-
extension Homevc {
    
    func likePost(islike: Bool, postId : String,section : Int) {
        var param: [String: Any] = [:]
        param["teasureId"] = postId
        param["role"] = "teasurelike"
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
                
                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.homeDataList?.dashBoardList?[section].isLiked = islike
            guard let count = self.homeDataList?.dashBoardList?[section].likes else {
                return
            }
            if islike == true {
                self.homeDataList?.dashBoardList?[section].likes = count+1
                if let count = self.homeDataList?.dashBoardList?[section].likes {
                    if count == 0 {
                        self.likeCountLbl.text = ""
                    }else {
                        self.likeCountLbl.text = "\(count)"
                    }
                }else {
                    self.likeCountLbl.text = ""
                }
                
                
            }else {
                self.homeDataList?.dashBoardList?[section].likes = count-1
                if let count = self.homeDataList?.dashBoardList?[section].likes {
                    if count == 0 {
                        self.likeCountLbl.text = ""
                    }else {
                        self.likeCountLbl.text = "\(count)"
                    }
                    
                }else {
                    self.likeCountLbl.text = ""
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                if self.homeDataList?.dashBoardList?[section].isLiked == true  {
                    self.likeBtn.setImage(UIImage(named: "likeHomeIcon"), for: .normal)
                }else {
                self.likeBtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
                }
                UIApplication.topViewController()?.view.isUserInteractionEnabled = true
            }
        }
    }
}


