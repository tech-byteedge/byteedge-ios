
import UIKit
import  Foundation
import  KRProgressHUD
import  Alamofire

class CertificatedVc: UIViewController {
@IBOutlet weak var tblCertificate: UITableView!
    
    var objReport: GetUserData?
    var entity_uuid = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getReport(email: "jeetvishwa111@gmail.com")
        self.setui()
  
    }
    private func setui()
    {
    self.tblCertificate.delegate = self
    self.tblCertificate.dataSource = self
    }
    
    @IBAction func back(_sender:UIButton)
    {
    self.navigationController?.popViewController(animated: false)
    }
}

extension CertificatedVc: UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if objReport?.data?.result?.count == 0
        {
        self.tblCertificate.setEmptyMessageshow()
        }
        else
        {
        self.tblCertificate.restore()
        }
        return objReport?.data?.result?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblCertificate.dequeueReusableCell(withIdentifier: "CertificateCell") as! CertificateCell
        let totalRows = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == totalRows - 1 {
        cell.line.isHidden = true
        }
        
        let urlImage =  kUserDefaults.retriveString(.image)
        cell.imgCell.maskCircle()
        cell.imgCell.sd_setImage(with: URL(string:urlImage as! String), placeholderImage: UIImage(named:"prfImg"), options: .highPriority, context: nil)
        cell.title.text = objReport?.data?.result?[indexPath.row].certificate_name
        cell.subtitle.text = kUserDefaults.retriveString(.email)
        let time =  self.objReport?.data?.result?[indexPath.row].issue_date ?? ""
        cell.dateLbl.text = time
        cell.callBack =
        {
          let entityID = self.objReport?.data?.result?[indexPath.row].entity_uuid ?? ""
          self.DownLoad(entity_uuid: entityID)
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension CertificatedVc
{
    func getReport(email:String)
    {
        var param: [String: Any] = [:]
        param["email"] = email
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.getReportCerti.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: GetUserData.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
                self.objReport = model
                DispatchQueue.main.async {
                self.tblCertificate.reloadData()
                }
                
            }
            else
            {
            
            }
        }
    }
    
    func DownLoad(entity_uuid:String)
    {
        
        var param: [String: Any] = [:]
        param["entity_uuid"] = entity_uuid
    ServiceManager.instance.request(method: .get,
                                    apiURL:Api.downloadCertificate.url + "?entityuuid=\(entity_uuid)",
                                            headers: CommonUtility.instance.headers,
                                            parameters: param,
                                            decodable: DownLoadC.self,
                                            encoding: URLEncoding.default
            )
            { (status, json, model, error) in
                if model?.status == 200
                {
                print(model)
                    let resultsave = model?.data?.result
//                    resultsave?.urlSafeBase64Decoded()
//                    self.saveBase64StringToPDF((model?.data?.result)!)
//                    print(resultsave?.urlSafeBase64Decoded() ?? "* decoding failed*")
                    
                DispatchQueue.main.async {
                    self.tblCertificate.reloadData()
                       }
                    
                }
                else
                {
                }
            }
        }
    
    
    
    
    func saveBase64StringToPDF(_ base64String: String) {

//        guard
        var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last
        documentsURL!.appendPathComponent("yourFileName.pdf")

            let convertedData = Data(base64Encoded: base64String)
       // Data(base64Encoded: <#T##String#>)
//            else {
//            //handle error when getting documents URL
//            return
//        }

        //name your file however you prefer

        do {
            try convertedData!.write(to: documentsURL!)
        } catch {
            print("fail")
            //handle write error here
        }

        //if you want to get a quick output of where your
        //file was saved from the simulator on your machine
        //just print the documentsURL and go there in Finder
        print(documentsURL)
    }

}


extension String {
    func urlSafeBase64Decoded() -> String? {
        var st = self
            .replacingOccurrences(of: "_", with: "/")
            .replacingOccurrences(of: "-", with: "+")
        let remainder = self.count % 4
        if remainder > 0 {
            st = self.padding(toLength: self.count + 4 - remainder,
                              withPad: "=",
                              startingAt: 0)
        }
        guard let d = Data(base64Encoded: st, options: .ignoreUnknownCharacters) else{
            return nil
        }
        return String(data: d, encoding: .utf8)
    }
}





struct DownLoadC:Codable {
    var Code: Int?
    var status:Int?
    var message: String?
    var data: DataDownload?
}

struct DataDownload:Codable{
    var result: String?
}
