


import UIKit
import  Foundation

class ProfileTermVcViewController: UIViewController {
    @IBOutlet weak  var profileTerm: UITableView!
    
    var profileTermData = [
        ["tilte":"Privacy Policy",
         "rightIcon":"Icon", "leftIcon":"privacyPolicy"],
        ["tilte":"Terms and Conditions",
         "rightIcon":"Icon","leftIcon":"termCondition"],
        ["tilte":"Data Privacy",
         "rightIcon":"Icon","leftIcon":"copyRight"],
        
        ["tilte":"Community guidelines",
         "rightIcon":"Icon","leftIcon":"copyRight"],
        
//        ["tilte":"Account Security",
//         "rightIcon":"Icon","leftIcon":"AccoutSecurity"],
//        ["tilte":"How to change language",
//         "rightIcon":"Icon","leftIcon":"howTochangeLan"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
    }
    
    
    func setui()
    {
        self.profileTerm.delegate = self
        self.profileTerm.dataSource = self
    }
    
    
    @IBAction func bacKTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension ProfileTermVcViewController:UITableViewDelegate,UITableViewDataSource
{
    @objc func clickonCell(sender:UIButton)
    {
        if sender.tag == 0
        {
            print("pawan")
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "TermchangeViewController") as! TermchangeViewController
            vc.titleMain = "Privacy Policy"
            vc.type = 3
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if sender.tag == 1
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "TermchangeViewController") as! TermchangeViewController
            self.navigationController?.pushViewController(vc, animated: false)
            vc.titleMain = "Terms & Conditions"
            vc.type = 2
        }
        
        else if sender.tag == 2
        {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "TermchangeViewController") as! TermchangeViewController
        vc.type = 4
        vc.titleMain = "Data Privacy"
        self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "TermchangeViewController") as! TermchangeViewController
            vc.type = 5
            vc.titleMain = "Community guidelines"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileTermData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.profileTerm.dequeueReusableCell(withIdentifier: "ProfileTermCell") as! ProfileTermCell
        cell.titlelbl.text = profileTermData[indexPath.row]["tilte"]
        cell.celltap.addTarget(self, action: #selector(clickonCell), for:.touchUpInside)
        cell.celltap.tag = indexPath.row
        cell.leftImg.image = UIImage(named: profileTermData[indexPath.item]["leftIcon"] ?? "")
        
        let totalRow =
        profileTerm.numberOfRows(inSection: indexPath.section)
        if(indexPath.row == totalRow - 1)
        {
            cell.lineView.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}





//MARK: Class Cell
class ProfileTermCell: UITableViewCell {
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var celltap: UIButton!
    @IBOutlet weak var leftimg: UIImageView!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var leftImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
