//
//  BadgeVc.swift
//  byteEDGE
//
//  Created by Pawan Kumar on 07/01/22.
//

import UIKit

class BadgeVc: UIViewController {
    @IBOutlet weak var tblview: UITableView!
    

  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblview.delegate = self
        self.tblview.dataSource = self
        
    }
    

    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated:false)
    }
}

extension BadgeVc: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BadgeCellTableViewCell") as! BadgeCellTableViewCell
        cell.badgeImg.clipsToBounds = true
        cell.badgeImg.image = UIImage(named: "Ind")
        cell.badgeImg.contentMode = .scaleToFill
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
