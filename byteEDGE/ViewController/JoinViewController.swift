
import UIKit
import Foundation
import Alamofire
import KRProgressHUD
import  SKPhotoBrowser

class  JoinViewController: UIViewController {
    @IBOutlet weak var grpTbl:UITableView!
    @IBOutlet weak var joinBtn: UIButton!
    @IBOutlet weak var groupFeed: UIButton!
    @IBOutlet weak var height: NSLayoutConstraint!
    //MARK: VARIABLE
    var groupDetailWithJoin: GroupDetailwithJoin?
    var _id = String()
    var joinid = String()
    var joinBool = false
    var callBAckID:((_ id: String)->())?
    var joinStatus = String()
     override func viewDidLoad() {
//         if UIDevice.current.userInterfaceIdiom == .pad {
//             self.height.constant  = 400
//         }
//         else
//         {
//            self.height.constant = 250
//         }
            
        super.viewDidLoad()
        grouplistwithJoinApi()
//        grpTbl.separatorColor = UIColor.white.withAlphaComponent(0.5)
        self.grpTbl.isPagingEnabled = false
        self.grpTbl.delegate = self
        self.grpTbl.dataSource = self
        self.grpTbl.register(UINib(nibName: "TitleCell", bundle: nil), forCellReuseIdentifier: "TitleCell")
    }
    
    @IBAction func back_Tap()
    {
    self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func joinTap(_ sender: Any) {
        DispatchQueue.main.async {
            self.joinApi()
        }
    }

    func join()
    {
        if (joinBool == false && joinStatus == "pending")
        {
            self.joinBtn.isHidden = false
            self.joinBtn.isUserInteractionEnabled = false
            self.groupFeed.isHidden = true
            self.joinBtn.setTitle("Pending", for: .normal)
        }
        
        else if (joinBool == false && joinStatus == "")
        {
            self.joinBtn.isHidden = false
            self.joinBtn.isUserInteractionEnabled = true
            self.joinBtn.setTitle("Join", for: .normal)
            self.groupFeed.isHidden = true
        }
        
        else if (joinBool == true && joinStatus == "")
        {
            self.joinBtn.isHidden = true
            self.joinBtn.isUserInteractionEnabled = true
            //            self.joinBtn.setTitle("Join", for: .normal)
            self.groupFeed.isHidden = false
        }
        
        else
        {
            
        }
       
    }
    
    @IBAction func  groupFeedTap(_sender: UIButton)
    {
        let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "GroupFeeds") as! GroupFeeds
        vc.idforFeed = _id
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension JoinViewController: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2
        {
            return  2
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = self.grpTbl.dequeueReusableCell(withIdentifier: "GrpTopCellTableViewCell", for: indexPath) as! GrpTopCellTableViewCell
            cell.obj = self.groupDetailWithJoin
            cell.collectionImg.reloadData()
            cell.lblTxt.text = groupDetailWithJoin?.data?.groupDetails?.name
            var urlImage = self.groupDetailWithJoin?.data?.groupDetails?.image ?? ""
            cell.topimg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: "logobyte"), options: .highPriority, context: nil)
            cell.callBackimgShow = {
                    var images = [SKPhoto]()
                    var urlImage = self.groupDetailWithJoin?.data?.groupDetails?.image ?? ""
                    let photo = SKPhoto.photoWithImageURL(urlImage)
                    photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                    images.append(photo)
                    
                    // 2. create PhotoBrowser Instance, and present.
                    let browser = SKPhotoBrowser(photos: images)
                    browser.initializePageIndex(0)
                    self.present(browser, animated: true, completion: {})
                
            }
            
            
            return cell
        case 1:
            let cell = self.grpTbl.dequeueReusableCell(withIdentifier: "GrpAboutCell", for: indexPath) as! GrpAboutCell
            cell.disTxt.text =  groupDetailWithJoin?.data?.groupDetails?.about
           
            return cell
            
        case 2:
            if indexPath.row == 0
            {
                let cell = self.grpTbl.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
                cell.titlLbl.text = "WHY SHOULD YOU JOIN GROUP?"
                return cell
            }
            
            else
            {
                let cell = self.grpTbl.dequeueReusableCell(withIdentifier: "GrpWhyShouldCell", for: indexPath) as! GrpWhyShouldCell
                cell.txtLbl.text = groupDetailWithJoin?.data?.groupDetails?.joinReason
                return cell
            }
            
        default:
            return fatalError() as! UITableViewCell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
}

extension JoinViewController
{
    func grouplistwithJoinApi()
    {
        var param: [String: Any] = [:]
        param["groupId"] =  _id
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.GroupdetailswithJoinIcon.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: GroupDetailwithJoin.self,
                                        encoding: URLEncoding.queryString)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            self.joinid = model?.data?.groupDetails?._id ?? ""
            self.joinStatus = model?.data?.groupDetails?.joinStatus ?? ""
            UserDefaults.standard.set(self.joinid, forKey: "ID")
            self.groupDetailWithJoin = model
            DispatchQueue.main.async {
                self.join()
                self.grpTbl.reloadData()
                
            }
        }
    }
}


//MARK: JOIN Api
extension JoinViewController
{
    func joinApi()
    {
        var param: [String: Any] = [:]
        param["groupId"] = _id
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.joinGroup.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: JoinMember.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
                
                self.createAlertCallback(title: AppName, message:message ?? "" , cancelTitle: "Cancel") { (value)in
                    if value == true
                    {
                        self.navigationController?.popViewController(animated: false)
                    }
                    else
                    {
                        print("Nothing")
                    }
                }
                DispatchQueue.main.async {
                    self.grpTbl.reloadData()
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
//                NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
                self.navigationController?.popViewController(animated: false)
            }
            else
            {
                self.createAlertCallback(title: AppName, message:message ?? "" , cancelTitle: "Cancel") { (value) in
                    if value == true
                    {
                        self.navigationController?.popViewController(animated: false)
                    }
                    else
                    {
                        print("Nothing")
                    }
                }
            }
        }
    }
}
struct JoinMember: Codable
{
    let Code: Int?
    let status: Int?
    let message: String?
}

