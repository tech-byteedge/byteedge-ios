//  VedioplayViewController.swift
//  byteEDGE
//  Created by Pawan Kumar on 01/04/22.

import UIKit
import SDWebImage
import AVKit
import AVFoundation
import Firebase
import FirebaseDynamicLinks
import JWPlayerKit
import Alamofire
import KRProgressHUD

class VedioplayViewController: UIViewController,BookMarkStatus {
    func status(int: Int) {
        print("")
    }
    
    // MARK: -VARIABLES-
    // MARK: -OUTLETS
    @IBOutlet weak var mainCollection: UICollectionView!
    @IBOutlet weak var likeCountLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    // MARK: -Variable
    
    public var isPlaying: Bool = false
    var homeDataList: HomeData?
    var dashBoardList = [DashBoardListData]()
    var videoUrl : URL?
    var videoRow : Int = 0
    var postid: String = ""
    var filetye = Int()
    var previous_node = Jwplayer()
    var current_node =  Jwplayer()
    var next_node = Jwplayer()
    var currnetposssition  = 0
    var version: String?
    var isLoadingList : Bool = false
    var pageCount = 1
    var index = Int()
    
    override func viewWillDisappear(_ animated: Bool) {
        self.current_node.player.pause()
        self.previous_node.player.pause()
        self.next_node.player.pause()
        //       self.homeDataList?.dashBoardList?.removeAll()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.current_node.player.pause()
        self.previous_node.player.pause()
        self.next_node.player.pause()
    }
    
    var isCollapse = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageCount = 1
        self.homeData(pageNum: self.pageCount, isAppend: false)
        self.addChild(current_node)
        self.addChild(previous_node)
        self.addChild(next_node)
        current_node.didMove(toParent: self)
        previous_node.didMove(toParent: self)
        next_node.didMove(toParent: self)
        self.registerCell()
        setui()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("refreshh-home"), object: nil)
        //        refreshpage()
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.mainCollection.reloadData()
    }
    
    func registerCell()
    {
        self.mainCollection.registerCell(type:ScrollCollectionIndicator.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.pageCount = 1
        self.homeData(pageNum: self.pageCount, isAppend: false)
        self.mainCollection.reloadData()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    func refreshpage()
    {
        let refreshControl = mainCollection.addRefreshControl(target: self,action: #selector(doRefreshhome(_:)))
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle =
        NSAttributedString(string: "",attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-UltraLight",size: 36.0)! ])
    }
    
    @objc func doRefreshhome(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.tintColor = self.view.backgroundColor // store color
            self.view.backgroundColor = UIColor.black
        }) { _ in
            self.homeData(pageNum: self.pageCount, isAppend: false)
            self.view.backgroundColor = self.view.tintColor // restore color
        }
    }
    //MARK: -ACTIONS-
    @IBAction func notificationTap(_ sender: UIButton) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    private  func setui()
    {
        self.mainCollection.isPagingEnabled = true
        mainCollection.delegate = self
        mainCollection.dataSource = self
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
    }
}

//MARK: -COLLECTION VIEW DATASOURCE-
extension VedioplayViewController:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate , UIScrollViewDelegate
{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0 {
            return dashBoardList.count ?? 0
        }
        else if section == 1 && self.isLoadingList == true
        {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if indexPath.section == 0
        {
            let cell = mainCollection.dequeueReusableCell(withReuseIdentifier: "HomeVedioRealsCellCollectionViewCell", for: indexPath) as! HomeVedioRealsCellCollectionViewCell
            print("byte path ::::::::::::::::::::: \(indexPath.row)")
            cell.bookMarkDelegte = self
            cell.bookmarkTap.tag = indexPath.row
            cell.layoutIfNeeded()
            cell.captionTap.layer.borderWidth = 0.3
            cell.captionTap.layer.cornerRadius = 15
            cell.captionTap.layer.borderColor = UIColor.white.cgColor
            cell.sharebtn.makeRounded()
            
            if self.dashBoardList[indexPath.row].courseId == "" || self.dashBoardList[indexPath.row].courseData == nil || self.dashBoardList[indexPath.row].courseId == nil{
                cell.captionTap.isHidden = true
            }else{
                cell.captionTap.isHidden = false
            }
            cell.callBackofTreasure = {
                if kUserDefaults.bool(forKey: "isSubscribed") == true
                {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CourseDetailshowVc") as! CourseDetailshowVc
                    //                vc.back = true
                    vc.playlistID = self.dashBoardList[indexPath.row].courseData?.playlistId ?? ""
                    vc.courseId =  self.dashBoardList[indexPath.row].courseData?._id ?? ""
                    let quizStatus = self.dashBoardList[indexPath.row].courseData?.isQuizExist ?? false
                    vc.isQuizExist = quizStatus
                    self.navigationController?.pushViewController(vc, animated: false)
                    //                self.present(vc, animated: false, completion: nil)
                }
                else if kUserDefaults.bool(forKey: "isSubscribed") == false
                {
                    let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CoursePreviewViewController") as! CoursePreviewViewController
                    vc.viaHome = true
                    let id  = self.dashBoardList[indexPath.row].courseData?._id
                    vc.courseId = id ?? ""
                    vc.mediaID  = self.dashBoardList[indexPath.row].courseData?.previewId ?? ""
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else{}
            }
            cell.courseHeadinglbl.textColor = UIColor.white
            cell.lblDescription.textColor = UIColor.white
            //        print(cell.lblDescription.numberOfLines)
            
            if(indexPath.row == 0)
            {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cell.videoView.addSubview(current_node.view)
                }
                else
                {
                    self.current_node.mediaID =  self.dashBoardList[currnetposssition].video ?? ""
                    cell.videoView.addSubview(current_node.view)
                    self.current_node.setUpPlayer(isautostart: true,volume: 0.0)
                }
            }
            
            self.postid = dashBoardList[indexPath.row]._id ?? ""
            cell.courseHeadinglbl.text = self.dashBoardList[indexPath.row].teasureName ?? ""
            cell.lblDescription.text = self.dashBoardList[indexPath.row].description ?? ""
            if let setcount = (self.dashBoardList[indexPath.row].likes)
            {
                cell.counlikeLbl.text =  String(setcount)
            }
            if  self.dashBoardList[indexPath.row].isLiked == true{
                cell.heartbtn.setImage(UIImage(named: "selected-1"), for: .normal)
            }else{
                cell.heartbtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
            }
            
            //MARK: ShareButton
            cell.callBackofsharebtn = {
                
                var idshare = String()
                var  mediaid = String()
                
                if let xCondition = self.dashBoardList[indexPath.row]._id
                {
                    idshare = xCondition
                }
                if let yCondition  = self.dashBoardList[indexPath.row].video
                {
                    mediaid = yCondition
                }
                
                let urlStrig  = "https://byteedgetest2.page.link/dynamic_link?videoId=\(idshare)"
                guard let link = URL(string: urlStrig) else { return }
                
                let dynamicLinksDomainURIPrefix = "https://byteedgetest2.page.link"
                print(link)
                let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
                
                linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID:"com.bytedge1")
                linkBuilder?.iOSParameters?.appStoreID = "1631500899"
                linkBuilder?.iOSParameters?.minimumAppVersion = "1.3"
                
                linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.app.byteedge")
                linkBuilder?.androidParameters?.minimumVersion = 0
                linkBuilder?.androidParameters?.fallbackURL = URL.init(string: "https://play.google.com/store/apps/details?id=com.app.byteedge")
                
                linkBuilder?.iOSParameters?.fallbackURL = URL.init(string: "https://apps.apple.com/us/app/byteedge/id1631500899")
                
                linkBuilder?.iOSParameters?.customScheme = "com.bytedge1"
                
                guard let longDynamicLink = linkBuilder?.url else { return }
                print("The long URL is: \(longDynamicLink)")
                
                let textToShare = [ longDynamicLink ] as [Any]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                // so that iPads won't crash
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                self.present(activityViewController, animated: true, completion: nil)
                
                //       linkBuilder?.shorten() { url, warnings, error in
                //                print("ERRRRRRR")
                //                print(error)
                //                print(warnings)
                //              guard let url = url, error != nil else { return }
                //              print("The short URL is: \(url)")
                //                let textToShare = [ url ] as [Any]
                //                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                //                activityViewController.popoverPresentationController?.sourceView = self.view
                //                // so that iPads won't crash
                //                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                //                self.present(activityViewController, animated: true, completion: nil)
                //
                //            }
                
                
                
                //            if UIDevice.current.userInterfaceIdiom == .pad {
                //                let text = "This is some text that I want to share."
                //                let someText:String = "Hello want to share text also"
                //                let objectsToShare:URL = URL(string: "https://byteedgetest2.page.link/\(self.dashBoardList[indexPath.row].video)")!
                //                let textToShare = [ text, objectsToShare ] as [Any]
                //                let activityVC = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                //                let nav = UINavigationController(rootViewController: activityVC)
                //                nav.modalPresentationStyle = UIModalPresentationStyle.popover
                //                let popover = nav.popoverPresentationController as UIPopoverPresentationController?
                //                popover?.sourceView = self.view
                //                popover?.sourceRect = //self.frame
                //                CGRect(x: self.view.bounds.midX, y: self.view.bounds.midX,width: 500,height: 0)
                //                self.present(nav, animated: true, completion: nil)
                
                //              }
                //              else
                //              {
                //                let text = "ByteEdge"
                //                let someText:String = "Hello want to share text also"
                //                let objectsToShare:URL = URL(string: "https://byteedgetest2.page.link")!
                //
                //
                //                let textToShare = [ text,objectsToShare] as [Any]
                //                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                //                activityViewController.popoverPresentationController?.sourceView = self.view
                //                // so that iPads won't crash
                //                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                //                self.present(activityViewController, animated: true, completion: nil)
                //            }
            }
            
            cell.callBackofLike =
            {
                var count = self.dashBoardList[indexPath.row].likes ?? 0
                let status = self.dashBoardList[indexPath.row].isLiked
                if status == false
                {
                    count += 1
                    self.likePost(islike: status ?? false, postId: self.postid)
                    cell.heartbtn.setImage(UIImage(named: "selected-1"), for: .normal)
                    self.dashBoardList[indexPath.row].likes = count
                    self.dashBoardList[indexPath.row].isLiked = true
                    cell.counlikeLbl.text = "\(count)"
                }
                
                else
                {
                    count -= 1
                    self.likePost(islike: status ?? true, postId: self.postid)
                    cell.heartbtn.setImage(UIImage(named: "unlikeHomeIcon"), for: .normal)
                    self.dashBoardList[indexPath.row].isLiked = false
                    self.dashBoardList[indexPath.row].likes = count
                    cell.counlikeLbl.text = "\(count)"
                }
            }
            
            cell.callBackbookmark =
            {
                let videId = self.dashBoardList[indexPath.row]._id ?? ""
                var statusBookmark = self.dashBoardList[indexPath.row].isBookMark // true
                if statusBookmark == true
                {
                    print("statusBookmark\(statusBookmark)")
                    //                     self.addBookMark(postId:videId )
                    self.addmark1(postId: videId)
                    cell.bookmarkTap.setImage(UIImage(named: "unbook"), for: .normal)
                    self.dashBoardList[indexPath.row].isBookMark = false
                    
                }
                
                else
                {
                    print("statusBookmark\(statusBookmark)")
                    self.addmark1(postId: videId)
                    //                     self.addBookMark(postId: videId)
                    cell.bookmarkTap.setImage(UIImage(named: "book"), for: .normal)
                    self.dashBoardList[indexPath.row].isBookMark = true
                }
            }
            
            if self.dashBoardList[indexPath.row].isBookMark ?? false
            {
                cell.bookmarkTap.setImage(UIImage(named: "book"), for: .normal)
            }
            
            else{
                cell.bookmarkTap.setImage(UIImage(named:"unbook"), for: .normal)
            }
            return  cell
        }
        else
        {
            let cell = mainCollection.dequeueReusableCell(withReuseIdentifier: "ScrollCollectionIndicator", for: indexPath) as! ScrollCollectionIndicator
            cell.indicator.startAnimating()
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //         DispatchQueue.main.async { [self] in
        let offset = scrollView.contentOffset.y
        let contenHeight = scrollView.contentSize.height
        if offset > contenHeight - scrollView.frame.height   && self.homeDataList?.count ?? 0 > dashBoardList.count - 3 {
            //                KRProgressHUD.show()
            if !self.isLoadingList{
                //                 view.layoutIfNeeded()
                self.isLoadingList = true
                let indexSet = IndexSet(integer: 0)
                self.mainCollection.reloadSections(indexSet)
                DispatchQueue.main.async {
                    self.pageCount = self.pageCount + 1
                    self.homeData(pageNum: self.pageCount,isAppend: true)
                }
                //                 DispatchQueue.main.asyncAfter(deadline: .now() + 0.30) {
                //                     self.pageCount = self.pageCount + 1
                //                     self.homeData(pageNum: self.pageCount,isAppend: true)
                //                 }
                
            }
        }
    }
    
    //    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //           if dashBoardList.count ?? 0 > indexPath.row
    //                   {
    //               if !self.isLoadingList{
    //                   view.layoutIfNeeded()
    //                   self.isLoadingList = true
    //                   let indexSet = IndexSet(integer: 1)
    //                   self.mainCollection.reloadSections(indexSet)
    ////                   self.mainCollection.reloadData()
    //                   DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
    //                       self.pageCount = self.pageCount + 1
    //                       self.homeData(pageNum: self.pageCount,isAppend: true)
    //                       }
    //                      }
    //                    pageCount = /self.homeDataList?.count
    //
    //                   }
    //
    //    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        findCenterIndex()
        print("scroll current position =====> \(currnetposssition)")
        let vcell = self.mainCollection.visibleCells
        
        if (vcell.count > 0)
        {
            print("current position =====> \(currnetposssition)")
            if let previous_cell = self.mainCollection.cellForItem(at: IndexPath(row: currnetposssition - 1, section: 0)) as? HomeVedioRealsCellCollectionViewCell
            {
                self.previous_node.mediaID =  self.dashBoardList[currnetposssition - 1].video ?? "https://cdn.jwplayer.com/manifests/Zlt6Luyf.m3u8"
                previous_cell.videoView.addSubview(previous_node.view)
                self.previous_node.setUpPlayer(isautostart: false, volume:0.0)
            }
            
            if let current_cell = self.mainCollection.cellForItem(at: IndexPath(row: currnetposssition, section: 0)) as? HomeVedioRealsCellCollectionViewCell
            {
                self.current_node.mediaID =  self.dashBoardList[currnetposssition].video ?? "https://cdn.jwplayer.com/manifests/Zlt6Luyf.m3u8"
                current_cell.videoView.addSubview(current_node.view)
                self.current_node.setUpPlayer(isautostart: true, volume: 0.0)
            }
            
            if let next_cell = self.mainCollection.cellForItem(at: IndexPath(row: currnetposssition + 1, section: 0)) as? HomeVedioRealsCellCollectionViewCell
            {
                self.next_node.mediaID =  dashBoardList[currnetposssition + 1].video ?? "https://cdn.jwplayer.com/manifests/Zlt6Luyf.m3u8"
                next_cell.videoView.addSubview(next_node.view)
                self.next_node.setUpPlayer(isautostart: false,volume: 0.0)
            }
        }
        
        
    }
    
    func findCenterIndex()
    {
        let center = self.view.convert(self.mainCollection.center, to: self.mainCollection)
        let index = mainCollection.indexPathForItem(at: center)
        currnetposssition = index? .row ?? 0
        print(index ?? "index not found")
    }
    
    // Called when the cell is displayed
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("end display ========> \(indexPath.row)")
        print("end current position =====> \(currnetposssition)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isLoadingList {
            if indexPath.section == 0{
                return CGSize.init(width: self.mainCollection.frame.size.width, height: self.mainCollection.frame.size.height - 48)
            }else{
                return CGSize.init(width: self.mainCollection.frame.size.width, height: 48)
            }
            
        }else{
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                return CGSize.init(width: self.mainCollection.frame.size.width, height: self.mainCollection.frame.size.height)
            }
            else
            {
                return CGSize.init(width: self.mainCollection.frame.size.width, height: self.mainCollection.frame.size.height)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
}

// MARK: -HIT HOME API-
extension VedioplayViewController {
    func homeData(pageNum:Int,isAppend:Bool?){
        var param: [String: Any] = [:]
        param["page"] = pageNum
        ServiceManager.instance.request(
            method: .get,
            apiURL: Api.homeData.url + "?page=\(pageNum)",
            headers: CommonUtility.instance.headers,
            parameters: nil,
            
            decodable: Home.self)
        { (status, json, model, error) in
            if model?.status == 200 {
                kUserDefaults.set("2", forKey: "signFlow")
            }
            guard model?.status == 200 else {
                print(model)
                return
            }
            
            if isAppend == true{
                if model?.data?.dashBoardList?.count ?? 0 > 0{
                    for i in 0...(model?.data?.dashBoardList?.count ?? 0) - 1{
                        self.dashBoardList.append((model?.data?.dashBoardList?[i])!)
                    }
                }
                
            }
            
            else
            {
                self.dashBoardList = model?.data?.dashBoardList ?? []
                self.homeDataList = model?.data
                //                KRProgressHUD.dismiss()
            }
            
            self.isLoadingList = false
            DispatchQueue.main.async {
                self.mainCollection.reloadData()
            }
            
        }
    }
    
    //     func addBookMark(postId :String) {
    //         var param: [String: Any] = [:]
    //         param["videoId"] = postId
    //         ServiceRequest.instance.addBookMark(param) { (isSucess, response, error) in
    //             guard response?.status == 200 else {
    //                 return
    //             }
    //             print(response)
    //             self.createAlert(title: AppName, message: response?.message ?? "")
    //         }
    //     }
    
    func addmark1(postId :String)
    {
        var param: [String: Any] = [:]
        param["videoId"] = postId
        //         KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.addBookmark.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: CommonResponseModel.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            //             KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
                print(message)
            }
            else
            {
                
            }
        }
    }
}

//MARK: -LIKE BUTTON API-
extension VedioplayViewController {
    func likePost(islike: Bool, postId : String) {
        var param: [String: Any] = [:]
        param["teasureId"] = postId
        param["role"] = "teasurelike"
        
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                //                print(response?.status as Any)
                self.createAlert(title: kAppName, message: response?.message ?? "")
                return
            }
        }
    }
}



//extension NSAttributedString {
//    func lines(fittingWidth width: CGFloat) -> [String] {
//        let attributedString = self
//        let framesetter =
//        CTFramesetterCreateWithAttributedString(
//            attributedString
//        )
//        let path = CGPath(rect: CGRect(
//            x: 0,
//            y: 0,
//            width: width,
//            height: .greatestFiniteMagnitude
//        ), transform: nil)
//        let frame = CTFramesetterCreateFrame(
//            framesetter,
//            CFRange(location: 0, length: 0),
//            path,
//            nil
//        )
//
//        let lines = CTFrameGetLines(frame) as! [CTLine]
//        var strLines = [String]()
//        let nsString = attributedString.string as NSString
//        for line in lines {
//            let lineRange = CTLineGetStringRange(line)
//            let range = NSMakeRange(lineRange.location, lineRange.length)
//            strLines.append(nsString.substring(with: range))
//        }
//        return strLines
//    }
//}

//extension UILabel {
//    var maxNumberOfLines: Int {
//        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
//        let text = (self.text ?? "") as NSString
//        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil).height
//        let lineHeight = font.lineHeight
//        return Int(ceil(textHeight / lineHeight))
//    }
//}
// extension UILabel {
//    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
//        let readMoreText: String = trailingText + moreText
//
//        let lengthForVisibleString: Int = self.vissibleTextLength
//        let mutableString: String = self.text!
//        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
//        let readMoreLength: Int = (readMoreText.count)
//        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
//        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
//        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
//        answerAttributed.append(readMoreAttributed)
//        self.attributedText = answerAttributed
//    }
//
//    var vissibleTextLength: Int {
//        let font: UIFont = self.font
//        let mode: NSLineBreakMode = self.lineBreakMode
//        let labelWidth: CGFloat = self.frame.size.width
//        let labelHeight: CGFloat = self.frame.size.height
//        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
//
//        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
//        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
//        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
//
//        if boundingRect.size.height > labelHeight {
//            var index: Int = 0
//            var prev: Int = 0
//            let characterSet = CharacterSet.whitespacesAndNewlines
//            repeat {
//                prev = index
//                if mode == NSLineBreakMode.byClipping {
//                    index += 1
//                } else {
//                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
//                }
//            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
//            return prev
//        }
//        return self.text!.count
//    }
//}
