
//  DownloadVc.swift
//  byteEDGE
//  Created by Pawan Kumar on 07/01/22.


import UIKit
class DownloadVc: UIViewController {
    @IBOutlet weak  var downloadTbl: UITableView!
    var dicData = [
        ["tilte":"Project Management",
         "rightIcon":"Icon"],
        ["tilte":"Business Analyst",
         "rightIcon":"Icon"],
        ["tilte":"English",
         "rightIcon":"Icon"],
        ["tilte":"UI/UX Design",
         "rightIcon":"Icon"],
        ["tilte":"Programming",
         "rightIcon":"Icon"],
        ["tilte":"Project Coordinator",
         "rightIcon":"Icon"],
        ["tilte":"Agile",
         "rightIcon":"Icon"],
        ["tilte":"Physiology",
         "rightIcon":"Icon"]
    ]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setui()
    }
    

    func setui()
    {
    self.downloadTbl.delegate = self
    self.downloadTbl.dataSource = self
    }
    
    
    @IBAction func bacKTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension DownloadVc:UITableViewDelegate,UITableViewDataSource
{
    @objc func clickonCell(sender:UIButton)
    {
        if sender.tag == 0
        {
            print("pawan")
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "PersonalnfoVcViewController") as! PersonalnfoVcViewController
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if sender.tag == 2
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "CertificatedVc") as! CertificatedVc
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else if sender.tag == 6
        {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "FAQsVC") as! FAQsVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dicData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.downloadTbl.dequeueReusableCell(withIdentifier: "DownloadCell") as! DownloadCell
        cell.titlelbl.text = dicData[indexPath.row]["tilte"]
        cell.celltap.addTarget(self, action: #selector(clickonCell), for: .touchUpInside)
        cell.celltap.tag = indexPath.row
//        cell.leftimg.image = UIImage(named: dicData[indexPath.item]["Icon"] ?? "")
        
        let totalRow =
        downloadTbl.numberOfRows(inSection: indexPath.section)
        if(indexPath.row == totalRow - 1)
        {
        cell.lineView.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
