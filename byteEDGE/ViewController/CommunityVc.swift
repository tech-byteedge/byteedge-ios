
import UIKit
import  Alamofire
import CoreAudio
import KRProgressHUD
import  AVKit
import  AVFoundation
import GameKit
import IQKeyboardManagerSwift
import  SDWebImage
import  FirebaseDynamicLinks

enum callBAckType
{
    case firstCallBack,secondCallBack
}

class CommunityVc: UIViewController, VcTransfer, JoinDelegate {
    @IBOutlet weak var moveCreatpostVc: UIButton!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var tblCommunity: UITableView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var txtWriteSome: UITextField!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet var titleheader: UIView!
    @IBOutlet weak var buttonView: UIView!
    var refreshControl: UIRefreshControl!
    var maintype = ""
    var text = ""
    var selected = Int()
    var callBAckType : callBAckType = .secondCallBack
    var objfeedlist: FeelList?
    var obj_ModelFeed: FeedDataModel?
    var  objgrouplist: GroupList?// Feed declare variable
    let playerViewController = AVPlayerViewController()
    var _id = String()
    var section_type = Int()
    var selectedCell = Int()
    var grpIDFeed = ""
    var viafeedpost = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.grouplistApi()
        refreshapage()

        
        var callBack : callBAckType = .firstCallBack
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(GroupUpdate),name: NSNotification.Name ("GrouupUpdate"),object: nil)
        //MARK: Referesh page
        let refreshControl = tblCommunity.addRefreshControl(target: self,
                                                            action: #selector(doRefresh(_:)))
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle =
        NSAttributedString(string: "",
                           attributes: [
                            NSAttributedString.Key.foregroundColor: UIColor.black,
                            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-UltraLight",
                                                                size: 36.0)! ])
        
        // self.grouplistApi()
        tblCommunity.isUserInteractionEnabled = true
        self.registedXib()
        setUi()
        let view = CommonButtonView.fromNib()
        view.firstCallBack = {
            self.callBAckType = .firstCallBack
            self.feedlistApi()
            view.firstName.text = "Feeds"
            view.secondName.text = "Groups"
            view.thirdView.isHidden = true
            view.firstView.isHidden = false
            view.secondView.isHidden = false
            view.firstView.backgroundColor = #colorLiteral(red: 0.1461485922, green: 0.1526356339, blue: 0.1830024123, alpha: 1)
            view.secondView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.firstName.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            view.firstName.textColor = .white
            view.img1st.isHidden = false
            view.img2nd.isHidden = true
            self.tblCommunity.reloadData()
        }
        
          view.secondCallBack = {
            self.callBAckType = .secondCallBack
            self.grouplistApi()
            view.secondName.text = "Groups"
            view.secondView.backgroundColor = #colorLiteral(red: 0.1461485922, green: 0.1526356339, blue: 0.1830024123, alpha: 1)
            view.firstView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.thirdView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            view.firstName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            view.secondName.textColor = .white
            view.thirdName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.64)
            view.img1st.isHidden = true
            view.img2nd.isHidden = false
            view.img3rd.isHidden = true
            self.tblCommunity.reloadData()
        }
        
        view.initSetup(.community)
        view.frame = buttonView.bounds
        self.buttonView.addSubview(view)
        buttonView.backgroundColor = .black
        
    }
    
    
    func refreshapage()
    {
        tblCommunity.alwaysBounceVertical = true
        tblCommunity.bounces  = true
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.tblCommunity.addSubview(refreshControl)
    }
    
    @objc func didPullToRefresh() {
        self.grouplistApi()
        refreshControl?.endRefreshing()
    }
    
    //MARK: refresh page
    @objc func doRefresh(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.tintColor = self.view.backgroundColor // store color
            self.view.backgroundColor = UIColor.black
        }) { _ in
            if self.callBAckType == .firstCallBack
            {
                self.feedlistApi()
            }
            else
            {
                self.grouplistApi()
            }
            self.view.backgroundColor = self.view.tintColor // restore color
        }
    }
    
    @IBAction func movetopost(_ sender: Any) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier:
                                                                        "CreatePostVc") as! CreatePostVc
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.grouplistApi()
        //        self.feedlistApi()
    }
    
    @objc func GroupUpdate(notification: Notification) {
        self.grouplistApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.feedlistApi()
    }
    
    func join(index: Int, type: String) {
        if type == "Groups you may like"
        {
            self.maintype = objgrouplist?.data?.mayLikeGroup?[index]._id ?? ""
            self.joinApi(type:type)
        }
        
        else if type == "Most popular"
        {
            self.maintype = objgrouplist?.data?.popularGroup?[index]._id ?? ""
            self.joinApi(type:type)
        }
    }
    //MARK: Register XibCell
    func registedXib()
    {
        self.tblCommunity.register(UINib(nibName: "CreateGroupsCellTableViewCell", bundle: nil),forCellReuseIdentifier: "CreateGroupsCellTableViewCell")
        
        self.tblCommunity.register(UINib(nibName: "Community", bundle: nil), forCellReuseIdentifier: "Community")
        self.tblCommunity.register(UINib(nibName: "MultipleImgCell", bundle: nil), forCellReuseIdentifier: "MultipleImgCell")
        tblCommunity.delegate = self
        tblCommunity.dataSource = self
        
        
        
        tblCommunity.delegate = self
        tblCommunity.dataSource = self
    }
    
    func setUi()
    {
        self.borderView.selectedborder()
    }
    
    //MARK: SEND DID SELECTION
    func sendVc(index: Int, type: String?) {
        if type == "My Groups"
        {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "EditGroupDetails") as! EditGroupDetails
            vc.memberID = objgrouplist?.data?.myGroup?[index].memberDetails?.first?.memberId ?? ""
            if let id = objgrouplist?.data?.myGroup?[index]._id {
                vc._id = id
                
            } else {
            }
            vc.removeCallBAck = {
                self.grouplistApi()
                
            }
            
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if type == "Groups you may like" //
        {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "JoinViewController") as! JoinViewController
            if let id = objgrouplist?.data?.mayLikeGroup?[index]._id {
                vc._id = id
                if let join = objgrouplist?.data?.mayLikeGroup?[index].isJoin
                {
                    vc.joinBool = join
                    print(join)
                }
            }
            
            else
            {
                
            }
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        else // Popular
        {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "JoinViewController") as! JoinViewController
            if let id = objgrouplist?.data?.popularGroup?[index]._id {
                vc._id = id
                if let join = objgrouplist?.data?.popularGroup?[index].isJoin
                {
                    vc.joinBool = join
                    print(join)
                }
            } else {
            }
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
}

//MARK:  Delegate and DataSource
extension CommunityVc: UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if callBAckType == .firstCallBack{
            return  1
        } else if callBAckType == .secondCallBack {
            return 4
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if callBAckType == .secondCallBack{
            print("objfeedlist?.data?.feedList?.count ?? 0 \(objfeedlist?.data?.feedList?.count ?? 0)")
            self.tblCommunity.restoredata()
            return 1
        }
        
        else if callBAckType == .firstCallBack {
            if self.obj_ModelFeed?.data?.feedList.count ?? 0 == 0{
                self.tblCommunity.setEmptyMessageshow()
            }else{
                self.tblCommunity.restoredata()
            }
            
            return self.obj_ModelFeed?.data?.feedList.count ?? 0
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if callBAckType == .secondCallBack // group
        {
            if indexPath.section == 0  {
                let cell = self.tblCommunity.dequeueReusableCell(withIdentifier: "CreateGroupsCellTableViewCell") as! CreateGroupsCellTableViewCell
                
                //MARK: - Change By Udit
                if  objgrouplist?.data?.myGroup?.count == 0 {
                    cell.viewTop.isHidden = false
                    cell.createGroupBtn.isHidden = true
                }
                else{
                    cell.viewTop.isHidden = true
                    cell.createGroupBtn.isHidden = false
                }
                //MARK: -
                cell.creatAlert = { ( ) -> Void in
                    let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CreateGroup") as! CreateGroup
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                return cell
            }
            else
            {
                let cell = self.tblCommunity.dequeueReusableCell(withIdentifier: "GroupstableviewCell") as! GroupstableviewCell
                cell.selectedCell = 1
                cell.delegatevc = self
                switch indexPath.section {
                case 1:
                    cell.type = "My Groups"
                    
                case 2:
                    cell.type = "Groups you may like"
                    
                default:
                    cell.type = "Most popular"
                    
                    
                }
                
                cell.objgrouplist_get = self.objgrouplist?.data
                cell.delegate  = self
                print("mayLikeGroup\(objgrouplist?.data?.mayLikeGroup?.count)")
                cell.collectionGroups.reloadData()
                
                return cell
            }
        }
        else if callBAckType == .firstCallBack //Feed
        {
            let cell = self.tblCommunity.dequeueReusableCell(withIdentifier: "MultipleImgCell") as! MultipleImgCell
            if kUserDefaults.retriveString(._id) == obj_ModelFeed?.data?.feedList[indexPath.row].userDetail?._id{
                cell.btnFeedOptions.isHidden = false
            }else{
                cell.btnFeedOptions.isHidden = true
            }
            print("indexPath ::::::::::::::::::::: \(indexPath.row)")
            cell.multiProfileImage.layer.masksToBounds = true
            cell.multiProfileImage.layer.cornerRadius = cell.multiProfileImage.frame.size.height/2;
            cell.multiProfileImage.layer.masksToBounds = true
            cell.multiProfileImage.layer.borderWidth=1
            cell.multiName.text = obj_ModelFeed?.data?.feedList[indexPath.row].userDetail?.name
            cell.multiDES.text = obj_ModelFeed?.data?.feedList[indexPath.row].description
            var url = obj_ModelFeed?.data?.feedList[indexPath.row].userDetail?.image ?? ""
            cell.multiProfileImage.sd_setImage(with: URL(string: url), placeholderImage:(UIImage(named:"prfImg")), options: .highPriority, context: nil)
            cell.multiName.text = obj_ModelFeed?.data?.feedList[indexPath.row].userDetail?.name ?? "pawan kumar"
            cell.timelbl.text = CommonUtility.instance.getDateTIMEFromString(inputDate:(obj_ModelFeed?.data?.feedList[indexPath.row].createdAt)!)
            UserDefaults.standard.set(cell.timelbl.text, forKey: "Time")
            cell.deleteOption = {
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "PopMember") as! PopMember
                        vc.viafeedpost = true
//                        vc.tbeview = self.tblCommunity
//                        vc.FeedId = self.obj_ModelFeed?.data?.feedList[indexPath.row]._id ?? ""
                        vc.obj_ModelFeed = self.obj_ModelFeed
//                        vc.heightpopcons.constant = 100
                        vc.modalPresentationStyle = .fullScreen
                        vc.providesPresentationContextTransitionStyle = true
                        vc.definesPresentationContext = true
                        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
                        vc.view.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
                        vc.feedcallback = {
                            var param : [String: Any] = [:]
                                           param["FeedId"] = self.obj_ModelFeed?.data?.feedList[indexPath.row]._id
                                           ServiceRequest.instance.DeleteFeedApi(param){ (isSucess, response, error) in
                                           guard response?.status == 200 else {
                                                   print(response?.status as Any)
                                                   return
                                               }
                                               self.obj_ModelFeed?.data?.feedList.remove(at: indexPath.row)
                                               self.tblCommunity.reloadData()
                            
//                            self.removeCallBAck?()
//                            self.navigationController?.popViewController(animated: false)
                        }
                       
                    }
                    self.present(vc, animated: true, completion: nil)
    
           }
            }
            if obj_ModelFeed?.data?.feedList[indexPath.row].image?.count == 0  && ((obj_ModelFeed?.data?.feedList[indexPath.row].video) != nil)
            {
                cell.secodImg.isHidden = true
                cell.thirdImg.isHidden = true
                cell.fourthImage.isHidden = true
                cell.singleImg.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = true
                
                                DispatchQueue.main.async { [self] in
                                    var urlImage  = obj_ModelFeed?.data?.feedList[indexPath.row].thumbnails ?? ""
                                    cell.singleImg.sd_setImage(with: URL(string:urlImage), placeholderImage: UIImage(named:""), options: .highPriority, context: nil)
                                }
                
                
                
                
    
                
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cell.height.constant = 380
                }
                else
                {
                cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = false
                cell.countLbl.isHidden = true
                
               
                
                cell.callBackvedio = {
                    if let url = URL(string: self.obj_ModelFeed?.data?.feedList[indexPath.row].video ?? "" ) {
                        let vedio_play = AVPlayer(url: url)
                        self.playerViewController.player = vedio_play
                        self.present(self.playerViewController, animated: true) {
                            vedio_play.play()
                        }
                    }
                }
            
            }
            if  obj_ModelFeed?.data?.feedList[indexPath.row].image?.count == 0 && obj_ModelFeed?.data?.feedList[indexPath.row].video == ""
            {
                cell.secodImg.isHidden = true
                cell.thirdImg.isHidden = true
                cell.singleImg.isHidden = true
                cell.fourthImage.isHidden = true
                cell.height.constant = 0
                cell.vedioPLAY.isHidden = true
                cell.stackViewImg.isHidden = true
                cell.twoImgStackView.isHidden = true
                cell.countLbl.isHidden = true
            
            }
            else if obj_ModelFeed?.data?.feedList[indexPath.row].image?.count == 1  {
                cell.secodImg.isHidden = true
                cell.thirdImg.isHidden = true
                cell.fourthImage.isHidden = true
                cell.singleImg.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = true
                var urlImage = obj_ModelFeed?.data?.feedList[indexPath.row].image?[0] ?? ""
                cell.singleImg.sd_setImage(with: URL(string: urlImage ), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cell.height.constant = 380
                }
                else
                {
                    cell.height.constant = 230
                }
                cell.countLbl.isHidden = true
                cell.vedioPLAY.isHidden = true
            }
            
            else if obj_ModelFeed?.data?.feedList[indexPath.row].image?.count == 2
            {   cell.singleImg.isHidden = false
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = true
                cell.fourthImage.isHidden = true
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = true
                let urlImagefirst = obj_ModelFeed?.data?.feedList[indexPath.row].image?[0] ?? ""
                let urlImageSecond = obj_ModelFeed?.data?.feedList[indexPath.row].image?[1] ?? ""
                
                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst ), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                //                cell.height.constant = 175
                if UIDevice.current.userInterfaceIdiom == .pad {
                 cell.height.constant = 380
                }
                else
                {
                 cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = true
            }
            
            else if obj_ModelFeed?.data?.feedList[indexPath.row].image?.count == 3
            {   cell.singleImg.isHidden = false
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.fourthImage.isHidden = true
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = true
                let urlImagefirst = obj_ModelFeed?.data?.feedList[indexPath.row].image?[0] ?? ""
                let urlImageSecond = obj_ModelFeed?.data?.feedList[indexPath.row].image?[1] ?? ""
                let urlImageThird = obj_ModelFeed?.data?.feedList[indexPath.row].image?[2] ?? ""
                
                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst ), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.thirdImg.sd_setImage(with: URL(string: urlImageThird), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                //                cell.height.constant = 175
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cell.height.constant = 380
                }
                else
                {
                    cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = true
            }
            
            else if obj_ModelFeed?.data?.feedList[indexPath.row].image?.count == 4
            {   cell.singleImg.isHidden = false
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.fourthImage.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = true
                let urlImagefirst = obj_ModelFeed?.data?.feedList[indexPath.row].image?[0] ?? ""
                let urlImageSecond = obj_ModelFeed?.data?.feedList[indexPath.row].image?[1] ?? ""
                let urlImageThird = obj_ModelFeed?.data?.feedList[indexPath.row].image?[2] ?? ""
                let urlImageFourth = obj_ModelFeed?.data?.feedList[indexPath.row].image?[3] ?? ""
                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst ), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.thirdImg.sd_setImage(with: URL(string: urlImageThird), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                cell.fourthImage.sd_setImage(with: URL(string: urlImageFourth), placeholderImage: UIImage(named: "prfImg"), options: .highPriority, context: nil)
                //                cell.height.constant = 175
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cell.height.constant = 380
                }
                else
                {
                    cell.height.constant = 230
                }
                cell.vedioPLAY.isHidden = true
            }
            
            else if obj_ModelFeed?.data?.feedList[indexPath.row].image?.count ?? 0 > 4 && ((obj_ModelFeed?.data?.feedList[indexPath.row].video) == "")
            {
                cell.secodImg.isHidden = false
                cell.thirdImg.isHidden = false
                cell.singleImg.isHidden = false
                cell.stackViewImg.isHidden = false
                cell.twoImgStackView.isHidden = false
                cell.countLbl.isHidden = false
                cell.countLbl.textColor = .green
                if obj_ModelFeed?.data?.feedList[indexPath.row].image!.count ?? 0 > 3 {
                    let count  = obj_ModelFeed?.data?.feedList[indexPath.row].image!.count ?? 0
                    cell.countLbl.text =  "+" + String(count - 4)
                    cell.countLbl.textColor = .white
                }
                else{
                    cell.countLbl.text = ""
                }
                let urlImagefirst = obj_ModelFeed?.data?.feedList[indexPath.row].image?[0] ?? ""
                let urlImageSecond = obj_ModelFeed?.data?.feedList[indexPath.row].image?[1] ?? ""
                let urlImageThird = obj_ModelFeed?.data?.feedList[indexPath.row].image?[2] ?? ""
                let urlImageFourth = obj_ModelFeed?.data?.feedList[indexPath.row].image?[3] ?? ""
                cell.singleImg.sd_setImage(with: URL(string: urlImagefirst), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                cell.secodImg.sd_setImage(with: URL(string: urlImageSecond), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                cell.thirdImg.sd_setImage(with: URL(string: urlImageThird), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                cell.fourthImage.sd_setImage(with: URL(string: urlImageFourth), placeholderImage: UIImage(named: ""), options: .highPriority, context: nil)
                if UIDevice.current.userInterfaceIdiom == .pad {
                    cell.height.constant = 380
                }
                else
                {
                    cell.height.constant = 230
                }
                 cell.vedioPLAY.isHidden = true
            }
            
            if obj_ModelFeed?.data?.feedList[indexPath.row].ownLikes == true {
                cell.likeBtn.setImage(UIImage(named: "heart"), for: .normal)
            }else {
                cell.likeBtn.setImage(UIImage(named: "unlikeHeartIcon"), for: .normal)
            }
            ///for like count
            if let likeCount = obj_ModelFeed?.data?.feedList[indexPath.row].likes {
                likeCount == 0 ? (cell.likeBtn.setNormalTitle(normalTitle: "Like")) : (cell.likeBtn.setNormalTitle(normalTitle: "\(likeCount)" + " " +  "Like"))
                
            }else {
                cell.likeBtn.setNormalTitle(normalTitle: "Like")
            }
            
            ///for comment count
            if let commentCount = obj_ModelFeed?.data?.feedList[indexPath.row].comments {
                commentCount == 0 ? (cell.commetnBtn.setNormalTitle(normalTitle: "Comments")) : (cell.commetnBtn.setNormalTitle(normalTitle: "\(commentCount)" + " " +  "Comments"))
                
            }else {
                cell.commetnBtn.setNormalTitle(normalTitle: "Comments")
            }
            
            cell.commentsAction = {
                print("path ::::::::::::::::::::: \(indexPath.row)")
                let vc = CommentsViewController.instantiate(fromAppStoryboard: .Main)
                
                
                if let postId = self.obj_ModelFeed?.data?.feedList[indexPath.row]._id {
                    vc.postId = postId
                }
                if let grpId =  self.obj_ModelFeed?.data?.feedList[indexPath.row].groupId {
                    vc.group_id = grpId
                }
                
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            cell.likeBtnAciton =  { status in
                
                print("like ::::::::::::::::::::: \(indexPath.row)")
                let grpid =  self.obj_ModelFeed?.data?.feedList[indexPath.row].groupId ?? ""
                if let postId = self.obj_ModelFeed?.data?.feedList[indexPath.row]._id {
                    self.likePost(islike: status, postId: postId, section: indexPath, groupid: grpid)
                }
            }
            
             cell.share =
               {
                var grpId = String()
                var  postID = String()
                   
                if let xCondition = self.obj_ModelFeed?.data?.feedList[indexPath.row].groupId
                {
                 grpId = xCondition
                }
                if let postId = self.obj_ModelFeed?.data?.feedList[indexPath.row]._id {
                  postID = postId
                   }
                let urlStrig  = "https://byteedgetest2.page.link/dynamic_link?groupId=\(grpId)&feedId=\(postID)"
                guard let link = URL(string: urlStrig) else { return }
                let dynamicLinksDomainURIPrefix = "https://byteedgetest2.page.link"
                print(link)
                let linkBuilder = DynamicLinkComponents(link: link, domainURIPrefix: dynamicLinksDomainURIPrefix)
                linkBuilder?.iOSParameters = DynamicLinkIOSParameters(bundleID:"com.bytedge1")
                linkBuilder?.iOSParameters?.appStoreID = "1631500899"
                linkBuilder?.iOSParameters?.minimumAppVersion = "1.3"
                linkBuilder?.androidParameters = DynamicLinkAndroidParameters(packageName: "com.app.byteedge")
                linkBuilder?.androidParameters?.minimumVersion = 1
                linkBuilder?.androidParameters?.fallbackURL = URL.init(string: "https://play.google.com/store/apps/details?id=com.app.byteedge")
                linkBuilder?.iOSParameters?.fallbackURL = URL.init(string: "https://apps.apple.com/us/app/byteedge/id1631500899")
                linkBuilder?.iOSParameters?.customScheme = "com.bytedge1"
                guard let longDynamicLink = linkBuilder?.url else { return }
                print("The long URL is: \(longDynamicLink)")
               
                   
                let textToShare = [ longDynamicLink ] as [Any]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                // so that iPads won't crash
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                self.present(activityViewController, animated: true, completion: nil)

            }
           
            
            return cell
        }
        else
        {
            print("Nothing")
        }
        
        return UITableViewCell()
    }
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        if let vurl = url as? String{
            if vurl != ""{
                let asset = AVAsset(url: URL(string: vurl)!)
                let assetImgGenerate = AVAssetImageGenerator(asset: asset)
                assetImgGenerate.appliesPreferredTrackTransform = true
                //Can set this to improve performance if target size is known before hand
                //assetImgGenerate.maximumSize = CGSize(width,height)
                let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
                do {
                    let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                    let thumbnail = UIImage(cgImage: img)
                    return thumbnail
                } catch {
                    print(error.localizedDescription)
                    return nil
                }
                
            }
            else{
                return nil
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if callBAckType == .firstCallBack {
            let vc = AppStoryboard.Account.instance.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            if let postId = self.obj_ModelFeed?.data?.feedList[indexPath.row]._id {
                vc.postId = postId
            }else {
             print("post Id not found")
            }
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            print(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if callBAckType == .secondCallBack
        {
            if indexPath.section == 0{
                if objgrouplist?.data?.myGroup?.count == 0{
                    return UITableView.automaticDimension
                }
                else{
                    return 70
                }
            }
            if indexPath.section == 1 {
                if  objgrouplist?.data?.myGroup?.count == 0 {
                    return 0.1
                }
                else
                {
                    return  UITableView.automaticDimension
                }
            }
            
            else if indexPath.section == 2
            {
                if  objgrouplist?.data?.mayLikeGroup?.count == 0 {
                    return 0.1
                }
                else
                {
                    return  UITableView.automaticDimension
                }
            }
            
            else if indexPath.section == 3
            {
                if  objgrouplist?.data?.popularGroup?.count == 0 {
                    return 0.1
                }
                else
                {
                    return  UITableView.automaticDimension
                }
                
            }
            else
            {
                return UITableView.automaticDimension
            }
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if callBAckType == .secondCallBack
        {
            let view = HeadingTitleview.fromNib()
            view.backgroundColor = .clear
            view.callbackofViewAll = { ( ) -> Void in
                let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "ViewAllVc") as! ViewAllVc
                switch section {
                case 1:
                    vc.typeofViewAll = 1
                case 2:
                    vc.typeofViewAll = 2
                default:
                    vc.typeofViewAll = 3
                }
                self.navigationController?.pushViewController(vc, animated: false)
                self.tblCommunity.reloadData()
                
            }
            if section == 0
            {
                //MARK: - Change BY udit
                if objgrouplist?.data?.myGroup?.count != 0
                {
                    view.isHidden = true
                    view.frame = CGRect(x: 0.1, y: 0.1 ,width: 0.1, height: 0)
                    view.viewHeight.constant = 0.1
                    view.viewAllBtn.isHidden = true
                }
                else{
                    view.titleLbl.text = "My Groups"
                    self.titleheader.isHidden = true
                    view.viewAllBtn.isUserInteractionEnabled = false
                    view.viewAllBtn.isHidden = true
                }
            }
            
            else if section == 1
            {
                view.titleLbl.text = "My Groups"
                
                if objgrouplist?.data?.myGroup?.count == 0
                {
                    view.isHidden = true
                    view.frame = CGRect(x: 0.1, y: 0.1 ,width: 0.1, height: 0)
                    view.viewHeight.constant = 0.1
                    view.viewAllBtn.isHidden = true
                }
                else if objgrouplist?.data?.myGroup!.count ?? 0 < 8
                {
                    view.viewAllBtn.isHidden = true
                }
                
            }
            
            
            
            
            else if section == 2
            {
                //            view.titleLbl.text = "Groups you may like"
                view.titleLbl.text = "Groups you may like"
                if objgrouplist?.data?.mayLikeGroup?.count == 0
                {
                    view.isHidden = true
                    view.frame = CGRect(x: 0.1, y: 0.1 ,width: 0.1, height: 0)
                    view.viewHeight.constant = 0.1
                    view.viewAllBtn.isHidden = true
                }
                else if objgrouplist?.data?.mayLikeGroup!.count ?? 0 < 3
                {
                    view.viewAllBtn.isHidden = true
                }
                
            }
            
            
            else
            {
                view.titleLbl.text = "Most Popular"
                if objgrouplist?.data?.popularGroup?.count == 0
                {
                    view.isHidden = true
                    view.frame = CGRect(x: 0.1, y: 0.1 ,width: 0.1, height: 0)
                    view.viewHeight.constant = 0.1
                    view.viewAllBtn.isHidden = true
                }
                else if objgrouplist?.data?.popularGroup!.count ?? 0 < 3
                {
                    view.viewAllBtn.isHidden = true
                }
            }
            
            return view
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if callBAckType == .secondCallBack{
            if section == 1 && objgrouplist?.data?.myGroup?.count == 0 {
                return 0.0
            }
            
            else if section == 2 && objgrouplist?.data?.mayLikeGroup?.count == 0
            {
                return 0.1
            }
            else if section == 3 && objgrouplist?.data?.popularGroup?.count == 0
            {
                return 0.1
            }
            else
            {
                return 40
            }
            
        }
        else
        {
            return 0.1
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "CreatePostVc") as! CreatePostVc
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

extension UIView {
    class func fromNib(named: String? = nil) -> Self {
        let name = named ?? "\(Self.self)"
        guard
            let nib = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        else { fatalError("missing expected nib named: \(name)") }
        guard
            /// we're using `first` here because compact map chokes compiler on
            /// optimized release, so you can't use two views in one nib if you wanted to
            /// and are now looking at this
            let view = nib.first as? Self
        else { fatalError("view of type \(Self.self) not found in \(nib)") }
        return view
    }
}

extension CommunityVc
{
    func feedlistApi()
    {
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.feedsAll.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: FeedDataModel.self)
        
        { (status, json, model, error) in
            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                self.obj_ModelFeed?.data?.feedList.removeAll()
                self.obj_ModelFeed = model
                print(model)
                //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                //                    self.tblCommunity.reloadData()
                //                }
                
                DispatchQueue.main.async {
                    self.tblCommunity.reloadData()
                }
            }
            else
            {
                //                self.createAlert(title: AppName, message:model?.message ?? "")
            }
        }
    }
}



// MARK: GroupList Api
extension CommunityVc
{
    func grouplistApi()
    {
        //        KRProgressHUD.show()
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.CommumnityGroupList.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: nil,
                                        decodable: GroupList.self)
        { (status, json, model, error) in
            //            KRProgressHUD.dismiss()
            if model?.status == 200
            {
                self.objgrouplist?.data?.myGroup?.removeAll()
                self.objgrouplist?.data?.mayLikeGroup?.removeAll()
                self.objgrouplist?.data?.popularGroup?.removeAll()
                self.objgrouplist = model
                print(model)
                DispatchQueue.main.async {
                    self.tblCommunity.reloadData()
                }
            }
            else
            {
                
            }
        }
    }
}

//MARK:  join Api
extension CommunityVc
{
    func joinApi(type:String)
    {
        var param: [String: Any] = [:]
        param["groupId"] = self.maintype
        KRProgressHUD.show()
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.joinGroup.url,
                                        headers: CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: JoinMember.self,
                                        encoding: JSONEncoding.default)
        { (status, json, model, error) in
            print(model)
            KRProgressHUD.dismiss()
            let message = model?.message
            let status = model?.status
            if status == 200
            {
                self.createAlertCallback(title: AppName, message:message ?? "" , cancelTitle: "Cancel") { (value)in
                    if value == true
                    {
                        self.grouplistApi()
                        self.tblCommunity.reloadData()
                    }
                    else
                    {
                        print("Nothing")
                    }
                }
                
                DispatchQueue.main.async {
                    self.tblCommunity.reloadData()
                }
            }
            else
            {
                self.createAlertCallback(title: AppName, message:message ?? "" , cancelTitle: "Cancel") { (value) in
                    if value == true
                    {
                        self.grouplistApi()
                        self.tblCommunity.reloadData()
                    }
                    else
                    {
                        print("Nothing")
                    }
                }
            }
        }
    }
}

extension CommunityVc
{
    func likePost(islike: Bool, postId : String,section : IndexPath,groupid:String) {
        var param: [String: Any] = [:]
        param["postId"] = postId
        param["role"] = "postlike"
        param["groupId"] = groupid
        ServiceRequest.instance.likeCommentReply(param) { (isSucess, response, error) in
            guard response?.status == 200 else {
                print(response?.status as Any)
                self.createAlert(title: kAppName, message: response?.message ?? "Something Went Wrong")
                return
            }
            self.obj_ModelFeed?.data?.feedList[section.row].ownLikes = islike
            guard let count =  self.obj_ModelFeed?.data?.feedList[section.row].likes else {
                return
            }
            
            
            if islike == true {
                self.obj_ModelFeed?.data?.feedList[section.row].likes = count+1
                
                
            }else {
                if  count == 0 {
                    self.obj_ModelFeed?.data?.feedList[section.row].likes = count
                }
                else {
                    self.obj_ModelFeed?.data?.feedList[section.row].likes = count-1
                }
                
                
            }
            
            DispatchQueue.main.async {
                self.tblCommunity.reloadData()
            }
            UIApplication.topViewController()?.view.isUserInteractionEnabled = true
        }
    }
}






















