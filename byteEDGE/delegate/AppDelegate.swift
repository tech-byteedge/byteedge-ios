
import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import FirebaseDynamicLinks
import JWPlayerKit
import Alamofire
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth

@main
class AppDelegate:  UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate,MessagingDelegate {
    var firebase_token = ""
    var objAddGraph:GraphTimeModel?
    var orientationLock = UIInterfaceOrientationMask.portrait
    var myOrientation: UIInterfaceOrientationMask = .portrait
    
    var window: UIWindow?
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Thread.sleep(forTimeInterval: 0.2)
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 8
        FirebaseApp.configure()
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "com.bytedge1"
//        FirebaseApp.dynamicLink.
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        UNUserNotificationCenter.current().delegate = self
        registerForPushNotifications()
        JWPlayerKitLicense.setLicenseKey("ERVhlZzQoBIk+t4VzDH4pHqOeUQ7RVorp0wxY1vbeTpTyR38")
        
        return true
    }
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
            print("Handeling dynamic link")
            if let incomingURL = userActivity.webpageURL {
                print("Incoming URL is \(incomingURL)")
                let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                    guard error == nil else {
                        print("Found an error! \(error!.localizedDescription)")
                        return
                    }
                    if let dynamicLink = dynamicLink {
                       // self.handleIncomingDynamicLink(dynamicLink)
                    }
                }
                if linkHandled {
                    return true
                } else {
                    // Maybe do other things with our incoming url
                    return false
                }
            }
            return false
        }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
       // guard let url = self.launchURL else { return }
        //self.launchURL = nil
        print("heyt")
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {     // wait to init. notifs.
//            DynamicLinks.dynamicLinks().handleUniversalLink(url) { (dynamiclink, error) in
//                if let dynamiclink = dynamiclink {
//                   // self.handleIncomingDynamicLink(dynamiclink)
//                }
//            }
//        })
    }
    // MARK: UISceneSession Lifecycle
     func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
//     func application(_ app: UIApplication, open url: URL,
//                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        if let scheme = url.scheme,
//            scheme.localizedCaseInsensitiveCompare("com.bytedge1") == .orderedSame,
//            let view = url.host {
//            var parameters: [String: String] = [:]
//            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
//                parameters[$0.name] = $0.value
//            }
//
////            redirect(to: view, with: parameters)
//        }
//        return true
//    }
    
   
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?,
//                     annotation: Any) -> Bool {
//      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
//        // Handle the deep link. For example, show the deep-linked content or
//        // apply a promotional offer to the user's account.
//        // ...
//        return true
//      }
//      return false
//    }
    
    
    
//    public func application(_ application: UIApplication,
//                            continue userActivity: NSUserActivity,
//                            restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
//          if let url = userActivity.webpageURL {
//            var view = url.lastPathComponent
//            var parameters: [String: String] = [:]
//            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
//                parameters[$0.name] = $0.value
//            }
//
////            redirect(to: view, with: parameters)
//        }
//        return true
//    }

    
    
    
    
//    @available(iOS 9.0, *)
//    func application(_ app: UIApplication, open url: URL,options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
//      return application(app, open: url,
//                         sourceApplication: options[UIApplication.OpenURLOptionsKey
//                           .sourceApplication] as? String,
//                         annotation: "")
//    }
//
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?,
//                     annotation: Any) -> Bool {
//      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
//        return true
//      }
//      return false
//    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return orientationLock
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
        self?.endBackgroundTask()
        }
        if  kUserDefaults.bool(forKey: "isLoggedIn") == true
        {
        let EndTime = CommonUtility.instance.getCurrentDatedate()

        if (EndTime != nil)
            {
            addgraph1(endTime: EndTime, devicetype: "ios")
            }
            
        }
    }

    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    var  appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        print("AppVersion: \(appVersion) ")
        self.apiVersion(ver:appVersion ?? "")
     }
    
    func apiVersion(ver:String)
         {
           ServiceManager.instance.request(method: .get,
           apiURL: Api.ApiVersion.url,
           headers: nil,
           parameters: nil,
           decodable: VersioUpdate.self,
           encoding: URLEncoding.default)
               { (status, json, model, error) in
                   print(model)
                   if model?.status == 200
                   {
                   print(model)
                   if let version = model?.data?.ios?.version
                       {
                           if ver != version
                             {
                               if let force_update =  model?.data?.ios?.forceUpdate{
                                 if force_update{
                                     let alert = UIAlertController(title: AppName, message:"Update Available", preferredStyle: UIAlertController.Style.alert)
                               
                                 let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                   let myUrl = "https://apps.apple.com/us/app/byteedge/id1631500899"
                                   if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty {
                                   UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                   }
                               }
                               alert.addAction(okAction)
                               // show the alert
                               self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                               }
                                 else{
                                     let alert = UIAlertController(title: AppName, message:"Update Available" , preferredStyle: UIAlertController.Style.alert)
                                     let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       let myUrl = "https://apps.apple.com/us/app/byteedge/id1631500899"
                                       if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty {
                                           UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                       }
                                   }
                                   let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                                       UIAlertAction in
                                    Thread.sleep(forTimeInterval: 0.2)
                                   }
                                   
                                   alert.addAction(okAction)
                                   alert.addAction(cancel)
                                   // show the alert
                                   self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                   }
                           
                           }
                           }
                           else{
                               return
                           }
                           
                       }
                   }
                   else
                   {
//                    self.UIApplication.topViewController()?.createAlert(title: AppName, message: model?.message ?? "")
                   }

               }
           }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        let startTime =   CommonUtility.instance.getCurrentDatedate()
        UserDefaults.standard.set(startTime, forKey: "STARTTIME")
        let sessioID = UserDefaults.standard.object(forKey: "SesionID")
//        CommonUtility.instance.addgraph(startTime: startTime, endTime: startTime, type: "ACTIVE", devicetype: "ios", sessionId: sessioID
//        )
    }
    
//    func applicationDidBecomeActive(_ application: UIApplication) {
//        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//         let isloggedIN = kUserDefaults.bool(forKey: "isLoggedIn")
//          if  isloggedIN  ==  true{
//            let startTime =  CommonUtility.instance.getCurrentDatedate()
//            let sessioID = UserDefaults.standard.object(forKey: "SesionID")
//            UserDefaults.standard.set(startTime, forKey: "STARTTIME")
////              CommonUtility.instance.addgraph(startTime: startTime, endTime: startTime, type: "ACTIVE", devicetype: "ios", sessionId:sessioID as! Int)
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Push"), object: nil)
//        }
//    }
    

 
    func applicationWillTerminate(_ application: UIApplication) {
        if  kUserDefaults.bool(forKey: "isLoggedIn") == true
        {
        let EndTime = CommonUtility.instance.getCurrentDatedate()
        let StartTime = UserDefaults.standard.object(forKey: "STARTTIME")
        let sessioID = UserDefaults.standard.object(forKey: "SesionID")
        if (StartTime != nil){
        addgraph1(endTime:EndTime, devicetype: "ios")
            }
        }
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "byteEDGE")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

//MARK: - TO GET TOP VIEW CONTROLLER-
extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController
        {
        return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController
        {
            if let selected = tab.selectedViewController
            {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController
        {
            return topViewController(base: presented)
        }
        return base
    }
}



extension AppDelegate
{
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.badge, .alert , .sound]) { (granted, error) in
                if granted
                {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
            center.delegate = self
        }
        else
        {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        NSLog("device token%@", token)
        print("This is device Token \(token)")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?)
    {
        print("Firebase registration token: \(fcmToken)")
        let defaults = UserDefaults.standard
        defaults.set(fcmToken, forKey: "device_token")
        defaults.synchronize()
    }

    //when app is in foregorund
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("working")
        //Push Notification


        let info = notification.request.content.userInfo
        // get Action key by hashable value
        print(info[AnyHashable("action")] as Any)
       // let action = info[AnyHashable("action")] as? String
//        if action == "2"{
////            Refresh Home Order
//                            NotificationCenter.default.post(name: Notification.Name("PushNotificationFired_chat"), object: nil, userInfo: ["One":"Two"])
//        }
//        else{
//         NotificationCenter.default.post(name: Notification.Name("PushNotificationFired_AnyNotification"), object: nil,
//                                         userInfo: ["One":"Two"])
//        }
        // foreground notification
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }

    //when tapped in notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification)
        let info = response.notification.request.content.userInfo
        // get Action key by hashable value
        print(info[AnyHashable("action")] as Any)
       // let action = info[AnyHashable("action")] as? String
//        if action == "2"{
//            CUserDefaults.set(true, forKey: "isViaNotification")
//            CommonUtilities.shared.setLoginScreen()
//            UIApplication.shared.applicationIconBadgeNumber = 0
//        }
//        else{
//            CUserDefaults.set(false, forKey: "isViaNotification")
//            CommonUtilities.shared.setLoginScreen()
//            UIApplication.shared.applicationIconBadgeNumber = 0
//        }

    }
}

//Dynamic Link
extension AppDelegate{
//    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
//                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//      let handled = DynamicLinks.dynamicLinks()
//        .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
//
//          // ...
//        }
//
//      return handled
//    }
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
      return application(app, open: url,
                         sourceApplication: options[UIApplication.OpenURLOptionsKey
                           .sourceApplication] as? String,
                         annotation: "")
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?,
                     annotation: Any) -> Bool {
      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        return true
      }
      return false
    }
}

extension AppDelegate
{
func addgraph1(endTime: String,devicetype: String){
    let sessionID = kUserDefaults.object(forKey:"SESSIONID")
    var param: [String: Any] = [:]
    param ["endTime"] = endTime
    param["deviceType"] = devicetype
    param["sessionId"] = sessionID
    
    ServiceManager.instance.request(method: .post,
                                    apiURL: Api.graphAdd.url,
                                    headers:CommonUtility.instance.headers,
                                    parameters: param,
                                    decodable: GraphTimeModel.self
    ) { (status, json, model, error) in
        if model?.status == 200
        {
        self.objAddGraph = model
        }
        else {
        print(model?.message ?? "")
        }
    }
}
}

struct AppUtility {
    
    static let shared = AppUtility()
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        DispatchQueue.main.async {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
       
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        self.lockOrientation(orientation)
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
}







struct VersioUpdate:Codable {
 var Code: Int?
  var status: Int?
  var message: String?
  var data: DataVersion?
}
struct DataVersion:Codable {
    var ios:IOS?
   var _id: String?
    var createdAt: String?
    var __v:Int?
}

struct IOS:Codable {
    var version: String?
    var forceUpdate:Bool?
}
