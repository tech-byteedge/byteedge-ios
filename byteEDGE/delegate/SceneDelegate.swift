
import UIKit
import  IQKeyboardManagerSwift
import JWPlayerKit
import Alamofire
import Firebase
import FirebaseDynamicLinks

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var objAddGraph: GraphTimeModel?
    
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    private(set) static var shared: SceneDelegate?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        Thread.sleep(forTimeInterval: 0.001)
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 8
        JWPlayerKitLicense.setLicenseKey("ERVhlZzQoBIk+t4VzDH4pHqOeUQ7RVorp0wxY1vbeTpTyR38")
        guard let scene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: scene)
        self.window = window
        Self.shared = self
        
        guard let _ = (scene as? UIWindowScene) else { return }
        let value  = kUserDefaults.object(forKey: "signFlow") as? String
        if value == "2" {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            let navController = UINavigationController(rootViewController: vc)
            navController.navigationBar.isHidden = true
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
            self.window?.makeKeyAndVisible()
        }
        
          else if value == "3" || value == "1" {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "Setup_profile") as! Setup_profile
            let navController = UINavigationController(rootViewController: vc)
            navController.navigationBar.isHidden = true
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
            self.window?.makeKeyAndVisible()
        }
        
        else {
            let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "SplashScreen") as! SplashScreen
            let navController = UINavigationController(rootViewController: vc)
            navController.navigationBar.isHidden = true
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
            self.window?.makeKeyAndVisible()
        }
        
        guard let userActivity = connectionOptions.userActivities.first(where: { $0.webpageURL != nil }) else { return }
        print("url: \(userActivity.webpageURL!)")
    
        let handled = DynamicLinks.dynamicLinks()
        
            .handleUniversalLink(userActivity.webpageURL!)  { dynamiclink, error in
               
                if  dynamiclink?.url == nil
                {
                print("::")
                }
                  else
                  {
                    let value  = kUserDefaults.object(forKey: "signFlow") as? String
                      if value == "2"
                       {
                          var mediaID = String()
                          var grpID = String()
                          var postID = ""
                          var courseId = ""
                          var videoId = ""
                          print("dynamiclink?.url",dynamiclink?.url)
                          let url = String(describing: dynamiclink?.url)
                          
                          videoId = self.getQueryStringParameter(url: url, param: "videoId") ?? "" //this is media id for reel play used same as android
                          videoId = videoId.replacingOccurrences(of: ")", with: "")
                          
                          mediaID = self.getQueryStringParameter(url: url, param: "mediaId") ?? ""//this is playlist id changed due to android
                          mediaID = mediaID.replacingOccurrences(of: ")", with: "")
                          
                          
                          postID = self.getQueryStringParameter(url: url, param: "feedId") ?? "" // this is post id
                          postID = postID.replacingOccurrences(of: ")", with: "")
                          
                          grpID = self.getQueryStringParameter(url: url, param: "groupId") ?? "" //group id
                          grpID = grpID.replacingOccurrences(of: ")", with: "")
                          print("mediaID\(mediaID) grpID\(grpID) postID\(postID)")
                         
                        
                          courseId = self.getQueryStringParameter(url: url, param: "courseId") ?? ""
                          courseId = courseId.replacingOccurrences(of: ")", with: "")
                          if courseId != ""{
                              let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                              vc.viaCourse = true
                              vc.courseId = courseId
                              vc.playlistId = mediaID
                              let navController = UINavigationController(rootViewController: vc)
                              navController.navigationBar.isHidden = true
                              self.window?.rootViewController = navController
                              self.window?.makeKeyAndVisible()
                              self.window?.makeKeyAndVisible()
                              
                          }
                          else{
                          let vc = BeforeSplash.instantiate(fromAppStoryboard: .Main)
                          vc.id = videoId
                          vc.groupid = grpID
                          vc.postID = postID
                          let navController = UINavigationController(rootViewController: vc)
                          self.window?.rootViewController = navController
                          navController.navigationBar.isHidden = true
                          self.window?.makeKeyAndVisible()
                          self.window?.makeKeyAndVisible()
                          }
                         
      //                  UIApplication.topViewController()?.present(vc, animated: false)
                      }


                    
                }
            }
           }
    
        func scene(_ scene: UIScene, continue userActivity: NSUserActivity) -> Bool {
        if let url = userActivity.webpageURL {
            var view = url.lastPathComponent
            var parameters: [String: String] = [:]
            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                parameters[$0.name] = $0.value
            }
        }
        return true
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        if let url = userActivity?.webpageURL {
            var view = url.lastPathComponent
            var parameters: [String: String] = [:]
            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
                parameters[$0.name] = $0.value
            }
        }
        
    }
    
    //    continue userActivity
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        kUserDefaults.set(token, forKey: "DeviceToken")
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator :( \(error)")
    }
    
    
    //
    
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        let isloggedIN = kUserDefaults.bool(forKey: "isLoggedIn")
        if  isloggedIN  ==  true{
            let startTime =  CommonUtility.instance.getCurrentDatedate()
            addgraph(startTime: startTime, devicetype: "ios")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Push"), object: nil)
        }
    }
    
    
    func sceneWillResignActive(_ scene: UIScene) {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            self?.endBackgroundTask()
        }
        if  kUserDefaults.bool(forKey: "isLoggedIn") == true
        {
            let endtime =  CommonUtility.instance.getCurrentDatedate()
            if (endtime != nil){
                addgraph1(endTime: endtime, devicetype: "ios")
            }
        }
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool{
        print("asdasd")
        return true
    }
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        let handled = DynamicLinks.dynamicLinks()
            .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
                let value  = kUserDefaults.object(forKey: "signFlow") as? String
                if value == "2" {
                    var mediaID = String()
                    var grpID = String()
                    var postID = ""
                    var courseId = ""
                    var videoId = ""
                    print("dynamiclink?.url",dynamiclink?.url)
                    let url = String(describing: dynamiclink?.url)
                    
                    videoId = self.getQueryStringParameter(url: url, param: "videoId") ?? "" //this is media id for reel play used same as android
                    videoId = videoId.replacingOccurrences(of: ")", with: "")
                    
                    mediaID = self.getQueryStringParameter(url: url, param: "mediaId") ?? ""//this is playlist id changed due to android
                    mediaID = mediaID.replacingOccurrences(of: ")", with: "")
                    
                    
                    postID = self.getQueryStringParameter(url: url, param: "feedId") ?? "" // this is post id
                    postID = postID.replacingOccurrences(of: ")", with: "")
                    
                    grpID = self.getQueryStringParameter(url: url, param: "groupId") ?? "" //group id
                    grpID = grpID.replacingOccurrences(of: ")", with: "")
                    print("mediaID\(mediaID) grpID\(grpID) postID\(postID)")
                   
                  
                    courseId = self.getQueryStringParameter(url: url, param: "courseId") ?? ""
                    courseId = courseId.replacingOccurrences(of: ")", with: "")
                    if courseId != ""{
                        
                        let vc = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                        
                        vc.viaCourse = true
                        vc.courseId = courseId
                        vc.playlistId = mediaID
                        let navController = UINavigationController(rootViewController: vc)
                        navController.navigationBar.isHidden = true
                        self.window?.rootViewController = navController
                        self.window?.makeKeyAndVisible()
                        self.window?.makeKeyAndVisible()
                        
                    }
                    else{
                    let vc = BeforeSplash.instantiate(fromAppStoryboard: .Main)
                    vc.id = videoId
                    vc.groupid = grpID
                    vc.postID = postID
                    let navController = UINavigationController(rootViewController: vc)
                    self.window?.rootViewController = navController
                    navController.navigationBar.isHidden = true
                    self.window?.makeKeyAndVisible()
                    self.window?.makeKeyAndVisible()
                    }
                   
//                  UIApplication.topViewController()?.present(vc, animated: false)
                }
            }
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        print("AppVersion: \(appVersion) ")
        self.apiVersion(ver: appVersion ?? "")
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        let startTime =   CommonUtility.instance.getCurrentDatedate()
        //addgraph(startTime: startTime, devicetype: "ios")
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }
    
}

extension SceneDelegate
{
    func addgraph(startTime:String,devicetype: String){
        var param: [String: Any] = [:]
        param["startTime"] = startTime
        param["deviceType"] = devicetype
        
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.graphAdd.url,
                                        headers:CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: GraphTimeModel.self
        ) { (status, json, model, error) in
            if model?.status == 200
            {
                DispatchQueue.main.async {
                    self.objAddGraph = model
                    let session = self.objAddGraph?.data?.graph.sessionId ?? ""
                    kUserDefaults.set(session, forKey: "SESSIONID")
                }
            }
            else {
                print(model?.message ?? "")
            }
        }
    }
    
    func addgraph1(endTime: String,devicetype: String){
        let sessionID = kUserDefaults.object(forKey:"SESSIONID")
        var param: [String: Any] = [:]
        param ["endTime"] = endTime
        param["deviceType"] = devicetype
        param["sessionId"] = sessionID
        
        ServiceManager.instance.request(method: .post,
                                        apiURL: Api.graphAdd.url,
                                        headers:CommonUtility.instance.headers,
                                        parameters: param,
                                        decodable: GraphTimeModel.self
        ) { (status, json, model, error) in
            if model?.status == 200
            {
                DispatchQueue.main.async {
                    self.objAddGraph = model
                }
                
            }
            else {
                print(model?.message ?? "")
            }
        }
    }
    
    func apiVersion(ver:String){
        ServiceManager.instance.request(method: .get,
                                        apiURL: Api.ApiVersion.url,
                                        headers: nil,
                                        parameters: nil,
                                        decodable: VersioUpdate.self,
                                        encoding: URLEncoding.default)
        { (status, json, model, error) in
            print(model)
            if model?.status == 200
            {
                print(model,"model")
                if let version = model?.data?.ios?.version
                {
                    if ver != version
                    {
                        if let force_update =  model?.data?.ios?.forceUpdate{
                            if force_update{
                                let alert = UIAlertController(title: AppName, message:"Update Available", preferredStyle: UIAlertController.Style.alert)
                                
                                let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    let myUrl = "https://apps.apple.com/us/app/byteedge/id1631500899"
                                    if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty {
                                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                    }
                                }
                                alert.addAction(okAction)
                                // show the alert
                                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                            else{
                                let alert = UIAlertController(title: AppName, message:"Update Available" , preferredStyle: UIAlertController.Style.alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    let myUrl = "https://apps.apple.com/us/app/byteedge/id1631500899"
                                    if let url = URL(string: "\(myUrl)"), !url.absoluteString.isEmpty {
                                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                    }
                                }
                                let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                                    UIAlertAction in
                                    Thread.sleep(forTimeInterval: 0.2)
                                }
                                
                                alert.addAction(okAction)
                                alert.addAction(cancel)
                                // show the alert
                                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                    }
                    else{
                        return
                    }
                    
                }
            }
            else
            {
                //                    self.UIApplication.topViewController()?.createAlert(title: AppName, message: model?.message ?? "")
            }
            
        }
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    
    
}



